package cc.shacocloud.mirage.core;

import cc.shacocloud.mirage.utils.FutureUtils;
import io.vertx.core.Future;
import lombok.extern.slf4j.Slf4j;
import org.jetbrains.annotations.NotNull;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

/**
 * @author 思追(shaco)
 */
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class ApplicationEventMulticasterImplTest {
    
    private final ApplicationEventMulticaster applicationEventMulticaster = new ApplicationEventMulticasterImpl();
    
    @BeforeAll
    void init() {
        applicationEventMulticaster.addApplicationListener(new T1ApplicationListener());
        applicationEventMulticaster.addApplicationListener(new T2ApplicationListener());
    }
    
    @Test
    public void publishEvent() {
        FutureUtils.await(applicationEventMulticaster.publishEvent(new T1()));
        FutureUtils.await(applicationEventMulticaster.publishEvent(new T2()));
    }
    
    @Slf4j
    public static class T1ApplicationListener implements ApplicationListener<T1> {
        
        @Override
        public @NotNull Future<Void> onApplicationEvent(@NotNull T1 event) {
            log.info("t1 监听器收到事件");
            return Future.succeededFuture();
        }
    }
    
    @Slf4j
    public static class T2ApplicationListener implements ApplicationListener<T2> {
        
        @Override
        public @NotNull Future<Void> onApplicationEvent(@NotNull T2 event) {
            log.info("t2 监听器收到事件");
            return Future.succeededFuture();
        }
    }
    
    public static class T1 implements ApplicationEvent {
    
    }
    
    public static class T2 implements ApplicationEvent {
    
    }
}