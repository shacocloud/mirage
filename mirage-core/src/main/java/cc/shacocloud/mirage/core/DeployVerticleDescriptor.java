package cc.shacocloud.mirage.core;

import io.vertx.core.DeploymentOptions;
import io.vertx.core.json.JsonObject;
import lombok.Getter;
import lombok.Setter;

/**
 * @author shaco
 */
@Setter
@Getter
public class DeployVerticleDescriptor extends DeploymentOptions {
    
    /**
     * 部署站点的对象名称
     */
    private String verticleName;
    
    public DeployVerticleDescriptor() {
    }
    
    public DeployVerticleDescriptor(DeploymentOptions other) {
        super(other);
    }
    
    public DeployVerticleDescriptor(JsonObject json) {
        super(json);
    }
    
}
