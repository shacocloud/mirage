package cc.shacocloud.mirage.core.event;

import cc.shacocloud.mirage.core.ApplicationContext;

/**
 * 上下文完成启动事件
 *
 * @author 思追(shaco)
 */
public class ContextFinishStartEvent extends ApplicationContextEvent {
    
    public ContextFinishStartEvent(ApplicationContext applicationContext) {
        super(applicationContext);
    }
}
