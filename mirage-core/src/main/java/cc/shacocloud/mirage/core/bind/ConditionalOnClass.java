package cc.shacocloud.mirage.core.bind;

import cc.shacocloud.mirage.core.bind.support.OnClassCondition;

import java.lang.annotation.*;

/**
 * 当存在指定类时加载
 *
 * @author 思追(shaco)
 * @see Conditional
 * @see OnClassCondition
 */
@Target({ElementType.TYPE, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Conditional(OnClassCondition.class)
public @interface ConditionalOnClass {
    
    /**
     * 当存在指定类时加载
     */
    String[] value() default {};
    
    /**
     * 判断模式
     * <p>
     * {@link Mode#ANY} 满足任意一个即为满足
     * {@link Mode#ALL} 满足所有即为满足
     */
    Mode mode() default Mode.ANY;
    
    enum Mode {
        
        /**
         * 满足任意一个
         */
        ANY,
        
        /**
         * 满足所有
         */
        ALL
    }
    
}



