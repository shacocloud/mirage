package cc.shacocloud.mirage.core.event;

import cc.shacocloud.mirage.core.ApplicationContext;
import cc.shacocloud.mirage.core.ApplicationEvent;
import lombok.Getter;

/**
 * 应用上下文事件
 *
 * @author 思追(shaco)
 */
public abstract class ApplicationContextEvent implements ApplicationEvent {
    
    @Getter
    private final ApplicationContext applicationContext;
    
    public ApplicationContextEvent(ApplicationContext applicationContext) {
        this.applicationContext = applicationContext;
    }
}
