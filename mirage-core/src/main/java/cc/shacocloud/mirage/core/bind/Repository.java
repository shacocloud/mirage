package cc.shacocloud.mirage.core.bind;

import cc.shacocloud.mirage.bean.bind.Component;
import cc.shacocloud.mirage.utils.annotation.AliasFor;
import jakarta.inject.Named;

import java.lang.annotation.*;

/**
 * 存储组件注解，通常用来标识在外部存储定义的接口上，是给外部实现提供的一个规范注解
 *
 * @author 思追(shaco)
 */
@Component
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Repository {
    
    /**
     * @see Component#value()
     */
    @AliasFor(annotation = Named.class)
    String value() default "";
    
    /**
     * @see Component#lazy()
     */
    @AliasFor(annotation = Component.class)
    boolean lazy() default true;
    
}
