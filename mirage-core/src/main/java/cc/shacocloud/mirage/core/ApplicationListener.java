package cc.shacocloud.mirage.core;

import io.vertx.core.Future;
import org.jetbrains.annotations.NotNull;

/**
 * 应用监听器
 *
 * @author 思追(shaco)
 */
public interface ApplicationListener<E extends ApplicationEvent> {
    
    /**
     * 应用事件处理
     *
     * @param event 事件对象
     * @return {@link Future}
     */
    @NotNull
    Future<Void> onApplicationEvent(@NotNull E event);
    
}
