package cc.shacocloud.mirage.core;

import cc.shacocloud.mirage.utils.ResolvableType;
import io.vertx.core.CompositeFuture;

import java.util.List;
import java.util.function.Predicate;

/**
 * 应用事件多播器，用于可以管理许多 {@link ApplicationListener} 对象并向其发布事件。
 * <p>
 * 事件对象{@link ApplicationEvent}，判罚是否收到监听规则，使用 {@link ResolvableType#isAssignableFrom(ResolvableType)} 来判断
 *
 * @author 思追(shaco)
 * @see ApplicationListener
 * @see ApplicationEventMulticasterImpl
 */
public interface ApplicationEventMulticaster {
    
    /**
     * 添加侦听器以接收所有事件的通知
     *
     * @see #removeApplicationListener(ApplicationListener)
     * @see #removeApplicationListeners(Predicate)
     */
    void addApplicationListener(ApplicationListener<?> listener);
    
    /**
     * 批量添加侦听器以接收所有事件的通知
     *
     * @see #removeApplicationListener(ApplicationListener)
     * @see #removeApplicationListeners(Predicate)
     */
    void addApplicationListeners(List<ApplicationListener<?>> listeners);
    
    /**
     * 从通知列表中删除侦听器
     *
     * @see #addApplicationListener(ApplicationListener)
     * @see #removeApplicationListeners(Predicate)
     */
    void removeApplicationListener(ApplicationListener<?> listener);
    
    /**
     * 根据 {@link Predicate} 返回的结果删除对应的监听器
     *
     * @see #addApplicationListener(ApplicationListener)
     * @see #removeApplicationListener(ApplicationListener)
     */
    void removeApplicationListeners(Predicate<ApplicationListener<?>> predicate);
    
    /**
     * 删除所以监听器
     *
     * @see #removeApplicationListeners(Predicate)
     */
    void removeAllListeners();
    
    /**
     * 发布多播事件
     */
    <E extends ApplicationEvent> CompositeFuture publishEvent(E event);
    
}
