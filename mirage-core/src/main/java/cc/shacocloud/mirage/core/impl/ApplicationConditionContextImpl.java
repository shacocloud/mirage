package cc.shacocloud.mirage.core.impl;

import cc.shacocloud.mirage.bean.ConfigurableBeanFactory;
import cc.shacocloud.mirage.bean.meta.BeanKey;
import cc.shacocloud.mirage.core.ConditionContext;
import cc.shacocloud.mirage.core.ConfigurableApplicationContext;
import lombok.Getter;
import org.jetbrains.annotations.NotNull;

/**
 * 基于应用的条件上下文
 *
 * @author 思追(shaco)
 */
public class ApplicationConditionContextImpl implements ConditionContext {
    
    private final ConfigurableApplicationContext applicationContext;
    
    @Getter
    private final BeanKey beanKey;
    
    public ApplicationConditionContextImpl(ConfigurableApplicationContext applicationContext,
                                           BeanKey beanKey) {
        this.applicationContext = applicationContext;
        this.beanKey = beanKey;
    }
    
    @Override
    public @NotNull ConfigurableBeanFactory getBeanFactory() {
        return applicationContext.getBeanFactory();
    }
    
    
    @Override
    public @NotNull ClassLoader getClassLoader() {
        return getBeanFactory().getClassLoader();
    }
}
