package cc.shacocloud.mirage.core.event;

import cc.shacocloud.mirage.core.ApplicationEvent;
import cc.shacocloud.mirage.env.Environment;
import lombok.Getter;
import org.jetbrains.annotations.NotNull;

/**
 * 环境配置事件
 *
 * @author 思追(shaco)
 */
@Getter
public class EnvironmentEvent implements ApplicationEvent {
    
    @NotNull
    private final Environment environment;
    
    public EnvironmentEvent(@NotNull Environment environment) {
        this.environment = environment;
    }
}
