package cc.shacocloud.mirage.core;

import cc.shacocloud.mirage.core.bind.Conditional;
import cc.shacocloud.mirage.utils.annotation.AnnotatedElementMetadata;
import org.jetbrains.annotations.NotNull;

/**
 * 条件注解 {@link Conditional} 的判断条件接口
 * <p>
 * 注意：所有该接口的实现类，必须提供无参构造函数
 *
 * @author 思追(shaco)
 * @see Conditional
 */
public interface Condition {
    
    /**
     * 判断是否匹配指定条件
     *
     * @param context                  条件上下文信息
     * @param annotatedElementMetadata 注解元素的元数据对象
     * @return 如果匹配则返回 true，反之为 false
     */
    boolean matches(@NotNull ConditionContext context, @NotNull AnnotatedElementMetadata annotatedElementMetadata);
    
}
