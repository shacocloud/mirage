package cc.shacocloud.mirage.core.bind;

import cc.shacocloud.mirage.bean.bind.Component;
import cc.shacocloud.mirage.core.Condition;

import java.lang.annotation.*;

/**
 * 条件注解，通过 {@link Condition} 判断标注的 bean 是否可以注入到对象工厂中
 *
 * @author 思追(shaco)
 * @see Condition
 */
@Target({ElementType.TYPE, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Component
public @interface Conditional {
    
    /**
     * 指定的条件注解类
     */
    Class<? extends Condition>[] value();
    
}
