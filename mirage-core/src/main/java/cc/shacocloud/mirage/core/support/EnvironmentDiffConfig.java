package cc.shacocloud.mirage.core.support;

import io.vertx.core.json.JsonObject;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author 思追(shaco)
 */
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class EnvironmentDiffConfig {
    
    /**
     * 更新的配置，包含 新增和更新的
     */
    private JsonObject updateConfig;
    
    /**
     * 删除的配置
     */
    private JsonObject deleteConfig;
    
}
