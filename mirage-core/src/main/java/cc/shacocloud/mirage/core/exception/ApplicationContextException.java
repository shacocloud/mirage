package cc.shacocloud.mirage.core.exception;

/**
 * 应用上下文启动异常
 *
 * @author 思追(shaco)
 */
public class ApplicationContextException extends RuntimeException {
    
    public ApplicationContextException() {
    }
    
    public ApplicationContextException(String message) {
        super(message);
    }
    
    public ApplicationContextException(String message, Throwable cause) {
        super(message, cause);
    }
    
    public ApplicationContextException(Throwable cause) {
        super(cause);
    }
}
