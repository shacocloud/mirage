package cc.shacocloud.mirage.core.bind;

import cc.shacocloud.mirage.bean.BeanFactory;
import cc.shacocloud.mirage.core.bind.support.OnMissingBeanCondition;

import java.lang.annotation.*;

/**
 * 如果 {@link BeanFactory} 中不存在指定的对象，那么将当前对象添加到对象工厂中
 * <p>
 * 如果 {@link #type} 和 {@link #name} 都为空，则以被注解的对象类型作为判断条件，
 * 如果 {@link #type} 和 {@link #name} 都不为空，则将其组合作为判断条件，即两个数组合并
 *
 * @author 思追(shaco)
 * @see Conditional
 * @see OnMissingBeanCondition
 */
@Target({ElementType.TYPE, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Conditional(OnMissingBeanCondition.class)
public @interface ConditionalOnMissingBean {
    
    /**
     * 当 {@link BeanFactory} 不存在指定类型的对象时，条件匹配
     */
    Class<?>[] type() default {};
    
    /**
     * 当 {@link BeanFactory} 不存在指定名称的对象时，条件匹配
     */
    String[] name() default {};
    
}



