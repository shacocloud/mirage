package cc.shacocloud.mirage.core;

import cc.shacocloud.mirage.bean.aware.Aware;
import org.jetbrains.annotations.NotNull;

/**
 * 应用上下文感知接口
 *
 * @author 思追(shaco)
 */
public interface ApplicationContextAware extends Aware {
    
    /**
     * 在应用上下文启动成功后，会调用该方法注入 {@link ApplicationContext}
     */
    void setApplicationContext(@NotNull ApplicationContext applicationContext);
    
}
