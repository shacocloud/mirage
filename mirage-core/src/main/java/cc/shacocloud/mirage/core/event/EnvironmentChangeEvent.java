package cc.shacocloud.mirage.core.event;

import cc.shacocloud.mirage.core.support.EnvironmentDiffConfig;
import cc.shacocloud.mirage.env.Environment;
import io.vertx.core.json.JsonObject;
import lombok.Getter;
import org.jetbrains.annotations.NotNull;

/**
 * 环境配置变化事件
 *
 * @author 思追(shaco)
 */
@Getter
public class EnvironmentChangeEvent extends EnvironmentEvent {
    
    /**
     * 旧的配置
     */
    @NotNull
    private final JsonObject oldConfig;
    
    /**
     * 新的配置
     */
    @NotNull
    private final JsonObject newConfig;
    
    /**
     * 变更的配置
     */
    @NotNull
    private final EnvironmentDiffConfig environmentDiffConfig;
    
    
    public EnvironmentChangeEvent(@NotNull Environment environment,
                                  @NotNull JsonObject oldConfig,
                                  @NotNull JsonObject newConfig,
                                  @NotNull EnvironmentDiffConfig environmentDiffConfig) {
        super(environment);
        this.oldConfig = oldConfig;
        this.newConfig = newConfig;
        this.environmentDiffConfig = environmentDiffConfig;
    }
}
