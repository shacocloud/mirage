package cc.shacocloud.mirage.core;


import cc.shacocloud.mirage.bean.ConfigurableBeanFactory;
import cc.shacocloud.mirage.bean.meta.BeanKey;
import org.jetbrains.annotations.NotNull;

/**
 * 供 {@link Condition} 接口使用的上下文对象
 *
 * @author 思追(shaco)
 * @see Condition
 */
public interface ConditionContext {
    
    /**
     * 获取当前的对象工厂
     */
    @NotNull
    ConfigurableBeanFactory getBeanFactory();
    
    /**
     * 获取当前正在被判断的 {@link BeanKey}
     */
    @NotNull
    BeanKey getBeanKey();
    
    /**
     * 返回加载当前应用使用的类加载器
     */
    @NotNull
    ClassLoader getClassLoader();
    
}
