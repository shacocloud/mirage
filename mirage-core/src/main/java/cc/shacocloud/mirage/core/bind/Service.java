package cc.shacocloud.mirage.core.bind;

import cc.shacocloud.mirage.bean.bind.Component;
import cc.shacocloud.mirage.utils.annotation.AliasFor;
import jakarta.inject.Named;

import java.lang.annotation.*;

/**
 * 服务组件注解，通常用来定义应用内部的服务组件，暂时还没有特殊的意义
 *
 * @author 思追(shaco)
 */
@Component
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Service {
    
    /**
     * @see Component#value()
     */
    @AliasFor(annotation = Named.class)
    String value() default "";
    
    /**
     * @see Component#lazy()
     */
    @AliasFor(annotation = Component.class)
    boolean lazy() default true;
    
}
