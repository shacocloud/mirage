package cc.shacocloud.mirage.core;

import io.vertx.core.spi.VerticleFactory;
import org.jetbrains.annotations.NotNull;

/**
 * 一个由 {@link ApplicationContext} 支持的{@link VerticleFactory}。
 *
 * @author shaco
 */
public interface ApplicationContextVerticleFactory extends VerticleFactory, ApplicationContextAware {
    
    /**
     * 生成 vertx 部署站点的全名 (基于 Bean name 的)
     */
    default String verticleName(@NotNull String beanName) {
        return prefix() + ":" + beanName;
    }
    
    /**
     * 生成 vertx 部署站点的全名 (基于 Bean class 的)
     */
    default String verticleName(@NotNull Class<?> beanClass) {
        return prefix() + ":" + beanClass.getName();
    }
    
}
