package cc.shacocloud.mirage.core;

import cc.shacocloud.mirage.bean.bind.Component;
import cc.shacocloud.mirage.bean.bind.Prototype;
import cc.shacocloud.mirage.utils.annotation.AliasFor;
import io.vertx.core.DeploymentOptions;
import io.vertx.core.Verticle;
import io.vertx.core.VertxOptions;

import java.lang.annotation.*;

/**
 * Vertx 组件部署默认为 bean 工厂的组件，非单例
 * <p>
 * 使用 {@link DeployVerticle} 标识的对象必须是 {@link Verticle} 的子类
 *
 * @author shaco
 * @see DeploymentOptions
 */
@Prototype
@Component
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface DeployVerticle {
    
    /**
     * 该值可能表示对逻辑组件名称
     *
     * @see Component
     */
    @AliasFor(annotation = Component.class, value = "value")
    String name() default "";
    
    /**
     * 部署的实例数量
     */
    int instances() default DeploymentOptions.DEFAULT_INSTANCES;
    
    /**
     * 是否开启高可用
     */
    boolean ha() default DeploymentOptions.DEFAULT_HA;
    
    /**
     * 工作线程池名称，如果未定义则使用 {@link io.vertx.core.Vertx} 实例的工作线程池
     *
     * @see DeploymentOptions#setWorkerPoolName(String)
     */
    String workerPoolName() default "";
    
    /**
     * 工作线程池数量
     *
     * @see DeploymentOptions#setWorkerPoolSize(int)
     */
    int workerPoolSize() default VertxOptions.DEFAULT_WORKER_POOL_SIZE;
    
    /**
     * 最大工作线程执行时间，单位 秒
     *
     * @see DeploymentOptions#setMaxWorkerExecuteTime(long)
     */
    long maxWorkerExecuteTime() default 60L;
    
}
