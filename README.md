# mirage

## 介绍

mirage 使 vertx 变更更简单

文档地址：https://mirage.shacocloud.cc/



## 感谢

mirage 在设计时借鉴了很多优秀的框架代码，在这里表示由衷的感谢！

* [hutool](https://hutool.cn/docs/#/)
* [Spring](https://spring.io/)
* [Apache Commons](https://commons.apache.org/)
* [kouinject](https://blurpy.github.io/kouinject-doc/)
