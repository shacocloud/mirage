package cc.shacocloud.mirage.loader.jar;

/**
 * 可用于过滤和选择性重命名 jar 条目的接口
 */
interface JarEntryFilter {
    
    /**
     * 应用 jar 条目筛选器
     *
     * @param name 当前条目名称。如果应用了以前的筛选器，这可能与原始条目名称不同
     * @return 条目的新名称或 {@code null}（如果不应包含该条目）
     */
    AsciiBytes apply(AsciiBytes name);
    
}
