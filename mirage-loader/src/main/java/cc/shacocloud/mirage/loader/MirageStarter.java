package cc.shacocloud.mirage.loader;

/**
 * @author 思追(shaco)
 */
public class MirageStarter {
    
    public static void main(String[] args) throws Exception {
        Launcher launcher = new MirageJarLauncher();
        launcher.run(args);
    }
}
