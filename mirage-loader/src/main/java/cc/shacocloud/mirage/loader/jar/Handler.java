package cc.shacocloud.mirage.loader.jar;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.File;
import java.io.IOException;
import java.lang.ref.SoftReference;
import java.net.*;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Pattern;

/**
 * 用于加载 {@link JarFile} 的 {@link URLStreamHandler} 实现
 *
 * @see JarFile#registerUrlProtocolHandler()
 */
public class Handler extends URLStreamHandler {
    
    // 注意：为了找到作为 URL 协议处理程序，此类必须是公共的，必须命名为 Handler，并且必须在以“.jar”结尾的包中
    
    private static final String JAR_PROTOCOL = "jar:";
    
    private static final String FILE_PROTOCOL = "file:";
    
    private static final String SEPARATOR = "!/";
    
    private static final Pattern SEPARATOR_PATTERN = Pattern.compile(SEPARATOR, Pattern.LITERAL);
    
    private static final String CURRENT_DIR = "/./";
    
    private static final Pattern CURRENT_DIR_PATTERN = Pattern.compile(CURRENT_DIR, Pattern.LITERAL);
    
    private static final String PARENT_DIR = "/../";
    
    private static final String PROTOCOL_HANDLER = "java.protocol.handler.pkgs";
    
    private static final String[] FALLBACK_HANDLERS = {"sun.net.www.protocol.jar.Handler"};
    
    private static URL jarContextUrl;
    
    private static SoftReference<Map<File, JarFile>> rootFileCache;
    
    static {
        rootFileCache = new SoftReference<>(null);
    }
    
    private final JarFile jarFile;
    
    private URLStreamHandler fallbackHandler;
    
    public Handler() {
        this(null);
    }
    
    public Handler(JarFile jarFile) {
        this.jarFile = jarFile;
    }
    
    /**
     * 将给定的 {@link JarFile} 添加到根文件缓存中
     */
    static void addToRootFileCache(File sourceFile, JarFile jarFile) {
        Map<File, JarFile> cache = rootFileCache.get();
        if (cache == null) {
            cache = new ConcurrentHashMap<>();
            rootFileCache = new SoftReference<>(cache);
        }
        cache.put(sourceFile, jarFile);
    }
    
    /**
     * 如果可能，请捕获使用原始 jar 处理程序配置的 URL，以便我们稍后可以将其用作回退上下文。
     * 只有当我们知道我们可以在之后重置处理程序时，我们才能这样做。
     */
    static void captureJarContextUrl() {
        if (canResetCachedUrlHandlers()) {
            String handlers = System.getProperty(PROTOCOL_HANDLER);
            try {
                System.clearProperty(PROTOCOL_HANDLER);
                try {
                    resetCachedUrlHandlers();
                    jarContextUrl = new URL("jar:file:context.jar!/");
                    URLConnection connection = jarContextUrl.openConnection();
                    if (connection instanceof JarURLConnection) {
                        jarContextUrl = null;
                    }
                } catch (Exception ignore) {
                }
            } finally {
                if (handlers == null) {
                    System.clearProperty(PROTOCOL_HANDLER);
                } else {
                    System.setProperty(PROTOCOL_HANDLER, handlers);
                }
            }
            resetCachedUrlHandlers();
        }
    }
    
    private static boolean canResetCachedUrlHandlers() {
        try {
            resetCachedUrlHandlers();
            return true;
        } catch (Error ex) {
            return false;
        }
    }
    
    private static void resetCachedUrlHandlers() {
        URL.setURLStreamHandlerFactory(null);
    }
    
    /**
     * 设置在无法连接 URL 时是否可以引发通用静态异常。此优化在类加载期间使用，以节省创建大量异常
     */
    public static void setUseFastConnectionExceptions(boolean useFastConnectionExceptions) {
        JarURLConnection.setUseFastExceptions(useFastConnectionExceptions);
    }
    
    @Override
    protected URLConnection openConnection(URL url) throws IOException {
        if (this.jarFile != null && isUrlInJarFile(url, this.jarFile)) {
            return JarURLConnection.get(url, this.jarFile);
        }
        try {
            return JarURLConnection.get(url, getRootJarFileFromUrl(url));
        } catch (Exception ex) {
            return openFallbackConnection(url, ex);
        }
    }
    
    private boolean isUrlInJarFile(@NotNull URL url, @NotNull JarFile jarFile) throws MalformedURLException {
        // 首先尝试路径以节省每次构建新的 url 字符串
        return url.getPath().startsWith(jarFile.getUrl().getPath()) && url.toString().startsWith(jarFile.getUrlString());
    }
    
    private URLConnection openFallbackConnection(URL url, Exception reason) throws IOException {
        try {
            URLConnection connection = openFallbackContextConnection(url);
            return (connection != null) ? connection : openFallbackHandlerConnection(url);
        } catch (Exception ex) {
            if (reason instanceof IOException) {
                log(false, "无法打开回退链接处理程序", ex);
                throw (IOException) reason;
            }
            log(true, "无法打开回退链接处理程序", ex);
            if (reason instanceof RuntimeException) {
                throw (RuntimeException) reason;
            }
            throw new IllegalStateException(reason);
        }
    }
    
    /**
     * 尝试使用在将 jar 处理程序替换为我们自己的版本之前捕获的上下文 URL 打开回退连接。
     * 由于此方法不使用反射，因此不会在 Java 13+ 上触发发生了非法反射访问操作警告。
     *
     * @return {@link URLConnection}
     */
    private @Nullable URLConnection openFallbackContextConnection(URL url) {
        try {
            if (jarContextUrl != null) {
                return new URL(jarContextUrl, url.toExternalForm()).openConnection();
            }
        } catch (Exception ignore) {
        }
        return null;
    }
    
    /**
     * 尝试通过使用反射来访问 Java 的默认 jar {@link URLStreamHandler} 来打开回退连接
     *
     * @return {@link URLConnection}
     */
    private URLConnection openFallbackHandlerConnection(@NotNull URL url) throws Exception {
        URLStreamHandler fallbackHandler = getFallbackHandler();
        return new URL(null, url.toExternalForm(), fallbackHandler).openConnection();
    }
    
    private URLStreamHandler getFallbackHandler() {
        if (this.fallbackHandler != null) {
            return this.fallbackHandler;
        }
        for (String handlerClassName : FALLBACK_HANDLERS) {
            try {
                Class<?> handlerClass = Class.forName(handlerClassName);
                this.fallbackHandler = (URLStreamHandler) handlerClass.getDeclaredConstructor().newInstance();
                return this.fallbackHandler;
            } catch (Exception ignore) {
            }
        }
        throw new IllegalStateException("找不到回退处理程序");
    }
    
    private void log(boolean warning, String message, Exception cause) {
        try {
            Level level = warning ? Level.WARNING : Level.FINEST;
            Logger.getLogger(getClass().getName()).log(level, message, cause);
        } catch (Exception ex) {
            if (warning) {
                System.err.println("警告: " + message);
            }
        }
    }
    
    @Override
    protected void parseURL(URL context, @NotNull String spec, int start, int limit) {
        if (spec.regionMatches(true, 0, JAR_PROTOCOL, 0, JAR_PROTOCOL.length())) {
            setFile(context, getFileFromSpec(spec.substring(start, limit)));
        } else {
            setFile(context, getFileFromContext(context, spec.substring(start, limit)));
        }
    }
    
    @Contract("_ -> param1")
    private @NotNull String getFileFromSpec(@NotNull String spec) {
        int separatorIndex = spec.lastIndexOf(SEPARATOR);
        if (separatorIndex == -1) {
            throw new IllegalArgumentException("内容 '" + spec + "' 不包含 " + SEPARATOR);
        }
        try {
            new URL(spec.substring(0, separatorIndex));
            return spec;
        } catch (MalformedURLException ex) {
            throw new IllegalArgumentException("无效的地址：" + spec, ex);
        }
    }
    
    private @NotNull String getFileFromContext(@NotNull URL context, @NotNull String spec) {
        String file = context.getFile();
        if (spec.startsWith("/")) {
            return trimToJarRoot(file) + SEPARATOR + spec.substring(1);
        }
        if (file.endsWith("/")) {
            return file + spec;
        }
        int lastSlashIndex = file.lastIndexOf('/');
        if (lastSlashIndex == -1) {
            throw new IllegalArgumentException("在 URL '" + file + "' 上下文中中找不到 /");
        }
        return file.substring(0, lastSlashIndex + 1) + spec;
    }
    
    private @NotNull String trimToJarRoot(@NotNull String file) {
        int lastSeparatorIndex = file.lastIndexOf(SEPARATOR);
        if (lastSeparatorIndex == -1) {
            throw new IllegalArgumentException("在 URL '" + file + "' 上下文中中找不到 " + SEPARATOR);
        }
        return file.substring(0, lastSeparatorIndex);
    }
    
    private void setFile(URL context, String file) {
        String path = normalize(file);
        String query = null;
        int queryIndex = path.lastIndexOf('?');
        if (queryIndex != -1) {
            query = path.substring(queryIndex + 1);
            path = path.substring(0, queryIndex);
        }
        setURL(context, JAR_PROTOCOL, null, -1, null, null, path, query, context.getRef());
    }
    
    private @NotNull String normalize(@NotNull String file) {
        if (!file.contains(CURRENT_DIR) && !file.contains(PARENT_DIR)) {
            return file;
        }
        int afterLastSeparatorIndex = file.lastIndexOf(SEPARATOR) + SEPARATOR.length();
        String afterSeparator = file.substring(afterLastSeparatorIndex);
        afterSeparator = replaceParentDir(afterSeparator);
        afterSeparator = replaceCurrentDir(afterSeparator);
        return file.substring(0, afterLastSeparatorIndex) + afterSeparator;
    }
    
    private String replaceParentDir(@NotNull String file) {
        int parentDirIndex;
        while ((parentDirIndex = file.indexOf(PARENT_DIR)) >= 0) {
            int precedingSlashIndex = file.lastIndexOf('/', parentDirIndex - 1);
            if (precedingSlashIndex >= 0) {
                file = file.substring(0, precedingSlashIndex) + file.substring(parentDirIndex + 3);
            } else {
                file = file.substring(parentDirIndex + 4);
            }
        }
        return file;
    }
    
    private String replaceCurrentDir(String file) {
        return CURRENT_DIR_PATTERN.matcher(file).replaceAll("/");
    }
    
    @Override
    protected int hashCode(@NotNull URL u) {
        return hashCode(u.getProtocol(), u.getFile());
    }
    
    private int hashCode(String protocol, @NotNull String file) {
        int result = (protocol != null) ? protocol.hashCode() : 0;
        int separatorIndex = file.indexOf(SEPARATOR);
        if (separatorIndex == -1) {
            return result + file.hashCode();
        }
        String source = file.substring(0, separatorIndex);
        String entry = canonicalize(file.substring(separatorIndex + 2));
        try {
            result += new URL(source).hashCode();
        } catch (MalformedURLException ex) {
            result += source.hashCode();
        }
        result += entry.hashCode();
        return result;
    }
    
    @Override
    protected boolean sameFile(@NotNull URL u1, URL u2) {
        if (!u1.getProtocol().equals("jar") || !u2.getProtocol().equals("jar")) {
            return false;
        }
        int separator1 = u1.getFile().indexOf(SEPARATOR);
        int separator2 = u2.getFile().indexOf(SEPARATOR);
        if (separator1 == -1 || separator2 == -1) {
            return super.sameFile(u1, u2);
        }
        String nested1 = u1.getFile().substring(separator1 + SEPARATOR.length());
        String nested2 = u2.getFile().substring(separator2 + SEPARATOR.length());
        if (!nested1.equals(nested2)) {
            String canonical1 = canonicalize(nested1);
            String canonical2 = canonicalize(nested2);
            if (!canonical1.equals(canonical2)) {
                return false;
            }
        }
        String root1 = u1.getFile().substring(0, separator1);
        String root2 = u2.getFile().substring(0, separator2);
        try {
            return super.sameFile(new URL(root1), new URL(root2));
        } catch (MalformedURLException ignore) {
        }
        return super.sameFile(u1, u2);
    }
    
    private String canonicalize(String path) {
        return SEPARATOR_PATTERN.matcher(path).replaceAll("/");
    }
    
    public JarFile getRootJarFileFromUrl(@NotNull URL url) throws IOException {
        String spec = url.getFile();
        int separatorIndex = spec.indexOf(SEPARATOR);
        if (separatorIndex == -1) {
            throw new MalformedURLException("Jar URL 不包含 !/ 分隔符");
        }
        String name = spec.substring(0, separatorIndex);
        return getRootJarFile(name);
    }
    
    private @NotNull JarFile getRootJarFile(String name) throws IOException {
        try {
            if (!name.startsWith(FILE_PROTOCOL)) {
                throw new IllegalStateException("不是文件 URL");
            }
            File file = new File(URI.create(name));
            Map<File, JarFile> cache = rootFileCache.get();
            JarFile result = (cache != null) ? cache.get(file) : null;
            if (result == null) {
                result = new JarFile(file);
                addToRootFileCache(file, result);
            }
            return result;
        } catch (Exception ex) {
            throw new IOException("无法打开 Jar 文件 '" + name + "'", ex);
        }
    }
    
}
