package cc.shacocloud.mirage.loader.jar;

/**
 * 用于处理 ZIP 文件中的字节的实用程序
 */
final class Bytes {
    
    private Bytes() {
    }
    
    static long littleEndianValue(byte[] bytes, int offset, int length) {
        long value = 0;
        for (int i = length - 1; i >= 0; i--) {
            value = ((value << 8) | (bytes[offset + i] & 0xFF));
        }
        return value;
    }
    
}
