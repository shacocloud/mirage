package cc.shacocloud.mirage.loader.jar;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.CodeSigner;
import java.security.cert.Certificate;
import java.util.jar.Attributes;
import java.util.jar.Manifest;

/**
 * 由 {@link JarFile} 返回的 {@link java.util.jar.JarEntry} 的扩展变体
 */
public class JarEntry extends java.util.jar.JarEntry implements FileHeader {
    
    private final int index;
    
    private final AsciiBytes name;
    
    private final AsciiBytes headerName;
    
    private final JarFile jarFile;
    
    private final long localHeaderOffset;
    
    private volatile JarEntryCertification certification;
    
    JarEntry(JarFile jarFile, int index, CentralDirectoryFileHeader header, AsciiBytes nameAlias) {
        super((nameAlias != null) ? nameAlias.toString() : header.getName().toString());
        this.index = index;
        this.name = (nameAlias != null) ? nameAlias : header.getName();
        this.headerName = header.getName();
        this.jarFile = jarFile;
        this.localHeaderOffset = header.getLocalHeaderOffset();
        setCompressedSize(header.getCompressedSize());
        setMethod(header.getMethod());
        setCrc(header.getCrc());
        setComment(header.getComment().toString());
        setSize(header.getSize());
        setTime(header.getTime());
        if (header.hasExtra()) {
            setExtra(header.getExtra());
        }
    }
    
    int getIndex() {
        return this.index;
    }
    
    AsciiBytes getAsciiBytesName() {
        return this.name;
    }
    
    @Override
    public boolean hasName(CharSequence name, char suffix) {
        return this.headerName.matches(name, suffix);
    }
    
    /**
     * 返回此 {@link JarEntry} 的 {@link URL}
     */
    URL getUrl() throws MalformedURLException {
        return new URL(this.jarFile.getUrl(), getName());
    }
    
    @Override
    public Attributes getAttributes() throws IOException {
        Manifest manifest = this.jarFile.getManifest();
        return (manifest != null) ? manifest.getAttributes(getName()) : null;
    }
    
    @Override
    public Certificate[] getCertificates() {
        return getCertification().getCertificates();
    }
    
    @Override
    public CodeSigner[] getCodeSigners() {
        return getCertification().getCodeSigners();
    }
    
    private JarEntryCertification getCertification() {
        if (!this.jarFile.isSigned()) {
            return JarEntryCertification.NONE;
        }
        JarEntryCertification certification = this.certification;
        if (certification == null) {
            certification = this.jarFile.getCertification(this);
            this.certification = certification;
        }
        return certification;
    }
    
    @Override
    public long getLocalHeaderOffset() {
        return this.localHeaderOffset;
    }
    
}
