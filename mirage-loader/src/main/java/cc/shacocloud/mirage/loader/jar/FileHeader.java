package cc.shacocloud.mirage.loader.jar;

import java.util.zip.ZipEntry;

/**
 * 已从 Jar 文件加载的文件头记录
 *
 * @see JarEntry
 * @see CentralDirectoryFileHeader
 */
interface FileHeader {
    
    /**
     * 如果标头具有给定名称，则返回 {@code true}
     *
     * @param name   要测试的名称
     * @param suffix 附加后缀（或 {@code 0}）
     * @return 如果标头具有给定名称则为 {@code true}
     */
    boolean hasName(CharSequence name, char suffix);
    
    /**
     * 返回归档数据中加载文件头的偏移量
     */
    long getLocalHeaderOffset();
    
    /**
     * 返回条目的压缩大小
     */
    long getCompressedSize();
    
    /**
     * 返回条目的未压缩大小
     */
    long getSize();
    
    /**
     * 返回用于压缩数据的方法
     *
     * @see ZipEntry#STORED
     * @see ZipEntry#DEFLATED
     */
    int getMethod();
    
}
