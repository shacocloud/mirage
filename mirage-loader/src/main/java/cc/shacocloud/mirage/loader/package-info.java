/**
 * mirage 启动加载器，自定义 JarFile 支持加载 Jar 中 jar
 * <p>
 * 该模块参考和借鉴了大量 spring-boot-loader 模块的设计，spring 是个非常优秀的框架
 *
 * @author 思追(shaco)
 */
package cc.shacocloud.mirage.loader;