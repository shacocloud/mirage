package cc.shacocloud.mirage.loader.jar;

import cc.shacocloud.mirage.loader.data.RandomAccessData;

/**
 * 由 {@link CentralDirectoryParser} 触发的回调访问者
 */
interface CentralDirectoryVisitor {
    
    void visitStart(CentralDirectoryEndRecord endRecord, RandomAccessData centralDirectoryData);
    
    void visitFileHeader(CentralDirectoryFileHeader fileHeader, long dataOffset);
    
    void visitEnd();
    
}
