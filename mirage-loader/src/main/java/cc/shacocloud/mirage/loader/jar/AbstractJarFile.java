package cc.shacocloud.mirage.loader.jar;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.Permission;

/**
 * {@link java.util.jar.JarFile} 扩展变体的基类
 */
abstract class AbstractJarFile extends java.util.jar.JarFile {
    
    protected AbstractJarFile(File file) throws IOException {
        super(file);
    }
    
    /**
     * 返回可用于访问此 JAR 文件的 URL
     * <p>
     * 注意：指定的 URL 无法序列化或克隆。
     */
    abstract URL getUrl() throws MalformedURLException;
    
    /**
     * 返回此实例的 {@link JarFileType}
     */
    abstract JarFileType getType();
    
    /**
     * 返回此 JAR 的安全权限
     */
    abstract Permission getPermission();
    
    /**
     * 返回整个 jar 内容的 {@link InputStream}。
     */
    abstract InputStream getInputStream() throws IOException;
    
    enum JarFileType {
        
        DIRECT, NESTED_DIRECTORY, NESTED_JAR
        
    }
    
}
