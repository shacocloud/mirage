package cc.shacocloud.mirage.loader.data;

import java.io.IOException;
import java.io.InputStream;

/**
 * 提供对某些基础数据的只读随机访问的接口。实现必须允许以线程安全的方式并发读取
 */
public interface RandomAccessData {
    
    /**
     * 返回可用于读取基础数据的 {@link InputStream}。调用方负责关闭基础流
     */
    InputStream getInputStream() throws IOException;
    
    /**
     * 为此数据的特定子部分返回一个新的 {@link RandomAccessData}
     *
     * @param offset 偏移量
     * @param length 长度
     */
    RandomAccessData getSubsection(long offset, long length);
    
    /**
     * 读取所有数据并将其作为字节数组返回
     */
    byte[] read() throws IOException;
    
    /**
     * 读取从给定 {@code offset}开始的数据的 {@code length} 字节
     */
    byte[] read(long offset, long length) throws IOException;
    
    /**
     * 返回数据的大小
     */
    long getSize();
    
}
