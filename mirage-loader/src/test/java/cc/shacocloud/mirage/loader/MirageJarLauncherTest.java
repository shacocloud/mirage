package cc.shacocloud.mirage.loader;

import org.junit.jupiter.api.Test;

import java.io.File;

/**
 * @author 思追(shaco)
 */
class MirageJarLauncherTest {
    
    @Test
    public void jarRun() throws Exception {
        File file = new File("xxx.jar");
        Launcher launcher = new MirageJarLauncher(file);
        launcher.run(new String[]{});
    }
    
}