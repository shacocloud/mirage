package cc.shacocloud.mirage.context.restful;

import cc.shacocloud.mirage.core.bind.ConditionalOnClass;
import cc.shacocloud.mirage.env.bind.ConfigurationProperties;
import cc.shacocloud.mirage.restful.bind.BodyHandlerOptions;
import cc.shacocloud.mirage.restful.bind.SessionHandlerOptions;
import io.vertx.core.DeploymentOptions;
import io.vertx.core.http.HttpServerOptions;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * mirage restful 配置文件
 */
@Getter
@Setter
@NoArgsConstructor
@ConfigurationProperties("mirage.restful")
@ConditionalOnClass({"cc.shacocloud.mirage.restful.MirageRequestMappingHandler"})
public class MirageRestfulConfigProperties {
    
    /**
     * http server 配置属性
     */
    private HttpServerOptions httpServerOptions = new HttpServerOptions();
    
    /**
     * Verticle 部署的配置属性
     */
    private DeploymentOptions deploymentOptions = new DeploymentOptions();
    
    /**
     * 请求体处理器配置属性
     */
    private BodyHandlerOptions bodyOptions = new BodyHandlerOptions();
    
    /**
     * 请求会话处理器配置属性
     */
    private SessionHandlerOptions sessionOptions = new SessionHandlerOptions();
    
    
}
