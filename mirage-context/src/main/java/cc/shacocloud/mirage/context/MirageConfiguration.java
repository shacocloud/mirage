package cc.shacocloud.mirage.context;

import cc.shacocloud.mirage.bean.bind.Bean;
import cc.shacocloud.mirage.bean.bind.Configuration;
import cc.shacocloud.mirage.core.bind.ConditionalOnMissingBean;
import cc.shacocloud.mirage.utils.converter.TypeConverterSupport;
import cc.shacocloud.mirage.utils.converter.TypeConverterSupportImpl;
import cc.shacocloud.mirage.utils.reflection.DefaultParameterNameDiscoverer;
import cc.shacocloud.mirage.utils.reflection.ParameterNameDiscoverer;
import io.vertx.core.Vertx;

/**
 * @author 思追(shaco)
 */
@Configuration
public class MirageConfiguration {
    
    /**
     * 使用统一的 {@link Vertx}
     */
    @Bean
    @ConditionalOnMissingBean
    public Vertx vertx(MirageVertxProperties properties) {
        return Vertx.vertx(properties);
    }
    
    /**
     * 类型转换器
     */
    @Bean
    @ConditionalOnMissingBean
    public TypeConverterSupport typeConverterSupport() {
        return new TypeConverterSupportImpl();
    }
    
    /**
     * 参数名称发现器
     */
    @Bean
    @ConditionalOnMissingBean
    public ParameterNameDiscoverer parameterNameDiscoverer() {
        return new DefaultParameterNameDiscoverer();
    }
}
