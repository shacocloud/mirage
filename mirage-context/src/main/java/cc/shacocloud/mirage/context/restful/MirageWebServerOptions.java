package cc.shacocloud.mirage.context.restful;

import cc.shacocloud.mirage.restful.RouterMappingHandler;
import cc.shacocloud.mirage.restful.VertxRouterDispatcherOptions;
import io.vertx.core.DeploymentOptions;
import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;

import java.util.List;

/**
 * {@link MirageWebServerVerticle} 的参数配置对象
 *
 * @author shaco
 * @see MirageWebServerVerticle
 */
@Getter
@Setter
public class MirageWebServerOptions extends VertxRouterDispatcherOptions {
    
    private final DeploymentOptions deploymentOptions;
    
    public MirageWebServerOptions(@NotNull MirageRestfulConfigProperties webMvcConfigProperties,
                                  @NotNull List<RouterMappingHandler> routerMappingHandlers) {
        this.deploymentOptions = webMvcConfigProperties.getDeploymentOptions();
        
        setServerOptions(webMvcConfigProperties.getHttpServerOptions());
        setBodyOptions(webMvcConfigProperties.getBodyOptions());
        setSessionOptions(webMvcConfigProperties.getSessionOptions());
        setRouterMappingHandlers(routerMappingHandlers);
    }
    
    /**
     * 获取部署参数
     */
    public DeploymentOptions getDeploymentOptions() {
        return deploymentOptions;
    }
}
