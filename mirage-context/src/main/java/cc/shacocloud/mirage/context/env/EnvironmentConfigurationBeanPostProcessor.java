package cc.shacocloud.mirage.context.env;

import cc.shacocloud.mirage.bean.BeanPostProcessor;
import cc.shacocloud.mirage.bean.bind.Component;
import cc.shacocloud.mirage.bean.exception.BeanException;
import cc.shacocloud.mirage.core.ApplicationListener;
import cc.shacocloud.mirage.core.event.EnvironmentChangeEvent;
import cc.shacocloud.mirage.env.Environment;
import cc.shacocloud.mirage.env.EnvironmentAware;
import cc.shacocloud.mirage.env.bind.BindEnvProperties;
import cc.shacocloud.mirage.env.bind.ConfigurationProperties;
import cc.shacocloud.mirage.env.bind.EnvValue;
import cc.shacocloud.mirage.utils.ClassUtil;
import cc.shacocloud.mirage.utils.Utils;
import cc.shacocloud.mirage.utils.annotation.AnnotatedElementUtils;
import cc.shacocloud.mirage.utils.collection.CollUtil;
import cc.shacocloud.mirage.utils.converter.TypeConverter;
import cc.shacocloud.mirage.utils.converter.TypeDescriptor;
import cc.shacocloud.mirage.utils.reflection.ReflectUtil;
import io.vertx.core.Future;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.jetbrains.annotations.NotNull;

import java.lang.reflect.AnnotatedElement;
import java.lang.reflect.Field;
import java.lang.reflect.Member;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * 基于 {@link BeanPostProcessor} 实现 {@link ConfigurationProperties} 和 {@link EnvValue} 注解的配置注入，
 * 基于 {@link ApplicationListener} 实现配置属性变更监听，在配置变更时重新加载配置信息。<b>需要注意的是：如果配置变更重新注入配置时发生例外，将导致系统异常退出</b>
 *
 * @author 思追(shaco)
 * @see BindEnvProperties
 * @see ConfigurationProperties
 * @see EnvValue
 */
@Slf4j
@Component
public class EnvironmentConfigurationBeanPostProcessor implements BeanPostProcessor, EnvironmentAware, ApplicationListener<EnvironmentChangeEvent> {
    
    private static final Class<ConfigurationProperties> CONFIGURATION_PROPERTIES_CLASS = ConfigurationProperties.class;
    private static final Class<EnvValue> ENV_VALUE_CLASS = EnvValue.class;
    
    // @ConfigurationProperties 注解配置对象集
    private final List<ConfigurationPropertiesBean> configurationPropertiesBeans = new ArrayList<>(20);
    
    // @EnvValue 对象成员配置集
    private final List<BeanEnvValueMembers> beanEnvValueMembers = new ArrayList<>(20);
    private final TypeConverter typeConverter;
    private Environment environment;
    
    public EnvironmentConfigurationBeanPostProcessor(TypeConverter typeConverter) {
        this.typeConverter = typeConverter;
    }
    
    @Override
    public @NotNull Object postProcessBeforeInitialization(@NotNull String beanName,
                                                           @NotNull Object bean) throws BeanException {
        Object result = bean;
        
        Class<?> beanClass = bean.getClass();
        
        // @ConfigurationProperties 注解检查
        ConfigurationProperties configurationProperties = AnnotatedElementUtils.getAnnotation(beanClass, CONFIGURATION_PROPERTIES_CLASS);
        if (Objects.nonNull(configurationProperties)) {
            result = BindEnvProperties.bind(environment, bean, configurationProperties, typeConverter);
            
            configurationPropertiesBeans.add(new ConfigurationPropertiesBean(bean, configurationProperties));
        }
        
        // @EnvValue 注解检查
        List<Member> members = Arrays.stream(ReflectUtil.getMembers(beanClass))
                .filter(member -> member instanceof AnnotatedElement)
                .filter(member -> AnnotatedElementUtils.hasAnnotation((AnnotatedElement) member, ENV_VALUE_CLASS))
                .collect(Collectors.toList());
        
        if (CollUtil.isNotEmpty(members)) {
            bindEnvValue(bean, members);
            
            beanEnvValueMembers.add(new BeanEnvValueMembers(bean, members));
        }
        
        return result;
    }
    
    /**
     * 当配置发生变更时，刷新所有配置对象的配置信息
     */
    @Override
    public @NotNull Future<Void> onApplicationEvent(@NotNull EnvironmentChangeEvent event) {
        
        try {
            // 配置更新重新绑定 @ConfigurationProperties 注解
            for (ConfigurationPropertiesBean configurationPropertiesBean : configurationPropertiesBeans) {
                BindEnvProperties.bind(environment, configurationPropertiesBean.getBean(),
                        configurationPropertiesBean.getConfigurationProperties(), typeConverter);
            }
            
            // 配置更新重新绑定 @EnvValue 注解值
            for (BeanEnvValueMembers beanEnvValueMember : beanEnvValueMembers) {
                bindEnvValue(beanEnvValueMember.getBean(), beanEnvValueMember.getMembers());
            }
        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("重新绑定配置信息发生未知例外，系统异常退出！", e);
            }
            
            System.exit(-1);
            
            return Future.failedFuture(e);
        }
        
        return Future.succeededFuture();
    }
    
    /**
     * 基于 {@link EnvValue} 注解绑定环境配置的值
     */
    protected void bindEnvValue(@NotNull Object bean,
                                @NotNull List<Member> members) {
        for (Member member : members) {
            if (!(member instanceof AnnotatedElement)) continue;
            
            EnvValue envValue = AnnotatedElementUtils.getAnnotation((AnnotatedElement) member, ENV_VALUE_CLASS);
            if (Objects.isNull(envValue)) continue;
            
            String expressionResult = environment.resolveRequiredPlaceholders(envValue.value());
            
            // 属性
            if (member instanceof Field) {
                Field field = (Field) member;
                Object value = typeConverter.convertIfNecessary(expressionResult, TypeDescriptor.valueOf(field.getType()));
                ReflectUtil.setFieldValue(bean, field, value);
            }
            // 方法
            else if (member instanceof Method) {
                Method method = (Method) member;
                
                if (!ClassUtil.isSetMethod(method)) {
                    throw new IllegalArgumentException(String.format("%s 不满足 @EnvValue 注解的格式要求，方法必须是一个 set 方法", Utils.methodDescription(method)));
                }
                
                Object value = typeConverter.convertIfNecessary(expressionResult, TypeDescriptor.valueOf(method.getParameterTypes()[0]));
                ReflectUtil.invokeMethod(method, bean, value);
            }
        }
    }
    
    @Override
    public void setEnvironment(@NotNull Environment environment) {
        this.environment = environment;
    }
    
    @Getter
    @RequiredArgsConstructor
    private static class ConfigurationPropertiesBean {
        private final Object bean;
        private final ConfigurationProperties configurationProperties;
    }
    
    @Getter
    @RequiredArgsConstructor
    private static class BeanEnvValueMembers {
        private final Object bean;
        private final List<Member> members;
    }
}
