package cc.shacocloud.mirage.context;

import cc.shacocloud.mirage.env.bind.ConfigurationProperties;
import io.vertx.core.VertxOptions;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author 思追(shaco)
 */
@Getter
@Setter
@NoArgsConstructor
@ConfigurationProperties("mirage.vertx")
public class MirageVertxProperties extends VertxOptions {
}
