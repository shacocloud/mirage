package cc.shacocloud.mirage.starter.banner;

import cc.shacocloud.mirage.starter.MirageBootVersion;

import java.io.PrintStream;

/**
 * Mirage banner
 */
public class MirageBootBanner {
    
    private static final String[] BANNER = {" ______  _                                       ",
            "|  ___ \\(_)                      _  _ _  _ _  _  ",
            "| | _ | |_  ____ ____  ____  ___( \\( ( \\( ( \\( \\ ",
            "| || || | |/ ___) _  |/ _  |/ _  ) )) ) )) ) )) )",
            "| || || | | |  ( ( | ( ( | ( (/ (_/(_(_/(_(_/(_/ ",
            "|_||_||_|_|_|   \\_||_|\\_|| |\\____)               ",
            "                     (_____|                     "};
    
    
    private static final String MIRAGE_BOOT = " :: Mirage Boot :: ";
    
    private static final int STRAP_LINE_SIZE = 42;
    
    /**
     * 打印 banner
     */
    public void printBanner(PrintStream printStream) {
        for (String line : BANNER) {
            printStream.println(line);
        }
        String version = MirageBootVersion.getVersion();
        version = (version != null) ? " (v" + version + ")" : "";
        StringBuilder padding = new StringBuilder();
        while (padding.length() < STRAP_LINE_SIZE - (version.length() + MIRAGE_BOOT.length())) {
            padding.append(" ");
        }
        
        printStream.println(AnsiOutput.toString(AnsiColor.GREEN, MIRAGE_BOOT, AnsiColor.DEFAULT, padding.toString(),
                AnsiStyle.FAINT, version));
        printStream.println();
    }
}
