package cc.shacocloud.mirage.starter;

import cc.shacocloud.mirage.core.GenericApplicationContext;
import lombok.extern.slf4j.Slf4j;

/**
 * Mirage 默认的应用启动上下文，基于 {@link GenericApplicationContext} 实现
 *
 * @author shaco
 */
@Slf4j
public class MirageApplicationContext extends GenericApplicationContext {
    
    public MirageApplicationContext() {
    }
    
    public MirageApplicationContext(ClassLoader classLoader) {
        super(classLoader);
    }
}
