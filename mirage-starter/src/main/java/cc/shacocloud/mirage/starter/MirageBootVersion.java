package cc.shacocloud.mirage.starter;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.File;
import java.io.IOException;
import java.net.JarURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.security.CodeSource;
import java.util.jar.Attributes;
import java.util.jar.JarFile;

/**
 * mirage 版本信息  {@link MirageBootVersion}
 */
public final class MirageBootVersion {
    
    /**
     * 返回当前引导代码基的完整版本字符串，如果不能确定，则返回{@code null}。
     *
     * @see Package#getImplementationVersion()
     */
    public static String getVersion() {
        return determineMirageBootVersion();
    }
    
    @Nullable
    private static String determineMirageBootVersion() {
        String implementationVersion = MirageBootVersion.class.getPackage().getImplementationVersion();
        if (implementationVersion != null) {
            return implementationVersion;
        }
        CodeSource codeSource = MirageBootVersion.class.getProtectionDomain().getCodeSource();
        if (codeSource == null) {
            return null;
        }
        URL codeSourceLocation = codeSource.getLocation();
        try {
            URLConnection connection = codeSourceLocation.openConnection();
            if (connection instanceof JarURLConnection) {
                return getImplementationVersion(((JarURLConnection) connection).getJarFile());
            }
            try (JarFile jarFile = new JarFile(new File(codeSourceLocation.toURI()))) {
                return getImplementationVersion(jarFile);
            }
        } catch (Exception ex) {
            return null;
        }
    }
    
    private static String getImplementationVersion(@NotNull JarFile jarFile) throws IOException {
        return jarFile.getManifest().getMainAttributes().getValue(Attributes.Name.IMPLEMENTATION_VERSION);
    }
    
}
