package cc.shacocloud.mirage.starter;

import cc.shacocloud.mirage.bean.bind.ComponentScan;
import cc.shacocloud.mirage.bean.bind.Configuration;
import cc.shacocloud.mirage.bean.bind.ExcludeComponent;
import cc.shacocloud.mirage.utils.annotation.AliasFor;

import java.lang.annotation.*;

/**
 * 启动类注解
 * <p>
 * 在应用启动类上标识该注解
 */
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Configuration
@ComponentScan
@ExcludeComponent
public @interface MirageBootApplication {
    
    /**
     * 排除的类
     */
    @AliasFor(annotation = ExcludeComponent.class)
    Class<?>[] excludeClasses() default {};
    
    /**
     * 排除的类名称，即 对象工厂中的对象名称
     */
    @AliasFor(annotation = ExcludeComponent.class)
    String[] excludeBeanName() default {};
    
    /**
     * 基础扫描路径
     *
     * @see ComponentScan#scanBasePackages()
     */
    @AliasFor(annotation = ComponentScan.class)
    String[] scanBasePackages() default {};
    
    /**
     * 扫描的基础类
     *
     * @see ComponentScan#scanBasePackageClasses()
     */
    @AliasFor(annotation = ComponentScan.class)
    Class<?>[] scanBasePackageClasses() default {};
    
}
