package cc.shacocloud.mirage.starter.banner;

public interface AnsiElement {
    
    /**
     * @return ANSI 转义码
     */
    @Override
    String toString();
    
}
