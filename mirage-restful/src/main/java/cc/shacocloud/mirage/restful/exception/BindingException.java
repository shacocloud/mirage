package cc.shacocloud.mirage.restful.exception;

import cc.shacocloud.mirage.restful.bind.validation.BindingResult;
import lombok.Getter;
import org.jetbrains.annotations.NotNull;

/**
 * 绑定异常
 *
 * @author 思追(shaco)
 * @see BindingResult
 */
public class BindingException extends Exception {
    
    @Getter
    private final BindingResult bindingResult;
    
    public BindingException(@NotNull BindingResult bindingResult) {
        super(bindingResult.toString());
        this.bindingResult = bindingResult;
    }
    
}
