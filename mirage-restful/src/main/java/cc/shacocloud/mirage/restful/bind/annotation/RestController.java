package cc.shacocloud.mirage.restful.bind.annotation;


import cc.shacocloud.mirage.utils.annotation.AliasFor;

import java.lang.annotation.*;

/**
 * RestApi的控制器，{@link Controller}与{@link ResponseBody}的结合体
 *
 * @see Controller
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Controller
@ResponseBody
public @interface RestController {
    
    /**
     * 被标识的处理器名称
     */
    @AliasFor(annotation = Controller.class)
    String value() default "";
    
}
