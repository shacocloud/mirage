package cc.shacocloud.mirage.restful;

import io.vertx.core.Future;
import io.vertx.core.Handler;
import io.vertx.core.http.HttpServerRequest;
import io.vertx.ext.web.Router;
import org.jetbrains.annotations.Nullable;

/**
 * 允许定制处理程序执行链的工作流接口。
 * <p>
 * 应用程序可以为某些处理程序组注册任意数量的现有或自定义拦截器，以添加通用的预处理行为，而无需修改每个处理程序实现。
 */
public interface HandlerInterceptor {
    
    /**
     * 拦截处理程序的执行前，在 {@link Router#handle(Object)} 前调用。
     * <p>
     * 该执行链由任意数量的拦截器组成，处理程序本身位于末端。
     * 使用此方法，每个拦截器都可以决定中止执行链，通常是发送HTTP错误或编写自定义响应。
     *
     * @param request  当前的HTTP请求
     * @param response 当前HTTP响应
     * @param handler  即将要执行的处理程序
     * @return 如果需要继续向后执行则返回 {@code true} 否者系统将认为在该拦截器中已经处理{@link HttpResponse}，将结束处理。
     */
    default Future<Boolean> preHandle(HttpRequest request,
                                      HttpResponse response,
                                      VertxInvokeHandler handler) {
        return Future.succeededFuture(true);
    }
    
    /**
     * 设置一个结束处理程序。一旦流结束，并且没有更多的数据要读取，这个处理程序将被调用。
     *
     * @param request  当前的HTTP请求
     * @param response 当前HTTP响应
     * @param handler  执行的处理程序
     * @param cause    当响应被释放或遇到异常时该值不为空，允许在这进行一个统一的处理
     * @see HttpServerRequest#endHandler(Handler)
     */
    default Future<Void> afterCompletion(HttpRequest request,
                                         HttpResponse response,
                                         VertxInvokeHandler handler,
                                         @Nullable Throwable cause) {
        return Future.succeededFuture();
    }
    
}
