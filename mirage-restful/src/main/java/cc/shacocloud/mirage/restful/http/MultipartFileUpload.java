package cc.shacocloud.mirage.restful.http;

import cc.shacocloud.mirage.restful.bind.annotation.FileUpload;
import cc.shacocloud.mirage.restful.bind.support.MultipartFileUploadArgumentResolver;
import org.jetbrains.annotations.Nullable;

import java.util.Map;

/**
 * 表示来自HTTP多部分表单提交的文件上传，在需要的控制器方法上指定该类型对象
 *
 * @see FileUpload
 * @see MultipartFileUploadArgumentResolver
 */
public interface MultipartFileUpload {
    
    /**
     * 返回上传的文件是否为空。
     * <p>
     * 也就是说，在多部分格式中没有选择文件，或者选择的文件没有内容。
     */
    boolean isEmpty();
    
    /**
     * @return 上传的文件数量
     */
    int size();
    
    /**
     * @return 如果上传的文件只有一个则直接返回，如果不存在或存在多个将抛出例外！
     */
    MultipartFile get() throws IllegalArgumentException;
    
    /**
     * 根据文件上传时指定的key名称获取文件，如果不存在则返回{@code null}
     *
     * @param name 文件上传时指定的key
     * @return {@link MultipartFile}
     */
    @Nullable
    MultipartFile get(String name);
    
    /**
     * @return 返回文件Map，keu为文件上传时传入的名称
     */
    Map<String, MultipartFile> files();
    
    
}
