package cc.shacocloud.mirage.restful.bind.annotation;

import cc.shacocloud.mirage.bean.bind.Component;
import cc.shacocloud.mirage.restful.Filter;
import cc.shacocloud.mirage.utils.annotation.AliasFor;
import cc.shacocloud.mirage.utils.comparator.Ordered;

import java.lang.annotation.*;

/**
 * web 过滤器类注解，定义的类必须是 {@link Filter} 的实现类
 *
 * @see RequestMapping
 */
@Target({ElementType.TYPE, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Component
public @interface WebFilter {
    
    /**
     * 过滤器名称
     */
    @AliasFor(annotation = Component.class, value = "value")
    String filterName();
    
    /**
     * 拦截路径，如果不设置默认全匹配
     */
    String[] includePatterns() default {};
    
    /**
     * 排除路径
     */
    String[] excludePatterns() default {};
    
    /**
     * 排序字段
     */
    int order() default Ordered.LOWEST_PRECEDENCE;
    
}
