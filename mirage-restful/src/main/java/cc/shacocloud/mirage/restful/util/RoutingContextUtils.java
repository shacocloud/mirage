package cc.shacocloud.mirage.restful.util;

import cc.shacocloud.mirage.restful.RoutingContext;
import cc.shacocloud.mirage.restful.VertXRoutingContextImpl;

public class RoutingContextUtils {
    
    /**
     * 创建{@link RoutingContext}，并且添加到 {@link io.vertx.ext.web.RoutingContext} 中进行传递
     */
    public static RoutingContext createVertXRoutingContext(io.vertx.ext.web.RoutingContext ctx) {
        RoutingContext routingContext = ctx.get(RoutingContext.INTERNAL_PARAMETERS_PREFIX + RoutingContext.class.getName());
        
        if (routingContext == null) {
            routingContext = new VertXRoutingContextImpl(ctx);
            ctx.put(RoutingContext.INTERNAL_PARAMETERS_PREFIX + RoutingContext.class.getName(), routingContext);
        }
        
        return routingContext;
    }
    
    
}
