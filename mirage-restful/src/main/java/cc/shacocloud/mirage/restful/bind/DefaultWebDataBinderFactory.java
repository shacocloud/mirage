package cc.shacocloud.mirage.restful.bind;

import cc.shacocloud.mirage.restful.HttpRequest;
import org.jetbrains.annotations.Nullable;

/**
 * 默认的{@link WebDataBinderFactory}，并且使用{@link WebBindingInitializer}进行初始化。
 */
public class DefaultWebDataBinderFactory implements WebDataBinderFactory {
    
    @Nullable
    private final WebBindingInitializer initializer;
    
    public DefaultWebDataBinderFactory(@Nullable WebBindingInitializer initializer) {
        this.initializer = initializer;
    }
    
    @Override
    public WebDataBinder createBinder(HttpRequest request, Object target, String objectName) {
        WebDataBinder dataBinder = createBinderInstance(target, objectName, request);
        if (this.initializer != null) {
            this.initializer.initBinder(dataBinder);
        }
        initBinder(dataBinder, request);
        return dataBinder;
    }
    
    /**
     * 创建{@link WebDataBinder}实例
     */
    protected WebDataBinder createBinderInstance(@Nullable Object target, String objectName, HttpRequest request) {
        return new WebDataBinder(target, objectName);
    }
    
    /**
     * 用于在通过  {@link WebBindingInitializer} 进行全局初始化之后，进一步初始化创建的数据绑定程序实例。
     */
    protected void initBinder(WebDataBinder dataBinder, HttpRequest request) {
    
    }
    
}
