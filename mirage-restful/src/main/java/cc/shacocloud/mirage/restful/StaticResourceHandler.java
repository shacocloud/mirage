package cc.shacocloud.mirage.restful;

import cc.shacocloud.mirage.utils.resource.Resource;
import cc.shacocloud.mirage.utils.resource.ResourceUtil;
import io.vertx.core.Future;
import io.vertx.ext.web.common.WebEnvironment;
import org.jetbrains.annotations.NotNull;

import java.util.Set;

/**
 * @author 思追(shaco)
 */
public interface StaticResourceHandler {
    
    /**
     * Web 根目录，默认为 类路径下的 webroot 目录
     */
    String DEFAULT_WEB_ROOT = ResourceUtil.CLASSPATH_URL_PREFIX + "webroot";
    
    /**
     * 文件是否为只读且永远不会更新
     */
    boolean DEFAULT_FILES_READ_ONLY = true;
    
    /**
     * 缓存标头的默认最长期限
     */
    long DEFAULT_MAX_AGE_SECONDS = 86400;
    
    /**
     * 是否启用缓存标头处理
     */
    boolean DEFAULT_CACHING_ENABLED = !WebEnvironment.development();
    
    /**
     * 是否可以提供隐藏文件
     */
    boolean DEFAULT_INCLUDE_HIDDEN = false;
    
    /**
     * 缓存时的默认缓存超时时间，单位 毫秒
     */
    long DEFAULT_CACHE_ENTRY_TIMEOUT = 30000;
    
    /**
     * 默认索引页，即访问 / 会返回 /index.html 的文件内容
     */
    String DEFAULT_INDEX_PAGE = "index.html";
    
    /**
     * 默认最大缓存大小
     */
    int DEFAULT_MAX_CACHE_SIZE = 10000;
    
    /**
     * 是否应使用范围请求处理支持
     */
    boolean DEFAULT_RANGE_SUPPORT = true;
    
    /**
     * 是否应发送 vary 标头
     */
    boolean DEFAULT_SEND_VARY_HEADER = true;
    
    /**
     * 设置文件是否为只读且永不更改
     */
    StaticResourceHandler setFilesReadOnly(boolean readOnly);
    
    /**
     * 设置缓存标头中最长期限的值
     */
    StaticResourceHandler setMaxAgeSeconds(long maxAgeSeconds);
    
    /**
     * 设置是否启用缓存标头处理
     */
    StaticResourceHandler setCachingEnabled(boolean enabled);
    
    /**
     * 设置是否应提供隐藏文件
     */
    StaticResourceHandler setIncludeHidden(boolean includeHidden);
    
    /**
     * 设置启用缓存时的服务器缓存条目超时
     */
    StaticResourceHandler setCacheEntryTimeout(long timeout);
    
    /**
     * 设置索引页，即访问目录路径时会自动拼接该索引页
     */
    StaticResourceHandler setIndexPage(@NotNull String indexPage);
    
    /**
     * 设置启用缓存时的最大缓存大小
     */
    StaticResourceHandler setMaxCacheSize(int maxCacheSize);
    
    /**
     * 如果要发送的文件的媒体类型位于提供的 {@code mediaTypes}，则跳过压缩
     * {@code Content-Encoding} 标头设置为 {@code identity} 表示 {@code mediaTypes} 存在的类型
     */
    StaticResourceHandler skipCompressionForMediaTypes(@NotNull Set<String> mediaTypes);
    
    /**
     * 如果要发送的文件的后缀位于提供的 {@code fileSuffixes} 中，则跳过压缩
     * {@code Content-Encoding} 标头设置为 {@code identity} 表示 {@code fileSuffixes} 中存在的后缀
     */
    StaticResourceHandler skipCompressionForSuffixes(@NotNull Set<String> fileSuffixes);
    
    /**
     * 设置是否应启用范围请求
     */
    StaticResourceHandler setEnableRangeSupport(boolean enableRangeSupport);
    
    /**
     * 设置是否应随响应一起发送 vary 标头
     */
    StaticResourceHandler setSendVaryHeader(boolean varyHeader);
    
    /**
     * 设置文本相关文件的默认内容编码，这允许覆盖系统设置默认值，默认  "UTF-8"
     */
    StaticResourceHandler setDefaultContentEncoding(@NotNull String contentEncoding);
    
    /**
     * 基于路由上下文 {@link RoutingContext} 获取资源对象
     */
    Future<Resource> findPathResource(@NotNull RoutingContext context);
    
    /**
     * 发送资源文件到响应中
     *
     * @param response 响应
     * @param resource 资源文件
     */
    Future<Void> sendResource(@NotNull HttpResponse response, @NotNull Resource resource);
    
}
