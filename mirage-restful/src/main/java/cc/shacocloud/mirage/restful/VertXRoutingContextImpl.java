package cc.shacocloud.mirage.restful;

import io.vertx.core.AsyncResult;
import io.vertx.core.Handler;
import io.vertx.core.Vertx;
import io.vertx.core.http.HttpMethod;

import java.util.Map;
import java.util.Objects;
import java.util.function.Supplier;

/**
 * 基于{@link io.vertx.ext.web.RoutingContext} 的代理实现
 */
public class VertXRoutingContextImpl implements RoutingContext {
    
    private final io.vertx.ext.web.RoutingContext routingContext;
    private HttpRequest request;
    private HttpResponse response;
    
    public VertXRoutingContextImpl(io.vertx.ext.web.RoutingContext routingContext) {
        this.routingContext = routingContext;
        this.request = new VertXHttpRequestImpl(routingContext, this);
        this.response = new VertXHttpResponseImpl(routingContext, this);
    }
    
    @Override
    public Vertx vertx() {
        return routingContext.vertx();
    }
    
    @Override
    public HttpRequest request() {
        return request;
    }
    
    @Override
    public HttpRequest request(HttpRequest request) {
        this.request = request;
        return request;
    }
    
    @Override
    public HttpResponse response() {
        return response;
    }
    
    @Override
    public HttpResponse response(HttpResponse response) {
        this.response = response;
        return response;
    }
    
    @Override
    public RoutingContext put(String key, Object obj) {
        routingContext.put(key, obj);
        return this;
    }
    
    @Override
    public <T> T get(String key) {
        return routingContext.get(key);
    }
    
    @Override
    public <T> T get(String key, Supplier<T> supplier) {
        T res = get(key);
        if (Objects.nonNull(res)) {
            return res;
        }
        return supplier.get();
    }
    
    @Override
    public <T> T remove(String key) {
        return routingContext.remove(key);
    }
    
    @Override
    public Map<String, Object> data() {
        return routingContext.data();
    }
    
    @Override
    public String normalizedPath() {
        return routingContext.normalizedPath();
    }
    
    @Override
    public void reroute(HttpMethod method, String path) {
        routingContext.reroute(method, path);
    }
    
    @Override
    public void next() {
        routingContext.next();
    }
    
    @Override
    public int addEndHandler(Handler<AsyncResult<Void>> handler) {
        return routingContext.addEndHandler(handler);
    }
    
    @Override
    public boolean removeEndHandler(int handlerId) {
        return routingContext.removeEndHandler(handlerId);
    }
    
    @Override
    public io.vertx.ext.web.RoutingContext getSource() {
        return routingContext;
    }
}
