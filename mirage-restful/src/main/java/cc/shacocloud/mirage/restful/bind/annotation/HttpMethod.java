package cc.shacocloud.mirage.restful.bind.annotation;

/**
 * http 请求方式
 *
 * @author shaco
 */
public enum HttpMethod {
    
    GET,
    HEAD,
    POST,
    PUT,
    PATCH,
    DELETE,
    OPTIONS,
    TRACE
    
    
}
