package cc.shacocloud.mirage.restful.bind.validation;

import jakarta.validation.Validator;

/**
 * 参数校验器，基于 {@link Validator} 拓展
 *
 * @author 思追(shaco)
 */
public interface SmartValidator extends Validator {


}
