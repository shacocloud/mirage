package cc.shacocloud.mirage.restful.exception;

import io.netty.handler.codec.http.HttpResponseStatus;
import lombok.Getter;

/**
 * 直接返回指定状态码的异常
 *
 * @author 思追(shaco)
 */
public class HttpStatusException extends RuntimeException {
    
    @Getter
    private final HttpResponseStatus status;
    
    public HttpStatusException(HttpResponseStatus status) {
        this.status = status;
    }
    
    public HttpStatusException(String message, HttpResponseStatus status) {
        super(message);
        this.status = status;
    }
    
    public HttpStatusException(String message, Throwable cause, HttpResponseStatus status) {
        super(message, cause);
        this.status = status;
    }
    
    public HttpStatusException(Throwable cause, HttpResponseStatus status) {
        super(cause);
        this.status = status;
    }
    
}

