package cc.shacocloud.mirage.restful.bind.validation;

import cc.shacocloud.mirage.restful.bind.validation.errors.BindingResultError;
import cc.shacocloud.mirage.utils.charSequence.CharUtil;
import org.jetbrains.annotations.NotNull;

import java.util.List;

/**
 * @author 思追(shaco)
 */
public interface BindingResult {
    
    /**
     * 模型中 BindingResult 实例名称的前缀，后跟对象名称
     */
    String MODEL_KEY_PREFIX = BindingResult.class.getName() + ".";
    
    /**
     * 是否存在错误
     *
     * @return 如果存在则返回 {@code true} 反之为 {@code false}
     */
    boolean hasErrors();
    
    /**
     * @return 返回错误的数量
     */
    int getErrorCount();
    
    /**
     * 获取所有错误信息
     */
    List<BindingResultError> getAllErrors();
    
    /**
     * 使用给定的错误描述为当前对象的指定嵌套路径字段注册错误信息
     * <p>
     * 嵌套路径，多级使用 {@link CharUtil#DOT} 分割，如果是集合可以通过 [0] 的方式来指定下标，例如：a.b[1]
     *
     * @param nestedPath 嵌套路径字段
     * @param errorCode  错误代码
     */
    void rejectValue(@NotNull String nestedPath, @NotNull String errorCode);
    
    /**
     * 使用给定的错误描述为当前对象的指定嵌套路径字段注册错误信息
     * <p>
     * 嵌套路径，多级使用 {@link CharUtil#DOT} 分割，如果是集合可以通过 [0] 的方式来指定下标，例如：a.b[1]
     *
     * @param nestedPath 嵌套路径字段
     * @param errorCodes 多个错误代码
     */
    void rejectValue(@NotNull String nestedPath, @NotNull String @NotNull ... errorCodes);
    
}
