package cc.shacocloud.mirage.restful.bind.annotation;

import cc.shacocloud.mirage.restful.bind.support.PathVariableMethodArgumentResolver;
import cc.shacocloud.mirage.utils.annotation.AliasFor;

import java.lang.annotation.*;

/**
 * 指示方法参数应绑定到HTTP路径变量的注解
 * <p>
 * 例如： 当前方法的路径绑定{@code RequestMapping("/api/user/:id")}，请求路径为{@code /api/user/1}，
 * 那么指定的路径变量{@code id}的值为 {@code 1}
 *
 * @see RequestMapping
 * @see PathVariableMethodArgumentResolver
 */
@Target(ElementType.PARAMETER)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface PathVariable {
    
    /**
     * {@link #name}的别名
     */
    @AliasFor("name")
    String value() default "";
    
    /**
     * 要绑定到的路径变量的名称
     */
    @AliasFor("value")
    String name() default "";
    
}
