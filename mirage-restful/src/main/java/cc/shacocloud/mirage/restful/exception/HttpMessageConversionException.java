package cc.shacocloud.mirage.restful.exception;

import cc.shacocloud.mirage.restful.http.HttpMessageConverter;
import org.jetbrains.annotations.Nullable;

/**
 * 当转换尝试失败时，由{@link HttpMessageConverter}实现抛出。
 */
public class HttpMessageConversionException extends RuntimeException {
    
    /**
     * 创建一个新的{@link HttpMessageConversionException}
     *
     * @param msg 详细的消息
     */
    public HttpMessageConversionException(String msg) {
        super(msg);
    }
    
    /**
     * 创建一个新的{@link HttpMessageConversionException}
     *
     * @param msg   详细的消息
     * @param cause 根本原因(如果有的话)
     */
    public HttpMessageConversionException(String msg, @Nullable Throwable cause) {
        super(msg, cause);
    }
    
}
