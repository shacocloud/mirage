package cc.shacocloud.mirage.restful;

import cc.shacocloud.mirage.restful.http.HttpHeaderMap;
import io.vertx.core.AsyncResult;
import io.vertx.core.Handler;
import io.vertx.core.buffer.Buffer;
import io.vertx.core.http.Cookie;
import io.vertx.core.http.HttpServerResponse;
import io.vertx.ext.web.Session;

/**
 * {@link HttpServerResponse} 的包装对象
 *
 * @author 思追(shaco)
 */
public class VertXHttpResponseImpl implements HttpResponse {
    
    private final HttpServerResponse httpServerResponse;
    private final HttpHeaderMap httpHeaderMap;
    private final RoutingContext nativeRoutingContext;
    private final io.vertx.ext.web.RoutingContext routingContext;
    
    public VertXHttpResponseImpl(io.vertx.ext.web.RoutingContext routingContext,
                                 RoutingContext nativeRoutingContext) {
        this.routingContext = routingContext;
        this.nativeRoutingContext = nativeRoutingContext;
        this.httpServerResponse = routingContext.response();
        this.httpHeaderMap = new HttpHeaderMap(httpServerResponse.headers());
    }
    
    @Override
    public RoutingContext context() {
        return nativeRoutingContext;
    }
    
    @Override
    public HttpRequest request() {
        return nativeRoutingContext.request();
    }
    
    @Override
    public long bytesWritten() {
        return httpServerResponse.bytesWritten();
    }
    
    @Override
    public HttpHeaderMap headers() {
        return httpHeaderMap;
    }
    
    @Override
    public HttpServerResponse setHeader(String name, String value) {
        return httpServerResponse.putHeader(name, value);
    }
    
    @Override
    public HttpServerResponse setHeader(CharSequence name, CharSequence value) {
        return httpServerResponse.putHeader(name, value);
    }
    
    @Override
    public HttpServerResponse setHeader(String name, Iterable<String> values) {
        return httpServerResponse.putHeader(name, values);
    }
    
    @Override
    public HttpServerResponse setHeader(CharSequence name, Iterable<CharSequence> values) {
        return httpServerResponse.putHeader(name, values);
    }
    
    @Override
    public HttpServerResponse addCookie(Cookie cookie) {
        return httpServerResponse.addCookie(cookie);
    }
    
    @Override
    public Cookie removeCookie(String name, boolean invalidate) {
        return httpServerResponse.removeCookie(name, invalidate);
    }
    
    @Override
    public Session session() {
        return routingContext.session();
    }
    
    @Override
    public HttpResponse setStatusCode(int statusCode) {
        httpServerResponse.setStatusCode(statusCode);
        return this;
    }
    
    @Override
    public int getStatusCode() {
        return httpServerResponse.getStatusCode();
    }
    
    @Override
    public String getStatusMessage() {
        return httpServerResponse.getStatusMessage();
    }
    
    @Override
    public HttpResponse setStatusMessage(String statusMessage) {
        httpServerResponse.setStatusMessage(statusMessage);
        return this;
    }
    
    @Override
    public boolean ended() {
        return httpServerResponse.ended();
    }
    
    @Override
    public boolean closed() {
        return httpServerResponse.closed();
    }
    
    @Override
    public boolean headWritten() {
        return httpServerResponse.headWritten();
    }
    
    @Override
    public void end() {
        httpServerResponse.end();
    }
    
    @Override
    public void end(String data) {
        httpServerResponse.end(data);
    }
    
    @Override
    public void end(Buffer data) {
        httpServerResponse.end(data);
    }
    
    @Override
    public void end(Buffer data, Handler<AsyncResult<Void>> handler) {
        httpServerResponse.end(data, handler);
    }
    
    @Override
    public HttpResponse write(String data) {
        httpServerResponse.write(data);
        return this;
    }
    
    @Override
    public HttpResponse write(Buffer data) {
        httpServerResponse.write(data);
        return this;
    }
    
    @Override
    public HttpResponse write(Buffer data, Handler<AsyncResult<Void>> handler) {
        httpServerResponse.write(data, handler);
        return this;
    }
    
    @Override
    public HttpResponse sendFile(String filename, long offset, long length) {
        httpServerResponse.sendFile(filename, offset, length);
        return this;
    }
    
    @Override
    public HttpResponse sendFile(String filename, long offset, long length, Handler<AsyncResult<Void>> resultHandler) {
        httpServerResponse.sendFile(filename, offset, length, resultHandler);
        return this;
    }
    
    @Override
    public HttpResponse bodyEndHandler(Handler<Void> handler) {
        httpServerResponse.bodyEndHandler(handler);
        return this;
    }
    
    @Override
    public HttpServerResponse getSource() {
        return httpServerResponse;
    }
}
