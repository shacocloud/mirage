package cc.shacocloud.mirage.restful.bind.validation;

import cc.shacocloud.mirage.restful.bind.validation.errors.ConstraintViolationError;
import jakarta.validation.ConstraintViolation;
import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * 参数验证失败的绑定结果对象
 *
 * @author 思追(shaco)
 */
public class ConstraintViolationBindingResult extends AbstractBindingResult {
    
    @Getter
    private final Set<ConstraintViolation<Object>> constraintViolations;
    
    public ConstraintViolationBindingResult(@NotNull Object target,
                                            @Nullable Set<ConstraintViolation<Object>> constraintViolations) {
        super(target, Objects.isNull(constraintViolations) ? new ArrayList<>() :
                constraintViolations.stream().map(ConstraintViolationError::new).collect(Collectors.toList()));
        
        this.constraintViolations = Objects.isNull(constraintViolations) ? Collections.emptySet() : constraintViolations;
    }
    
}
