package cc.shacocloud.mirage.restful;

import cc.shacocloud.mirage.restful.http.HttpMessageConverter;
import cc.shacocloud.mirage.utils.converter.ConversionRegistry;
import org.jetbrains.annotations.NotNull;

import java.util.List;

/**
 * mirage web 配置管理器
 */
public interface MirageRestfulConfigurer {
    
    /**
     * 注册转换器，子类可以自行拓展
     */
    default void registryConversion(@NotNull ConversionRegistry conversionRegistry) {
    }
    
    /**
     * 注册请求消息转换器 {@link HttpMessageConverter}
     */
    default void registerHttpMessageConverter(@NotNull List<HttpMessageConverter<?>> messageConverters) {
    }
    
    /**
     * 注册异常解析器 {@link HandlerExceptionResolver}
     *
     * @see HandlerInterceptor
     */
    default void registerExceptionResolver(@NotNull List<HandlerExceptionResolver> exceptionResolvers) {
    }
    
    /**
     * 注册异常处理器，将由 {@link ExceptionHandlerExceptionResolver#registerExceptionHandler(Object)}进行解析
     *
     * @see ExceptionHandlerExceptionResolver
     * @see ExceptionHandlerMethodResolver
     */
    default void registerExceptionHandler(@NotNull List<Object> exceptionHandler) {
    }
    
    /**
     * 注册请求拦截器 {@link HandlerInterceptor}
     *
     * @param interceptorMappings 拦截器映射信息
     */
    default void registerHandlerInterceptor(@NotNull List<InterceptorMappingInfo> interceptorMappings) {
    }
    
    /**
     * 注册静态资源处理程序
     */
    default void registerStaticResourceHandler(@NotNull StaticResourceHandlerRegistry registry) {
    }
    
}
