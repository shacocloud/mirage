package cc.shacocloud.mirage.restful.http;

import cc.shacocloud.mirage.restful.exception.InvalidMimeTypeException;
import cc.shacocloud.mirage.utils.cache.ConcurrentLruCache;
import cc.shacocloud.mirage.utils.charSequence.StrUtil;
import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.nio.charset.StandardCharsets;
import java.nio.charset.UnsupportedCharsetException;
import java.security.SecureRandom;
import java.util.*;
import java.util.stream.Collectors;

/**
 * {@link MimeType} 实用工具方法。
 */
public abstract class MimeTypeUtils {
    
    /**
     * 使用的比较器 {@link MimeType.SpecificityComparator}
     */
    public static final Comparator<MimeType> SPECIFICITY_COMPARATOR = new MimeType.SpecificityComparator<>();
    /**
     * 包含所有媒体范围的公共常量 MIME 类型 (即 "&#42;/&#42;").
     */
    public static final MimeType ALL;
    /**
     * 字符串等效于 {@link MimeTypeUtils#ALL}.
     */
    public static final String ALL_VALUE = "*/*";
    /**
     * {@code application/graphql+json}.
     *
     * @see <a href="https://github.com/graphql/graphql-over-http">GraphQL over HTTP spec</a>
     */
    public static final MimeType APPLICATION_GRAPHQL;
    /**
     * 字符串等效于 {@link MimeTypeUtils#APPLICATION_GRAPHQL}.
     */
    public static final String APPLICATION_GRAPHQL_VALUE = "application/graphql+json";
    /**
     * {@code application/json}
     */
    public static final MimeType APPLICATION_JSON;
    /**
     * 字符串等效于 {@link MimeTypeUtils#APPLICATION_JSON}.
     */
    public static final String APPLICATION_JSON_VALUE = "application/json";
    /**
     * {@code application/octet-stream}.
     */
    public static final MimeType APPLICATION_OCTET_STREAM;
    /**
     * 字符串等效于 {@link MimeTypeUtils#APPLICATION_OCTET_STREAM}.
     */
    public static final String APPLICATION_OCTET_STREAM_VALUE = "application/octet-stream";
    /**
     * {@code application/xml}.
     */
    public static final MimeType APPLICATION_XML;
    /**
     * 字符串等效于 {@link MimeTypeUtils#APPLICATION_XML}.
     */
    public static final String APPLICATION_XML_VALUE = "application/xml";
    /**
     * {@code image/gif}.
     */
    public static final MimeType IMAGE_GIF;
    /**
     * 字符串等效于 {@link MimeTypeUtils#IMAGE_GIF}.
     */
    public static final String IMAGE_GIF_VALUE = "image/gif";
    /**
     * {@code image/jpeg}.
     */
    public static final MimeType IMAGE_JPEG;
    /**
     * 字符串等效于 {@link MimeTypeUtils#IMAGE_JPEG}.
     */
    public static final String IMAGE_JPEG_VALUE = "image/jpeg";
    /**
     * {@code image/png}.
     */
    public static final MimeType IMAGE_PNG;
    /**
     * 字符串等效于 {@link MimeTypeUtils#IMAGE_PNG}.
     */
    public static final String IMAGE_PNG_VALUE = "image/png";
    /**
     * {@code text/html}.
     */
    public static final MimeType TEXT_HTML;
    /**
     * 字符串等效于 {@link MimeTypeUtils#TEXT_HTML}.
     */
    public static final String TEXT_HTML_VALUE = "text/html";
    /**
     * {@code text/plain}.
     */
    public static final MimeType TEXT_PLAIN;
    /**
     * 字符串等效于 {@link MimeTypeUtils#TEXT_PLAIN}.
     */
    public static final String TEXT_PLAIN_VALUE = "text/plain";
    /**
     * {@code text/xml}.
     */
    public static final MimeType TEXT_XML;
    /**
     * 字符串等效于 {@link MimeTypeUtils#TEXT_XML}.
     */
    public static final String TEXT_XML_VALUE = "text/xml";
    private static final byte[] BOUNDARY_CHARS =
            new byte[]{'-', '_', '1', '2', '3', '4', '5', '6', '7', '8', '9', '0', 'a', 'b', 'c', 'd', 'e', 'f', 'g',
                    'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'A',
                    'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U',
                    'V', 'W', 'X', 'Y', 'Z'};
    private static final ConcurrentLruCache<String, MimeType> cachedMimeTypes =
            new ConcurrentLruCache<>(64, MimeTypeUtils::parseMimeTypeInternal);
    
    @Nullable
    private static volatile Random random;
    
    static {
        ALL = new MimeType("*", "*");
        APPLICATION_GRAPHQL = new MimeType("application", "graphql+json");
        APPLICATION_JSON = new MimeType("application", "json");
        APPLICATION_OCTET_STREAM = new MimeType("application", "octet-stream");
        APPLICATION_XML = new MimeType("application", "xml");
        IMAGE_GIF = new MimeType("image", "gif");
        IMAGE_JPEG = new MimeType("image", "jpeg");
        IMAGE_PNG = new MimeType("image", "png");
        TEXT_HTML = new MimeType("text", "html");
        TEXT_PLAIN = new MimeType("text", "plain");
        TEXT_XML = new MimeType("text", "xml");
    }
    
    
    /**
     * 将给定的字符串解析为单个 {@code MimeType}
     * <p>
     * 最近分析的 {@code MimeType} 将被缓存以供进一步检索。
     *
     * @param mimeType 要分析的字符串
     * @return {@link MimeType}
     * @throws InvalidMimeTypeException 如果无法解析字符串
     */
    public static MimeType parseMimeType(String mimeType) {
        if (StrUtil.isEmpty(mimeType)) {
            throw new InvalidMimeTypeException(mimeType, "'mimeType' 不得为空");
        }
        // 不要缓存具有随机边界的多部分 MIME 类型
        if (mimeType.startsWith("multipart")) {
            return parseMimeTypeInternal(mimeType);
        }
        return cachedMimeTypes.get(mimeType);
    }
    
    @Contract("_ -> new")
    private static @NotNull MimeType parseMimeTypeInternal(@NotNull String mimeType) {
        int index = mimeType.indexOf(';');
        String fullType = (index >= 0 ? mimeType.substring(0, index) : mimeType).trim();
        if (fullType.isEmpty()) {
            throw new InvalidMimeTypeException(mimeType, "'mimeType' 不得为空");
        }
        
        // java.net.HttpURLConnection 返回 a *; q=.2 Accept 标头
        if (MimeType.WILDCARD_TYPE.equals(fullType)) {
            fullType = "*/*";
        }
        int subIndex = fullType.indexOf('/');
        if (subIndex == -1) {
            throw new InvalidMimeTypeException(mimeType, "不包含 '/'");
        }
        if (subIndex == fullType.length() - 1) {
            throw new InvalidMimeTypeException(mimeType, "不包含之后的子类型 '/'");
        }
        String type = fullType.substring(0, subIndex);
        String subtype = fullType.substring(subIndex + 1);
        if (MimeType.WILDCARD_TYPE.equals(type) && !MimeType.WILDCARD_TYPE.equals(subtype)) {
            throw new InvalidMimeTypeException(mimeType, "通配符类型仅在 '*/*' (所有 mime types)");
        }
        
        Map<String, String> parameters = null;
        do {
            int nextIndex = index + 1;
            boolean quoted = false;
            while (nextIndex < mimeType.length()) {
                char ch = mimeType.charAt(nextIndex);
                if (ch == ';') {
                    if (!quoted) {
                        break;
                    }
                } else if (ch == '"') {
                    quoted = !quoted;
                }
                nextIndex++;
            }
            String parameter = mimeType.substring(index + 1, nextIndex).trim();
            if (parameter.length() > 0) {
                if (parameters == null) {
                    parameters = new LinkedHashMap<>(4);
                }
                int eqIndex = parameter.indexOf('=');
                if (eqIndex >= 0) {
                    String attribute = parameter.substring(0, eqIndex).trim();
                    String value = parameter.substring(eqIndex + 1).trim();
                    parameters.put(attribute, value);
                }
            }
            index = nextIndex;
        }
        while (index < mimeType.length());
        
        try {
            return new MimeType(type, subtype, parameters);
        } catch (UnsupportedCharsetException ex) {
            throw new InvalidMimeTypeException(mimeType, "不支持的字符集 '" + ex.getCharsetName() + "'");
        } catch (IllegalArgumentException ex) {
            throw new InvalidMimeTypeException(mimeType, ex.getMessage());
        }
    }
    
    /**
     * 将逗号分隔的字符串解析为 {@code MimeType} 对象列表
     *
     * @param mimeTypes 要分析的字符串
     * @return {@link MimeType} 列表
     * @throws InvalidMimeTypeException 如果无法解析字符串
     */
    public static List<MimeType> parseMimeTypes(String mimeTypes) {
        if (StrUtil.isEmpty(mimeTypes)) {
            return Collections.emptyList();
        }
        return tokenize(mimeTypes).stream()
                .filter(StrUtil::isNotBlank)
                .map(MimeTypeUtils::parseMimeType)
                .collect(Collectors.toList());
    }
    
    /**
     * 将给定的逗号分隔字符串 {@code MimeType} 对象标记为 {@code List<String>}。与通过“，”进行简单的标记化不同，此方法考虑了带引号的参数。
     *
     * @param mimeTypes 要标记化的字符串
     * @return 令牌列表
     */
    public static List<String> tokenize(String mimeTypes) {
        if (StrUtil.isEmpty(mimeTypes)) {
            return Collections.emptyList();
        }
        List<String> tokens = new ArrayList<>();
        boolean inQuotes = false;
        int startIndex = 0;
        int i = 0;
        while (i < mimeTypes.length()) {
            switch (mimeTypes.charAt(i)) {
                case '"':
                    inQuotes = !inQuotes;
                    break;
                case ',':
                    if (!inQuotes) {
                        tokens.add(mimeTypes.substring(startIndex, i));
                        startIndex = i + 1;
                    }
                    break;
                case '\\':
                    i++;
                    break;
            }
            i++;
        }
        tokens.add(mimeTypes.substring(startIndex));
        return tokens;
    }
    
    /**
     * 返回给定的 {@code MimeType} 对象列表的字符串表示形式
     *
     * @param mimeTypes 要分析的字符串
     */
    public static @NotNull String toString(@NotNull Collection<? extends MimeType> mimeTypes) {
        StringBuilder builder = new StringBuilder();
        for (Iterator<? extends MimeType> iterator = mimeTypes.iterator(); iterator.hasNext(); ) {
            MimeType mimeType = iterator.next();
            mimeType.appendTo(builder);
            if (iterator.hasNext()) {
                builder.append(", ");
            }
        }
        return builder.toString();
    }
    
    /**
     * 延迟初始化 {@link SecureRandom} 在 {@link #generateMultipartBoundary()}.
     */
    private static Random initRandom() {
        Random randomToUse = random;
        if (randomToUse == null) {
            synchronized (MimeTypeUtils.class) {
                randomToUse = random;
                if (randomToUse == null) {
                    randomToUse = new SecureRandom();
                    random = randomToUse;
                }
            }
        }
        return randomToUse;
    }
    
    /**
     * 以字节形式生成随机 MIME 边界，通常用于多部分 MIME 类型
     */
    public static byte @NotNull [] generateMultipartBoundary() {
        Random randomToUse = initRandom();
        byte[] boundary = new byte[randomToUse.nextInt(11) + 30];
        for (int i = 0; i < boundary.length; i++) {
            boundary[i] = BOUNDARY_CHARS[randomToUse.nextInt(BOUNDARY_CHARS.length)];
        }
        return boundary;
    }
    
    /**
     * 生成随机 MIME 边界作为字符串，通常用于多部分 MIME 类型。
     */
    @Contract(" -> new")
    public static @NotNull String generateMultipartBoundaryString() {
        return new String(generateMultipartBoundary(), StandardCharsets.US_ASCII);
    }
    
}
