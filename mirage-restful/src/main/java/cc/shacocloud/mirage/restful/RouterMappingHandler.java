package cc.shacocloud.mirage.restful;

import cc.shacocloud.mirage.utils.comparator.Ordered;
import io.vertx.core.Future;
import io.vertx.ext.web.Router;

/**
 * 路由映射处理器
 */
public interface RouterMappingHandler extends Ordered {
    
    /**
     * 处理器初始化
     */
    default void init() {
    }
    
    /**
     * 绑定路由映射
     *
     * @param router 路由
     * @return 考虑到路由初始化过程可能是异步的所以使用 {@link Future} 作为返回值
     */
    Future<Void> bindRouterMapping(Router router);
    
    @Override
    default int getOrder() {
        return 0;
    }
}
