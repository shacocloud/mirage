package cc.shacocloud.mirage.restful.bind;

import io.vertx.ext.web.handler.BodyHandler;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 请求体处理配置属性
 */
@Getter
@Setter
@NoArgsConstructor
public class BodyHandlerOptions {
    
    public static final BodyHandlerOptions DEFAULT = new BodyHandlerOptions();
    
    /**
     * 请求主体的默认最大大小。-1 意味着无限的
     */
    private long bodyLimit = BodyHandler.DEFAULT_BODY_LIMIT;
    
    /**
     * 如果需要处理文件上传，则为真
     */
    private boolean handleFileUploads = true;
    
    /**
     * 文件上传目录，为系统运行的的相对路径
     */
    private String uploadsDir = BodyHandler.DEFAULT_UPLOADS_DIRECTORY;
    
    /**
     * 表单属性是否应合并到请求参数中
     */
    private boolean mergeFormAttributes = false;
    
    /**
     * 处理请求后是否删除上传的文件
     */
    private boolean deleteUploadedFilesOnEnd = true;
    
    /**
     * 是否根据 HTTP 请求头的内容长度预先分配主体缓冲区大小
     */
    private boolean isPreallocateBodyBuffer = BodyHandler.DEFAULT_PREALLOCATE_BODY_BUFFER;
    
}
