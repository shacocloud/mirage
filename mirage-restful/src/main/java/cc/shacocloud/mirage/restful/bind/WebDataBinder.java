package cc.shacocloud.mirage.restful.bind;

import cc.shacocloud.mirage.restful.bind.validation.DataBinder;
import org.jetbrains.annotations.Nullable;

/**
 * 用于从web请求参数到JavaBean对象的数据绑定器
 */
public class WebDataBinder extends DataBinder {
    
    /**
     * 创建一个新的WebDataBinder实例。
     *
     * @param target 要绑定到的目标对象(如果绑定器只是用于转换普通参数值，则为{@code null})
     */
    public WebDataBinder(@Nullable Object target) {
        super(target);
    }
    
    /**
     * 创建一个新的WebDataBinder实例。
     *
     * @param target     要绑定到的目标对象(如果绑定器只是用于转换普通参数值，则为{@code null})
     * @param objectName 目标对象的名称
     */
    public WebDataBinder(@Nullable Object target, String objectName) {
        super(target, objectName);
    }
    
}
