package cc.shacocloud.mirage.restful.exception;

import cc.shacocloud.mirage.restful.http.MediaType;

/**
 * 在遇到无效的媒体类型规范字符串时，从{@link MediaType#parseMediaType(String)}抛出异常。
 */
public class InvalidMediaTypeException extends IllegalArgumentException {
    
    private final String mediaType;
    
    
    /**
     * 为给定的媒体类型创建一个新的{@link InvalidMediaTypeException}
     *
     * @param mediaType 媒体类型
     * @param message   详细消息
     */
    public InvalidMediaTypeException(String mediaType, String message) {
        super("无效的媒体类型  \"" + mediaType + "\": " + message);
        this.mediaType = mediaType;
    }
    
    /**
     * 允许包装{@link InvalidMimeTypeException}的构造函数。
     */
    public InvalidMediaTypeException(InvalidMimeTypeException ex) {
        super(ex.getMessage(), ex);
        this.mediaType = ex.getMimeType();
    }
    
    
    /**
     * 返回有问题的媒体类型。
     */
    public String getMediaType() {
        return this.mediaType;
    }
    
}
