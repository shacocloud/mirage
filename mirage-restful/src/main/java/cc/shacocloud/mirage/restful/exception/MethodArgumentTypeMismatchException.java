package cc.shacocloud.mirage.restful.exception;

import cc.shacocloud.mirage.utils.MethodParameter;
import cc.shacocloud.mirage.utils.converter.TypeMismatchException;
import org.jetbrains.annotations.Nullable;

/**
 * 解析控制器方法参数时引发的{@link TypeMismatchException}异常。
 * <p>
 * 提供对目标{@link MethodParameter}的访.
 */
public class MethodArgumentTypeMismatchException extends TypeMismatchException {
    
    private final String name;
    
    private final MethodParameter parameter;
    
    public MethodArgumentTypeMismatchException(@Nullable Object value,
                                               @Nullable Class<?> requiredType,
                                               String name,
                                               MethodParameter param,
                                               Throwable cause) {
        
        super(value, requiredType, cause);
        this.name = name;
        this.parameter = param;
    }
    
    
    /**
     * 返回方法参数的名称。
     */
    public String getName() {
        return this.name;
    }
    
    /**
     * 返回目标方法参数。
     */
    public MethodParameter getParameter() {
        return this.parameter;
    }
    
}
