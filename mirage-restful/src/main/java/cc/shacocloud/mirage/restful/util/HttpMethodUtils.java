package cc.shacocloud.mirage.restful.util;

import io.vertx.core.http.HttpMethod;
import org.jetbrains.annotations.Nullable;

import java.util.HashMap;
import java.util.Map;

/**
 * vertx的{@link HttpMethod}对象的工具拓展
 */
public class HttpMethodUtils {
    
    private static final Map<String, HttpMethod> mappings = new HashMap<>(16);
    
    static {
        for (HttpMethod httpMethod : HttpMethod.values()) {
            mappings.put(httpMethod.name(), httpMethod);
        }
    }
    
    /**
     * 将给定的方法值解析为 {@code HttpMethod}。
     *
     * @param method 方法值作为字符串
     * @return 对应的{@code HttpMethod} 如果没有找到返回{@code null}
     */
    @Nullable
    public static HttpMethod resolve(@Nullable String method) {
        return (method != null ? mappings.get(method) : null);
    }
    
    
    /**
     * 确定这个{@code HttpMethod}是否与给定的方法值匹配。
     *
     * @param httpMethod 等待匹配的{@link HttpMethod}
     * @param method     方法值作为字符串
     * @return 如果匹配{@code true}，否则{@code false}
     */
    public static boolean matches(HttpMethod httpMethod, String method) {
        return httpMethod == resolve(method);
    }
    
    
}
