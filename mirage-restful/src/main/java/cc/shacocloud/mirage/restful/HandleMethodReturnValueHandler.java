package cc.shacocloud.mirage.restful;

import cc.shacocloud.mirage.utils.MethodParameter;
import io.vertx.core.Future;
import io.vertx.core.buffer.Buffer;

/**
 * 处理方法返回值处理程序
 */
public interface HandleMethodReturnValueHandler {
    
    /**
     * 是否支持该处理该结果值
     *
     * @param returnType 结果类型
     * @return 如果支持返回 {@code true} 反之为 {@code false}
     */
    boolean supportsReturnType(MethodParameter returnType);
    
    /**
     * 处理结果值
     *
     * @param response    响应体
     * @param returnType  结果类型
     * @param returnValue 结果值
     * @return {@link Future} 方法的执行解析后的结果
     */
    Future<Buffer> handleReturnValue(HttpResponse response, MethodParameter returnType, Object returnValue);
    
}
