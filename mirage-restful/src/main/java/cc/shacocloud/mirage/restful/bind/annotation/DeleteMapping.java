package cc.shacocloud.mirage.restful.bind.annotation;

import cc.shacocloud.mirage.utils.annotation.AliasFor;

import java.lang.annotation.*;

/**
 * 将 HTTP {@code DELETE}请求映射到特定处理程序方法的注解
 *
 * @author 思追(shaco)
 * @see RequestMapping
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
@RequestMapping(method = HttpMethod.DELETE)
public @interface DeleteMapping {
    
    @AliasFor(annotation = RequestMapping.class)
    PathPatterns patterns() default PathPatterns.PATH;
    
    @AliasFor(annotation = RequestMapping.class)
    String[] value() default {};
    
    @AliasFor(annotation = RequestMapping.class)
    String[] path() default {};
    
    @AliasFor(annotation = RequestMapping.class)
    String[] consumes() default {};
    
    @AliasFor(annotation = RequestMapping.class)
    String[] produces() default {};
    
}
