package cc.shacocloud.mirage.restful.bind;

import cc.shacocloud.mirage.restful.HttpRequest;
import org.jetbrains.annotations.Nullable;

/**
 * 用于为已命名的目标对象创建{@link WebDataBinder}实例的工厂
 */
public interface WebDataBinderFactory {
    
    /**
     * 为给定对象创建{@link WebDataBinder}
     *
     * @param request    当前请求
     * @param target     要为其创建数据绑定的对象，如果为简单类型创建绑定，则为{@code null}
     * @param objectName 目标对象的名称
     * @return 创建的 {@link WebDataBinder} 实例，决不为空
     */
    WebDataBinder createBinder(HttpRequest request, @Nullable Object target, String objectName);
    
}
