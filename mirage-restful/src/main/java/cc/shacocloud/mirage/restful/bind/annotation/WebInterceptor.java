package cc.shacocloud.mirage.restful.bind.annotation;

import cc.shacocloud.mirage.bean.bind.Component;
import cc.shacocloud.mirage.restful.HandlerInterceptor;
import cc.shacocloud.mirage.restful.util.PathUtil;
import cc.shacocloud.mirage.utils.PathMatcher;
import cc.shacocloud.mirage.utils.annotation.AliasFor;
import cc.shacocloud.mirage.utils.comparator.AnnotationOrderComparator;
import cc.shacocloud.mirage.utils.comparator.Ordered;

import java.lang.annotation.*;

/**
 * web 拦截器类注解，定义的类必须是 {@link HandlerInterceptor} 的实现类
 * <p>
 * 使用 AntPathMatcher来实现，使用以下规则匹配路径：
 * <ul>
 *     <li>? 匹配一个字符</li>
 *     <li>* 匹配零个或多个字符</li>
 *     <li>** 匹配路径中的零个或多个层级</li>
 * </ul>
 * 一些示例：
 * <ul>
 *     <li>@WebInterceptor(includePatterns="/api/*") 匹配所有以/api开头，但只有2层级的路径</li>
 *     <li>@WebInterceptor(includePatterns="/api/**") 匹配所有以/api开头，不限制层级的路径</li>
 *     <li>@WebInterceptor(includePatterns="/api/t?st") 匹配/api/test也匹配/api/tast</li>
 *     <li>@WebInterceptor(includePatterns="/api/*",excludePatterns="/api/test") 匹配所有以/api开头，但只有2层级的路径，排除/api/test</li>
 * </ul>
 *
 * @author 思追(shaco)
 * @see RequestMapping
 */
@Target({ElementType.TYPE, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Component
public @interface WebInterceptor {
    
    /**
     * 拦截器名称，默认情况下使用容器中的对象名称
     */
    @AliasFor(annotation = Component.class, value = "value")
    String interceptorName() default "";
    
    /**
     * 拦截路径，如果不设置默认全匹配
     *
     * @see PathMatcher
     * @see PathUtil#DEFAULT_PATH_MATCHER
     */
    String[] includePatterns() default {};
    
    /**
     * 排除路径
     *
     * @see PathMatcher
     * @see PathUtil#DEFAULT_PATH_MATCHER
     */
    String[] excludePatterns() default {};
    
    /**
     * 排序字段，根据该值对拦截器链进行排序
     *
     * @see AnnotationOrderComparator
     */
    int order() default Ordered.LOWEST_PRECEDENCE;
    
}
