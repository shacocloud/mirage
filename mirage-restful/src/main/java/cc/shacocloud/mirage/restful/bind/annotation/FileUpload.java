package cc.shacocloud.mirage.restful.bind.annotation;

import cc.shacocloud.mirage.restful.bind.support.MultipartFileUploadArgumentResolver;
import cc.shacocloud.mirage.restful.http.MultipartFileUpload;

import java.lang.annotation.*;

/**
 * 指方法参数的注解应该绑定到web请求的文件。
 * <p>
 * 与 {@link MultipartFileUpload} 组合使用，用于处理文件上传请求
 *
 * @see MultipartFileUploadArgumentResolver
 * @see MultipartFileUpload
 */
@Target(ElementType.PARAMETER)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface FileUpload {

}
