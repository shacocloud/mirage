package cc.shacocloud.mirage.restful.bind.annotation;

import cc.shacocloud.mirage.restful.bind.ValueConstants;
import cc.shacocloud.mirage.restful.bind.support.RequestHeaderMethodArgumentResolver;
import cc.shacocloud.mirage.utils.annotation.AliasFor;

import java.lang.annotation.*;

/**
 * 指示方法参数应绑定到HTTP请求头的注解
 *
 * @see RequestMapping
 * @see RequestHeaderMethodArgumentResolver
 * @see io.vertx.core.http.HttpHeaders
 */
@Target(ElementType.PARAMETER)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface RequestHeader {
    
    /**
     * {@link #name}的别名
     */
    @AliasFor("name")
    String value() default "";
    
    /**
     * 要绑定到的表单属性的名称
     */
    @AliasFor("value")
    String name() default "";
    
    /**
     * 参数是否必需
     */
    boolean required() default true;
    
    /**
     * 当请求参数未提供或有空值时，用作回退的默认值。
     * <p>
     * 提供默认值将隐式的设置{@link #required}为{@code false}。
     */
    String defaultValue() default ValueConstants.DEFAULT_NONE;
}
