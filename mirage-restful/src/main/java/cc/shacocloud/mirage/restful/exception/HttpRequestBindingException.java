package cc.shacocloud.mirage.restful.exception;

/**
 * 致命的绑定异常，当我们想要将绑定异常视为不可恢复时抛出
 */
public class HttpRequestBindingException extends RuntimeException {
    
    public HttpRequestBindingException(String msg) {
        super(msg);
    }
    
    public HttpRequestBindingException(String msg, Throwable cause) {
        super(msg, cause);
    }
}
