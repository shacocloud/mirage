package cc.shacocloud.mirage.restful.bind.validation.errors;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

/**
 * 默认的 {@link BindingResultError}
 *
 * @author 思追(shaco)
 */
@Setter
@Getter
@AllArgsConstructor
public class DefaultBindingResultError implements BindingResultError {
    
    /**
     * 错误代码
     */
    @NotNull
    private String code;
    
    /**
     * 错误字段的嵌套路径
     */
    @NotNull
    private String nestedPath;
    
    /**
     * 此错误失败的值
     */
    @Nullable
    private Object rejectedValue;
    
}
