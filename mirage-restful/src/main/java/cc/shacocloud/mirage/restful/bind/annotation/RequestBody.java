package cc.shacocloud.mirage.restful.bind.annotation;

import cc.shacocloud.mirage.restful.bind.support.RequestResponseBodyMethodProcessor;
import cc.shacocloud.mirage.restful.http.HttpMessageConverter;

import java.lang.annotation.*;


/**
 * 指方法参数的注解应该绑定到web请求的主体。
 * <p>
 * 请求的主体通过{@link HttpMessageConverter}来解析方法参数，具体取决于请求的内容类型。
 *
 * @see RequestResponseBodyMethodProcessor
 */
@Target({ElementType.PARAMETER, ElementType.ANNOTATION_TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface RequestBody {
    
    /**
     * 是否需要正文内容
     * <p>
     * 默认为{@code true}，在没有正文内容的情况下导致抛出异常。
     * 如果您希望在正文内容为{@code null}时传递{@code null}，请将其切换为{@code false}。
     */
    boolean required() default true;
    
}
