package cc.shacocloud.mirage.restful.bind.converter.json;

import com.fasterxml.jackson.databind.ser.FilterProvider;
import org.jetbrains.annotations.Nullable;

/**
 * 一个用于通过{@link JacksonHttpMessageConverter}进行序列化的POJO的简单占位符，以及将传递到转换器的进一步序列化指令。
 *
 * <p>
 * 在服务器端，在内容协商选择了要使用的转换器之后，在写入之前，这个包装器会添加一个 {@code ResponseBodyInterceptor}。
 *
 * <p>
 * 在客户端，只需包装POJO并将其传递给 {@code RestTemplate}。
 */
public class MappingJacksonValue {
    
    private Object value;
    
    @Nullable
    private Class<?> serializationView;
    
    @Nullable
    private FilterProvider filters;
    
    /**
     * 创建一个包装要序列化的给定POJO的新实例。
     *
     * @param value 要序列化的对象
     */
    public MappingJacksonValue(Object value) {
        this.value = value;
    }
    
    /**
     * 返回需要序列化的POJO。
     */
    public Object getValue() {
        return this.value;
    }
    
    /**
     * 将POJO修改为序列化。
     */
    public void setValue(Object value) {
        this.value = value;
    }
    
    /**
     * 返回要使用的序列化视图。
     *
     * @see com.fasterxml.jackson.databind.ObjectMapper#writerWithView(Class)
     * @see com.fasterxml.jackson.annotation.JsonView
     */
    @Nullable
    public Class<?> getSerializationView() {
        return this.serializationView;
    }
    
    /**
     * 设置序列化视图来序列化POJO。
     *
     * @see com.fasterxml.jackson.databind.ObjectMapper#writerWithView(Class)
     * @see com.fasterxml.jackson.annotation.JsonView
     */
    public void setSerializationView(@Nullable Class<?> serializationView) {
        this.serializationView = serializationView;
    }
    
    /**
     * 返回要使用的Jackson过滤器提供程序。
     *
     * @see com.fasterxml.jackson.databind.ObjectMapper#writer(FilterProvider)
     * @see com.fasterxml.jackson.annotation.JsonFilter
     */
    @Nullable
    public FilterProvider getFilters() {
        return this.filters;
    }
    
    /**
     * 设置Jackson过滤器提供程序来序列化POJO。
     *
     * @see com.fasterxml.jackson.databind.ObjectMapper#writer(FilterProvider)
     * @see com.fasterxml.jackson.annotation.JsonFilter
     * @see Jackson2ObjectMapperBuilder#filters(FilterProvider)
     */
    public void setFilters(@Nullable FilterProvider filters) {
        this.filters = filters;
    }
    
}
