package cc.shacocloud.mirage.restful.http;

import cc.shacocloud.mirage.utils.Utils;
import cc.shacocloud.mirage.utils.charSequence.StrUtil;
import cc.shacocloud.mirage.utils.map.LinkedCaseInsensitiveMap;
import cc.shacocloud.mirage.utils.map.MapUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.Serializable;
import java.nio.charset.Charset;
import java.util.*;

/**
 * 表示最初在 RFC 2046 中定义的 MIME 类型，随后在其他互联网协议（包括 HTTP）中使用。
 *
 * <p>
 * 但是，此类不包含对 HTTP 内容协商中使用的 q 参数的支持
 */
public class MimeType implements Comparable<MimeType>, Serializable {
    
    protected static final String WILDCARD_TYPE = "*";
    private static final long serialVersionUID = 4085923477777865903L;
    private static final String PARAM_CHARSET = "charset";
    
    private static final BitSet TOKEN;
    
    static {
        // variable names refer to RFC 2616, section 2.2
        BitSet ctl = new BitSet(128);
        for (int i = 0; i <= 31; i++) {
            ctl.set(i);
        }
        ctl.set(127);
        
        BitSet separators = new BitSet(128);
        separators.set('(');
        separators.set(')');
        separators.set('<');
        separators.set('>');
        separators.set('@');
        separators.set(',');
        separators.set(';');
        separators.set(':');
        separators.set('\\');
        separators.set('\"');
        separators.set('/');
        separators.set('[');
        separators.set(']');
        separators.set('?');
        separators.set('=');
        separators.set('{');
        separators.set('}');
        separators.set(' ');
        separators.set('\t');
        
        TOKEN = new BitSet(128);
        TOKEN.set(0, 128);
        TOKEN.andNot(ctl);
        TOKEN.andNot(separators);
    }
    
    
    private final String type;
    
    private final String subtype;
    
    private final Map<String, String> parameters;
    
    @Nullable
    private transient Charset resolvedCharset;
    
    @Nullable
    private volatile String toStringValue;
    
    
    /**
     * 为给定的主要类型创建新的 {@code MimeType}
     *
     * @param type 主要类型
     * @throws IllegalArgumentException 如果任何参数包含非法字符
     */
    public MimeType(String type) {
        this(type, WILDCARD_TYPE);
    }
    
    /**
     * 为给定的主要类型和子类型创建新的 {@code MimeType}
     * <p>
     * 参数为空
     *
     * @param type    主要类型
     * @param subtype 子类型
     * @throws IllegalArgumentException 如果任何参数包含非法字符
     */
    public MimeType(String type, String subtype) {
        this(type, subtype, Collections.emptyMap());
    }
    
    /**
     * 为给定类型、子类型和字符集创建新的 {@code MimeType}
     *
     * @param type    主要类型
     * @param subtype 子类型
     * @param charset 字符集
     * @throws IllegalArgumentException 如果任何参数包含非法字符
     */
    public MimeType(String type, String subtype, @NotNull Charset charset) {
        this(type, subtype, Collections.singletonMap(PARAM_CHARSET, charset.name()));
        this.resolvedCharset = charset;
    }
    
    /**
     * 复制构造函数，复制给定 {@code MimeType} 的类型、子类型、参数，并允许设置指定的字符集。
     *
     * @param other   另一个 {@code MimeType}
     * @param charset 字符集
     * @throws IllegalArgumentException 如果任何参数包含非法字符
     */
    public MimeType(@NotNull MimeType other, @NotNull Charset charset) {
        this(other.getType(), other.getSubtype(), addCharsetParameter(charset, other.getParameters()));
        this.resolvedCharset = charset;
    }
    
    /**
     * 复制构造函数，用于复制给定 {@code MimeType} 的类型和子类型，并允许不同的参数
     *
     * @param other      另一个 {@code MimeType}
     * @param parameters 参数（可以是 {@code 空}）
     * @throws IllegalArgumentException 如果任何参数包含非法字符
     */
    public MimeType(@NotNull MimeType other, @Nullable Map<String, String> parameters) {
        this(other.getType(), other.getSubtype(), parameters);
    }
    
    /**
     * 为给定类型、子类型和参数创建新的 {@code MimeType}
     *
     * @param type       主要类型
     * @param subtype    子类型
     * @param parameters 参数（可以是 {@code 空}）
     * @throws IllegalArgumentException 如果任何参数包含非法字符
     */
    public MimeType(String type, String subtype, @Nullable Map<String, String> parameters) {
        if (StrUtil.isEmpty(type)) {
            throw new IllegalArgumentException("type 不得为空");
        }
        if (StrUtil.isEmpty(subtype)) {
            throw new IllegalArgumentException("subtype 不得为空");
        }
        
        checkToken(type);
        checkToken(subtype);
        this.type = type.toLowerCase(Locale.ENGLISH);
        this.subtype = subtype.toLowerCase(Locale.ENGLISH);
        if (MapUtil.isNotEmpty(parameters)) {
            Map<String, String> map = new LinkedCaseInsensitiveMap<>(parameters.size(), Locale.ENGLISH);
            parameters.forEach((parameter, value) -> {
                checkParameters(parameter, value);
                map.put(parameter, value);
            });
            this.parameters = Collections.unmodifiableMap(map);
        } else {
            this.parameters = Collections.emptyMap();
        }
    }
    
    /**
     * 复制构造函数，它复制给定 {@code MimeType} 的类型、子类型和参数，跳过在其他构造函数中执行的检查
     */
    protected MimeType(MimeType other) {
        this.type = other.type;
        this.subtype = other.subtype;
        this.parameters = other.parameters;
        this.resolvedCharset = other.resolvedCharset;
        this.toStringValue = other.toStringValue;
    }
    
    private static @NotNull Map<String, String> addCharsetParameter(@NotNull Charset charset, Map<String, String> parameters) {
        Map<String, String> map = new LinkedHashMap<>(parameters);
        map.put(PARAM_CHARSET, charset.name());
        return map;
    }
    
    /**
     * 检查给定令牌字符串中是否存在非法字符，如 RFC 2616 第 2.2 节中所定义
     *
     * @throws IllegalArgumentException 在非法字符的情况下
     * @see <a href="https://tools.ietf.org/html/rfc2616#section-2.2">HTTP 1.1, section 2.2</a>
     */
    private void checkToken(@NotNull String token) {
        for (int i = 0; i < token.length(); i++) {
            char ch = token.charAt(i);
            if (!TOKEN.get(ch)) {
                throw new IllegalArgumentException("令牌字符无效 '" + ch + "' 在令牌中  \"" + token + "\"");
            }
        }
    }
    
    protected void checkParameters(String parameter, String value) {
        if (StrUtil.isEmpty(parameter)) {
            throw new IllegalArgumentException("parameter 不得为空");
        }
        if (StrUtil.isEmpty(value)) {
            throw new IllegalArgumentException("value 不得为空");
        }
        checkToken(parameter);
        if (PARAM_CHARSET.equals(parameter)) {
            if (this.resolvedCharset == null) {
                this.resolvedCharset = Charset.forName(unquote(value));
            }
        } else if (!isQuotedString(value)) {
            checkToken(value);
        }
    }
    
    private boolean isQuotedString(@NotNull String s) {
        if (s.length() < 2) {
            return false;
        } else {
            return ((s.startsWith("\"") && s.endsWith("\"")) || (s.startsWith("'") && s.endsWith("'")));
        }
    }
    
    protected String unquote(String s) {
        return (isQuotedString(s) ? s.substring(1, s.length() - 1) : s);
    }
    
    /**
     * 指示 {@linkplain #getType} 是否为通配符
     */
    public boolean isWildcardType() {
        return WILDCARD_TYPE.equals(getType());
    }
    
    /**
     * 指示 {@linkplain #getSubtype} 是否为通配符，还是通配符后跟后缀
     *
     * @return 子类型是否为通配符
     */
    public boolean isWildcardSubtype() {
        return WILDCARD_TYPE.equals(getSubtype()) || getSubtype().startsWith("*+");
    }
    
    /**
     * 指示此 MIME 类型是否具体，即类型和子类型是否不是通配符
     *
     * @return 此 MIME 类型是否具体
     */
    public boolean isConcrete() {
        return !isWildcardType() && !isWildcardSubtype();
    }
    
    /**
     * 返回主要类型
     */
    public String getType() {
        return this.type;
    }
    
    /**
     * 返回子类型
     */
    public String getSubtype() {
        return this.subtype;
    }
    
    /**
     * 返回 RFC 6839 中定义的子类型后缀
     */
    @Nullable
    public String getSubtypeSuffix() {
        int suffixIndex = this.subtype.lastIndexOf('+');
        if (suffixIndex != -1) {
            return this.subtype.substring(suffixIndex + 1);
        }
        return null;
    }
    
    /**
     * 返回字符集，如 {@code 字符集} 参数（如果有）所示
     *
     * @return 字符集，如果不可用，则为 {@code null}
     */
    @Nullable
    public Charset getCharset() {
        return this.resolvedCharset;
    }
    
    /**
     * 返回给定参数名称的泛型参数值
     *
     * @param name 参数名称
     * @return 参数值，如果不存在，则为 {@code null}
     */
    @Nullable
    public String getParameter(String name) {
        return this.parameters.get(name);
    }
    
    /**
     * 返回所有泛型参数值
     *
     * @return 只读映射（可能为空，从不 {@code null}）
     */
    public Map<String, String> getParameters() {
        return this.parameters;
    }
    
    /**
     * 指示此 MIME 类型是否包括给定的 MIME 类型
     * <p>
     * 例如
     * {@code text/*} 包含 {@code text/plain} 和 {@code text/html} <br/>
     * {@code application/*+xml} 包含 {@code application/soap+xml} 等等
     * <p>
     * 注意：此方法不是对称的
     *
     * @param other 要与之比较的参考 MIME 类型
     * @return 如果此 MIME 类型包含给定的 MIME 类型则为 {@code true}
     */
    public boolean includes(@Nullable MimeType other) {
        if (other == null) {
            return false;
        }
        if (isWildcardType()) {
            // */* 包括任何内容
            return true;
        } else if (getType().equals(other.getType())) {
            if (getSubtype().equals(other.getSubtype())) {
                return true;
            }
            if (isWildcardSubtype()) {
                // 带后缀的通配符，例如 application/*+xml
                int thisPlusIdx = getSubtype().lastIndexOf('+');
                if (thisPlusIdx == -1) {
                    return true;
                } else {
                    // application/*+xml 包含 application/soap+xml
                    int otherPlusIdx = other.getSubtype().lastIndexOf('+');
                    if (otherPlusIdx != -1) {
                        String thisSubtypeNoSuffix = getSubtype().substring(0, thisPlusIdx);
                        String thisSubtypeSuffix = getSubtype().substring(thisPlusIdx + 1);
                        String otherSubtypeSuffix = other.getSubtype().substring(otherPlusIdx + 1);
                        return thisSubtypeSuffix.equals(otherSubtypeSuffix) && WILDCARD_TYPE.equals(thisSubtypeNoSuffix);
                    }
                }
            }
        }
        return false;
    }
    
    /**
     * 指示此 MIME 类型是否与给定的 MIME 类型兼容
     * <p>
     * 例如：
     * {@code text/*} 兼容 {@code text/plain}，{@code text/html}，反之亦然。实际上，此方法类似于 {@link #includes}，只是它是对称的。
     *
     * @param other 要与之比较的参考 MIME 类型
     * @return 如果此 MIME 类型与给定的 MIME 类型兼容 则为 {@code true}
     */
    public boolean isCompatibleWith(@Nullable MimeType other) {
        if (other == null) {
            return false;
        }
        if (isWildcardType() || other.isWildcardType()) {
            return true;
        } else if (getType().equals(other.getType())) {
            if (getSubtype().equals(other.getSubtype())) {
                return true;
            }
            if (isWildcardSubtype() || other.isWildcardSubtype()) {
                String thisSuffix = getSubtypeSuffix();
                String otherSuffix = other.getSubtypeSuffix();
                if (getSubtype().equals(WILDCARD_TYPE) || other.getSubtype().equals(WILDCARD_TYPE)) {
                    return true;
                } else if (isWildcardSubtype() && thisSuffix != null) {
                    return (thisSuffix.equals(other.getSubtype()) || thisSuffix.equals(otherSuffix));
                } else if (other.isWildcardSubtype() && otherSuffix != null) {
                    return (this.getSubtype().equals(otherSuffix) || otherSuffix.equals(thisSuffix));
                }
            }
        }
        return false;
    }
    
    /**
     * 类似于 {@link #equals}，但仅基于类型和子类型，即忽略参数。
     *
     * @param other 要比较的另一种 MIME 类型
     * @return 两种 MIME 类型是否具有相同的类型和子类型
     */
    public boolean equalsTypeAndSubtype(@Nullable MimeType other) {
        if (other == null) {
            return false;
        }
        return this.type.equalsIgnoreCase(other.type) && this.subtype.equalsIgnoreCase(other.subtype);
    }
    
    /**
     * 与依赖于 {@link MimeType#equals} 的 {@link Collection#contains} 不同，此方法仅检查类型和子类型，否则忽略参数。
     *
     * @param mimeTypes 要执行检查的 MIME 类型列表
     * @return 列表是否包含给定的 MIME 类型
     */
    public boolean isPresentIn(@NotNull Collection<? extends MimeType> mimeTypes) {
        for (MimeType mimeType : mimeTypes) {
            if (mimeType.equalsTypeAndSubtype(this)) {
                return true;
            }
        }
        return false;
    }
    
    @Override
    public boolean equals(@Nullable Object other) {
        if (this == other) {
            return true;
        }
        if (!(other instanceof MimeType)) {
            return false;
        }
        MimeType otherType = (MimeType) other;
        return (this.type.equalsIgnoreCase(otherType.type) &&
                this.subtype.equalsIgnoreCase(otherType.subtype) &&
                parametersAreEqual(otherType));
    }
    
    /**
     * 确定此 {@code MimeType} 中的参数和提供的 {@code MimeType} 中的参数是否相等，并对 {@link Charset}执行不区分大小写的比较。
     */
    private boolean parametersAreEqual(@NotNull MimeType other) {
        if (this.parameters.size() != other.parameters.size()) {
            return false;
        }
        
        for (Map.Entry<String, String> entry : this.parameters.entrySet()) {
            String key = entry.getKey();
            if (!other.parameters.containsKey(key)) {
                return false;
            }
            if (PARAM_CHARSET.equals(key)) {
                if (!Utils.nullSafeEquals(getCharset(), other.getCharset())) {
                    return false;
                }
            } else if (!Utils.nullSafeEquals(entry.getValue(), other.parameters.get(key))) {
                return false;
            }
        }
        
        return true;
    }
    
    @Override
    public int hashCode() {
        int result = this.type.hashCode();
        result = 31 * result + this.subtype.hashCode();
        result = 31 * result + this.parameters.hashCode();
        return result;
    }
    
    @Override
    public String toString() {
        String value = this.toStringValue;
        if (value == null) {
            StringBuilder builder = new StringBuilder();
            appendTo(builder);
            value = builder.toString();
            this.toStringValue = value;
        }
        return value;
    }
    
    protected void appendTo(@NotNull StringBuilder builder) {
        builder.append(this.type);
        builder.append('/');
        builder.append(this.subtype);
        appendTo(this.parameters, builder);
    }
    
    private void appendTo(@NotNull Map<String, String> map, StringBuilder builder) {
        map.forEach((key, val) -> {
            builder.append(';');
            builder.append(key);
            builder.append('=');
            builder.append(val);
        });
    }
    
    /**
     * 按字母顺序将此 MIME 类型与另一种 MIME 类型进行比较
     *
     * @param other 要比较的 MIME 类型
     */
    @Override
    public int compareTo(@NotNull MimeType other) {
        int comp = getType().compareToIgnoreCase(other.getType());
        if (comp != 0) {
            return comp;
        }
        comp = getSubtype().compareToIgnoreCase(other.getSubtype());
        if (comp != 0) {
            return comp;
        }
        comp = getParameters().size() - other.getParameters().size();
        if (comp != 0) {
            return comp;
        }
        
        TreeSet<String> thisAttributes = new TreeSet<>(String.CASE_INSENSITIVE_ORDER);
        thisAttributes.addAll(getParameters().keySet());
        TreeSet<String> otherAttributes = new TreeSet<>(String.CASE_INSENSITIVE_ORDER);
        otherAttributes.addAll(other.getParameters().keySet());
        Iterator<String> thisAttributesIterator = thisAttributes.iterator();
        Iterator<String> otherAttributesIterator = otherAttributes.iterator();
        
        while (thisAttributesIterator.hasNext()) {
            String thisAttribute = thisAttributesIterator.next();
            String otherAttribute = otherAttributesIterator.next();
            comp = thisAttribute.compareToIgnoreCase(otherAttribute);
            if (comp != 0) {
                return comp;
            }
            if (PARAM_CHARSET.equals(thisAttribute)) {
                Charset thisCharset = getCharset();
                Charset otherCharset = other.getCharset();
                if (thisCharset != otherCharset) {
                    if (thisCharset == null) {
                        return -1;
                    }
                    if (otherCharset == null) {
                        return 1;
                    }
                    comp = thisCharset.compareTo(otherCharset);
                    if (comp != 0) {
                        return comp;
                    }
                }
            } else {
                String thisValue = getParameters().get(thisAttribute);
                String otherValue = other.getParameters().get(otherAttribute);
                if (otherValue == null) {
                    otherValue = "";
                }
                comp = thisValue.compareTo(otherValue);
                if (comp != 0) {
                    return comp;
                }
            }
        }
        
        return 0;
    }
    
    private void readObject(@NotNull ObjectInputStream ois) throws IOException, ClassNotFoundException {
        // 依靠默认序列化，只需在反序列化后初始化状态即可
        ois.defaultReadObject();
        
        // 初始化瞬态字段
        String charsetName = getParameter(PARAM_CHARSET);
        if (charsetName != null) {
            this.resolvedCharset = Charset.forName(unquote(charsetName));
        }
    }
    
    /**
     * Comparator to 按特异性排序 {@link MimeType MimeTypes}
     */
    public static class SpecificityComparator<T extends MimeType> implements Comparator<T> {
        
        @Override
        public int compare(@NotNull T mimeType1, T mimeType2) {
            if (mimeType1.isWildcardType() && !mimeType2.isWildcardType()) {  // */* < audio/*
                return 1;
            } else if (mimeType2.isWildcardType() && !mimeType1.isWildcardType()) {  // audio/* > */*
                return -1;
            } else if (!mimeType1.getType().equals(mimeType2.getType())) {  // audio/basic == text/html
                return 0;
            } else {  // mediaType1.getType().equals(mediaType2.getType())
                if (mimeType1.isWildcardSubtype() && !mimeType2.isWildcardSubtype()) {  // audio/* < audio/basic
                    return 1;
                } else if (mimeType2.isWildcardSubtype() && !mimeType1.isWildcardSubtype()) {  // audio/basic > audio/*
                    return -1;
                } else if (!mimeType1.getSubtype().equals(mimeType2.getSubtype())) {  // audio/basic == audio/wave
                    return 0;
                } else {  // mediaType2.getSubtype().equals(mediaType2.getSubtype())
                    return compareParameters(mimeType1, mimeType2);
                }
            }
        }
        
        protected int compareParameters(@NotNull T mimeType1, @NotNull T mimeType2) {
            int paramsSize1 = mimeType1.getParameters().size();
            int paramsSize2 = mimeType2.getParameters().size();
            return Integer.compare(paramsSize2, paramsSize1);  // audio/basic;level=1 < audio/basic
        }
    }
    
}
