package cc.shacocloud.mirage.restful.exception;

import org.jetbrains.annotations.Nullable;

/**
 * http 请求绑定指定值时，值不存在并且没有设置默认值的情况下将抛出该例外
 *
 * @author 思追(shaco)
 */
public class HttpRequestBindMissingException extends HttpMessageConversionException {
    
    public HttpRequestBindMissingException(String msg) {
        super(msg);
    }
    
    public HttpRequestBindMissingException(String msg, @Nullable Throwable cause) {
        super(msg, cause);
    }
}
