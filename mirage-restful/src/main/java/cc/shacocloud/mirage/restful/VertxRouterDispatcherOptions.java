package cc.shacocloud.mirage.restful;

import cc.shacocloud.mirage.restful.bind.BodyHandlerOptions;
import cc.shacocloud.mirage.restful.bind.SessionHandlerOptions;
import io.vertx.core.http.HttpServerOptions;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.List;

/**
 * Vertx 路由器调度程序选项
 *
 * @author shaco
 */
@Slf4j
@Getter
@Setter
@NoArgsConstructor
public class VertxRouterDispatcherOptions {
    
    /**
     * http 服务配置参数
     */
    private HttpServerOptions serverOptions = new HttpServerOptions();
    
    /**
     * 消息体参数配置
     */
    private BodyHandlerOptions bodyOptions = BodyHandlerOptions.DEFAULT;
    
    /**
     * 会话处理器参数配置
     */
    private SessionHandlerOptions sessionOptions = SessionHandlerOptions.DEFAULT;
    
    /**
     * 路由映射处理器
     */
    private List<RouterMappingHandler> routerMappingHandlers = new ArrayList<>();
    
}
