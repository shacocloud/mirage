package cc.shacocloud.mirage.restful;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;

import java.util.Objects;

/**
 * 过滤器映射
 */
@Getter
@Builder
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class FilterMappingInfo {
    
    /**
     * 过滤器名称
     */
    private final String filterName;
    
    /**
     * 拦截路径，如果不设置默认全匹配
     */
    private final String[] includePatterns;
    
    /**
     * 排除路径
     */
    private final String[] excludePatterns;
    
    /**
     * 过滤器
     */
    private final Filter filter;
    
    /**
     * 排序字段
     */
    private final int order;
    
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        FilterMappingInfo that = (FilterMappingInfo) o;
        return Objects.equals(filterName, that.filterName);
    }
    
    @Override
    public int hashCode() {
        return Objects.hash(filterName);
    }
}
