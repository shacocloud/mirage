package cc.shacocloud.mirage.restful.exception;

import cc.shacocloud.mirage.restful.http.MediaType;

import java.util.List;


/**
 * 当请求处理程序不能生成客户端可接受的响应时抛出异常。
 */
@SuppressWarnings("serial")
public class HttpMediaTypeNotAcceptableException extends HttpMediaTypeException {
    
    /**
     * 创建一个新的HttpMediaTypeNotAcceptableException。
     *
     * @param message 异常消息
     */
    public HttpMediaTypeNotAcceptableException(String message) {
        super(message);
    }
    
    /**
     * 创建一个新的HttpMediaTypeNotSupportedException。
     *
     * @param supportedMediaTypes 支持的媒体类型列表
     */
    public HttpMediaTypeNotAcceptableException(List<MediaType> supportedMediaTypes) {
        super("找不到可接受的媒体列表", supportedMediaTypes);
    }
    
}
