package cc.shacocloud.mirage.restful;

import io.vertx.core.Future;


/**
 * 异常处理程序解析器，用于解决处理程序映射或执行期间抛出的异常。
 */
public interface HandlerExceptionResolver {
    
    // 表示当前异常无法处理，继续寻找下一个解析器
    Object SKIP = new Object();
    
    // 表示请求已经完成
    Object FINISH = new Object();
    
    /**
     * 尝试解决在处理程序执行过程中抛出的给定异常，
     * 异常解析的结果使用 {@link VertxInvokeHandler#resultHandle(Object, HandlerMethod, HttpResponse)} 方法处理结果。
     * <p>
     * 方法执行结束默认已经处理好了响应
     *
     * @param request  当前的请求
     * @param response 当前的响应
     * @param handler  当前执行的处理程序
     * @param cause    当前抛出的异常
     * @return {@link Future} 返回异常处理解析后的结果，如果返回的结果为 {@link #SKIP} 则表示当前异常无法处理，继续寻找下一个解析器，
     * 如果返回结果为 {@link #FINISH} 则表示请求已经完成
     */
    Future<Object> resolveException(HttpRequest request,
                                    HttpResponse response,
                                    VertxInvokeHandler handler,
                                    Throwable cause);
    
}
