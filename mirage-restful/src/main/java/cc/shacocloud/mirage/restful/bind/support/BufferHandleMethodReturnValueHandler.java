package cc.shacocloud.mirage.restful.bind.support;

import cc.shacocloud.mirage.restful.HandleMethodReturnValueHandler;
import cc.shacocloud.mirage.restful.HttpResponse;
import cc.shacocloud.mirage.utils.MethodParameter;
import io.vertx.core.Future;
import io.vertx.core.buffer.Buffer;
import org.jetbrains.annotations.NotNull;

/**
 * 处理 {@link Buffer} 的结果处理器
 */
public class BufferHandleMethodReturnValueHandler implements HandleMethodReturnValueHandler {
    
    @Override
    public boolean supportsReturnType(@NotNull MethodParameter returnType) {
        return Buffer.class.isAssignableFrom(returnType.getParameterType());
    }
    
    @Override
    public Future<Buffer> handleReturnValue(HttpResponse response, MethodParameter returnType, Object returnValue) {
        return Future.succeededFuture((Buffer) returnValue);
    }
}
