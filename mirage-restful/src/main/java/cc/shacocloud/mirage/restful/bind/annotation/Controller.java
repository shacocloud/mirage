package cc.shacocloud.mirage.restful.bind.annotation;

import cc.shacocloud.mirage.bean.bind.Component;
import cc.shacocloud.mirage.utils.annotation.AliasFor;

import java.lang.annotation.*;

/**
 * 控制器注解
 *
 * @author 思追(shaco)
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Component
public @interface Controller {
    
    @AliasFor(annotation = Component.class)
    String value() default "";
    
}
