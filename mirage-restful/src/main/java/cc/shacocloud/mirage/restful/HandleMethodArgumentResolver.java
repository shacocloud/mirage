package cc.shacocloud.mirage.restful;

import cc.shacocloud.mirage.restful.bind.WebDataBinderFactory;
import cc.shacocloud.mirage.utils.MethodParameter;
import io.vertx.core.Future;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

/**
 * 处理方法参数解析器
 */
public interface HandleMethodArgumentResolver {
    
    /**
     * 是否支持出来该方法参数
     *
     * @param parameter 参数
     * @return 如果支持返回 {@code true} 反之为 {@code false}
     */
    boolean supportsParameter(@NotNull MethodParameter parameter);
    
    /**
     * 解析参数
     */
    Future<Object> resolveArgument(@NotNull HttpRequest request,
                                   @NotNull MethodParameter parameter,
                                   @Nullable WebDataBinderFactory binderFactory);
    
}
