package cc.shacocloud.mirage.restful.exception;

import cc.shacocloud.mirage.restful.http.HttpMessageConverter;
import org.jetbrains.annotations.Nullable;

/**
 * 当{@link HttpMessageConverter#write}方法失败时，由{@link HttpMessageConverter}实现抛出。
 */
public class HttpMessageNotWritableException extends HttpMessageConversionException {
    
    /**
     * 创建一个新的{@link HttpMessageNotWritableException}
     *
     * @param msg 详细的消息
     */
    public HttpMessageNotWritableException(String msg) {
        super(msg);
    }
    
    /**
     * 创建一个新的{@link HttpMessageNotWritableException}
     *
     * @param msg   详细的消息
     * @param cause 根本原因(如果有的话)
     */
    public HttpMessageNotWritableException(String msg, @Nullable Throwable cause) {
        super(msg, cause);
    }
    
}
