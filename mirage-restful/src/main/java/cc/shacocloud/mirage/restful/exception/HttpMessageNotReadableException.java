package cc.shacocloud.mirage.restful.exception;

import cc.shacocloud.mirage.restful.HttpRequest;
import cc.shacocloud.mirage.restful.http.HttpMessageConverter;
import org.jetbrains.annotations.Nullable;

/**
 * 当 {@link HttpMessageConverter#read}方法失败时，由{@link HttpMessageConverter}抛出。
 */
public class HttpMessageNotReadableException extends HttpMessageConversionException {
    
    @Nullable
    private final HttpRequest request;
    
    @Deprecated
    public HttpMessageNotReadableException(String msg) {
        super(msg);
        this.request = null;
    }
    
    @Deprecated
    public HttpMessageNotReadableException(String msg, @Nullable Throwable cause) {
        super(msg, cause);
        this.request = null;
    }
    
    public HttpMessageNotReadableException(String msg, @Nullable HttpRequest request) {
        super(msg);
        this.request = request;
    }
    
    public HttpMessageNotReadableException(String msg, @Nullable Throwable cause,
                                           @Nullable HttpRequest request) {
        super(msg, cause);
        this.request = request;
    }
    
    
    /**
     * 返回原始的{@link HttpRequest}
     */
    public HttpRequest getRoutingContext() {
        if (this.request == null) {
            throw new IllegalStateException("没有request可用!");
        }
        return this.request;
    }
    
}
