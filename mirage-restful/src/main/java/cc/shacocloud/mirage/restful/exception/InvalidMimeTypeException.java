package cc.shacocloud.mirage.restful.exception;

import cc.shacocloud.mirage.restful.http.MimeTypeUtils;
import lombok.Getter;

/**
 * 在遇到无效的内容类型规范字符串时，从 {@link MimeTypeUtils#parseMimeType} 引发的异常。
 */
public class InvalidMimeTypeException extends IllegalArgumentException {
    
    @Getter
    private final String mimeType;
    
    /**
     * 为给定内容类型创建新的 InvalidContentTypeException
     *
     * @param mimeType 有问题的媒体类型
     * @param message  指示无效部件的详细信息消息
     */
    public InvalidMimeTypeException(String mimeType, String message) {
        super("无效的 mimeType \"" + mimeType + "\": " + message);
        this.mimeType = mimeType;
    }
    
}
