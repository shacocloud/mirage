package cc.shacocloud.mirage.restful.bind.validation.errors;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

/**
 * 绑定错误结果对象
 *
 * @author 思追(shaco)
 */
public interface BindingResultError {
    
    /**
     * 获取此错误的错误代码
     */
    @NotNull
    String getCode();
    
    /**
     * 返回此错误字段的嵌套路径
     */
    @NotNull
    String getNestedPath();
    
    /**
     * 返回此错误失败的值
     */
    @Nullable
    Object getRejectedValue();
    
}
