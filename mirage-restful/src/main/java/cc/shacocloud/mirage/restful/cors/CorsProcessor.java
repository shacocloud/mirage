package cc.shacocloud.mirage.restful.cors;

import cc.shacocloud.mirage.restful.HttpRequest;
import cc.shacocloud.mirage.restful.HttpResponse;
import io.vertx.core.Future;
import org.jetbrains.annotations.Nullable;

/**
 * 接受请求和CorsConfiguration并更新响应的策略。
 * <p>
 * 该组件不关心如何选择CorsConfiguration，而是采取后续操作，如应用CORS验证检查，拒绝响应或向响应添加CORS头。
 */
public interface CorsProcessor {
    
    
    /**
     * 处理给定的 {@link CorsConfiguration}的请求
     *
     * @param configuration 适用的CORS配置(可能是{@code null})
     * @param request       {@link HttpRequest}
     * @param response      {@link HttpResponse}
     * @return 如果请求被拒绝则为{@code false}，否则{@code true}
     */
    Future<Boolean> processRequest(@Nullable CorsConfiguration configuration,
                                   HttpRequest request,
                                   HttpResponse response);
    
}
