package cc.shacocloud.mirage.restful.bind.support;

import cc.shacocloud.mirage.restful.HandleMethodArgumentResolver;
import cc.shacocloud.mirage.restful.HttpRequest;
import cc.shacocloud.mirage.restful.bind.WebDataBinderFactory;
import cc.shacocloud.mirage.restful.bind.annotation.FileUpload;
import cc.shacocloud.mirage.restful.http.MultipartFileUpload;
import cc.shacocloud.mirage.utils.MethodParameter;
import io.vertx.core.Future;
import org.jetbrains.annotations.NotNull;

/**
 * 解析文件长传的方法参数
 *
 * @see MultipartFileUpload
 * @see FileUpload
 */
public class MultipartFileUploadArgumentResolver implements HandleMethodArgumentResolver {
    
    @Override
    public boolean supportsParameter(@NotNull MethodParameter parameter) {
        return parameter.hasParameterAnnotation(FileUpload.class)
                && MultipartFileUpload.class.isAssignableFrom(parameter.getParameterType());
    }
    
    @Override
    public Future<Object> resolveArgument(@NotNull HttpRequest request,
                                          @NotNull MethodParameter parameter,
                                          WebDataBinderFactory binderFactory) {
        return Future.succeededFuture(request.fileUploads());
    }
    
}
