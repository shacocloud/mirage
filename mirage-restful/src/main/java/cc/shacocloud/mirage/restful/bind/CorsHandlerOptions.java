package cc.shacocloud.mirage.restful.bind;

import io.vertx.core.http.HttpMethod;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.LinkedHashSet;
import java.util.Set;

/**
 * 跨域请求处理配置属性
 */
@Getter
@Setter
@NoArgsConstructor
public class CorsHandlerOptions {
    
    private final Set<HttpMethod> allowedMethods = new LinkedHashSet<>();
    private final Set<String> allowedHeaders = new LinkedHashSet<>();
    private final Set<String> exposedHeaders = new LinkedHashSet<>();
    private boolean allowCredentials;
    private String maxAgeSeconds;
    
}
