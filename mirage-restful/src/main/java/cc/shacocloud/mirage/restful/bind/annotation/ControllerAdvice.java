package cc.shacocloud.mirage.restful.bind.annotation;

import cc.shacocloud.mirage.bean.bind.Component;

import java.lang.annotation.*;

/**
 * 对声明{@link ExceptionHandler} 等等的类在多个 {@link Controller}类方法之间共享的特殊化{@link Component}
 *
 * @author 思追(shaco)
 * @see Controller
 * @see RestControllerAdvice
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Component
public @interface ControllerAdvice {

}
