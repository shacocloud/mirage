package cc.shacocloud.mirage.restful.bind.annotation;

/**
 * 路径表达式规则枚举
 *
 * @author 思追(shaco)
 */
public enum PathPatterns {
    
    /**
     * 路径匹配模式
     */
    PATH,
    
    /**
     * 正则匹配模式
     */
    REGEX
}
