package cc.shacocloud.mirage.restful.http;

import cc.shacocloud.mirage.restful.exception.InvalidMediaTypeException;
import cc.shacocloud.mirage.restful.exception.InvalidMimeTypeException;
import cc.shacocloud.mirage.utils.charSequence.StrUtil;
import cc.shacocloud.mirage.utils.collection.CollUtil;
import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.Serializable;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.*;

/**
 * {@link MimeType}的一个子类，添加了对HTTP规范中定义的质量参数的支持。
 *
 * @see <a href="https://tools.ietf.org/html/rfc7231#section-3.1.1.1"> HTTP 1.1: 语义和内容, 部分 3.1.1.1</a>
 */
public class MediaType extends MimeType implements Serializable {
    
    /**
     * 公共常量媒体类型，包括所有媒体范围 (i.e. "&#42;/&#42;").
     */
    public static final MediaType ALL;
    /**
     * 等价于字符串 {@link MediaType#ALL}.
     */
    public static final String ALL_VALUE = "*/*";
    /**
     * 用于的公共常量媒体类型 {@code application/atom+xml}.
     */
    public static final MediaType APPLICATION_ATOM_XML;
    /**
     * 等价于字符串 {@link MediaType#APPLICATION_ATOM_XML}.
     */
    public static final String APPLICATION_ATOM_XML_VALUE = "application/atom+xml";
    /**
     * 用于的公共常量媒体类型 {@code application/cbor}.
     */
    public static final MediaType APPLICATION_CBOR;
    /**
     * 等价于字符串 {@link MediaType#APPLICATION_CBOR}.
     */
    public static final String APPLICATION_CBOR_VALUE = "application/cbor";
    /**
     * 用于的公共常量媒体类型 {@code application/x-www-form-urlencoded}.
     */
    public static final MediaType APPLICATION_FORM_URLENCODED;
    /**
     * 等价于字符串 {@link MediaType#APPLICATION_FORM_URLENCODED}.
     */
    public static final String APPLICATION_FORM_URLENCODED_VALUE = "application/x-www-form-urlencoded";
    /**
     * 用于的公共常量媒体类型 {@code application/json}.
     */
    public static final MediaType APPLICATION_JSON;
    /**
     * 等价于字符串 {@link MediaType#APPLICATION_JSON}.
     *
     * @see #APPLICATION_JSON_UTF8_VALUE
     */
    public static final String APPLICATION_JSON_VALUE = "application/json";
    /**
     * 用于的公共常量媒体类型 {@code application/json;charset=UTF-8}.
     */
    public static final MediaType APPLICATION_JSON_UTF8;
    /**
     * 等价于字符串 {@link MediaType#APPLICATION_JSON_UTF8}.
     */
    public static final String APPLICATION_JSON_UTF8_VALUE = "application/json;charset=UTF-8";
    /**
     * 用于的公共常量媒体类型 {@code application/octet-stream}.
     */
    public static final MediaType APPLICATION_OCTET_STREAM;
    /**
     * 等价于字符串 {@link MediaType#APPLICATION_OCTET_STREAM}.
     */
    public static final String APPLICATION_OCTET_STREAM_VALUE = "application/octet-stream";
    /**
     * 用于的公共常量媒体类型 {@code application/pdf}.
     */
    public static final MediaType APPLICATION_PDF;
    /**
     * 等价于字符串 {@link MediaType#APPLICATION_PDF}.
     */
    public static final String APPLICATION_PDF_VALUE = "application/pdf";
    /**
     * 用于的公共常量媒体类型 {@code application/problem+json}.
     *
     * @see <a href="https://tools.ietf.org/html/rfc7807#section-6.1">
     * Problem Details for HTTP APIs, 6.1. application/problem+json</a>
     */
    public static final MediaType APPLICATION_PROBLEM_JSON;
    /**
     * 等价于字符串 {@link MediaType#APPLICATION_PROBLEM_JSON}.
     */
    public static final String APPLICATION_PROBLEM_JSON_VALUE = "application/problem+json";
    /**
     * 用于的公共常量媒体类型 {@code application/problem+json}.
     *
     * @see <a href="https://tools.ietf.org/html/rfc7807#section-6.1">
     * Problem Details for HTTP APIs, 6.1. application/problem+json</a>
     */
    public static final MediaType APPLICATION_PROBLEM_JSON_UTF8;
    /**
     * 等价于字符串 {@link MediaType#APPLICATION_PROBLEM_JSON_UTF8}.
     */
    public static final String APPLICATION_PROBLEM_JSON_UTF8_VALUE = "application/problem+json;charset=UTF-8";
    /**
     * 用于的公共常量媒体类型 {@code application/problem+xml}.
     *
     * @see <a href="https://tools.ietf.org/html/rfc7807#section-6.2">
     * Problem Details for HTTP APIs, 6.2. application/problem+xml</a>
     */
    public static final MediaType APPLICATION_PROBLEM_XML;
    /**
     * 等价于字符串 {@link MediaType#APPLICATION_PROBLEM_XML}.
     */
    public static final String APPLICATION_PROBLEM_XML_VALUE = "application/problem+xml";
    /**
     * 用于的公共常量媒体类型 {@code application/rss+xml}.
     */
    public static final MediaType APPLICATION_RSS_XML;
    /**
     * 等价于字符串 {@link MediaType#APPLICATION_RSS_XML}.
     */
    public static final String APPLICATION_RSS_XML_VALUE = "application/rss+xml";
    /**
     * 用于的公共常量媒体类型 {@code application/stream+json}.
     */
    public static final MediaType APPLICATION_STREAM_JSON;
    /**
     * 等价于字符串 {@link MediaType#APPLICATION_STREAM_JSON}.
     */
    public static final String APPLICATION_STREAM_JSON_VALUE = "application/stream+json";
    /**
     * 用于的公共常量媒体类型 {@code application/xhtml+xml}.
     */
    public static final MediaType APPLICATION_XHTML_XML;
    /**
     * 等价于字符串 {@link MediaType#APPLICATION_XHTML_XML}.
     */
    public static final String APPLICATION_XHTML_XML_VALUE = "application/xhtml+xml";
    /**
     * 用于的公共常量媒体类型 {@code application/xml}.
     */
    public static final MediaType APPLICATION_XML;
    /**
     * 等价于字符串 {@link MediaType#APPLICATION_XML}.
     */
    public static final String APPLICATION_XML_VALUE = "application/xml";
    /**
     * 用于的公共常量媒体类型 {@code image/gif}.
     */
    public static final MediaType IMAGE_GIF;
    /**
     * 等价于字符串 {@link MediaType#IMAGE_GIF}.
     */
    public static final String IMAGE_GIF_VALUE = "image/gif";
    /**
     * 用于的公共常量媒体类型 {@code image/jpeg}.
     */
    public static final MediaType IMAGE_JPEG;
    /**
     * 等价于字符串 {@link MediaType#IMAGE_JPEG}.
     */
    public static final String IMAGE_JPEG_VALUE = "image/jpeg";
    /**
     * 用于的公共常量媒体类型 {@code image/png}.
     */
    public static final MediaType IMAGE_PNG;
    /**
     * 等价于字符串 {@link MediaType#IMAGE_PNG}.
     */
    public static final String IMAGE_PNG_VALUE = "image/png";
    /**
     * 用于的公共常量媒体类型 {@code multipart/form-data}.
     */
    public static final MediaType MULTIPART_FORM_DATA;
    /**
     * 等价于字符串 {@link MediaType#MULTIPART_FORM_DATA}.
     */
    public static final String MULTIPART_FORM_DATA_VALUE = "multipart/form-data";
    /**
     * 用于的公共常量媒体类型 {@code multipart/mixed}.
     */
    public static final MediaType MULTIPART_MIXED;
    /**
     * 等价于字符串 {@link MediaType#MULTIPART_MIXED}.
     */
    public static final String MULTIPART_MIXED_VALUE = "multipart/mixed";
    /**
     * 用于的公共常量媒体类型 {@code multipart/related}.
     */
    public static final MediaType MULTIPART_RELATED;
    /**
     * 等价于字符串 {@link MediaType#MULTIPART_RELATED}.
     */
    public static final String MULTIPART_RELATED_VALUE = "multipart/related";
    /**
     * 用于的公共常量媒体类型 {@code text/event-stream}.
     *
     * @see <a href="https://www.w3.org/TR/eventsource/">服务器发送事件W3C推荐标准</a>
     */
    public static final MediaType TEXT_EVENT_STREAM;
    /**
     * 等价于字符串 {@link MediaType#TEXT_EVENT_STREAM}.
     */
    public static final String TEXT_EVENT_STREAM_VALUE = "text/event-stream";
    /**
     * 用于的公共常量媒体类型 {@code text/html}.
     */
    public static final MediaType TEXT_HTML;
    /**
     * 等价于字符串 {@link MediaType#TEXT_HTML}.
     */
    public static final String TEXT_HTML_VALUE = "text/html";
    /**
     * 用于的公共常量媒体类型 {@code text/markdown}.
     */
    public static final MediaType TEXT_MARKDOWN;
    /**
     * 等价于字符串 {@link MediaType#TEXT_MARKDOWN}.
     */
    public static final String TEXT_MARKDOWN_VALUE = "text/markdown";
    /**
     * 用于的公共常量媒体类型 {@code text/plain}.
     */
    public static final MediaType TEXT_PLAIN;
    /**
     * 等价于字符串 {@link MediaType#TEXT_PLAIN}.
     */
    public static final String TEXT_PLAIN_VALUE = "text/plain";
    /**
     * 用于的公共常量媒体类型 {@code text/xml}.
     */
    public static final MediaType TEXT_XML;
    /**
     * 等价于字符串 {@link MediaType#TEXT_XML}.
     */
    public static final String TEXT_XML_VALUE = "text/xml";
    private static final long serialVersionUID = 2069937152339670231L;
    private static final String PARAM_QUALITY_FACTOR = "q";
    public static final Comparator<MediaType> QUALITY_VALUE_COMPARATOR = (mediaType1, mediaType2) -> {
        double quality1 = mediaType1.getQualityValue();
        double quality2 = mediaType2.getQualityValue();
        int qualityComparison = Double.compare(quality2, quality1);
        if (qualityComparison != 0) {
            return qualityComparison;  // audio/*;q=0.7 < audio/*;q=0.3
        } else if (mediaType1.isWildcardType() && !mediaType2.isWildcardType()) {  // */* < audio/*
            return 1;
        } else if (mediaType2.isWildcardType() && !mediaType1.isWildcardType()) {  // audio/* > */*
            return -1;
        } else if (!mediaType1.getType().equals(mediaType2.getType())) {  // audio/basic == text/html
            return 0;
        } else {
            if (mediaType1.isWildcardSubtype() && !mediaType2.isWildcardSubtype()) {  // audio/* < audio/basic
                return 1;
            } else if (mediaType2.isWildcardSubtype() && !mediaType1.isWildcardSubtype()) {  // audio/basic > audio/*
                return -1;
            } else if (!mediaType1.getSubtype().equals(mediaType2.getSubtype())) {  // audio/basic == audio/wave
                return 0;
            } else {
                int paramsSize1 = mediaType1.getParameters().size();
                int paramsSize2 = mediaType2.getParameters().size();
                return Integer.compare(paramsSize2, paramsSize1);  // audio/basic;level=1 < audio/basic
            }
        }
    };
    public static final Comparator<MediaType> SPECIFICITY_COMPARATOR = new SpecificityComparator<MediaType>() {
        
        @Override
        protected int compareParameters(@NotNull MediaType mediaType1, @NotNull MediaType mediaType2) {
            double quality1 = mediaType1.getQualityValue();
            double quality2 = mediaType2.getQualityValue();
            int qualityComparison = Double.compare(quality2, quality1);
            if (qualityComparison != 0) {
                return qualityComparison;  // audio/*;q=0.7 < audio/*;q=0.3
            }
            return super.compareParameters(mediaType1, mediaType2);
        }
    };
    
    static {
        ALL = new MediaType("*", "*");
        APPLICATION_ATOM_XML = new MediaType("application", "atom+xml");
        APPLICATION_CBOR = new MediaType("application", "cbor");
        APPLICATION_FORM_URLENCODED = new MediaType("application", "x-www-form-urlencoded");
        APPLICATION_JSON = new MediaType("application", "json");
        APPLICATION_JSON_UTF8 = new MediaType("application", "json", StandardCharsets.UTF_8);
        APPLICATION_OCTET_STREAM = new MediaType("application", "octet-stream");
        APPLICATION_PDF = new MediaType("application", "pdf");
        APPLICATION_PROBLEM_JSON = new MediaType("application", "problem+json");
        APPLICATION_PROBLEM_JSON_UTF8 = new MediaType("application", "problem+json", StandardCharsets.UTF_8);
        APPLICATION_PROBLEM_XML = new MediaType("application", "problem+xml");
        APPLICATION_RSS_XML = new MediaType("application", "rss+xml");
        APPLICATION_STREAM_JSON = new MediaType("application", "stream+json");
        APPLICATION_XHTML_XML = new MediaType("application", "xhtml+xml");
        APPLICATION_XML = new MediaType("application", "xml");
        IMAGE_GIF = new MediaType("image", "gif");
        IMAGE_JPEG = new MediaType("image", "jpeg");
        IMAGE_PNG = new MediaType("image", "png");
        MULTIPART_FORM_DATA = new MediaType("multipart", "form-data");
        MULTIPART_MIXED = new MediaType("multipart", "mixed");
        MULTIPART_RELATED = new MediaType("multipart", "related");
        TEXT_EVENT_STREAM = new MediaType("text", "event-stream");
        TEXT_HTML = new MediaType("text", "html");
        TEXT_MARKDOWN = new MediaType("text", "markdown");
        TEXT_PLAIN = new MediaType("text", "plain");
        TEXT_XML = new MediaType("text", "xml");
    }
    
    /**
     * 为给定的主类型创建一个新的{@code MediaType}。
     *
     * @param type 的主要类型
     * @throws IllegalArgumentException 如果任何参数包含非法字符
     */
    public MediaType(String type) {
        super(type);
    }
    
    /**
     * 为给定的主类型和子类型创建一个新的{@code MediaType}。
     * <p>
     * 参数为空。
     *
     * @param type    的主要类型
     * @param subtype 的子类型
     * @throws IllegalArgumentException 如果任何参数包含非法字符
     */
    public MediaType(String type, String subtype) {
        super(type, subtype, Collections.emptyMap());
    }
    
    /**
     * 为给定的类型、子类型和字符集创建一个新的{@code MediaType}。
     *
     * @param type    主要类型
     * @param subtype 子类型
     * @param charset 字符集
     * @throws IllegalArgumentException if any of the parameters contain illegal characters
     */
    public MediaType(String type, String subtype, Charset charset) {
        super(type, subtype, charset);
    }
    
    /**
     * 为给定的类型、子类型和质量值创建一个新的{@code MediaType}。
     *
     * @param type         主要类型
     * @param subtype      子类型
     * @param qualityValue 质量值
     * @throws IllegalArgumentException 如果任何参数包含非法字符
     */
    public MediaType(String type, String subtype, double qualityValue) {
        this(type, subtype, Collections.singletonMap(PARAM_QUALITY_FACTOR, Double.toString(qualityValue)));
    }
    
    /**
     * 复制构造函数，复制给定的 {@code MediaType}的类型、子类型和参数，并允许设置指定的字符集。
     *
     * @param other   其他媒体类型
     * @param charset 的字符集
     * @throws IllegalArgumentException 如果任何参数包含非法字符
     */
    public MediaType(MediaType other, Charset charset) {
        super(other, charset);
    }
    
    
    /**
     * 复制给定{@code MediaType}的类型和子类型的构造函数， 允许不同的参数。
     *
     * @param other      其他媒体类型
     * @param parameters 参数可以是{@code null}
     * @throws IllegalArgumentException 如果任何参数包含非法字符
     */
    public MediaType(MediaType other, @Nullable Map<String, String> parameters) {
        super(other.getType(), other.getSubtype(), parameters);
    }
    
    /**
     * 为给定的类型、子类型和参数创建一个新的{@code MediaType}。
     *
     * @param type       主要类型
     * @param subtype    子类型
     * @param parameters 参数可以是{@code null}
     * @throws IllegalArgumentException 如果任何参数包含非法字符
     */
    public MediaType(String type, String subtype, @Nullable Map<String, String> parameters) {
        super(type, subtype, parameters);
    }
    
    /**
     * 将给定的字符串值解析为{@code MediaType}对象，
     *
     * @param value 要解析的字符串
     * @throws InvalidMediaTypeException 如果无法解析媒体类型值
     * @see #parseMediaType(String)
     */
    @Contract("_ -> new")
    public static @NotNull MediaType valueOf(String value) {
        return parseMediaType(value);
    }
    
    /**
     * 将给定字符串解析为一个{@code MediaType}。
     *
     * @param mediaType 要解析的字符串
     * @return 媒体类型
     * @throws InvalidMediaTypeException 如果无法解析媒体类型值
     */
    @Contract("_ -> new")
    public static @NotNull MediaType parseMediaType(String mediaType) {
        MimeType type;
        try {
            type = MimeTypeUtils.parseMimeType(mediaType);
        } catch (InvalidMimeTypeException ex) {
            throw new InvalidMediaTypeException(ex);
        }
        
        try {
            return new MediaType(type.getType(), type.getSubtype(), type.getParameters());
        } catch (IllegalArgumentException ex) {
            throw new InvalidMediaTypeException(mediaType, ex.getMessage());
        }
    }
    
    /**
     * 将逗号分隔的字符串解析为{@code MediaType}对象列表。
     * <p>
     * 此方法可用于解析接受头或内容类型头。
     *
     * @param mediaTypes 要解析的字符串
     * @return 媒体类型列表
     * @throws InvalidMediaTypeException 如果无法解析媒体类型值
     */
    public static @NotNull List<MediaType> parseMediaTypes(@Nullable String mediaTypes) {
        if (StrUtil.isEmpty(mediaTypes)) {
            return Collections.emptyList();
        }
        // 避免在热路径中使用 java.util.stream.Stream
        List<String> tokenizedTypes = MimeTypeUtils.tokenize(mediaTypes);
        List<MediaType> result = new ArrayList<>(tokenizedTypes.size());
        for (String type : tokenizedTypes) {
            if (StrUtil.isNotBlank(type)) {
                result.add(parseMediaType(type));
            }
        }
        return result;
    }
    
    /**
     * 将给定的(可能的)逗号分隔的字符串列表解析为{@code MediaType}对象的列表。
     * <p>
     * 此方法可用于解析接受头或内容类型头。
     *
     * @param mediaTypes 要解析的字符串
     * @return 媒体类型列表
     * @throws InvalidMediaTypeException 如果无法解析媒体类型值
     */
    public static List<MediaType> parseMediaTypes(@Nullable List<String> mediaTypes) {
        if (CollUtil.isEmpty(mediaTypes)) {
            return Collections.emptyList();
        } else if (mediaTypes.size() == 1) {
            return parseMediaTypes(mediaTypes.get(0));
        } else {
            List<MediaType> result = new ArrayList<>(8);
            for (String mediaType : mediaTypes) {
                result.addAll(parseMediaTypes(mediaType));
            }
            return result;
        }
    }
    
    /**
     * 将给定的mime类型重新创建为媒体类型。
     */
    public static @NotNull List<MediaType> asMediaTypes(@NotNull List<MimeType> mimeTypes) {
        List<MediaType> mediaTypes = new ArrayList<>(mimeTypes.size());
        for (MimeType mimeType : mimeTypes) {
            mediaTypes.add(MediaType.asMediaType(mimeType));
        }
        return mediaTypes;
    }
    
    /**
     * 将给定的mime类型重新创建为媒体类型。
     */
    @Contract("null -> new")
    public static @NotNull MediaType asMediaType(MimeType mimeType) {
        if (mimeType instanceof MediaType) {
            return (MediaType) mimeType;
        }
        return new MediaType(mimeType.getType(), mimeType.getSubtype(), mimeType.getParameters());
    }
    
    /**
     * 返回给定{@code MediaType}对象列表的字符串表示形式。
     * <p>
     * 此方法可用于{@code Accept}或{@code Content-Type}标题。
     *
     * @param mediaTypes 要为其创建字符串表示的媒体类型
     * @return 字符串表示
     */
    public static @NotNull String toString(Collection<MediaType> mediaTypes) {
        return MimeTypeUtils.toString(mediaTypes);
    }
    
    /**
     * 按特异性对给定的{@code MediaType}对象列表进行排序。
     *
     * @param mediaTypes 要排序的媒体类型列表
     * @see <a href="https://tools.ietf.org/html/rfc7231#section-5.3.2">HTTP 1.1: Semantics
     * and Content, section 5.3.2</a>
     */
    public static void sortBySpecificity(@NotNull List<MediaType> mediaTypes) {
        if (mediaTypes.size() > 1) {
            mediaTypes.sort(SPECIFICITY_COMPARATOR);
        }
    }
    
    /**
     * 按质量值对给定的{@code MediaType}对象列表进行排序。
     *
     * @param mediaTypes 要排序的媒体类型列表
     * @see #getQualityValue()
     */
    public static void sortByQualityValue(@NotNull List<MediaType> mediaTypes) {
        if (mediaTypes.size() > 1) {
            mediaTypes.sort(QUALITY_VALUE_COMPARATOR);
        }
    }
    
    /**
     * 将给定的{@code MediaType}对象列表按特定程度作为主要标准，质量值作为次要标准。
     *
     * @see MediaType#sortBySpecificity(List)
     * @see MediaType#sortByQualityValue(List)
     */
    public static void sortBySpecificityAndQuality(@NotNull List<MediaType> mediaTypes) {
        if (mediaTypes.size() > 1) {
            mediaTypes.sort(MediaType.SPECIFICITY_COMPARATOR.thenComparing(MediaType.QUALITY_VALUE_COMPARATOR));
        }
    }
    
    @Override
    protected void checkParameters(String attribute, String value) {
        super.checkParameters(attribute, value);
        if (PARAM_QUALITY_FACTOR.equals(attribute)) {
            value = unquote(value);
            double d = Double.parseDouble(value);
            if (!(d >= 0D && d <= 1D)) {
                throw new IllegalArgumentException("无效的质量值 \"" + value + "\": 应该在0.0到1.0之间");
            }
        }
    }
    
    /**
     * 返回质量因子，如{@code q}参数所示(如果有的话)。 默认值为{@code 1.0}。
     *
     * @return 质量因素被称为双倍价值
     */
    public double getQualityValue() {
        String qualityFactor = getParameter(PARAM_QUALITY_FACTOR);
        return (qualityFactor != null ? Double.parseDouble(unquote(qualityFactor)) : 1D);
    }
    
    /**
     * 指示此 {@code MediaType} 是否包含给定的媒体类型。
     *
     * @param other 要与之比较的引用媒体类型
     * @return 如果该媒体类型包含给定的媒体类型，则为{@code true};否则为{@code false}
     */
    public boolean includes(@Nullable MediaType other) {
        return super.includes(other);
    }
    
    /**
     * 指示此 {@code MediaType} 是否与给定的媒体类型兼容。
     *
     * @param other 要与之比较的引用媒体类型
     * @return 如果该媒体类型兼容给定的媒体类型，则为{@code true};否则为{@code false}
     */
    public boolean isCompatibleWith(@Nullable MediaType other) {
        return super.isCompatibleWith(other);
    }
    
    /**
     * 返回此实例的副本，并带有给定的{@code MediaType}的质量值。
     *
     * @return 如果给定的中介类型没有质量值、或新值，则相同实例
     */
    public MediaType copyQualityValue(@NotNull MediaType mediaType) {
        if (!mediaType.getParameters().containsKey(PARAM_QUALITY_FACTOR)) {
            return this;
        }
        Map<String, String> params = new LinkedHashMap<>(getParameters());
        params.put(PARAM_QUALITY_FACTOR, mediaType.getParameters().get(PARAM_QUALITY_FACTOR));
        return new MediaType(this, params);
    }
    
    /**
     * 返回该实例的副本，并删除其质量值。
     *
     * @return 如果媒体类型不包含质量值，则为相同实例，否则为新值
     */
    public MediaType removeQualityValue() {
        if (!getParameters().containsKey(PARAM_QUALITY_FACTOR)) {
            return this;
        }
        Map<String, String> params = new LinkedHashMap<>(getParameters());
        params.remove(PARAM_QUALITY_FACTOR);
        return new MediaType(this, params);
    }
    
}
