package cc.shacocloud.mirage.restful;

import io.vertx.core.Promise;
import org.jetbrains.annotations.NotNull;

/**
 * 过滤器
 */
public interface Filter {
    
    /**
     * 每当一个请求上下文对通过链时，由于客户端对链末端的资源发出请求，容器就会调用过滤器的<code>doFilter</code>方法。
     * 传递给这个方法的过滤器链允许过滤器将请求上下文传递给链中的下一个实体。
     * <p>
     * 如果返回的实体为空 则不继续调用下一个实体,
     * 即
     * <pre>
     *     // 不继续调用
     *     doFilterFuture.complete();
     *
     *     // 继续调用
     *     doFilterFuture.complete(context);
     * </pre>
     */
    void doFilter(@NotNull RoutingContext context, @NotNull Promise<RoutingContext> doFilterFuture);
    
    /**
     * 当前请求结束后调用
     */
    default void destroy(@NotNull Promise<Void> destroyFuture) {
        destroyFuture.complete();
    }
}
