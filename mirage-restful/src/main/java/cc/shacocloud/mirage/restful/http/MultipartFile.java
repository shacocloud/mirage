package cc.shacocloud.mirage.restful.http;

import io.vertx.core.Future;
import io.vertx.core.buffer.Buffer;
import io.vertx.ext.web.FileUpload;

/**
 * 表示来自HTTP多部分表单上传的文件
 */
public interface MultipartFile extends FileUpload {
    
    /**
     * @return 读取上传的文件
     */
    Future<Buffer> readBuf();
    
}
