package cc.shacocloud.mirage.restful.bind.annotation;

import java.lang.annotation.*;

/**
 * 它本身被注释为{@link ControllerAdvice}和{@link ResponseBody}，当需要则两个注解时使用比较方便
 *
 * @author shaco
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
@ControllerAdvice
@ResponseBody
public @interface RestControllerAdvice {
}
