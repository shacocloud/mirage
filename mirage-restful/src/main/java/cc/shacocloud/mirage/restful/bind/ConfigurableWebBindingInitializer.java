package cc.shacocloud.mirage.restful.bind;

import cc.shacocloud.mirage.restful.bind.validation.SmartValidator;
import cc.shacocloud.mirage.utils.converter.TypeConverter;
import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;

/**
 * 方便的{@link WebBindingInitializer}用于在应用程序上下文中进行声明式配置。
 * <p>
 * 允许与多个控制器/处理程序一起重用预配置的初始化程序。
 */
public class ConfigurableWebBindingInitializer implements WebBindingInitializer {
    
    @Getter
    @Setter
    private SmartValidator validator;
    
    @Getter
    @Setter
    private TypeConverter typeConverter;
    
    @Override
    public void initBinder(@NotNull WebDataBinder binder) {
        binder.setValidator(validator);
        binder.setTypeConverter(typeConverter);
    }
    
}
