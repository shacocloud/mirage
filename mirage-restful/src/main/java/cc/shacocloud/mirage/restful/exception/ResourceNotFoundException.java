package cc.shacocloud.mirage.restful.exception;

/**
 * 资源不存在异常
 *
 * @author 思追(shaco)
 */
public class ResourceNotFoundException extends RuntimeException {
    
    public ResourceNotFoundException() {
    }
    
    public ResourceNotFoundException(String message) {
        super(message);
    }
    
    public ResourceNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }
}
