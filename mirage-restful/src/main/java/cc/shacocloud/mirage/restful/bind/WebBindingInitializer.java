package cc.shacocloud.mirage.restful.bind;


/**
 * 回调接口，用于初始化{@link WebDataBinder}，以便在特定Web请求的上下文中执行数据绑定。
 */
public interface WebBindingInitializer {
    
    /**
     * 初始化给定的DataBinder
     */
    void initBinder(WebDataBinder binder);
    
}
