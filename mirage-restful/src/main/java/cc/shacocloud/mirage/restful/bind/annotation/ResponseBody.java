package cc.shacocloud.mirage.restful.bind.annotation;

import cc.shacocloud.mirage.restful.bind.support.RequestResponseBodyMethodProcessor;

import java.lang.annotation.*;

/**
 * 指示方法返回值应绑定到web响应体的注解。支持带注解的处理程序方法。
 *
 * @see RequestResponseBodyMethodProcessor
 */
@Target({ElementType.TYPE, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface ResponseBody {

}
