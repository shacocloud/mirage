package cc.shacocloud.mirage.restful.exception;

import lombok.Getter;
import org.jetbrains.annotations.Nullable;

import java.util.Objects;

/**
 * 无效的请求路径
 *
 * @author 思追(shaco)
 */
public class InvalidRequestPathException extends IllegalArgumentException {
    
    @Nullable
    @Getter
    private final String path;
    
    public InvalidRequestPathException() {
        this(null);
    }
    
    public InvalidRequestPathException(@Nullable String path) {
        super("无效的路径 \"" + (Objects.isNull(path) ? "" : path) + "\"");
        this.path = path;
    }
    
    public InvalidRequestPathException(String message, @Nullable String path) {
        super("无效的路径 \"" + (Objects.isNull(path) ? "" : path) + "\": " + message);
        this.path = path;
    }
}
