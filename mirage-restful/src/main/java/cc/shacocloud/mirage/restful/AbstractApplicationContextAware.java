package cc.shacocloud.mirage.restful;

import cc.shacocloud.mirage.core.ApplicationContext;
import cc.shacocloud.mirage.core.ApplicationContextAware;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

/**
 * {@link ApplicationContext} 抽象
 *
 * @author 思追(shaco)
 */
public abstract class AbstractApplicationContextAware implements ApplicationContextAware {
    
    @Nullable
    private ApplicationContext applicationContext = null;
    
    @Override
    public void setApplicationContext(@NotNull ApplicationContext applicationContext) {
        this.applicationContext = applicationContext;
    }
    
    /**
     * 获取实际使用的 {@link ApplicationContext}
     */
    @Nullable
    protected final ApplicationContext obtainApplicationContext() {
        return this.applicationContext;
    }
    
}
