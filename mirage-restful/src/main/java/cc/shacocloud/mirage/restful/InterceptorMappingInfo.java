package cc.shacocloud.mirage.restful;

import cc.shacocloud.mirage.restful.bind.annotation.WebInterceptor;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;

import java.util.Objects;

/**
 * 拦截器映射信息
 *
 * @see WebInterceptor
 */
@Getter
@Builder
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public final class InterceptorMappingInfo {
    
    /**
     * 拦截器名称
     */
    private final String interceptorName;
    
    /**
     * 拦截路径，如果不设置默认全匹配
     */
    private final String[] includePatterns;
    
    /**
     * 排除路径
     */
    private final String[] excludePatterns;
    
    /**
     * 处理程序
     */
    private final HandlerInterceptor handler;
    
    /**
     * 排序字段
     */
    private final int order;
    
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        InterceptorMappingInfo that = (InterceptorMappingInfo) o;
        return Objects.equals(interceptorName, that.interceptorName);
    }
    
    @Override
    public int hashCode() {
        return Objects.hash(interceptorName);
    }
}
