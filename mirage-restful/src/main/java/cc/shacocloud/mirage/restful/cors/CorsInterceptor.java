package cc.shacocloud.mirage.restful.cors;

import cc.shacocloud.mirage.restful.HandlerInterceptor;
import cc.shacocloud.mirage.restful.HttpRequest;
import cc.shacocloud.mirage.restful.HttpResponse;
import cc.shacocloud.mirage.restful.VertxInvokeHandler;
import io.vertx.core.Future;
import org.jetbrains.annotations.Nullable;

/**
 * cors的拦截器实现，内部逻辑使用 {@link CorsProcessor}
 *
 * @see CorsProcessor
 */
public class CorsInterceptor implements HandlerInterceptor {
    
    private final CorsProcessor corsProcessor;
    
    @Nullable
    private final CorsConfiguration configuration;
    
    public CorsInterceptor(CorsProcessor corsProcessor,
                           @Nullable CorsConfiguration configuration) {
        this.corsProcessor = corsProcessor;
        this.configuration = configuration;
    }
    
    @Override
    public Future<Boolean> preHandle(HttpRequest request, HttpResponse response, VertxInvokeHandler handler) {
        return corsProcessor.processRequest(configuration, request, response);
    }
}
