package cc.shacocloud.mirage.restful.bind.validation;

import jakarta.validation.Valid;

import java.lang.annotation.*;

/**
 * JSR-303 的变体 {@link Valid}，支持验证组的规范
 */
@Target({ElementType.TYPE, ElementType.METHOD, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Validated {
    
    /**
     * 指定一个或多个验证组，以应用于此批注启动的验证步骤。
     */
    Class<?>[] value() default {};
    
}
