package cc.shacocloud.mirage.restful.bind;

import io.vertx.core.http.CookieSameSite;
import io.vertx.ext.web.handler.SessionHandler;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 会话处理配置属性
 */
@Getter
@Setter
@NoArgsConstructor
public class SessionHandlerOptions {
    
    public static final SessionStoreOptions STORE_DEFAULT = new SessionStoreOptions();
    
    public static final SessionHandlerOptions DEFAULT = new SessionHandlerOptions();
    
    /**
     * 存储配置
     */
    private SessionStoreOptions storeOptions = STORE_DEFAULT;
    
    /**
     * 会话 cookie 的默认名称
     */
    private String sessionCookieName = "mirage-restful.session";
    
    /**
     * 会话cookie的默认路径
     */
    private String sessionCookiePath = SessionHandler.DEFAULT_SESSION_COOKIE_PATH;
    
    /**
     * 会话在过期前不被访问的持续时间。 单位/ms
     */
    private long sessionTimeout = SessionHandler.DEFAULT_SESSION_TIMEOUT;
    
    /**
     * 默认情况下，如果会话处理程序是通过HTTP而不是HTTPS访问的，是否应该写入日志警告
     */
    private boolean nagHttps = false;
    
    /**
     * cookie是否设置了 "secure" 标志，只允许https传输。
     * <p>
     * 更多信息: <a href="https://www.owasp.org/index.php/SecureFlag" />
     */
    private boolean sessionCookieSecure = SessionHandler.DEFAULT_COOKIE_SECURE_FLAG;
    
    /**
     * 默认的cookie是否有HttpOnly标志设置
     * <p>
     * 更多信息: <a href="https://www.owasp.org/index.php/HttpOnly" />
     */
    private boolean sessionCookieHttpOnly = SessionHandler.DEFAULT_COOKIE_HTTP_ONLY_FLAG;
    
    /**
     * 会话id的默认最小长度。
     * <p>
     * 更多信息: <a href="https://www.owasp.org/index.php/Session_Management_Cheat_Sheet" />
     */
    private int minLength = SessionHandler.DEFAULT_SESSIONID_MIN_LENGTH;
    
    /**
     * 设置要使用的会话cookie SameSite策略。
     */
    private CookieSameSite cookieSameSite = CookieSameSite.STRICT;
    
    /**
     * 默认情况下是否应该延迟创建会话。
     */
    private boolean lazySession = SessionHandler.DEFAULT_LAZY_SESSION;
    
    /**
     * 会话存储配置
     * <p>
     * 本地还是集群 会更加 Vertx的属性自动配置
     */
    @Getter
    @Setter
    @NoArgsConstructor
    public static class SessionStoreOptions {
        
        /**
         * 会话映射使用的名称
         */
        private String mapName = "mirage-web.sessions";
        
        /**
         * 重试超时，用于未在此存储中找到的会话，单位/ms
         */
        private long retryTimeout = 5 * 1000; // 5秒
        
        /**
         * 检查过期会话的默认频率，单位/ms
         */
        private long reaperInterval = 1000;
    }
    
}
