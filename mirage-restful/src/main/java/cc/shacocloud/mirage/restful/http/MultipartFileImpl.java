package cc.shacocloud.mirage.restful.http;

import io.vertx.core.Future;
import io.vertx.core.Promise;
import io.vertx.core.buffer.Buffer;
import io.vertx.core.file.FileSystem;
import io.vertx.ext.web.FileUpload;

/**
 * 基于{@link MultipartFile}的默认实现
 */
public class MultipartFileImpl implements MultipartFile {
    
    private final FileUpload fileUpload;
    private final FileSystem fs;
    
    /**
     * @param fileUpload 文件上传信息
     * @param fs         vertx的文件系统操作多选
     */
    public MultipartFileImpl(FileUpload fileUpload, FileSystem fs) {
        this.fileUpload = fileUpload;
        this.fs = fs;
    }
    
    @Override
    public Future<Buffer> readBuf() {
        Promise<Buffer> promise = Promise.promise();
        fs.readFile(uploadedFileName(), promise);
        return promise.future();
    }
    
    // -------------- FileUpload
    
    @Override
    public String name() {
        return fileUpload.name();
    }
    
    @Override
    public String uploadedFileName() {
        return fileUpload.uploadedFileName();
    }
    
    @Override
    public String fileName() {
        return fileUpload.fileName();
    }
    
    @Override
    public long size() {
        return fileUpload.size();
    }
    
    @Override
    public String contentType() {
        return fileUpload.contentType();
    }
    
    @Override
    public String contentTransferEncoding() {
        return fileUpload.contentTransferEncoding();
    }
    
    @Override
    public String charSet() {
        return fileUpload.charSet();
    }
    
    @Override
    public boolean cancel() {
        return fileUpload.cancel();
    }
}
