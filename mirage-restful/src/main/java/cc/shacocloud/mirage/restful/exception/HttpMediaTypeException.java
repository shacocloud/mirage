package cc.shacocloud.mirage.restful.exception;

import cc.shacocloud.mirage.restful.http.MediaType;

import java.util.Collections;
import java.util.List;

/**
 * 与媒体类型相关的异常的抽象基础。添加支持的{@link MediaType MediaTypes}列表。
 */
public abstract class HttpMediaTypeException extends RuntimeException {
    
    private final List<MediaType> supportedMediaTypes;
    
    
    /**
     * 创建一个新的 HttpMediaTypeException。
     *
     * @param message 异常消息
     */
    protected HttpMediaTypeException(String message) {
        super(message);
        this.supportedMediaTypes = Collections.emptyList();
    }
    
    /**
     * 使用支持的媒体类型列表创建一个新的HttpMediaTypeException。
     *
     * @param supportedMediaTypes 支持的媒体类型列表
     */
    protected HttpMediaTypeException(String message, List<MediaType> supportedMediaTypes) {
        super(message);
        this.supportedMediaTypes = Collections.unmodifiableList(supportedMediaTypes);
    }
    
    
    /**
     * 返回支持的媒体类型列表。
     */
    public List<MediaType> getSupportedMediaTypes() {
        return this.supportedMediaTypes;
    }
    
}
