package cc.shacocloud.mirage.restful.exception;

import cc.shacocloud.mirage.restful.http.MediaType;
import org.jetbrains.annotations.Nullable;

import java.util.List;

/**
 * 当请求处理程序不支持的类型的内容时引发的异常。
 */
@SuppressWarnings("serial")
public class HttpMediaTypeNotSupportedException extends HttpMediaTypeException {
    
    @Nullable
    private final MediaType contentType;
    
    
    /**
     * 创建一个新的 {@link HttpMediaTypeNotSupportedException}
     *
     * @param message 异常消息
     */
    public HttpMediaTypeNotSupportedException(String message) {
        super(message);
        this.contentType = null;
    }
    
    /**
     * 创建一个新的{@link HttpMediaTypeNotSupportedException}
     *
     * @param contentType         不支持的内容类型
     * @param supportedMediaTypes 支持的媒体类型列表
     */
    public HttpMediaTypeNotSupportedException(@Nullable MediaType contentType, List<MediaType> supportedMediaTypes) {
        this(contentType, supportedMediaTypes, "内容类型 '" + (contentType != null ? contentType : "") + "' 不支持");
    }
    
    /**
     * 创建一个新的{@link HttpMediaTypeNotSupportedException}
     *
     * @param contentType         不支持的内容类型
     * @param supportedMediaTypes 支持的媒体类型列表
     * @param msg                 详细的消息
     */
    public HttpMediaTypeNotSupportedException(@Nullable MediaType contentType,
                                              List<MediaType> supportedMediaTypes, String msg) {
        
        super(msg, supportedMediaTypes);
        this.contentType = contentType;
    }
    
    /**
     * 返回导致失败的HTTP请求内容类型方法。
     */
    @Nullable
    public MediaType getContentType() {
        return this.contentType;
    }
    
}
