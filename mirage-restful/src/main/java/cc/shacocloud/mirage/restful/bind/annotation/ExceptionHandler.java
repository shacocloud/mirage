package cc.shacocloud.mirage.restful.bind.annotation;

import java.lang.annotation.*;

/**
 * 用于处理特定异常的处理程序方法注解
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface ExceptionHandler {
    
    /**
     * 带注解的方法处理的异常，如果为空，将默认为方法参数列表中列出的任何异常。
     */
    Class<? extends Throwable>[] value() default {};
    
}
