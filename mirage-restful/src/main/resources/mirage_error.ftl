<!DOCTYPE html>
<html lang="zh">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>mirage error page</title>
</head>
<body>
<section id="error" class="container text-center" style="height:800px;">
    <h1>mirage error page</h1>
    <p>
        请求地址：${context.normalizedPath()}<br/>
        响应状态：${context.response().getStatusCode()}<br/>
        响应消息：${context.response().getStatusMessage()}<br/>
        异常消息：${errorMsg}
    </p>
</section>
</body>
</html>
