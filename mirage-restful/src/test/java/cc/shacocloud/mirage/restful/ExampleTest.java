package cc.shacocloud.mirage.restful;

import cc.shacocloud.mirage.restful.example.ExampleServer;
import cc.shacocloud.mirage.restful.http.MediaType;
import io.vertx.core.AsyncResult;
import io.vertx.core.Handler;
import io.vertx.core.MultiMap;
import io.vertx.core.Vertx;
import io.vertx.core.buffer.Buffer;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.client.HttpRequest;
import io.vertx.ext.web.client.HttpResponse;
import io.vertx.ext.web.client.WebClient;
import io.vertx.ext.web.multipart.MultipartForm;
import io.vertx.junit5.VertxExtension;
import io.vertx.junit5.VertxTestContext;
import org.jetbrains.annotations.NotNull;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import java.io.File;
import java.nio.file.Paths;
import java.util.function.Function;

@ExtendWith(VertxExtension.class)
public class ExampleTest extends ExampleServer {
    
    private final Handler<AsyncResult<HttpResponse<Buffer>>> resultHandler = ar -> {
    
    };
    
    @BeforeAll
    public static void startVertxHttpServer(Vertx vertx) {
        startServer(vertx, new ExampleController());
    }
    
    private void responseHandler(VertxTestContext testContext, @NotNull AsyncResult<HttpResponse<Buffer>> ar) {
        if (ar.failed()) {
            testContext.failNow(ar.cause());
        } else {
            HttpResponse<Buffer> response = ar.result();
            
            System.out.println();
            System.out.println("响应状态码：" + response.statusCode());
            System.out.println("响应状态消息：" + response.statusMessage());
            System.out.println("响应结果体：" + response.body());
            
            testContext.completeNow();
        }
    }
    
    private void exec(Vertx vertx,
                      VertxTestContext testContext,
                      @NotNull Function<WebClient, HttpRequest<Buffer>> requestFun) {
        WebClient client = WebClient.create(vertx);
        
        requestFun.apply(client).send(ar -> responseHandler(testContext, ar));
    }
    
    /**
     * ꧁梦开始的地方꧂  测试
     */
    @Test
    public void getDemo(Vertx vertx, VertxTestContext testContext) {
        exec(vertx, testContext, client -> client.get(8080, "127.0.0.1", "/test/demo"));
    }
    
    /**
     * get 查询参数解析测试
     */
    @Test
    public void getAndQueryParam(Vertx vertx, VertxTestContext testContext) {
        exec(vertx, testContext,
                client -> client.get(8080, "127.0.0.1", "/test/get")
                        .addQueryParam("name", "hello word"));
    }
    
    /**
     * 路径变量测试
     */
    @Test
    public void pathVariable(Vertx vertx, VertxTestContext testContext) {
        exec(vertx, testContext, client -> client.get(8080, "127.0.0.1", "/test/mirage/1"));
    }
    
    /**
     * 请求异常测试
     */
    @Test
    public void exception(Vertx vertx, VertxTestContext testContext) {
        exec(vertx, testContext, client -> client.get(8080, "127.0.0.1", "/test/ex"));
    }
    
    /**
     * 参数对象校验异常测试
     */
    @Test
    public void validationBean(Vertx vertx, VertxTestContext testContext) {
        WebClient.create(vertx)
                .post(8080, "127.0.0.1", "/test/validation")
                .sendJsonObject(new JsonObject().put("name", "666").put("value", ""),
                        ar -> responseHandler(testContext, ar));
    }
    
    
    /**
     * get 查询参数解析测试
     */
    @Test
    public void queryParams(Vertx vertx, VertxTestContext testContext) {
        exec(vertx, testContext,
                client -> client.get(8080, "127.0.0.1", "/test/queryParams")
                        .addQueryParam("name", "hello word")
                        .addQueryParam("value", "666")
        );
    }
    
    /**
     * post 表单参数解析测试
     */
    @Test
    public void form(Vertx vertx, VertxTestContext testContext) {
        WebClient.create(vertx)
                .post(8080, "127.0.0.1", "/test/form")
                .sendForm(MultiMap.caseInsensitiveMultiMap().add("name", "hello word"),
                        ar -> responseHandler(testContext, ar));
    }
    
    /**
     * 文件上传测试
     */
    @Test
    public void fileUpload(Vertx vertx, VertxTestContext testContext) {
        String parentProjectPath = Paths.get("").toAbsolutePath().getParent().toString();
        
        String name = "readme";
        String filename = "README.md";
        String pathname = parentProjectPath + File.separator + filename;
        
        WebClient.create(vertx)
                .post(8080, "127.0.0.1", "/test/fileUpload")
                .sendMultipartForm(
                        MultipartForm.create()
                                .binaryFileUpload(name, filename, pathname, MediaType.TEXT_MARKDOWN_VALUE),
                        ar -> responseHandler(testContext, ar)
                );
    }
    
    /**
     * 文件上传带参数测试
     */
    @Test
    public void fileUploadWithParams(Vertx vertx, VertxTestContext testContext) {
        String parentProjectPath = Paths.get("").toAbsolutePath().getParent().toString();
        
        String name = "readme";
        String filename = "README.md";
        String pathname = parentProjectPath + File.separator + filename;
        
        WebClient.create(vertx)
                .post(8080, "127.0.0.1", "/test/fileUpload-withParams")
                .sendMultipartForm(
                        MultipartForm.create()
                                .attribute("options", "6666")
                                .binaryFileUpload(name, filename, pathname, MediaType.TEXT_MARKDOWN_VALUE)
                        ,
                        ar -> responseHandler(testContext, ar)
                );
    }
    
    
    /**
     * 请求头注解
     */
    @Test
    public void header(Vertx vertx, VertxTestContext testContext) {
        exec(vertx, testContext, client -> client.get(8080, "127.0.0.1", "/test/header"));
    }
    
    
    /**
     * 测试路径重复 请求类型不同
     */
    @Test
    public void restful(Vertx vertx, VertxTestContext testContext) {
        exec(vertx, testContext, client -> client.get(8080, "127.0.0.1", "/test/restful"));
    }
}
