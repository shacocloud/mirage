package cc.shacocloud.mirage.env;

import io.vertx.config.ConfigRetriever;
import io.vertx.config.ConfigRetrieverOptions;
import io.vertx.config.ConfigStoreOptions;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonObject;
import io.vertx.junit5.VertxExtension;
import io.vertx.junit5.VertxTestContext;
import org.jetbrains.annotations.NotNull;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

/**
 * @author 思追(shaco)
 */
@ExtendWith(VertxExtension.class)
public class VertxConfigTest {
    
    /**
     * 测试 yaml 格式配置
     */
    @Test
    public void yamlClasspathConfig(@NotNull Vertx vertx,
                                    @NotNull VertxTestContext testContext) {
        
        ConfigStoreOptions store = new ConfigStoreOptions()
                .setType("classpath")
                .setFormat("yaml")
                .setConfig(new JsonObject().put("classpath", "test.yaml"));
        
        ConfigRetriever retriever = ConfigRetriever.create(vertx,
                new ConfigRetrieverOptions().addStore(store));
        
        retriever.getConfig(result -> {
            if (result.succeeded()) {
                System.out.println(result.result());
                testContext.completeNow();
            } else {
                testContext.failNow(result.cause());
            }
        });
    }
}
