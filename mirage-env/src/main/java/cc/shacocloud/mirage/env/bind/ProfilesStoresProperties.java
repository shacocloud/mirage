package cc.shacocloud.mirage.env.bind;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Map;

/**
 * 环境配置存储属性
 *
 * @author 思追(shaco)
 */
@Setter
@Getter
@NoArgsConstructor
public class ProfilesStoresProperties {
    
    /**
     * 该配置存储是否是可选的，默认为 false 必须存在，如果不存在将抛出例外。
     */
    private Boolean optional = false;
    
    /**
     * 环境配置存储路径
     * <p>
     * <ul>
     *  <li>
     *    classpath:// 为前缀的表示为类路径存储，根路径为 src/main/resources/。
     *    例如：classpath://application-dev.yaml，表示为为 src/main/resources/application-dev.yaml
     *  </li>
     *  <li>
     *    file:// 为前缀的表示为文件系统存储，该路径为绝对路径。
     *    例如：file:///opt/app/mirage/application-dev.yaml，表示为当前系统文件 /opt/app/mirage/application-dev.yaml
     *  </li>
     *  <li>
     *    http:// 或 https:// 为前缀的表示为远端存储，需要发送请求来获取配置文件。
     *    例如：https://gitee.com/lulihu/mirage-demo/raw/master/mirage-kotlin-demo/src/main/resources/application-dev1.yaml
     *  </li>
     * </ul>
     * <p>
     * 环境配置存储路径目前支持三种文件后缀：分别是
     * <ul>
     *  <li>yaml 或者 yml 后缀都作为 yaml 格式</li>
     *  <li>properties 后缀作为 properties 格式</li>
     *  <li>json 后缀都作为 json 格式</li>
     * </ul>
     */
    private String path;
    
    /**
     * 请求头配置，当 {@link #path} 是 http:// 或 https:// 前缀时生效
     */
    private Map<String, String> headers;
    
}
