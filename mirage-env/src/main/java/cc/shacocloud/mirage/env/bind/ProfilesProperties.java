package cc.shacocloud.mirage.env.bind;

import cc.shacocloud.mirage.env.util.ProfilesStoresUtil;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

/**
 * 环境配置文件属性
 *
 * @author 思追(shaco)
 */
@Setter
@Getter
@NoArgsConstructor
public class ProfilesProperties {
    
    /**
     * 环境配置id
     *
     * @see EnvironmentProperties#getActive()
     */
    private String id;
    
    /**
     * 配置存储信息
     *
     * @see ProfilesStoresUtil#parse(List)
     */
    private List<ProfilesStoresProperties> stores;
    
}
