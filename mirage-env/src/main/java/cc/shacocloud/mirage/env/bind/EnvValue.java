package cc.shacocloud.mirage.env.bind;

import cc.shacocloud.mirage.utils.PropertyPlaceholderHelper;

import java.lang.annotation.*;
import java.lang.reflect.Method;

/**
 * 环境属性注解，用它定义在属性或者方法上，将在应用初始化前注入该属性值
 * <p>
 * 属性示例：
 * <pre>
 *  &#064;Component
 *  public class TestBean {
 *
 *  &#064;EnvValue("${mirage.environment.active:dev}")
 *   private String active;
 *
 *  }
 * </pre>
 * <p>
 * 方法示例：
 * <pre>
 *  &#064;Component
 *  public class TestBean {
 *
 *   private String active;
 *
 *   &#064;EnvValue("${mirage.environment.active:dev}")
 *   public void setActive(String active){
 *       this.active = active;
 *   }
 *
 *  }
 * </pre>
 * 需要注意的是，方法必须是 set 方法需要满足 {@link cc.shacocloud.mirage.utils.ClassUtil#isSetMethod(Method)} 的判断
 *
 * @author 思追(shaco)
 * @see PropertyPlaceholderHelper
 */
@Target({ElementType.FIELD, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface EnvValue {
    
    /**
     * 环境值表达式，格式 {@code ${表达式:默认值}}，如果表达式结果不存在且未设置默认值时，将抛出例外
     * <p>
     * 需要注意的是表达式解析的结果只支持字符串类型，如果是结构化的比如 object 或 array 类型无法处理，请使用 {@link ConfigurationProperties} 注解
     *
     * @see PropertyPlaceholderHelper
     */
    String value();
    
}
