package cc.shacocloud.mirage.env.support;

import cc.shacocloud.mirage.env.ConfigStoreFormatEnum;
import cc.shacocloud.mirage.env.ConfigStoreTypeProperties;

/**
 * {@code properties} 格式的配置文件
 *
 * @author 思追(shaco)
 */
public class PropertiesConfigStore extends AbstractConfigStore {
    
    public PropertiesConfigStore(ConfigStoreTypeProperties storeProperties) {
        super(storeProperties);
    }
    
    public PropertiesConfigStore(ConfigStoreTypeProperties storeProperties, boolean optional) {
        super(storeProperties, optional);
    }
    
    @Override
    public ConfigStoreFormatEnum getFormat() {
        return ConfigStoreFormatEnum.properties;
    }
    
}
