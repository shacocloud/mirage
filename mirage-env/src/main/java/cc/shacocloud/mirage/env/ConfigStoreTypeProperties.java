package cc.shacocloud.mirage.env;

import io.vertx.core.json.JsonObject;

/**
 * 配置存储类型参数
 *
 * @author 思追(shaco)
 */
public interface ConfigStoreTypeProperties {
    
    /**
     * 获取配置存储类型
     */
    ConfigStoreTypeEnum getType();
    
    /**
     * 获取配置存储类型属性
     */
    JsonObject getProperties();
    
}
