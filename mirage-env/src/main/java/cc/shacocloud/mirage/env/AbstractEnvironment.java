package cc.shacocloud.mirage.env;

import cc.shacocloud.mirage.env.bind.BindEnvProperties;
import cc.shacocloud.mirage.env.bind.EnvironmentProperties;
import cc.shacocloud.mirage.env.support.*;
import cc.shacocloud.mirage.env.util.ProfilesStoresUtil;
import cc.shacocloud.mirage.env.util.YamlUtil;
import cc.shacocloud.mirage.utils.AppUtil;
import cc.shacocloud.mirage.utils.PropertyPlaceholderHelper;
import cc.shacocloud.mirage.utils.charSequence.StrUtil;
import cc.shacocloud.mirage.utils.converter.TypeConverter;
import cc.shacocloud.mirage.utils.converter.TypeConverterSupportImpl;
import cc.shacocloud.mirage.utils.converter.TypeDescriptor;
import cc.shacocloud.mirage.utils.resource.ClassPathResource;
import cc.shacocloud.mirage.utils.resource.FileSystemResource;
import io.vertx.config.ConfigRetrieverOptions;
import io.vertx.config.ConfigStoreOptions;
import io.vertx.core.Future;
import io.vertx.core.json.JsonObject;
import lombok.extern.slf4j.Slf4j;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.File;
import java.util.*;
import java.util.stream.Collectors;

/**
 * 抽象的环境配置对象
 *
 * @author 思追(shaco)
 */
@Slf4j
public abstract class AbstractEnvironment implements ConfigurableEnvironment {
    
    /**
     * 环境配置存储文件
     * <p>
     * 该配置文件优先加载，用于判断当前环境使用的配置文件，该配置文件中可以方法应用启动参数和系统环境变量来进行变量覆盖
     *
     * @see #strictHelper
     * @see #nonStrictHelper
     */
    public static final String[] ENVIRONMENT_FILE = new String[]{"environment.properties", "environment.json", "environment.yaml"};
    /**
     * 应用配置存储文件
     *
     * @see #strictHelper
     * @see #nonStrictHelper
     */
    public static final String[] APPLICATION_FILE = new String[]{"application.properties", "application.json", "application.yaml"};
    /**
     * 配置对象结构化分隔符
     *
     * @see #structuredConfig(JsonObject)
     */
    public static final String CONFIG_KEY_SPLIT = StrUtil.DOT;
    private final LinkedList<ConfigStore> configStores = new LinkedList<>();
    // 不严格，可以为空
    @NotNull
    private final PropertyPlaceholderHelper nonStrictHelper = new PropertyPlaceholderHelper("${", "}", ":", true);
    // 严格，不可以为空
    @NotNull
    private final PropertyPlaceholderHelper strictHelper = new PropertyPlaceholderHelper("${", "}", ":", false);
    @Nullable
    private TypeConverter typeConverter;
    // 当前的配置对象
    private JsonObject currentConfig = new JsonObject();
    private volatile boolean isInit = false;
    // 配置检索配置项
    private ConfigRetrieverOptions retrieverOptions;
    // 环境配置对象
    private EnvironmentProperties environmentProperties;
    
    /**
     * 更新配置信息
     *
     * @param config 配置对象
     */
    protected Future<Void> updateConfig(@NotNull JsonObject config) {
        JsonObject oldConfig = this.currentConfig;
        this.currentConfig = structuredConfig(config);
        
        if (!oldConfig.equals(currentConfig)) {
            return configChange(oldConfig, currentConfig);
        }
        
        return Future.succeededFuture();
    }
    
    /**
     * 配置变更由子类实现处理
     */
    protected Future<Void> configChange(JsonObject oldConfig,
                                        JsonObject newConfig) {
        return Future.succeededFuture();
    }
    
    /**
     * 获取当前的配置存储信息
     */
    public List<ConfigStore> getConfigStores() {
        return Collections.unmodifiableList(configStores);
    }
    
    @Override
    public synchronized void init() {
        if (isInit) {
            throw new IllegalArgumentException("应用配置不允许重复初始化！");
        }
        
        // 注册默认配置信息
        registerDefaultLocationConfig();
        
        // 加载配置存储
        loadConfigStores();
        
        doInit();
        
        isInit = true;
    }
    
    /**
     * 加载配置存储信息
     */
    protected void loadConfigStores() {
        JsonObject config = new JsonObject();
        
        // 加载环境配置信息
        for (String file : ENVIRONMENT_FILE) {
            ClassPathResource resource = new ClassPathResource(file);
            if (resource.exists()) {
                try {
                    config = config.mergeIn(new JsonObject(YamlUtil.loadAll(resource)), true);
                } catch (Exception e) {
                    throw new RuntimeException(String.format("加载类路径环境配置文件 %s 发生未知的例外！", file), e);
                }
            }
        }
        
        if (AppUtil.isJarRun()) {
            File startDir = AppUtil.getStartDir(getClass());
            for (String file : ENVIRONMENT_FILE) {
                FileSystemResource resource = new FileSystemResource(new File(startDir, file));
                if (resource.exists()) {
                    try {
                        config = config.mergeIn(new JsonObject(YamlUtil.loadAll(resource)), true);
                    } catch (Exception e) {
                        throw new RuntimeException(String.format("加载文件路径环境配置文件 %s 发生未知的例外！", resource.getPath()), e);
                    }
                }
            }
        }
        
        // 加载应用外部配置信息，进行合并操作
        config = config.mergeIn(loadAppExternalConfig(), true);
        
        // 配置结构化处理
        currentConfig = structuredConfig(config);
        
        // 绑定环境配置信息
        environmentProperties = BindEnvProperties.bind(this, EnvironmentProperties.class, getTypeConverter());
        
        String active = environmentProperties.getActive();
        
        // 注册环境配置中的存储配置
        if (StrUtil.isNotBlank(active)) {
            environmentProperties.getProfiles().stream()
                    .filter(p -> active.equals(p.getId()))
                    .map(p -> ProfilesStoresUtil.parse(p.getStores()))
                    .flatMap(Collection::stream)
                    .forEach(this::registerConfig);
        }
        
        if (log.isInfoEnabled()) {
            log.info("应用配置工厂初始化，激活的配置环境：{}", StrUtil.isNotBlank(active) ? active : "none");
        }
    }
    
    /**
     * 注册默认配置
     * <p>
     * 默认加载为类路径的 {@link #APPLICATION_FILE}
     * 考虑应用扩展额外加载应用启动路径下的 {@link #APPLICATION_FILE}
     *
     * @see #APPLICATION_FILE
     */
    protected void registerDefaultLocationConfig() {
        // 加载环境配置信息
        registerDefaultLocationConfig(ENVIRONMENT_FILE);
        
        // 加载应用配置信息
        registerDefaultLocationConfig(APPLICATION_FILE);
        
        // 加载拓展信息
        registerConfig(new JsonConfigStore(loadAppExternalConfig().getMap()));
    }
    
    /**
     * 注册默认位置的配置文件，即 加载类路径和 jar 模式下的启动路径
     */
    protected void registerDefaultLocationConfig(@NotNull String @NotNull ... confFiles) {
        // 类路径
        for (String file : confFiles) {
            ClassPathResource resource = new ClassPathResource(file);
            if (resource.exists()) {
                ClasspathTypeProperties properties = new ClasspathTypeProperties(file);
                registerSupportTypeConfig(file, properties);
            }
        }
        
        // 如果是jar运行则包含启动路径
        if (AppUtil.isJarRun()) {
            File startDir = AppUtil.getStartDir(getClass());
            for (String file : confFiles) {
                FileSystemResource fileSystemResource = new FileSystemResource(new File(startDir, file));
                if (fileSystemResource.exists()) {
                    FileTypeProperties properties = new FileTypeProperties(fileSystemResource.getFile());
                    registerSupportTypeConfig(file, properties);
                }
            }
        }
    }
    
    /**
     * 注册支持类型的配置存储
     */
    protected void registerSupportTypeConfig(String file, ConfigStoreTypeProperties properties) {
        if (StrUtil.endWith(file, "properties")) {
            registerConfig(new PropertiesConfigStore(properties, true));
        } else if (StrUtil.endWith(file, "json")) {
            registerConfig(new JsonConfigStore(properties, true));
        } else if (StrUtil.endWith(file, "yaml") || StrUtil.endWith(file, "yml")) {
            registerConfig(new YamlConfigStore(properties, true));
        } else {
            throw new IllegalArgumentException("不支持的配置文件类型：" + file);
        }
    }
    
    /**
     * 加载应用外部配置
     * <p>
     * 例如： <br/>
     * jvm参数 {@link System#getProperties()} <br/>
     * 环境配置 {@link System#getenv()} <br/>
     * 等等，子类也可自行拓展
     */
    protected JsonObject loadAppExternalConfig() {
        JsonObject appExternalConfig = new JsonObject();
        
        // jvm 参数
        Map<String, Object> systemProperties = System.getProperties().entrySet().stream().collect(Collectors.toMap(e -> e.getKey().toString(), Map.Entry::getValue));
        appExternalConfig = appExternalConfig.mergeIn(new JsonObject(systemProperties), true);
        
        // 环境配置
        Map<String, Object> systemEnv = System.getenv().entrySet().stream().collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
        appExternalConfig = appExternalConfig.mergeIn(new JsonObject(systemEnv), true);
        
        return appExternalConfig;
    }
    
    /**
     * 获取 {@link ConfigRetrieverOptions} 配置信息
     */
    @NotNull
    public ConfigRetrieverOptions getRetrieverOptions() {
        if (Objects.isNull(this.retrieverOptions)) {
            this.retrieverOptions = doRetrieverOptions();
        }
        
        return this.retrieverOptions;
    }
    
    /**
     * 加载 {@link ConfigRetrieverOptions} 配置信息
     */
    @NotNull
    protected ConfigRetrieverOptions doRetrieverOptions() {
        ConfigRetrieverOptions retrieverOptions = new ConfigRetrieverOptions();
        retrieverOptions.setIncludeDefaultStores(false);
        
        // 默认不自动扫描更新
        retrieverOptions.setScanPeriod(-1);
        
        for (ConfigStore configStore : getConfigStores()) {
            ConfigStoreOptions configStoreOptions = configStore.getConfigStoreOptions();
            retrieverOptions.addStore(configStoreOptions);
        }
        
        return retrieverOptions;
    }
    
    /**
     * 执行内部的初始化逻辑，由子类提供
     */
    protected abstract void doInit();
    
    /**
     * 获取环境配置信息
     */
    protected EnvironmentProperties getEnvironmentProperties() {
        if (Objects.isNull(environmentProperties)) {
            throw new IllegalStateException("应用环境配置信息未初始化，无法访问！");
        }
        
        return environmentProperties;
    }
    
    @NotNull
    public TypeConverter getTypeConverter() {
        if (Objects.isNull(this.typeConverter)) {
            this.typeConverter = new TypeConverterSupportImpl();
        }
        return this.typeConverter;
    }
    
    /**
     * 设置环境配置的默认类型转换器
     */
    public void setTypeConverter(@NotNull TypeConverter typeConverter) {
        this.typeConverter = typeConverter;
    }
    
    /**
     * 结构化配置信息，将扁平的配置属性改为立体的结构以方便查询，并且将中划线配置转为驼峰方式
     * <p>
     * 例如：
     * <pre>
     *   {
     *      mirage.environment.active=dev
     *   }
     * </pre>
     * 结构化后：
     * <pre>
     *   {
     *       mirage: {
     *           environment: {
     *               active: dev
     *           }
     *       }
     *   }
     * </pre>
     * 配置键拆分基于 {@link #CONFIG_KEY_SPLIT} 拆分
     *
     * @param config 待结构化的配置
     * @return 结构化的配置对象
     * @see #formatKey
     */
    protected JsonObject structuredConfig(@NotNull JsonObject config) {
        JsonObject result = new JsonObject();
        
        for (Map.Entry<String, Object> entry : config) {
            String key = formatKey(entry.getKey());
            Object value = entry.getValue();
            
            // 值继续递归
            if (value instanceof JsonObject) {
                value = structuredConfig((JsonObject) value);
            }
            
            JsonObject currentConf = result;
            
            List<String> split = StrUtil.split(key, CONFIG_KEY_SPLIT, 0, true, true);
            int size = split.size();
            for (int i = 0; i < size; i++) {
                String k = split.get(i);
                
                // 到达结尾
                if (i + 1 == size) {
                    currentConf.put(k, value);
                } else {
                    Object oldValue = currentConf.getValue(k);
                    if (Objects.isNull(oldValue) || !(oldValue instanceof JsonObject)) {
                        JsonObject entries = new JsonObject();
                        currentConf.put(k, entries);
                        currentConf = entries;
                    } else {
                        currentConf = (JsonObject) oldValue;
                    }
                }
            }
            
        }
        
        return result;
    }
    
    // ------------- 委托
    
    @Override
    public void registerConfig(ConfigStore configStore) {
        checkInit();
        configStores.addLast(configStore);
    }
    
    @Override
    public void registerConfigToHead(ConfigStore configStore) {
        checkInit();
        configStores.addFirst(configStore);
    }
    
    @Override
    public void registerConfigBeforeOf(ConfigStore configStore, ConfigStore target) {
        checkInit();
        
        int i = configStores.indexOf(target);
        if (i <= 0) {
            registerConfigToHead(configStore);
        } else {
            configStores.add(i, configStore);
        }
    }
    
    @Override
    public void registerConfigAfterOf(ConfigStore configStore, ConfigStore target) {
        checkInit();
        int i = configStores.indexOf(target);
        if (i <= 0) {
            registerConfig(configStore);
        } else {
            configStores.add(i, configStore);
        }
    }
    
    @Override
    public <T> @Nullable T getProperty(String key, TypeDescriptor typeDescriptor) {
        key = formatKey(key);
        
        T value = getPropertyValue(key);
        if (Objects.isNull(value)) {
            return null;
        }
        // Object 类型直接返回
        else if (Object.class.equals(typeDescriptor.getType())) {
            return value;
        } else {
            return getTypeConverter().convertIfNecessary(value, typeDescriptor);
        }
    }
    
    @Override
    public @NotNull String resolvePlaceholders(@NotNull String text) {
        return nonStrictHelper.replacePlaceholders(text, placeholderName -> getProperty(placeholderName, String.class));
    }
    
    @Override
    public @NotNull String resolveRequiredPlaceholders(@NotNull String text) throws IllegalArgumentException {
        return strictHelper.replacePlaceholders(text, placeholderName -> getProperty(placeholderName, String.class));
    }
    
    protected void checkInit() {
        if (isInit) {
            throw new IllegalArgumentException("配置对象初始化后，不允许在继续添加配置存储信息");
        }
    }
    
    /**
     * 获取指定配置键的属性值，多层嵌套使用 {@link #CONFIG_KEY_SPLIT} 分割
     */
    @Nullable
    @SuppressWarnings("unchecked")
    public <T> T getPropertyValue(String key) {
        JsonObject config = this.currentConfig;
        
        List<String> split = StrUtil.split(key, CONFIG_KEY_SPLIT, 0, true, true);
        int size = split.size();
        
        T result = null;
        for (int i = 0; i < size; i++) {
            String k = split.get(i);
            
            // 到达结尾
            if (i + 1 == size) {
                result = (T) config.getValue(k);
            } else {
                Object value = config.getValue(k);
                if (value instanceof JsonObject) {
                    config = (JsonObject) value;
                } else {
                    return null;
                }
            }
        }
        
        return result;
    }
    
    /**
     * 格式化配置键，将中划线模式改为驼峰模式
     */
    protected String formatKey(String key) {
        return StrUtil.toCamelCase(key, '-');
    }
}
