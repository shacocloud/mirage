package cc.shacocloud.mirage.env.util;

import cc.shacocloud.mirage.env.ConfigStore;
import cc.shacocloud.mirage.env.ConfigStoreTypeProperties;
import cc.shacocloud.mirage.env.bind.ProfilesProperties;
import cc.shacocloud.mirage.env.bind.ProfilesStoresProperties;
import cc.shacocloud.mirage.env.support.*;
import cc.shacocloud.mirage.utils.charSequence.StrUtil;
import cc.shacocloud.mirage.utils.collection.CollUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * 配置文件存储解析工具
 *
 * @author 思追(shaco)
 */
public class ProfilesStoresUtil {
    
    public static final String CLASSPATH_PREFIX = "classpath://";
    public static final String FILE_PREFIX = "file://";
    public static final String HTTP_PREFIX = "http://";
    public static final String HTTPS_PREFIX = "https://";
    
    public static final String PROPERTIES_SUFFIX = ".properties";
    public static final String JSON_SUFFIX = ".json";
    public static final String YAML_SUFFIX = ".yaml";
    public static final String YML_SUFFIX = ".yml";
    
    /**
     * 解析配置存储信息
     *
     * @param stores 存储配置
     * @return 存储配置对象{@link ConfigStore}
     * @see ProfilesProperties#getStores()
     */
    @NotNull
    public static List<ConfigStore> parse(@Nullable List<ProfilesStoresProperties> stores) {
        if (CollUtil.isEmpty(stores)) return Collections.emptyList();
        
        List<ConfigStore> configStores = new ArrayList<>(stores.size());
        
        for (ProfilesStoresProperties store : stores) {
            String path = store.getPath();
            
            ConfigStoreTypeProperties storeTypeProperties;
            // 类路径
            if (StrUtil.startWith(path, CLASSPATH_PREFIX)) {
                storeTypeProperties = new ClasspathTypeProperties(StrUtil.removePrefix(path, CLASSPATH_PREFIX));
            }
            // 文件系统路径
            else if (StrUtil.startWith(path, FILE_PREFIX)) {
                storeTypeProperties = new FileTypeProperties(StrUtil.removePrefix(path, FILE_PREFIX));
            }
            // http 远端路径
            else if (StrUtil.startWith(path, HTTP_PREFIX) || StrUtil.startWith(path, HTTPS_PREFIX)) {
                try {
                    storeTypeProperties = HttpTypeProperties.of(path, store.getHeaders());
                } catch (URISyntaxException e) {
                    throw new IllegalArgumentException(String.format("远端请求路径地址不合法：%s", path), e);
                }
            } else {
                throw new IllegalStateException(String.format("无效的配置文件前缀，目前只支持 %s，%s，%s，%s", CLASSPATH_PREFIX,
                        FILE_PREFIX, HTTP_PREFIX, HTTPS_PREFIX));
            }
            
            ConfigStore configStore;
            
            // properties 文件格式
            if (StrUtil.endWith(path, PROPERTIES_SUFFIX)) {
                configStore = new PropertiesConfigStore(storeTypeProperties, store.getOptional());
            }
            // json 文件格式
            else if (StrUtil.endWith(path, JSON_SUFFIX)) {
                configStore = new JsonConfigStore(storeTypeProperties, store.getOptional());
            }
            // yaml 文件格式
            else if (StrUtil.endWith(path, YAML_SUFFIX) || StrUtil.endWith(path, YML_SUFFIX)) {
                configStore = new YamlConfigStore(storeTypeProperties, store.getOptional());
            } else {
                throw new IllegalStateException(String.format("无效的配置文件后缀名，目前只支持 %s，%s，%s，%s", JSON_SUFFIX,
                        PROPERTIES_SUFFIX, YAML_SUFFIX, YML_SUFFIX));
            }
            
            configStores.add(configStore);
        }
        
        return configStores;
    }
    
}
