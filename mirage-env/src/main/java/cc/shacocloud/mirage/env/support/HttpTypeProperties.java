package cc.shacocloud.mirage.env.support;

import cc.shacocloud.mirage.env.ConfigStoreTypeEnum;
import cc.shacocloud.mirage.env.ConfigStoreTypeProperties;
import cc.shacocloud.mirage.utils.charSequence.StrUtil;
import cc.shacocloud.mirage.utils.map.MapUtil;
import io.vertx.core.json.JsonObject;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Map;

/**
 * 基于 http 的存储类型配置
 *
 * @author 思追(shaco)
 */
public class HttpTypeProperties implements ConfigStoreTypeProperties {
    
    private final JsonObject properties;
    
    /**
     * 初始化统参数存储配置
     *
     * @param host    目标服务器地址，ip 或者 域名
     * @param port    目标服务器端口，如果为 -1 则使用 ssl 判断，开启为 443 反之 80
     * @param path    目标请求路径
     * @param ssl     是否基于开启 https
     * @param headers 请求头配置
     */
    public HttpTypeProperties(@NotNull String host,
                              int port,
                              @NotNull String path,
                              boolean ssl,
                              @Nullable Map<String, String> headers) {
        this.properties = new JsonObject();
        this.properties.put("host", host);
        this.properties.put("port", port == -1 ? (ssl ? 443 : 80) : port);
        this.properties.put("path", path);
        this.properties.put("ssl", ssl);
        
        if (MapUtil.isNotEmpty(headers)) {
            this.properties.put("headers", headers);
        }
    }
    
    @NotNull
    public static HttpTypeProperties of(@NotNull String url,
                                        @Nullable Map<String, String> headers) throws URISyntaxException {
        URI uri = new URI(url);
        return new HttpTypeProperties(uri.getHost(), uri.getPort(), uri.getPath(), StrUtil.startWith(url, "https://"), headers);
    }
    
    @Override
    public ConfigStoreTypeEnum getType() {
        return ConfigStoreTypeEnum.http;
    }
    
    @Override
    public JsonObject getProperties() {
        return properties;
    }
}
