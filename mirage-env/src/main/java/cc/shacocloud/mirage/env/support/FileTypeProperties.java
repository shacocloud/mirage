package cc.shacocloud.mirage.env.support;

import cc.shacocloud.mirage.env.ConfigStoreTypeEnum;
import cc.shacocloud.mirage.env.ConfigStoreTypeProperties;
import io.vertx.core.json.JsonObject;
import org.jetbrains.annotations.NotNull;

import java.io.File;

/**
 * 基于文件的存储类型配置
 *
 * @author 思追(shaco)
 */
public class FileTypeProperties implements ConfigStoreTypeProperties {
    
    private final JsonObject properties;
    
    /**
     * 初始化
     *
     * @param path 文件路径，相对或者绝对的
     */
    public FileTypeProperties(@NotNull String path) {
        this.properties = new JsonObject();
        this.properties.put("path", path);
    }
    
    /**
     * 初始化
     *
     * @param file 文件对象
     */
    public FileTypeProperties(@NotNull File file) {
        this.properties = new JsonObject();
        this.properties.put("path", file.getAbsolutePath());
    }
    
    @Override
    public ConfigStoreTypeEnum getType() {
        return ConfigStoreTypeEnum.file;
    }
    
    @Override
    public JsonObject getProperties() {
        return properties;
    }
}
