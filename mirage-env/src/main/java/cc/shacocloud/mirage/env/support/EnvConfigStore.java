package cc.shacocloud.mirage.env.support;

import cc.shacocloud.mirage.env.ConfigStoreFormatEnum;

/**
 * 系统环境变量配置存储
 *
 * @author 思追(shaco)
 */
public class EnvConfigStore extends AbstractConfigStore {
    
    public EnvConfigStore() {
        super(new EnvTypeProperties());
    }
    
    @Override
    public ConfigStoreFormatEnum getFormat() {
        return ConfigStoreFormatEnum.no;
    }
}
