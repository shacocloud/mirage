package cc.shacocloud.mirage.env.bind;

import cc.shacocloud.mirage.bean.bind.Component;
import cc.shacocloud.mirage.env.Environment;
import cc.shacocloud.mirage.utils.ClassUtil;
import cc.shacocloud.mirage.utils.annotation.AliasFor;

import java.lang.annotation.*;
import java.lang.reflect.Method;

/**
 * 配置属性注解，将其定义在对象上，该对象将从 {@link Environment} 加载配置信息
 * <p>
 * 该对象加载配置对象时使用反射 set 方法进行注入，不通过属性注入值，请注意 必须为你需要注入的属性设置 set 方法。
 * 注入的属性值等于 {@link ConfigurationProperties#prefix()} 配置的前缀加 set 方法去掉 set 的名称。
 * <p>
 * 例如：
 * <pre code="java">
 * &#064;ConfigurationProperties(prefix = "mirage.environment")
 * public class EnvironmentProperties {
 *     private String active;
 *     public String getActive() {
 *         return active;
 *     }
 *     public void setActive(String active) {
 *         this.active = active;
 *     }
 * }
 * </pre>
 * 以上例子将使用 mirage.environment.active 调用 setActive 方法进行注入
 * <p>
 * 注意：set 方法必须满足以下几个条件
 * <ul>
 *     <li>方法名称必须以 set 为前缀，且 set 的下一个字符大写</li>
 *     <li>方法必须只有一个入参</li>
 *     <li>方法的返回结果必须为 void 或者 Void </li>
 * </ul>
 *
 * @author 思追(shaco)
 * @see ClassUtil#isSetMethod(Method)
 */
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Component(lazy = false)
public @interface ConfigurationProperties {
    
    /**
     * 配置的前缀
     *
     * @see #prefix()
     */
    @AliasFor("prefix")
    String value() default "";
    
    /**
     * 配置的前缀
     *
     * @see #value()
     */
    @AliasFor("value")
    String prefix() default "";
    
    /**
     * 是否忽略无效的绑定字段，通常来说 无效表示的是无法进行类型装的字段。
     *
     * @return true 忽略，反之将抛出例外
     */
    boolean ignoreInvalidFields() default false;
    
    /**
     * 是否忽略未知的字段，通常来说 未知表示无法找到该配置信息
     *
     * @return true 忽略，什么也不做，反之将抛出例外
     */
    boolean ignoreUnknownFields() default true;
    
}
