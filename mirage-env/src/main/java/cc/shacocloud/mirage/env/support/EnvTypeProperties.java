package cc.shacocloud.mirage.env.support;

import cc.shacocloud.mirage.env.ConfigStoreTypeEnum;
import cc.shacocloud.mirage.env.ConfigStoreTypeProperties;
import io.vertx.core.json.JsonObject;

/**
 * 基于系统环境变量的存储类型配置
 *
 * @author 思追(shaco)
 */
public class EnvTypeProperties implements ConfigStoreTypeProperties {
    
    private final JsonObject properties;
    
    public EnvTypeProperties() {
        this.properties = new JsonObject();
    }
    
    @Override
    public ConfigStoreTypeEnum getType() {
        return ConfigStoreTypeEnum.env;
    }
    
    @Override
    public JsonObject getProperties() {
        return properties;
    }
}
