package cc.shacocloud.mirage.env;

import cc.shacocloud.mirage.utils.converter.TypeDescriptor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Objects;

/**
 * 表示当前正在运行的应用环境接口
 *
 * @author 思追(shaco)
 */
public interface Environment {
    
    /**
     * 返回给定的键是否存在，即 {@link #getProperty(String)} 是否为 null
     */
    default boolean containsProperty(@NotNull String key) {
        return Objects.nonNull(getProperty(key));
    }
    
    /**
     * 返回与给定键关联的属性值，如果无法解析键，则返回 {@code null}
     *
     * @param key 键
     * @see #getProperty(String, Object)
     * @see #getProperty(String, Class)
     * @see #getRequiredProperty(String)
     */
    @Nullable
    default Object getProperty(@NotNull String key) {
        return getProperty(key, Object.class);
    }
    
    /**
     * 返回与给定键关联的属性值，如果无法解析键，则返回 {@code defaultValue}
     *
     * @param key          键
     * @param defaultValue 如果未匹配则返回该默认值
     * @see #getRequiredProperty(String)
     * @see #getProperty(String, Class)
     */
    @NotNull
    default Object getProperty(@NotNull String key, @NotNull Object defaultValue) {
        Object property = getProperty(key);
        return Objects.isNull(property) ? defaultValue : property;
    }
    
    /**
     * 返回与给定键关联的属性值，如果无法解析键，则返回 {@code null}
     *
     * @param key        键
     * @param targetType 值类型，将值转为该类型
     * @see #getRequiredProperty(String, Class)
     */
    @Nullable
    default <T> T getProperty(String key, Class<T> targetType) {
        return getProperty(key, TypeDescriptor.valueOf(targetType));
    }
    
    /**
     * 返回与给定键关联的属性值，如果无法解析键，则返回 {@code null}
     *
     * @param key            键
     * @param typeDescriptor 类型描述，将值转为该类型
     * @see #getRequiredProperty(String, Class)
     */
    @Nullable <T> T getProperty(String key, TypeDescriptor typeDescriptor);
    
    /**
     * 返回与给定键关联的属性值，如果无法解析键，则返回 {@code defaultValue}
     *
     * @param key          键
     * @param targetType   值类型，将值转为该类型
     * @param defaultValue 如果未匹配则返回该默认值
     * @see #getRequiredProperty(String, Class)
     */
    @NotNull
    default <T> T getProperty(String key, Class<T> targetType, T defaultValue) {
        T property = getProperty(key, targetType);
        return Objects.isNull(property) ? defaultValue : property;
    }
    
    /**
     * 返回与给定键关联的属性值，如果无法解析键，则返回 {@code defaultValue}
     *
     * @param key            键
     * @param typeDescriptor 类型描述，将值转为该类型
     * @param defaultValue   如果未匹配则返回该默认值
     * @see #getRequiredProperty(String, Class)
     */
    @NotNull
    default <T> T getProperty(String key, TypeDescriptor typeDescriptor, T defaultValue) {
        T property = getProperty(key, typeDescriptor);
        return Objects.isNull(property) ? defaultValue : property;
    }
    
    /**
     * 返回与给定键关联的属性值
     *
     * @throws IllegalStateException 如果属性键不存在则抛出例外
     * @see #getRequiredProperty(String, Class)
     */
    @NotNull
    default Object getRequiredProperty(String key) throws IllegalStateException {
        Object property = getProperty(key);
        
        if (Objects.isNull(property)) {
            throw new IllegalStateException(String.format("无法在当前环境中匹配键：%s", key));
        }
        
        return property;
    }
    
    /**
     * 返回与给定键关联的属性值
     *
     * @throws IllegalStateException 如果属性键不存在则抛出例外
     */
    @NotNull
    default <T> T getRequiredProperty(String key, Class<T> targetType) throws IllegalStateException {
        T property = getProperty(key, targetType);
        
        if (Objects.isNull(property)) {
            throw new IllegalStateException(String.format("无法在当前环境中匹配键：%s", key));
        }
        
        return property;
    }
    
    /**
     * 返回与给定键关联的属性值
     *
     * @throws IllegalStateException 如果属性键不存在则抛出例外
     */
    @NotNull
    default <T> T getRequiredProperty(String key, TypeDescriptor typeDescriptor) throws IllegalStateException {
        T property = getProperty(key, typeDescriptor);
        
        if (Objects.isNull(property)) {
            throw new IllegalStateException(String.format("无法在当前环境中匹配键：%s", key));
        }
        
        return property;
    }
    
    /**
     * 解析给定文本中的 {@code ${表达式:默认值}} 占位符，将它们替换为由 {@link #getProperty(String)} 解析的相应属性值，
     * 没有默认值的不可解析占位符将被忽略并原封不动地传递
     * <p>
     * 需要注意的是表达式解析的结果只支持字符串类型，如果是结构化的比如 object 或 array 类型无法处理，请使用 {@link #getProperty(String, Object)} 注解
     *
     * @param text 要解析的字符串
     * @return 解析的字符串
     * @see #resolveRequiredPlaceholders
     */
    @NotNull
    String resolvePlaceholders(@NotNull String text);
    
    /**
     * 解析给定文本中的 {@code ${表达式:默认值}} 占位符，将它们替换为由 {@link #getRequiredProperty(String)} 解析的相应属性值，
     * 没有默认值的不可解析占位符将抛出例外
     * <p>
     * 需要注意的是表达式解析的结果只支持字符串类型，如果是结构化的比如 object 或 array 类型无法处理，请使用 {@link #getProperty(String, Object)} 注解
     *
     * @return 解析的字符串
     * @throws IllegalArgumentException 如果文本中的占位符不存在则抛出例外
     */
    @NotNull
    String resolveRequiredPlaceholders(@NotNull String text) throws IllegalArgumentException;
    
}
