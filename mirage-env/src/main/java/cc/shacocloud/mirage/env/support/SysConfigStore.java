package cc.shacocloud.mirage.env.support;

import cc.shacocloud.mirage.env.ConfigStoreFormatEnum;

/**
 * 系统参数配置存储
 *
 * @author 思追(shaco)
 */
public class SysConfigStore extends AbstractConfigStore {
    
    public SysConfigStore() {
        super(new SysTypeProperties());
    }
    
    @Override
    public ConfigStoreFormatEnum getFormat() {
        return ConfigStoreFormatEnum.no;
    }
}
