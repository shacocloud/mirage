package cc.shacocloud.mirage.env.bind;

import cc.shacocloud.mirage.env.Environment;
import cc.shacocloud.mirage.env.exception.UnknownFieldException;
import cc.shacocloud.mirage.utils.ClassUtil;
import cc.shacocloud.mirage.utils.MethodParameter;
import cc.shacocloud.mirage.utils.annotation.AnnotatedElementUtils;
import cc.shacocloud.mirage.utils.charSequence.StrUtil;
import cc.shacocloud.mirage.utils.converter.TypeConverter;
import cc.shacocloud.mirage.utils.converter.TypeDescriptor;
import cc.shacocloud.mirage.utils.reflection.ReflectUtil;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.lang.reflect.Array;
import java.lang.reflect.Method;
import java.util.*;

import static cc.shacocloud.mirage.utils.charSequence.StrUtil.lowerFirst;
import static cc.shacocloud.mirage.utils.charSequence.StrUtil.removePrefix;

/**
 * 绑定环境属性
 *
 * @author 思追(shaco)
 * @see ConfigurationProperties
 */
public class BindEnvProperties<T> {
    
    private static final Object IGNORE = new Object();
    /**
     * 环境对象
     */
    @NotNull
    private final Environment env;
    /**
     * 需要转换的配置类
     */
    @NotNull
    private final T properties;
    /**
     * @see ConfigurationProperties#prefix()
     */
    @NotNull
    private final String prefix;
    /**
     * @see ConfigurationProperties#ignoreInvalidFields()
     */
    private final boolean ignoreInvalidFields;
    /**
     * @see ConfigurationProperties#ignoreUnknownFields()
     */
    private final boolean ignoreUnknownFields;
    /**
     * 类型转换器
     */
    @NotNull
    private final TypeConverter typeConverter;
    
    public BindEnvProperties(@NotNull Environment env,
                             @NotNull T properties,
                             @NotNull ConfigurationProperties configurationProperties,
                             @NotNull TypeConverter typeConverter) {
        this.typeConverter = typeConverter;
        String confPrefix = configurationProperties.prefix();
        this.prefix = confPrefix.endsWith(".") ? StrUtil.removeSuffix(confPrefix, ".") : confPrefix;
        this.ignoreInvalidFields = configurationProperties.ignoreInvalidFields();
        this.ignoreUnknownFields = configurationProperties.ignoreUnknownFields();
        this.env = env;
        this.properties = properties;
    }
    
    /**
     * 绑定环境配置到指定的实例上
     *
     * @param env             环境对象
     * @param propertiesClass 配置实例类型，将通过反射初始化
     * @param <T>             泛型
     * @return T
     */
    @NotNull
    public static <T> T bind(@NotNull Environment env,
                             @NotNull Class<T> propertiesClass,
                             @NotNull TypeConverter typeConverter) {
        ConfigurationProperties configurationProperties = AnnotatedElementUtils.getAnnotation(propertiesClass, ConfigurationProperties.class);
        if (Objects.nonNull(configurationProperties)) {
            T properties = ClassUtil.instantiateClass(propertiesClass);
            return bind(env, properties, configurationProperties, typeConverter);
        }
        
        throw new IllegalArgumentException(String.format("配置类 %s 必须使用 @ConfigurationProperties 注解声明！", propertiesClass));
    }
    
    /**
     * 绑定环境配置到指定的实例上
     *
     * @param env                     环境对象
     * @param properties              配置实例类型
     * @param configurationProperties 配置属性注解
     * @param <T>                     泛型
     * @return T
     * @see ConfigurationProperties
     * @see BindEnvProperties#bind()
     */
    @NotNull
    public static <T> T bind(@NotNull Environment env,
                             @NotNull T properties,
                             @NotNull ConfigurationProperties configurationProperties,
                             @NotNull TypeConverter typeConverter) {
        return new BindEnvProperties<>(env, properties, configurationProperties, typeConverter).bind();
    }
    
    /**
     * 绑定环境配置到指定的实例上
     *
     * @return T
     * @see ConfigurationProperties
     */
    @NotNull
    public T bind() {
        Object instance = env.getProperty(prefix);
        
        if (Objects.isNull(instance)) {
            if (ignoreUnknownFields) {
                return properties;
            }
            throw new UnknownFieldException(String.format("无法在环境配置中加载键 %s 的信息", prefix));
        }
        
        Map<String, Object> propertyMap;
        if (instance instanceof JsonObject) {
            propertyMap = ((JsonObject) instance).getMap();
        } else {
            if (!ignoreInvalidFields) {
                throw new UnknownFieldException(String.format("环境配置键 %s 对应的类型无法赋值到对象 %s", prefix, properties));
            }
            return properties;
        }
        
        return bind(propertyMap, properties, prefix);
    }
    
    /**
     * 绑定属性
     */
    @NotNull
    private <N> N bind(@NotNull Map<String, Object> propertyMap,
                       @NotNull N properties,
                       @NotNull String prefix) {
        Method[] methods = ReflectUtil.getMethods(properties.getClass());
        for (Method method : methods) {
            if (!ClassUtil.isSetMethod(method)) continue;
            
            MethodParameter parameter = new MethodParameter(method, 0);
            TypeDescriptor typeDescriptor = new TypeDescriptor(parameter);
            
            String key = lowerFirst(removePrefix(method.getName(), "set"));
            String path = prefix + "." + key;
            
            Object value = propertyMap.get(key);
            
            // 绑定结果
            value = bind(value, typeDescriptor, path);
            if (IGNORE == value) continue;
            
            // 反射赋值
            ReflectUtil.invokeMethod(method, properties, value);
        }
        return properties;
    }
    
    /**
     * 绑定参数，如果遇到忽略的属性则返回 {@link #IGNORE}
     */
    @SuppressWarnings("unchecked")
    private <N> N bind(@Nullable Object value,
                       @NotNull TypeDescriptor typeDescriptor,
                       @NotNull String prefix) {
        Object result = IGNORE;
        
        if (Objects.isNull(value)) {
            if (ignoreUnknownFields) {
                return (N) IGNORE;
            }
            throw new UnknownFieldException(String.format("无法在环境配置中加载键 %s 的信息", prefix));
        }
        // 字符类型
        else if (typeDescriptor.getType().equals(String.class)) {
            result = env.resolvePlaceholders(value.toString());
        }
        // 基本数据类型
        else if (ClassUtil.isBasicType(typeDescriptor.getType())) {
            result = env.resolvePlaceholders(value.toString());
            result = typeConverter.convertIfNecessary(result, typeDescriptor);
        }
        // 数组类型 or 集合类型
        else if (typeDescriptor.isArray() || typeDescriptor.isCollection()) {
            if (value instanceof JsonArray) {
                value = ((JsonArray) value).getList();
            }
            
            if (!(value instanceof List)) {
                if (!ignoreInvalidFields) {
                    throw new UnknownFieldException(String.format("环境配置键 %s 对应的类型无法赋值给对象类型 %s",
                            prefix, typeDescriptor.getResolvableType()));
                }
                return (N) result;
            }
            
            TypeDescriptor elementTypeDescriptor = Objects.requireNonNull(typeDescriptor.getElementTypeDescriptor());
            int size = ((List<?>) value).size();
            
            // 数组
            if (typeDescriptor.isArray()) {
                T[] arr = (T[]) Array.newInstance(elementTypeDescriptor.getType(), size);
                
                for (int i = 0; i < size; i++) {
                    Object item = ((List<?>) value).get(i);
                    arr[i] = bind(item, elementTypeDescriptor, prefix + "[" + i + "]");
                }
                result = arr;
            }
            // 集合
            else {
                Class<?> collType = typeDescriptor.getType();
                
                Collection<?> coll;
                if (collType.isInterface()) {
                    if (List.class.equals(collType)) {
                        coll = new ArrayList<>(size);
                    } else if (Set.class.equals(collType)) {
                        coll = new HashSet<>(size, 1);
                    } else {
                        throw new IllegalArgumentException(String.format("暂不支持的集合类型：%s", collType));
                    }
                } else {
                    coll = (Collection<?>) ClassUtil.instantiateClass(collType);
                }
                
                for (int i = 0; i < size; i++) {
                    Object item = ((List<?>) value).get(i);
                    coll.add(bind(item, elementTypeDescriptor, prefix + "[" + i + "]"));
                }
                
                result = coll;
            }
        }
        // Map or Bean对象
        else if (typeDescriptor.isMap() || ClassUtil.isBean(typeDescriptor.getType())) {
            if (value instanceof JsonObject) {
                value = ((JsonObject) value).getMap();
            }
            
            if (!(value instanceof Map)) {
                if (!ignoreInvalidFields) {
                    throw new UnknownFieldException(String.format("环境配置键 %s 对应的类型无法赋值给对象类型 %s",
                            prefix, typeDescriptor.getResolvableType()));
                }
                return (N) result;
            }
            
            if (typeDescriptor.isMap()) {
                result = value;
            }
            // Bean 对象
            else {
                Object properties = ClassUtil.instantiateClass(typeDescriptor.getObjectType());
                result = bind((Map<String, Object>) value, properties, prefix);
            }
        }
        
        return (N) result;
    }
    
}
