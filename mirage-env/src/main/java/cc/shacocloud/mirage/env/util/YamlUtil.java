package cc.shacocloud.mirage.env.util;

import cc.shacocloud.mirage.utils.resource.Resource;
import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;
import org.yaml.snakeyaml.DumperOptions;
import org.yaml.snakeyaml.LoaderOptions;
import org.yaml.snakeyaml.Yaml;
import org.yaml.snakeyaml.constructor.Constructor;
import org.yaml.snakeyaml.reader.UnicodeReader;
import org.yaml.snakeyaml.representer.Representer;

import java.io.IOException;
import java.io.Reader;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

/**
 * 用于解析 {@code yaml} 格式的配置文件
 *
 * @author 思追(shaco)
 * @see Yaml
 */
public class YamlUtil {
    
    private static final Set<String> supportedTypes = Collections.emptySet();
    
    /**
     * 获取一个 Yaml 对象
     */
    @NotNull
    @Contract(" -> new")
    public static Yaml createYaml() {
        LoaderOptions loaderOptions = new LoaderOptions();
        loaderOptions.setAllowDuplicateKeys(false);
        DumperOptions dumperOptions = new DumperOptions();
        return new Yaml(new FilteringClassConstructor(loaderOptions), new Representer(dumperOptions), dumperOptions, loaderOptions);
    }
    
    /**
     * 从指定资源对象中加载 yaml 配置信息
     *
     * @param resource 资源对象
     * @return {@link Map}
     */
    @NotNull
    public static Map<String, Object> loadAll(@NotNull Resource resource) throws IOException {
        Yaml yaml = createYaml();
        
        Map<String, Object> result = new LinkedHashMap<>();
        try (Reader reader = new UnicodeReader(resource.getStream())) {
            for (Object object : yaml.loadAll(reader)) {
                if (object != null) {
                    result.putAll(asMap(object));
                }
            }
        }
        
        return result;
    }
    
    /**
     * 将 yaml 加载出来的对象转为 {@link Map}
     */
    @NotNull
    @SuppressWarnings("unchecked")
    private static Map<String, Object> asMap(Object object) {
        Map<String, Object> result = new LinkedHashMap<>();
        
        if (!(object instanceof Map)) {
            result.put("document", object);
            return result;
        }
        
        Map<Object, Object> map = (Map<Object, Object>) object;
        map.forEach((key, value) -> {
            if (value instanceof Map) {
                value = asMap(value);
            }
            if (key instanceof CharSequence) {
                result.put(key.toString(), value);
            } else {
                // 在这种情况下，它必须是映射键
                result.put("[" + key.toString() + "]", value);
            }
        });
        return result;
    }
    
    /**
     * 用于防止在 Yaml 文档中加载不授信的类对象
     */
    private static class FilteringClassConstructor extends Constructor {
        
        FilteringClassConstructor(LoaderOptions loaderOptions) {
            super(loaderOptions);
        }
        
        @Override
        protected Class<?> getClassForName(String name) throws ClassNotFoundException {
            if (!supportedTypes.contains(name)) {
                throw new IllegalArgumentException("yaml 文档中遇到的不支持的类型：" + name);
            }
            
            return super.getClassForName(name);
        }
    }
}
