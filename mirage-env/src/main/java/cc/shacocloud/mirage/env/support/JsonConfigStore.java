package cc.shacocloud.mirage.env.support;

import cc.shacocloud.mirage.env.ConfigStoreFormatEnum;
import cc.shacocloud.mirage.env.ConfigStoreTypeProperties;
import io.vertx.core.json.JsonObject;

import java.util.Map;

/**
 * {@code json} 格式的配置文件
 *
 * @author 思追(shaco)
 */
public class JsonConfigStore extends AbstractConfigStore {
    
    public JsonConfigStore(Map<String, Object> config) {
        super(new JsonTypeProperties(new JsonObject(config)));
    }
    
    public JsonConfigStore(ConfigStoreTypeProperties properties) {
        super(properties);
    }
    
    public JsonConfigStore(ConfigStoreTypeProperties storeProperties, boolean optional) {
        super(storeProperties, optional);
    }
    
    @Override
    public ConfigStoreFormatEnum getFormat() {
        return ConfigStoreFormatEnum.json;
    }
}
