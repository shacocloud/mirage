package cc.shacocloud.mirage.env;

/**
 * @author 思追(shaco)
 */
public enum ConfigStoreTypeEnum {
    
    /**
     * 本地文件存储类型
     */
    file,
    
    /**
     * 类路径存储类型
     */
    classpath,
    
    /**
     * json 格式键值对存储类型
     */
    json,
    
    /**
     * http 格式存储类型
     */
    http,
    
    /**
     * 环境变量存储类型
     */
    env,
    
    /**
     * 系统参数存储类型
     */
    sys
}
