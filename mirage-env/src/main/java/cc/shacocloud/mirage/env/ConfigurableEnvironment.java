package cc.shacocloud.mirage.env;

/**
 * 可配置的环境信息
 * <p>
 * <b>注意：配置存储注册的顺序，影响配置的结果，即 靠后的配置覆盖靠前的配置</b>
 * <p>
 * 覆盖规则：<br/>
 * A 提供 {a:value, b:1} 配置 <br/>
 * B 提供 {a:value2, c:2} 配置 <br/>
 * 以 A，B 的顺序声明配置，最终配置应该为： {a:value2, b:1, c:2} <br/>
 * 如果您将声明的顺序反过来（B，A），那么您会得到 {a:value, b:1, c:2}
 *
 * @author 思追(shaco)
 */
public interface ConfigurableEnvironment extends Environment {
    
    /**
     * 注册配置存储信息，添加到队列的结尾
     *
     * @param configStore 配置存储信息
     */
    void registerConfig(ConfigStore configStore);
    
    /**
     * 注册配置存储信息，添加到队列的头部
     *
     * @param configStore 配置存储信息
     */
    void registerConfigToHead(ConfigStore configStore);
    
    /**
     * 在指定配置前注册配置存储信息，如果目标配置不存在则添加到头部
     *
     * @param configStore 配置存储信息
     * @param target      目标配置，{@code configStore} 在该配置前
     */
    void registerConfigBeforeOf(ConfigStore configStore, ConfigStore target);
    
    /**
     * 在指定配置后注册配置存储信息，如果目标配置不存在则添加到尾部
     *
     * @param configStore 配置存储信息
     * @param target      目标配置，{@code configStore} 在该配置后
     */
    void registerConfigAfterOf(ConfigStore configStore, ConfigStore target);
    
    /**
     * 初始化配置信息
     * <p>
     * 注意：一旦初始化后，不在允许继续注册配置信息
     */
    void init();
    
    /**
     * 启动环境配置变更监听，没隔一段时间将刷新一遍配置信息
     */
    void startEnvironmentChangeMonitoring();
    
}
