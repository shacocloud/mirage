package cc.shacocloud.mirage.env;

import io.vertx.config.ConfigStoreOptions;

/**
 * 配置存储
 *
 * @author 思追(shaco)
 */
public interface ConfigStore {
    
    /**
     * 获取配置存储类型
     */
    default ConfigStoreTypeEnum getType() {
        return getStoreTypeProperties().getType();
    }
    
    /**
     * 获取配置存储类型参数
     */
    ConfigStoreTypeProperties getStoreTypeProperties();
    
    /**
     * 获取配置存储格式
     */
    ConfigStoreFormatEnum getFormat();
    
    /**
     * 当前的存储配置是否为可选的
     * <p>
     * 如果为 true 时，当配置获取失败时不抛出例外，以空对象作为结果
     */
    boolean isOptional();
    
    /**
     * 获取当前存储配置的 {@link ConfigStoreOptions}
     */
    ConfigStoreOptions getConfigStoreOptions();
    
}
