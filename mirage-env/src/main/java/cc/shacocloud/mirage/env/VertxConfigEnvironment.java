package cc.shacocloud.mirage.env;

import cc.shacocloud.mirage.utils.FutureUtils;
import io.vertx.config.ConfigRetriever;
import io.vertx.config.ConfigRetrieverOptions;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.DeploymentOptions;
import io.vertx.core.Future;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonObject;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Objects;

/**
 * 基于 vertx config 配置实现 {@link AbstractEnvironment}
 *
 * @author 思追(shaco)
 */
@Slf4j
public class VertxConfigEnvironment extends AbstractEnvironment {
    
    @Nullable
    @Setter
    private Vertx vertx;
    
    private ConfigRetriever configRetriever;
    
    /**
     * 配置刷新间隔
     */
    @Setter
    private volatile long refresh = 5000;
    
    @Override
    protected void doInit() {
        // 加载配置
        ConfigRetriever configRetriever = getConfigRetriever();
        FutureUtils.await(loadConfig(configRetriever));
    }
    
    @Override
    public void startEnvironmentChangeMonitoring() {
        this.refresh = getEnvironmentProperties().getRefresh();
        
        // 部署环境配置站点
        DeploymentOptions deploymentOptions = new DeploymentOptions();
        deploymentOptions.setInstances(1);
        FutureUtils.await(getVertx().deployVerticle(new EnvironmentVerticle(), deploymentOptions));
    }
    
    /**
     * 获取配置检索器
     */
    protected ConfigRetriever getConfigRetriever() {
        if (Objects.isNull(this.configRetriever)) {
            Vertx vertx = getVertx();
            ConfigRetrieverOptions retrieverOptions = getRetrieverOptions();
            this.configRetriever = ConfigRetriever.create(vertx, retrieverOptions);
        }
        
        return this.configRetriever;
    }
    
    /**
     * 获取 {@link Vertx}，如果不存在则创建一个
     */
    public Vertx getVertx() {
        if (Objects.isNull(vertx)) {
            vertx = Vertx.vertx();
        }
        return vertx;
    }
    
    /**
     * 加载配置，每次都创建一个 {@link ConfigRetriever}
     *
     * @param vertx            {@link Vertx}
     * @param retrieverOptions 配置检索器配置项
     */
    public Future<JsonObject> loadConfig(@NotNull Vertx vertx,
                                         @NotNull ConfigRetrieverOptions retrieverOptions) {
        retrieverOptions.setScanPeriod(-1);
        ConfigRetriever configRetriever = ConfigRetriever.create(vertx, retrieverOptions);
        try {
            return loadConfig(configRetriever);
        } finally {
            configRetriever.close();
        }
    }
    
    /**
     * 加载配置
     *
     * @param configRetriever 配置检索器
     */
    public Future<JsonObject> loadConfig(@NotNull ConfigRetriever configRetriever) {
        return configRetriever.getConfig()
                .compose(config -> updateConfig(config).map(v -> config));
    }
    
    /**
     * 环境站点，用于配置刷新
     */
    private class EnvironmentVerticle extends AbstractVerticle {
        
        // 内部定时器id
        private long timerId = -1;
        
        @Override
        public void start() {
            refreshAfterSomeTime();
        }
        
        /**
         * 一段时间后刷新配置信息
         */
        protected void refreshAfterSomeTime() {
            final ConfigRetriever configRetriever = getConfigRetriever();
            if (refresh <= 0) return;
            
            // 注册定时器，在一段时间后执行配置的刷新
            this.timerId = vertx.setTimer(refresh,
                    id -> loadConfig(configRetriever)
                            .onFailure(cause -> {
                                if (log.isWarnEnabled()) {
                                    log.warn("刷新配置信息发生例外！", cause);
                                }
                            })
                            // 配置加载完成后递归注册
                            .onComplete(ar -> refreshAfterSomeTime()));
        }
        
        
        @Override
        public void stop() {
            if (this.timerId > 0) {
                vertx.cancelTimer(this.timerId);
            }
        }
    }
    
}
