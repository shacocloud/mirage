package cc.shacocloud.mirage.env.exception;

/**
 * 无效的字段异常
 *
 * @author 思追(shaco)
 */
public class UnknownFieldException extends RuntimeException {
    
    public UnknownFieldException() {
    }
    
    public UnknownFieldException(String message) {
        super(message);
    }
    
    public UnknownFieldException(String message, Throwable cause) {
        super(message, cause);
    }
    
    public UnknownFieldException(Throwable cause) {
        super(cause);
    }
    
    public UnknownFieldException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
