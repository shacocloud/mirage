package cc.shacocloud.mirage.env.services;

import io.vertx.config.spi.ConfigStore;
import io.vertx.core.Future;
import io.vertx.core.Vertx;
import io.vertx.core.buffer.Buffer;
import io.vertx.core.impl.VertxInternal;
import io.vertx.core.json.JsonObject;
import org.jetbrains.annotations.NotNull;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Objects;

/**
 * 读取类路径配置存储对象
 * <p>
 * 示例：
 * <pre>
 *  ConfigStoreOptions store = new ConfigStoreOptions()
 *       .setType("classpath")
 *       .setFormat("yaml")
 *       .setConfig(new JsonObject().put("classpath", "test.yaml"));
 * </pre>
 *
 * @author 思追(shaco)
 * @see ClasspathConfigStoreFactory
 */
public class ClasspathConfigStore implements ConfigStore {
    private final VertxInternal vertx;
    private final String classpath;
    
    public ClasspathConfigStore(@NotNull Vertx vertx, @NotNull JsonObject configuration) {
        this.vertx = (VertxInternal) vertx;
        this.classpath = configuration.getString("classpath");
        if (this.classpath == null) {
            throw new IllegalArgumentException("缺失必要的配置项：classpath");
        }
    }
    
    @Override
    public Future<Buffer> get() {
        return vertx.executeBlocking(event -> {
            
            // 优先使用当前类加载器加载
            URL url = getClass().getClassLoader().getResource(classpath);
            
            // 不存在使用系统类加载器加载
            if (Objects.isNull(url)) {
                url = ClassLoader.getSystemResource(classpath);
            }
            
            if (Objects.isNull(url)) {
                event.fail(new FileNotFoundException(String.format("找不到指定的类路径文件：%s", classpath)));
            } else {
                Buffer buffer;
                try (InputStream stream = url.openStream();
                     ByteArrayOutputStream bos = new ByteArrayOutputStream()) {
                    byte[] buf = new byte[4096];
                    
                    int len;
                    while ((len = stream.read(buf)) != -1) {
                        bos.write(buf, 0, len);
                    }
                    
                    buffer = Buffer.buffer(bos.toByteArray());
                } catch (IOException e) {
                    throw new RuntimeException(e.getMessage(), e);
                }
                
                event.complete(buffer);
            }
        });
    }
    
}
