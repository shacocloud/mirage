package cc.shacocloud.mirage.env.bind;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

/**
 * 环境配置文件
 *
 * @author 思追(shaco)
 */
@Setter
@Getter
@NoArgsConstructor
@ConfigurationProperties(prefix = EnvironmentProperties.KEY)
public class EnvironmentProperties {
    
    public static final String KEY = "mirage.environment";
    
    /**
     * 指定激活的配置文件
     */
    private String active;
    
    /**
     * 配置文件刷新间隔
     */
    private Long refresh = 5000L;
    
    /**
     * 配置文件集合
     */
    private List<ProfilesProperties> profiles;
    
}
