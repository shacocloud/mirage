package cc.shacocloud.mirage.env;

import cc.shacocloud.mirage.bean.aware.Aware;
import org.jetbrains.annotations.NotNull;

/**
 * 环境对象感知接口
 *
 * @author 思追(shaco)
 */
public interface EnvironmentAware extends Aware {
    
    /**
     * 在应用上下文启动成功后，会调用该方法注入 {@link Environment}
     */
    void setEnvironment(@NotNull Environment environment);
    
}
