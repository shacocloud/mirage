package cc.shacocloud.mirage.env;

/**
 * 配置存储格式
 *
 * @author 思追(shaco)
 */
public enum ConfigStoreFormatEnum {
    
    /**
     * 参数使用 properties 格式化
     */
    properties,
    
    /**
     * 参数使用 yaml 格式化
     */
    yaml,
    
    /**
     * 参数使用 json 格式化
     */
    json,
    
    /**
     * 参数不格式化
     */
    no
}
