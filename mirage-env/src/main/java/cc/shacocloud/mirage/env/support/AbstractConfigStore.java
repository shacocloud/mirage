package cc.shacocloud.mirage.env.support;

import cc.shacocloud.mirage.env.ConfigStore;
import cc.shacocloud.mirage.env.ConfigStoreFormatEnum;
import cc.shacocloud.mirage.env.ConfigStoreTypeProperties;
import io.vertx.config.ConfigStoreOptions;
import lombok.Getter;

/**
 * 抽象的配置存储，封装 {@link ConfigStoreTypeProperties} 相关的基本属性
 *
 * @author 思追(shaco)
 */
public abstract class AbstractConfigStore implements ConfigStore {
    
    protected final ConfigStoreTypeProperties storeProperties;
    
    @Getter
    private final boolean optional;
    
    public AbstractConfigStore(ConfigStoreTypeProperties storeProperties) {
        this(storeProperties, false);
    }
    
    public AbstractConfigStore(ConfigStoreTypeProperties storeProperties, boolean optional) {
        this.storeProperties = storeProperties;
        this.optional = optional;
    }
    
    @Override
    public ConfigStoreTypeProperties getStoreTypeProperties() {
        return storeProperties;
    }
    
    @Override
    public ConfigStoreOptions getConfigStoreOptions() {
        ConfigStoreOptions configStoreOptions = new ConfigStoreOptions();
        configStoreOptions.setOptional(isOptional());
        configStoreOptions.setType(getType().name());
        
        ConfigStoreFormatEnum format = getFormat();
        if (!ConfigStoreFormatEnum.no.equals(format)) {
            configStoreOptions.setFormat(format.name());
        }
        
        configStoreOptions.setConfig(getStoreTypeProperties().getProperties());
        return configStoreOptions;
    }
    
}
