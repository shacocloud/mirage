package cc.shacocloud.mirage.env.services;

import io.vertx.config.spi.ConfigStore;
import io.vertx.config.spi.ConfigStoreFactory;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonObject;

import java.util.ServiceLoader;

/**
 * 基于类路径的配置存储工厂
 * <p>
 * 通过 vertx 声明的 {@link ServiceLoader} 方法拓展的对象
 *
 * @author 思追(shaco)
 * @see ClasspathConfigStore
 */
public class ClasspathConfigStoreFactory implements ConfigStoreFactory {
    
    @Override
    public String name() {
        return "classpath";
    }
    
    @Override
    public ConfigStore create(Vertx vertx, JsonObject configuration) {
        return new ClasspathConfigStore(vertx, configuration);
    }
}
