package cc.shacocloud.mirage.env.support;

import cc.shacocloud.mirage.env.ConfigStoreTypeEnum;
import cc.shacocloud.mirage.env.ConfigStoreTypeProperties;
import io.vertx.core.json.JsonObject;

/**
 * 基于系统参数的存储类型配置
 *
 * @author 思追(shaco)
 */
public class SysTypeProperties implements ConfigStoreTypeProperties {
    
    private final JsonObject properties;
    
    /**
     * 初始化默认系统参数存储配置
     */
    public SysTypeProperties() {
        this(true, true, false);
    }
    
    /**
     * 初始化统参数存储配置
     *
     * @param cache        cache 属性（默认为 true） 决定是否在第一次访问时缓存系统参数而后不再重新加载他们
     * @param hierarchical hierarchical 属性（默认为 true），系统属性将被解析为嵌套的 JSON 对象，使用点分隔的属性名称作为 JSON 对象中的路径，反之不解析
     * @param rawData      raw-data 属性（默认为 false ），如果 raw-data 为 true，则不会对值进行转换
     */
    public SysTypeProperties(boolean cache,
                             boolean hierarchical,
                             boolean rawData) {
        this.properties = new JsonObject();
        this.properties.put("cache", cache);
        this.properties.put("hierarchical", hierarchical);
        this.properties.put("rawData", rawData);
    }
    
    @Override
    public ConfigStoreTypeEnum getType() {
        return ConfigStoreTypeEnum.sys;
    }
    
    @Override
    public JsonObject getProperties() {
        return properties;
    }
}
