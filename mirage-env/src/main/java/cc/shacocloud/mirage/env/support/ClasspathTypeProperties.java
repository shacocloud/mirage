package cc.shacocloud.mirage.env.support;

import cc.shacocloud.mirage.env.ConfigStoreTypeEnum;
import cc.shacocloud.mirage.env.ConfigStoreTypeProperties;
import io.vertx.core.json.JsonObject;

/**
 * 基于类路径的存储类型配置
 *
 * @author 思追(shaco)
 */
public class ClasspathTypeProperties implements ConfigStoreTypeProperties {
    
    private final JsonObject properties;
    
    /**
     * 初始化
     *
     * @param classpath 类路径配置文件路径
     */
    public ClasspathTypeProperties(String classpath) {
        this.properties = new JsonObject();
        this.properties.put("classpath", classpath);
    }
    
    @Override
    public ConfigStoreTypeEnum getType() {
        return ConfigStoreTypeEnum.classpath;
    }
    
    @Override
    public JsonObject getProperties() {
        return properties;
    }
    
}
