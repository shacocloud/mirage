package cc.shacocloud.mirage.env.support;

import cc.shacocloud.mirage.env.ConfigStoreTypeEnum;
import cc.shacocloud.mirage.env.ConfigStoreTypeProperties;
import io.vertx.core.json.JsonObject;

/**
 * 基于 json 的存储类型配置
 *
 * @author 思追(shaco)
 */
public class JsonTypeProperties implements ConfigStoreTypeProperties {
    
    private final JsonObject properties;
    
    public JsonTypeProperties() {
        this(new JsonObject());
    }
    
    public JsonTypeProperties(JsonObject properties) {
        this.properties = properties;
    }
    
    @Override
    public ConfigStoreTypeEnum getType() {
        return ConfigStoreTypeEnum.json;
    }
    
    @Override
    public JsonObject getProperties() {
        return properties;
    }
    
    public Object getValue(String key, Object def) {
        return properties.getValue(key, def);
    }
    
    public JsonObject put(String key, Object value) {
        return properties.put(key, value);
    }
    
    public Object remove(String key) {
        return properties.remove(key);
    }
}
