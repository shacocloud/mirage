package cc.shacocloud.mirage.bean.component;

/**
 * 美国短毛猫
 *
 * @author 思追(shaco)
 */
public class AmericanShortCat implements Cat {
    
    @Override
    public String name() {
        return "美国短毛猫";
    }
}
