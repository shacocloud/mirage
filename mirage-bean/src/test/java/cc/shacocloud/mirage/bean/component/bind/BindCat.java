package cc.shacocloud.mirage.bean.component.bind;

import java.lang.annotation.*;

/**
 * @author 思追(shaco)
 */
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface BindCat {
}
