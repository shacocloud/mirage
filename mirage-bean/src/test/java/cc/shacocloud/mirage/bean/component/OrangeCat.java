package cc.shacocloud.mirage.bean.component;

import cc.shacocloud.mirage.bean.bind.Component;
import cc.shacocloud.mirage.bean.component.bind.BindCat;

/**
 * 橘猫
 *
 * @author 思追(shaco)
 */
@BindCat
@Component(OrangeCat.BeanName)
public class OrangeCat implements Cat {
    
    public static final String BeanName = "oCat";
    
    @Override
    public String name() {
        return "橘猫";
    }
}
