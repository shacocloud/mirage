package cc.shacocloud.mirage.bean.component;

import cc.shacocloud.mirage.bean.bind.Bean;
import cc.shacocloud.mirage.bean.bind.Configuration;

/**
 * @author 思追(shaco)
 */
@Configuration
public class CatConfiguration {
    
    @Bean
    public AmericanShortCat americanShortCat() {
        return new AmericanShortCat();
    }
    
    @Bean
    public PersianCat persianCat() {
        return new PersianCat();
    }
}
