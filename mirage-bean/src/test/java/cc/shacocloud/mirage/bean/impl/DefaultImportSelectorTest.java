package cc.shacocloud.mirage.bean.impl;

import cc.shacocloud.mirage.bean.BeanDescriptionRegistry;
import cc.shacocloud.mirage.bean.BeanKeyLoader;
import cc.shacocloud.mirage.bean.ImportBeanDescriptionRegistrar;
import cc.shacocloud.mirage.bean.ImportSelector;
import cc.shacocloud.mirage.bean.bind.Configuration;
import cc.shacocloud.mirage.bean.bind.Import;
import cc.shacocloud.mirage.bean.meta.ImportDescription;
import cc.shacocloud.mirage.utils.annotation.AnnotatedElementMetadata;
import cc.shacocloud.mirage.utils.annotation.AnnotationAttributes;
import org.jetbrains.annotations.NotNull;
import org.junit.jupiter.api.Test;

import java.lang.annotation.*;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @author 思追(shaco)
 */
class DefaultImportSelectorTest {
    
    @Test
    public void test() {
        ImportSelector importSelector = new DefaultImportSelector();
        
        ImportDescription importDescription = importSelector.findImports(MirageConfig.class);
        assertNotNull(importDescription);
        
        Map<ImportBeanDescriptionRegistrar, AnnotatedElementMetadata> importBeanDescriptionRegistrarMap =
                importDescription.getImportBeanDescriptionRegistrar();
        assertEquals(1, importBeanDescriptionRegistrarMap.size());
        
        Map.Entry<ImportBeanDescriptionRegistrar, AnnotatedElementMetadata> entry = importBeanDescriptionRegistrarMap.entrySet().iterator().next();
        AnnotatedElementMetadata annotatedElementMetadata = entry.getValue();
        
        AnnotationAttributes attributes = annotatedElementMetadata.getAnnotationAttributes(EnableDemo.class);
        assertTrue(attributes.getBoolean("enable"));
        
        ImportBeanDescriptionRegistrar importBeanDescriptionRegistrar = entry.getKey();
        assertTrue(importBeanDescriptionRegistrar instanceof MirageBeanDefinitionRegistrar);
    }
    
    @Target(ElementType.TYPE)
    @Retention(RetentionPolicy.RUNTIME)
    @Documented
    @Import(DefaultImportSelectorTest.MirageBeanDefinitionRegistrar.class)
    public @interface EnableDemo {
        
        boolean enable() default true;
        
    }
    
    @EnableDemo
    @Configuration
    public static class MirageConfig {
    
    }
    
    public static class MirageBeanDefinitionRegistrar implements ImportBeanDescriptionRegistrar {
        
        public MirageBeanDefinitionRegistrar() {
        }
        
        public void registerBeanDescriptions(@NotNull AnnotatedElementMetadata annotatedMetadata,
                                             @NotNull BeanDescriptionRegistry registry,
                                             @NotNull BeanKeyLoader beanKeyLoader) {
            // 做些什么...
        }
    }
    
    
}