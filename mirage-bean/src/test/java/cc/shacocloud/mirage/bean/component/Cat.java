package cc.shacocloud.mirage.bean.component;

/**
 * @author 思追(shaco)
 */
public interface Cat {
    
    /**
     * 猫的名称
     */
    String name();
    
}
