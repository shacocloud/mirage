package cc.shacocloud.mirage.bean.component;

/**
 * @author 思追(shaco)
 */
public class PersianCat implements Cat {
    
    @Override
    public String name() {
        return "波斯猫";
    }
}
