package cc.shacocloud.mirage.bean.component;

import cc.shacocloud.mirage.bean.bind.Autowired;
import cc.shacocloud.mirage.bean.bind.Component;
import jakarta.inject.Inject;
import jakarta.inject.Provider;
import lombok.Getter;

/**
 * @author 思追(shaco)
 */
@Getter
@Component
public class CatFactory {
    
    private final EnglishShortCat englishShortCat;
    @Inject
    private OrangeCat orangeCat;
    @Autowired
    private AmericanShortCat americanShortCat;
    @Inject
    private Provider<PersianCat> persianCatProvider;
    
    public CatFactory(EnglishShortCat englishShortCat) {
        this.englishShortCat = englishShortCat;
    }
    
    
}
