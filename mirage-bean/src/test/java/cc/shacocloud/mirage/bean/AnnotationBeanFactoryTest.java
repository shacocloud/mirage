package cc.shacocloud.mirage.bean;

import cc.shacocloud.mirage.bean.component.*;
import cc.shacocloud.mirage.bean.component.bind.BindCat;
import cc.shacocloud.mirage.bean.impl.ClassPathScanner;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @author 思追(shaco)
 */
class AnnotationBeanFactoryTest {
    
    private static ConfigurableBeanFactory beanFactory;
    
    @BeforeAll
    public static void init() throws Exception {
        AnnotationBeanFactory beanFactory = new AnnotationBeanFactory(new ClassPathScanner());
        beanFactory.loadBean("cc.shacocloud.mirage.bean.component");
        AnnotationBeanFactoryTest.beanFactory = beanFactory;
    }
    
    @Test
    void getBean() {
        OrangeCat orangeCat = beanFactory.getBean(OrangeCat.class);
        assertNotNull(orangeCat);
        assertEquals(orangeCat.name(), "橘猫");
        
        EnglishShortCat englishShortCat = beanFactory.getBean(EnglishShortCat.class);
        assertNotNull(englishShortCat);
        assertEquals(englishShortCat.name(), "英国短毛猫");
        
        AmericanShortCat americanShortCat = beanFactory.getBean(AmericanShortCat.class);
        assertNotNull(americanShortCat);
        assertEquals(americanShortCat.name(), "美国短毛猫");
        
        PersianCat persianCat = beanFactory.getBean(PersianCat.class);
        assertNotNull(persianCat);
        assertEquals(persianCat.name(), "波斯猫");
        
        CatFactory catFactory = beanFactory.getBean(CatFactory.class);
        assertEquals(catFactory.getOrangeCat(), orangeCat);
        assertEquals(catFactory.getEnglishShortCat(), englishShortCat);
        assertEquals(catFactory.getAmericanShortCat(), americanShortCat);
        assertEquals(catFactory.getPersianCatProvider().get(), persianCat);
    }
    
    @Test
    void containsBean() {
        assertTrue(beanFactory.containsBean(OrangeCat.class));
        assertTrue(beanFactory.containsBean(OrangeCat.BeanName));
        
        assertTrue(beanFactory.containsBean(EnglishShortCat.class));
        assertTrue(beanFactory.containsBean("englishShortCat"));
    }
    
    @Test
    void getBeanByType() {
        Map<String, Cat> beanMap = beanFactory.getBeanByType(Cat.class);
        assertEquals(4, beanMap.size());
        
        beanMap.values().forEach(cat -> System.out.println(cat.name()));
    }
    
    @Test
    void getBeanType() {
        Class<Object> beanType = beanFactory.getBeanType(OrangeCat.BeanName);
        assertEquals(OrangeCat.class, beanType);
    }
    
    @Test
    void getBeanNamesForType() {
        String[] beanNames = beanFactory.getBeanNamesForType(Cat.class);
        assertEquals(4, beanNames.length);
        
        String[] beanNames2 = beanFactory.getBeanNamesForType(OrangeCat.class);
        assertEquals(1, beanNames2.length);
        assertEquals(OrangeCat.BeanName, beanNames2[0]);
    }
    
    @Test
    void getBeanNamesForAnnotation() {
        String[] names = beanFactory.getBeanNamesForAnnotation(BindCat.class);
        assertEquals(1, names.length);
        assertEquals(OrangeCat.BeanName, names[0]);
        
        Class<Object> beanType = beanFactory.getBeanType(names[0]);
        assertEquals(OrangeCat.class, beanType);
    }
    
    @Test
    void registerBean() {
        beanFactory.registerBean("oCat1", new OrangeCat());
        
        Class<Object> beanType = beanFactory.getBeanType("oCat1");
        assertEquals(OrangeCat.class, beanType);
    }
}