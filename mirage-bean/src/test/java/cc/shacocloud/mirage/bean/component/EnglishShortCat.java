package cc.shacocloud.mirage.bean.component;

import cc.shacocloud.mirage.bean.bind.Component;

/**
 * 英短
 *
 * @author 思追(shaco)
 */
@Component
public class EnglishShortCat implements Cat {
    
    @Override
    public String name() {
        return "英国短毛猫";
    }
}
