package cc.shacocloud.mirage.bean.meta;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

/**
 * 此接口表示Bean对象的注入点，如{@link java.lang.reflect.Method} 或 {@link java.lang.reflect.Field}
 */
public interface BeanInjectionPoint {
    
    /**
     * 将参数注入到对象的此注入点中
     *
     * @param object     具有此注入点的对象，要将参数注入到其中
     * @param parameters 要注入的参数，如果是 {@link java.lang.reflect.Field} 参数长度为 1
     */
    void inject(@Nullable Object object, @NotNull Object[] parameters);
    
    /**
     * 此注入点执行注入所需的依赖项
     */
    List<BeanKey> getDependencies();
    
}
