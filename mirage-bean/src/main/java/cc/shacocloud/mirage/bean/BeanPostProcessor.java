package cc.shacocloud.mirage.bean;

import cc.shacocloud.mirage.bean.exception.BeanException;
import cc.shacocloud.mirage.utils.comparator.AnnotationOrderComparator;
import org.jetbrains.annotations.NotNull;

/**
 * 对象创建完成，在保存到工厂前的钩子函数，允许自定义修改或返回新的 Bean 实例
 * <p>
 * 多个 {@link BeanPostProcessor} 使用 {@link AnnotationOrderComparator} 进行排序，按照优先级从高到低执行
 *
 * @author 思追(shaco)
 */
public interface BeanPostProcessor {
    
    /**
     * 对象创建好后，初始化方法执行前执行该回调。初始化方法如 {@link InitializingBean} 或者其他一些自定义的初始化逻辑
     *
     * @param beanName 对象名称
     * @param bean     对象实例
     * @return 新的实例
     * @throws BeanException 如果执行失败则抛出该例外
     */
    @NotNull
    default Object postProcessBeforeInitialization(@NotNull String beanName, @NotNull Object bean) throws BeanException {
        return bean;
    }
    
    /**
     * 对象创建好后，初始化方法执行后执行该回调。初始化方法如 {@link InitializingBean} 或者其他一些自定义的初始化逻辑
     *
     * @param beanName 对象名称
     * @param bean     对象实例
     * @return 新的实例
     * @throws BeanException 如果执行失败则抛出该例外
     */
    @NotNull
    default Object postProcessAfterInitialization(@NotNull String beanName, @NotNull Object bean) throws BeanException {
        return bean;
    }
    
}
