package cc.shacocloud.mirage.bean.bind;

import cc.shacocloud.mirage.bean.ImportBeanDescriptionRegistrar;

import java.lang.annotation.*;

/**
 * 导入对象注解，通常来说必须定义在 {@link Configuration} 表示的对象上或者定义在嵌套的注解中（该注解需要定义在 {@link Configuration}上），
 * 即可以导入指定的类型
 * <p>
 * 普通组件示例：
 * <pre>
 *     &#064;Component
 *     public class MirageComponent{
 *
 *     }
 *
 *     &#064;Import(MirageComponent.class)
 *     &#064;Configuration
 *     public class MirageConfig{
 *
 *     }
 * </pre>
 * <p>
 * 支持导入对象描述的示例：
 * <pre>
 *     public class MirageBeanDefinitionRegistrar implements ImportBeanDescriptionRegistrar {
 *
 *         public MirageBeanDefinitionRegistrar(){}
 *
 *         public void registerBeanDescriptions(AnnotatedElementMetadata annotatedMetadata,
 *                                              BeanDescriptionRegistry registry,
 *                                              BeanKeyLoader beanKeyLoader) {
 *          // 做些什么...
 *        }
 *     }
 *
 *     &#064;Target(ElementType.TYPE)
 *     &#064;Retention(RetentionPolicy.RUNTIME)
 *     &#064;Documented
 *     &#064;Import(MirageBeanDefinitionRegistrar.class)
 *     public @interface EnableDemo {
 *
 *     }
 *
 *     &#064;EnableDemo
 *     &#064;Configuration
 *     public class MirageConfig {
 *
 *     }
 * </pre>
 *
 * @author 思追(shaco)
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Import {
    
    /**
     * {@link Configuration},{@link ImportBeanDescriptionRegistrar}, or regular component classes to import.
     */
    Class<?>[] value();
    
}