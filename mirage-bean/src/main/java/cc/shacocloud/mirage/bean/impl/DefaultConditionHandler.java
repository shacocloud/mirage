package cc.shacocloud.mirage.bean.impl;

import cc.shacocloud.mirage.bean.ConditionHandler;
import cc.shacocloud.mirage.bean.ConfigurableBeanFactory;
import cc.shacocloud.mirage.bean.meta.BeanKey;
import cc.shacocloud.mirage.bean.meta.FactoryPoint;
import org.jetbrains.annotations.NotNull;

/**
 * {@link ConditionHandler} 的默认实现，默认都返回 true
 *
 * @author 思追(shaco)
 */
public class DefaultConditionHandler implements ConditionHandler {
    
    @Override
    public boolean matches(@NotNull ConfigurableBeanFactory beanFactory, @NotNull BeanKey beanKey) {
        return true;
    }
    
    @Override
    public boolean matches(@NotNull ConfigurableBeanFactory beanFactory, @NotNull FactoryPoint factoryPoint) {
        return true;
    }
}
