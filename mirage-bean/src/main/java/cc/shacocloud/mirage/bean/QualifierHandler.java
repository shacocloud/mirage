package cc.shacocloud.mirage.bean;

import cc.shacocloud.mirage.bean.meta.BeanKey;
import jakarta.inject.Named;
import jakarta.inject.Qualifier;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.lang.reflect.AnnotatedElement;
import java.lang.reflect.Field;
import java.lang.reflect.Parameter;

/**
 * {@link Qualifier} 注解的名称处理器
 * <p>
 * 用于查找 {@link Class}，{@link Field}，{@link Parameter}  的限定符名称
 *
 * @author 思追(shaco)
 */
public interface QualifierHandler extends BeanNameHandler {
    
    /**
     * 从批注集中获取限定符。限定符是从 {@link Qualifier}继承的任何注释。
     *
     * <p>规则：</p>
     * <ul>
     *   <li>如果未找到 {@link Qualifier} 注解，则返回 则返回 {@link BeanKey#ANY_QUALIFIER}</li>
     *   <li>如果找到 {@link Named} 注解，存在值则返回该值，不存在则使用驼峰命名规范返回。</li>
     *   <li>如果找到任何其他 {@link Qualifier}，则返回注解的类名称</li>
     * </ul>
     */
    @Nullable
    String getQualifier(@NotNull AnnotatedElement annotatedElement);
    
    /**
     * 委托 {@link #getQualifier} 处理
     */
    @Override
    @Nullable
    default String getBeanName(@NotNull Class<?> beanClass) {
        return getQualifier(beanClass);
    }
}
