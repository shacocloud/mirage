package cc.shacocloud.mirage.bean.meta;

import cc.shacocloud.mirage.utils.reflection.ReflectUtil;
import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.lang.reflect.Field;
import java.util.List;
import java.util.Objects;

/**
 * 描述字段依赖关系的元数据
 *
 * @author 思追(shaco)
 */
public class FieldInjectionPoint implements BeanInjectionPoint {
    
    @Getter
    private final Field field;
    
    @Getter
    private final BeanKey dependency;
    
    public FieldInjectionPoint(Field field, BeanKey dependency) {
        this.field = field;
        this.dependency = dependency;
    }
    
    @Override
    public void inject(@Nullable Object object, @NotNull Object @NotNull [] parameters) {
        ReflectUtil.setFieldValue(object, field, parameters[0]);
    }
    
    @Override
    public List<BeanKey> getDependencies() {
        return List.of(dependency);
    }
    
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        FieldInjectionPoint that = (FieldInjectionPoint) o;
        return Objects.equals(field, that.field) && Objects.equals(dependency, that.dependency);
    }
    
    @Override
    public int hashCode() {
        return Objects.hash(field, dependency);
    }
}
