package cc.shacocloud.mirage.bean.bind;

import jakarta.inject.Inject;

import java.lang.annotation.*;

/**
 * 标识是否需要注入该对象
 * <p>
 * 和 {@link Inject} 不同的是，可以通过 {@link #required()} 设置非必填
 *
 * @see Inject
 */
@Target({ElementType.CONSTRUCTOR, ElementType.METHOD, ElementType.PARAMETER, ElementType.FIELD, ElementType.ANNOTATION_TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Autowired {
    
    /**
     * 声明当前依赖是否为必须的
     * <p>
     * 默认为 {@code true}
     */
    boolean required() default true;
    
}
