package cc.shacocloud.mirage.bean;

import cc.shacocloud.mirage.bean.bind.Component;
import cc.shacocloud.mirage.bean.bind.ComponentScan;
import cc.shacocloud.mirage.bean.meta.BeanKey;
import org.jetbrains.annotations.NotNull;

import java.util.Set;

/**
 * {@link BeanKey} 加载器
 *
 * @author 思追(shaco)
 * @see Component
 * @see ComponentScan
 */
public interface BeanKeyLoader {
    
    /**
     * 获取指定路径下所有满足Bean条件的 {@link BeanKey}
     */
    Set<BeanKey> findBeanKeys(@NotNull String... basePackages);
    
    /**
     * 获取指定基础类型的 {@link BeanKey}
     */
    Set<BeanKey> findBeanKeys(@NotNull Class<?>... baseClasses);
    
}
