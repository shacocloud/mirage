package cc.shacocloud.mirage.bean;

import cc.shacocloud.mirage.bean.bind.Prototype;
import cc.shacocloud.mirage.bean.impl.DefaultScopeHandler;
import jakarta.inject.Scope;
import jakarta.inject.Singleton;

import java.lang.reflect.AnnotatedElement;

/**
 * {@link Scope} 注解处理器
 * <p>
 * 默认情况下，即不标识 {@link Scope} 效果和 {@link Singleton} 一致，如果标识了 {@link Prototype} 则为多例的。
 * 即：默认情况下未定义 {@link Prototype} 时都是单例对象
 * <p>
 * 如果定义了新的范围注解，请继承 {@link DefaultScopeHandler} 拓展
 *
 * @author 思追(shaco)
 * @see Singleton
 * @see Prototype
 * @see DefaultScopeHandler
 */
public interface ScopeHandler {
    
    /**
     * 是否为单例对象
     *
     * @param element {@link AnnotatedElement}
     * @return 如果是单例则返回 true 反之为 false
     */
    boolean isSingleton(AnnotatedElement element);
    
}
