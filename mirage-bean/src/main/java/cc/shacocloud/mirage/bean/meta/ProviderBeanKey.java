package cc.shacocloud.mirage.bean.meta;

import cc.shacocloud.mirage.utils.ResolvableType;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

/**
 * {@link jakarta.inject.Provider} 队医的 {@link BeanKey}
 */
public class ProviderBeanKey extends BeanKey {
    
    public ProviderBeanKey(@NotNull Class<?> beanClass) {
        super(beanClass);
    }
    
    public ProviderBeanKey(@NotNull ResolvableType type) {
        super(type);
    }
    
    public ProviderBeanKey(@NotNull Class<?> beanClass, @Nullable String qualifier) {
        super(beanClass, qualifier);
    }
    
    public ProviderBeanKey(@NotNull ResolvableType type, @Nullable String qualifier) {
        super(type, qualifier);
    }
    
    public ProviderBeanKey(@NotNull ResolvableType type, @Nullable String qualifier, boolean required) {
        super(type, qualifier, required);
    }
    
    @Override
    public ResolvableType getType() {
        return super.getType().getNested(2);
    }
    
    @Override
    public boolean isProvider() {
        return true;
    }
    
    @Override
    public String toString() {
        return "[provider] " + super.toString();
    }
}
