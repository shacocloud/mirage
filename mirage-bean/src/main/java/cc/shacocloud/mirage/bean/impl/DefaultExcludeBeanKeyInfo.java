package cc.shacocloud.mirage.bean.impl;

import cc.shacocloud.mirage.bean.ExcludeBeanKeyInfo;

/**
 * {@link ExcludeBeanKeyInfo} 的默认实现
 *
 * @author 思追(shaco)
 */
public class DefaultExcludeBeanKeyInfo extends AbstractBeanKeyMap<Object> implements ExcludeBeanKeyInfo {
}
