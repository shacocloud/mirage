package cc.shacocloud.mirage.bean.exception;

/**
 * 没有这样的bean例外
 *
 * @author 思追(shaco)
 */
public class NoSuchBeanException extends BeanException {
    
    public NoSuchBeanException() {
    }
    
    public NoSuchBeanException(String message) {
        super(message);
    }
    
    public NoSuchBeanException(String message, Throwable cause) {
        super(message, cause);
    }
    
    public NoSuchBeanException(Throwable cause) {
        super(cause);
    }
}
