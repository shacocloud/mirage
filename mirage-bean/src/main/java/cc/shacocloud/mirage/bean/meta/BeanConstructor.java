package cc.shacocloud.mirage.bean.meta;

import org.jetbrains.annotations.NotNull;

import java.util.List;

/**
 * Bean 对象构造器
 *
 * @author 思追(shaco)
 */
public interface BeanConstructor {
    
    /**
     * 创建一个实例
     *
     * @param parameters 构造该对象的参数，来自于 {@link #getDependencies()}
     * @return 新的实例对象
     */
    Object createInstance(@NotNull Object[] parameters);
    
    /**
     * 获取该对象构造器的依赖
     */
    List<BeanKey> getDependencies();
    
}
