package cc.shacocloud.mirage.bean.bind;

import cc.shacocloud.mirage.utils.annotation.AliasFor;

import java.lang.annotation.*;

/**
 * 工厂类的标识注解
 * <p>
 * 标识一个或者多个 {@link Bean} 标识的对象
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Component(lazy = false)
public @interface Configuration {
    
    /**
     * 显式指定与 {@code @Configuration} 类关联的 Bean 定义的名称。如果未指定（常见情况），将自动生成 Bean 名称。
     */
    @AliasFor(annotation = Component.class)
    String value() default "";
    
}
