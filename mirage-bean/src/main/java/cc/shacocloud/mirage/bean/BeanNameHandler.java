package cc.shacocloud.mirage.bean;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

/**
 * Bean 名称处理器
 *
 * @author 思追(shaco)
 */
public interface BeanNameHandler {
    
    /**
     * 获取 Bean 类的对象名称
     *
     * @param beanClass bean 类型
     * @return 对象名称
     */
    @Nullable
    String getBeanName(@NotNull Class<?> beanClass);
    
}
