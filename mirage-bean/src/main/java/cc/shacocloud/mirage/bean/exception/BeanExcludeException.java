package cc.shacocloud.mirage.bean.exception;

import cc.shacocloud.mirage.bean.meta.BeanKey;
import lombok.Getter;

/**
 * 无法排除指定的 Bean 异常
 *
 * @author 思追(shaco)
 */
public class BeanExcludeException extends BeanException {
    
    @Getter
    private final BeanKey beanKey;
    
    public BeanExcludeException(BeanKey beanKey) {
        this.beanKey = beanKey;
    }
    
    public BeanExcludeException(String message, BeanKey beanKey) {
        super(message);
        this.beanKey = beanKey;
    }
    
    public BeanExcludeException(String message, Throwable cause, BeanKey beanKey) {
        super(message, cause);
        this.beanKey = beanKey;
    }
    
    public BeanExcludeException(Throwable cause, BeanKey beanKey) {
        super(cause);
        this.beanKey = beanKey;
    }
}
