package cc.shacocloud.mirage.bean.impl;

import cc.shacocloud.mirage.bean.ScopeHandler;
import cc.shacocloud.mirage.bean.bind.Prototype;
import cc.shacocloud.mirage.utils.annotation.AnnotatedElementUtils;

import java.lang.reflect.AnnotatedElement;

/**
 * {@link ScopeHandler} 的默认实现
 *
 * @author 思追(shaco)
 */
public class DefaultScopeHandler implements ScopeHandler {
    
    @Override
    public boolean isSingleton(AnnotatedElement element) {
        return !AnnotatedElementUtils.hasAnnotation(element, Prototype.class);
    }
    
}
