package cc.shacocloud.mirage.bean.aware;

import cc.shacocloud.mirage.bean.BeanFactory;
import org.jetbrains.annotations.NotNull;

/**
 * 对象工厂感知接口
 *
 * @author 思追(shaco)
 */
public interface BeanFactoryAware extends Aware {
    
    /**
     * 在对象工厂初始化后，注入工厂的对象，会调用该方法注入 {@link BeanFactory}
     */
    void setBeanFactory(@NotNull BeanFactory beanFactory);
}
