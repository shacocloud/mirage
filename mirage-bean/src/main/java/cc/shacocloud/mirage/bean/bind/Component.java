package cc.shacocloud.mirage.bean.bind;

import cc.shacocloud.mirage.bean.BeanFactory;
import cc.shacocloud.mirage.utils.annotation.AliasFor;
import jakarta.inject.Named;
import jakarta.inject.Singleton;

import java.lang.annotation.*;

/**
 * 带注解的对象被当作组件。在使用基于注释的配置和类路径扫描时，此类类被视为自动检测的候选项
 *
 * @author 思追(shaco)
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Named
@Singleton
public @interface Component {
    
    /**
     * 该值可能指示对逻辑组件名称的建议，以便在自动检测到组件的情况下注入带 bean factory 中，
     * 如果为空则默认为 对象类名的驼峰命名规范值，开头小写
     *
     * @see Class#getSimpleName()
     * @see cc.shacocloud.mirage.utils.charSequence.StrUtil#toCamelCase(CharSequence)
     */
    @AliasFor(annotation = Named.class)
    String value() default "";
    
    /**
     * 组件加载时机，默认情况下所有组件都是懒加载，即 在使用的时候才去初始化对象。
     * 如果设置为 false 则会在{@link BeanFactory} 初始化成功后初始化这些非懒加载对象。
     */
    boolean lazy() default true;
    
    
}
