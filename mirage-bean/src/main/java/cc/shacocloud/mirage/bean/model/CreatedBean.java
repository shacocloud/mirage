package cc.shacocloud.mirage.bean.model;

import cc.shacocloud.mirage.bean.meta.BeanKey;
import lombok.Getter;
import org.jetbrains.annotations.NotNull;

/**
 * 创建 Bean 的临时包装器
 *
 * @author 思追(shaco)
 */
@Getter
public class CreatedBean {
    
    private final Object instance;
    private final boolean singleton;
    private final BeanKey beanKey;
    
    public CreatedBean(@NotNull Object instance,
                       boolean singleton,
                       @NotNull BeanKey beanKey) {
        this.instance = instance;
        this.singleton = singleton;
        this.beanKey = beanKey;
    }
}
