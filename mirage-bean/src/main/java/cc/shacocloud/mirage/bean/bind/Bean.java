package cc.shacocloud.mirage.bean.bind;

import cc.shacocloud.mirage.bean.BeanFactory;
import cc.shacocloud.mirage.utils.annotation.AliasFor;
import jakarta.inject.Named;
import jakarta.inject.Singleton;

import java.lang.annotation.*;

/**
 * 指示方法生成由对象工厂管理的对象。
 */
@Target({ElementType.METHOD, ElementType.ANNOTATION_TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Named
@Singleton
public @interface Bean {
    
    /**
     * {@link #name} 的别名
     * <p>
     * 旨在在不需要其他属性时使用
     *
     * @see #name
     */
    @AliasFor(annotation = Named.class)
    String value() default "";
    
    /**
     * 此 Bean 的名称
     *
     * @see #value
     */
    @AliasFor(value = "value", annotation = Named.class)
    String name() default "";
    
    /**
     * 组件加载时机，默认情况下所有组件都是懒加载，即 在使用的时候才去初始化对象。
     * 如果设置为 false 则会在{@link BeanFactory} 初始化成功后初始化这些非懒加载对象。
     */
    boolean lazy() default true;
    
}
