package cc.shacocloud.mirage.bean.meta;

import cc.shacocloud.mirage.utils.reflection.ReflectUtil;
import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.lang.reflect.Method;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

/**
 * 描述方法依赖关系的元数据
 *
 * @author 思追(shaco)
 */
public class MethodInjectionPoint implements BeanInjectionPoint {
    
    @Getter
    private final Method method;
    
    private final List<BeanKey> dependencies;
    
    public MethodInjectionPoint(Method method,
                                List<BeanKey> dependencies) {
        this.method = method;
        this.dependencies = dependencies;
    }
    
    @Override
    public void inject(@Nullable Object object, @NotNull Object @NotNull [] parameters) {
        ReflectUtil.invokeMethod(method, object, parameters);
    }
    
    @Override
    public List<BeanKey> getDependencies() {
        return Collections.unmodifiableList(dependencies);
    }
    
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MethodInjectionPoint that = (MethodInjectionPoint) o;
        return Objects.equals(method, that.method) && Objects.equals(dependencies, that.dependencies);
    }
    
    @Override
    public int hashCode() {
        return Objects.hash(method, dependencies);
    }
}
