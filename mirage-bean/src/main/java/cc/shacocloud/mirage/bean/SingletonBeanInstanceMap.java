package cc.shacocloud.mirage.bean;

import cc.shacocloud.mirage.bean.meta.BeanKey;
import jakarta.inject.Singleton;

/**
 * 用于存储 {@link BeanKey} 和对应的对象实例映射关系的 Map
 * <p>
 * 只存储单例的对象
 *
 * @author 思追(shaco)
 * @see Singleton
 */
public interface SingletonBeanInstanceMap extends BeanKeyMap<Object> {

}
