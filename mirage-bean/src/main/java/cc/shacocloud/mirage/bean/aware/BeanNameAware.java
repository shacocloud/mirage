package cc.shacocloud.mirage.bean.aware;

import org.jetbrains.annotations.NotNull;

/**
 * 对象名称感知接口
 *
 * @author 思追(shaco)
 */
public interface BeanNameAware {
    
    /**
     * 将当前对象的名称通过该回调赋值给当前对象
     *
     * @param name 对象名称
     */
    void setBeanName(@NotNull String name);
    
}
