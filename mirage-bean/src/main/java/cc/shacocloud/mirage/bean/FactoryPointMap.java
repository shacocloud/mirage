package cc.shacocloud.mirage.bean;

import cc.shacocloud.mirage.bean.meta.BeanKey;
import cc.shacocloud.mirage.bean.meta.FactoryPoint;

/**
 * 用于存储 {@link BeanKey} 和 {@link FactoryPoint} 映射关系的 Map
 *
 * @author 思追(shaco)
 */
public interface FactoryPointMap extends BeanKeyMap<FactoryPoint> {

}
