package cc.shacocloud.mirage.bean;

import cc.shacocloud.mirage.bean.bind.Configuration;
import cc.shacocloud.mirage.bean.bind.Import;
import cc.shacocloud.mirage.bean.meta.BeanDescription;
import cc.shacocloud.mirage.bean.meta.BeanKey;
import cc.shacocloud.mirage.utils.annotation.AnnotatedElementMetadata;
import org.jetbrains.annotations.NotNull;

/**
 * 导入 {@link BeanDescription} 的注册表
 * <p>
 * 使用 {@link Import} 在 {@link Configuration} 上导入指定的对象描述注册器
 * <p>
 * 注意：该接口的实现不会注入对象工厂且必须存在无参构造函数，将用来初始化该对象
 *
 * @author 思追(shaco)
 * @see Import
 * @see BeanDescriptionRegistry
 */
public interface ImportBeanDescriptionRegistrar {
    
    /**
     * 根据需要注册 {@link BeanDescription} 来定义新的对象
     *
     * @param annotatedMetadata {@link Import} 定义的类元数据
     * @param registry          对象描述注册器
     * @param beanKeyLoader     {@link BeanKey} 加载器
     */
    void registerBeanDescriptions(@NotNull AnnotatedElementMetadata annotatedMetadata,
                                  @NotNull BeanDescriptionRegistry registry,
                                  @NotNull BeanKeyLoader beanKeyLoader);
    
    
}
