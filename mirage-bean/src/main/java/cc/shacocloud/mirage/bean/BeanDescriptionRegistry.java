package cc.shacocloud.mirage.bean;

import cc.shacocloud.mirage.bean.exception.BeanCannotBeOverriddenException;
import cc.shacocloud.mirage.bean.exception.NoSuchBeanException;
import cc.shacocloud.mirage.bean.meta.BeanDescription;
import cc.shacocloud.mirage.bean.meta.BeanKey;
import org.jetbrains.annotations.NotNull;

/**
 * 对象描述注册表
 *
 * @author 思追(shaco)
 */
public interface BeanDescriptionRegistry {
    
    /**
     * 注册 {@link BeanDescription}，如果已经存在将会覆盖，但是如果该实例已经被创建将无法覆盖！
     *
     * @param description 对象描述器
     */
    void registerBeanDescription(@NotNull BeanDescription description) throws BeanCannotBeOverriddenException;
    
    /**
     * 获取对象描述信息
     *
     * @param beanKey {@link BeanKey}
     * @return {@link BeanDescription}
     */
    @NotNull
    BeanDescription getBeanDescription(@NotNull BeanKey beanKey) throws NoSuchBeanException;
    
}
