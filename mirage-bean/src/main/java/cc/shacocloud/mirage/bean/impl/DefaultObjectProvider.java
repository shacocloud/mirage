package cc.shacocloud.mirage.bean.impl;

import cc.shacocloud.mirage.bean.ConfigurableBeanFactory;
import cc.shacocloud.mirage.bean.ObjectProvider;
import cc.shacocloud.mirage.bean.exception.BeanException;
import cc.shacocloud.mirage.bean.meta.BeanKey;

import java.util.stream.Stream;

/**
 * {@link ObjectProvider} 的默认提供器实现
 *
 * @author 思追(shaco)
 */
public class DefaultObjectProvider<T> implements ObjectProvider<T> {
    
    private final ConfigurableBeanFactory beanFactory;
    private final BeanKey beanKey;
    
    public DefaultObjectProvider(ConfigurableBeanFactory beanFactory,
                                 BeanKey beanKey) {
        this.beanFactory = beanFactory;
        this.beanKey = beanKey;
    }
    
    @Override
    public T get() throws BeanException {
        return beanFactory.getBean(beanKey);
    }
    
    @SuppressWarnings("unchecked")
    @Override
    public Stream<T> stream() {
        return (Stream<T>) beanFactory.getBeans(beanKey).values().stream();
    }
}
