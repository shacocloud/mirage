package cc.shacocloud.mirage.bean.bind;

import cc.shacocloud.mirage.utils.annotation.AliasFor;
import jakarta.inject.Named;

import java.lang.annotation.*;

/**
 * 组件扫描，定义在组件类上，将自动扫描该路径下的组件，通常来说 会定义在使用 {@link Configuration} 的类上
 * <p>
 * 如果 {@link #scanBasePackages} 和 {@link #scanBasePackageClasses} 都未定义则使用标识的类的包名作为扫描路径
 *
 * @author 思追(shaco)
 * @see Configuration
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Named
public @interface ComponentScan {
    
    /**
     * 基础扫描路径
     *
     * @see #scanBasePackages
     */
    @AliasFor("scanBasePackages")
    String[] value() default {};
    
    /**
     * 基础扫描路径
     *
     * @see #value
     */
    @AliasFor("value")
    String[] scanBasePackages() default {};
    
    /**
     * 扫描的基础类
     */
    Class<?>[] scanBasePackageClasses() default {};
    
}
