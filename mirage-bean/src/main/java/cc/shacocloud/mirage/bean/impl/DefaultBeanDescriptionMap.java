package cc.shacocloud.mirage.bean.impl;

import cc.shacocloud.mirage.bean.BeanDescriptionMap;
import cc.shacocloud.mirage.bean.meta.BeanDescription;

/**
 * {@link BeanDescriptionMap} 的默认实现
 *
 * @author 思追(shaco)
 */
public class DefaultBeanDescriptionMap extends AbstractBeanKeyMap<BeanDescription> implements BeanDescriptionMap {


}
