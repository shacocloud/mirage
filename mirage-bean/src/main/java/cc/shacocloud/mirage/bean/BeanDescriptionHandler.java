package cc.shacocloud.mirage.bean;

import cc.shacocloud.mirage.bean.meta.BeanDescription;
import cc.shacocloud.mirage.bean.meta.BeanKey;
import org.jetbrains.annotations.NotNull;

/**
 * 对象描述处理器
 * <p>
 * 基于 {@link BeanKey} 构建 {@link BeanDescription}
 *
 * @author 思追(shaco)
 */
public interface BeanDescriptionHandler {
    
    /**
     * 获取 {@link BeanKey} 的 {@link BeanDescription}。
     * <p>
     * 此元数据包含有关标记为依赖关系注入的构造函数、方法和字段的信息。
     *
     * @param beanKey {@link BeanKey}
     * @return {@link BeanDescription}
     */
    BeanDescription getBeanDescription(@NotNull BeanKey beanKey);
    
    
}
