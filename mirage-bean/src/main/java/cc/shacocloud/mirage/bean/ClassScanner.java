package cc.shacocloud.mirage.bean;

import java.util.Set;

/**
 * 用于扫描类的接口
 *
 * @author 思追(shaco)
 */
public interface ClassScanner {
    
    /**
     * 获取类扫描使用的类加载器
     */
    ClassLoader getClassLoader();
    
    /**
     * 查找给定包和子包中的所有类
     * <p>
     * 扫描的基本包路径规范：<br/>
     * 示例：cc.shacocloud.mirage.bean 使用 {@code .} 作为分隔符
     *
     * @param basePackages 基本包路径
     * @return 在搜索中找到的所有类的集合
     */
    Set<Class<?>> findClasses(String... basePackages);
    
}
