package cc.shacocloud.mirage.bean.exception;

/**
 * bean 模块的顶级异常对象
 *
 * @author 思追(shaco)
 */
public class BeanException extends RuntimeException {
    
    public BeanException() {
    }
    
    public BeanException(String message) {
        super(message);
    }
    
    public BeanException(String message, Throwable cause) {
        super(message, cause);
    }
    
    public BeanException(Throwable cause) {
        super(cause);
    }
}
