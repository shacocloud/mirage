package cc.shacocloud.mirage.bean.exception;

import cc.shacocloud.mirage.bean.meta.BeanKey;
import lombok.Getter;

/**
 * Bean 无法被覆盖异常
 *
 * @author 思追(shaco)
 */
public class BeanCannotBeOverriddenException extends BeanException {
    
    @Getter
    private final BeanKey beanKey;
    
    public BeanCannotBeOverriddenException(BeanKey beanKey) {
        super(beanKey + " 无法被覆盖！");
        this.beanKey = beanKey;
    }
    
    public BeanCannotBeOverriddenException(String message, BeanKey beanKey) {
        super(message);
        this.beanKey = beanKey;
    }
    
    public BeanCannotBeOverriddenException(String message, Throwable cause, BeanKey beanKey) {
        super(message, cause);
        this.beanKey = beanKey;
    }
    
    public BeanCannotBeOverriddenException(Throwable cause, BeanKey beanKey) {
        super(cause);
        this.beanKey = beanKey;
    }
}
