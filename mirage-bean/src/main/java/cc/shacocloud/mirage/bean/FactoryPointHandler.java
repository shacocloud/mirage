package cc.shacocloud.mirage.bean;

import cc.shacocloud.mirage.bean.bind.Configuration;
import cc.shacocloud.mirage.bean.impl.AnnotationFactoryPointHandler;
import cc.shacocloud.mirage.bean.meta.BeanKey;
import cc.shacocloud.mirage.bean.meta.FactoryPoint;
import org.jetbrains.annotations.NotNull;

import java.util.List;

/**
 * 用于从 bean 获取有关工厂点的元数据的接口
 *
 * @author 思追(shaco)
 * @see Configuration
 * @see AnnotationFactoryPointHandler
 */
public interface FactoryPointHandler {
    
    /**
     * 获取工厂 Bean 中所有工厂点的元数据
     */
    @NotNull
    List<FactoryPoint> getFactoryPoints(@NotNull BeanKey factoryBean);
    
}
