package cc.shacocloud.mirage.bean.meta;

import cc.shacocloud.mirage.bean.ImportBeanDescriptionRegistrar;
import cc.shacocloud.mirage.utils.annotation.AnnotatedElementMetadata;
import org.jetbrains.annotations.NotNull;

import java.util.List;
import java.util.Map;

/**
 * 导入描述器对象
 *
 * @author 思追(shaco)
 */
public interface ImportDescription {
    
    /**
     * 获取导入的组件类
     */
    @NotNull
    List<Class<?>> getComponentClass();
    
    /**
     * 获取导入对象描述注册器
     *
     * @return {@link ImportBeanDescriptionRegistrar}
     */
    @NotNull
    Map<ImportBeanDescriptionRegistrar, AnnotatedElementMetadata> getImportBeanDescriptionRegistrar();
    
}
