package cc.shacocloud.mirage.bean.impl;

import cc.shacocloud.mirage.bean.FactoryPointMap;
import cc.shacocloud.mirage.bean.meta.BeanKey;
import cc.shacocloud.mirage.bean.meta.FactoryPoint;
import cc.shacocloud.mirage.utils.annotation.AnnotatedElementUtils;
import org.jetbrains.annotations.NotNull;

import java.lang.annotation.Annotation;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;

/**
 * {@link FactoryPointMap} 的默认实现
 *
 * @author 思追(shaco)
 */
public class DefaultFactoryPointMap extends AbstractBeanKeyMap<FactoryPoint> implements FactoryPointMap {
    
    @Override
    protected boolean canInject(@NotNull BeanKey source, @NotNull BeanKey target) {
        return source.canInjectFromFactory(target) && !target.hasTheAnyQualifier();
    }
    
    /**
     * {@link FactoryPoint} 注解定义在了方法上使用方法来扫描
     */
    @NotNull
    @Override
    public Collection<BeanKey> findBeanKeysByAnnotation(@NotNull Class<? extends Annotation> annotation) {
        Collection<BeanKey> matches = new ArrayList<>();
        for (Map.Entry<BeanKey, FactoryPoint> entry : map.entrySet()) {
            FactoryPoint point = entry.getValue();
            if (AnnotatedElementUtils.hasAnnotation(point.getMethod(), annotation)) {
                matches.add(entry.getKey());
            }
        }
        
        return matches;
    }
}
