package cc.shacocloud.mirage.bean;

/**
 * 在 {@link BeanFactory} 创建好对象后，所有的依赖，配置属性注入后，执行该回调完成对象的初始化
 *
 * @author 思追(shaco)
 */
public interface InitializingBean {
    
    /**
     * 初始化回调方法
     *
     * @throws Exception 初始化失败则抛出该例外
     */
    void init() throws Exception;
    
}
