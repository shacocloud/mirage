package cc.shacocloud.mirage.bean.meta;

import org.jetbrains.annotations.NotNull;

import java.util.List;

/**
 * 该类描述创建一个 Bean 所需要的元数据信息
 *
 * @author 思追(shaco)
 */
public interface BeanDescription {
    
    /**
     * 当前对象的 {@link BeanKey}
     */
    @NotNull
    BeanKey getBeanKey();
    
    /**
     * 是否是单例对象
     *
     * @return true 为单例，false 为多例
     */
    boolean isSingleton();
    
    /**
     * 是否是懒加载对象
     *
     * @return true 是，false 不是
     */
    boolean isLazy();
    
    /**
     * 获取对象构造描述器
     *
     * @return {@link BeanConstructor}
     */
    @NotNull
    BeanConstructor getBeanConstructor();
    
    /**
     * 当前对象的注入点
     */
    @NotNull
    List<BeanInjectionPoint> getInjectionPoints();
    
}
