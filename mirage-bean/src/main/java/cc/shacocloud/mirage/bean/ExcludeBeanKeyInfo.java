package cc.shacocloud.mirage.bean;

import cc.shacocloud.mirage.bean.meta.BeanKey;
import org.jetbrains.annotations.NotNull;

/**
 * 用于排除的 {@link BeanKey}
 *
 * @author 思追(shaco)
 */
public interface ExcludeBeanKeyInfo extends BeanKeyMap<Object> {
    
    Object EMPTY = new Object();
    
    /**
     * 添加排除的 {@link BeanKey}
     */
    default void add(@NotNull BeanKey beanKey) {
        add(beanKey, EMPTY);
    }
    
}
