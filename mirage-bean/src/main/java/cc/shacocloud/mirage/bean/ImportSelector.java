package cc.shacocloud.mirage.bean;

import cc.shacocloud.mirage.bean.bind.Import;
import cc.shacocloud.mirage.bean.meta.ImportDescription;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

/**
 * {@link Import} 选择器
 *
 * @author 思追(shaco)
 */
public interface ImportSelector {
    
    /**
     * 查找指定类型定义的 {@link Import} 描述信息，如果未定义则返回 null
     *
     * @param beanClass 对象类型
     * @return null 或者 {@link ImportDescription}
     */
    @Nullable
    ImportDescription findImports(@NotNull Class<?> beanClass);
    
}
