package cc.shacocloud.mirage.bean.meta;

import cc.shacocloud.mirage.utils.reflection.ReflectUtil;
import lombok.Getter;
import org.jetbrains.annotations.NotNull;

import java.lang.reflect.Method;
import java.util.Collections;
import java.util.List;

/**
 * {@link FactoryPoint} 的默认实现
 *
 * @author 思追(shaco)
 */
public class FactoryPointMethod implements FactoryPoint {
    
    @Getter
    @NotNull
    private final BeanKey factoryKey;
    
    @Getter
    @NotNull
    private final Method method;
    
    @Getter
    @NotNull
    private final BeanKey returnType;
    
    @NotNull
    private final List<BeanKey> parameters;
    
    @Getter
    private final boolean singleton;
    
    @Getter
    private final boolean lazy;
    
    public FactoryPointMethod(@NotNull BeanKey factoryKey,
                              @NotNull Method method,
                              @NotNull BeanKey returnType,
                              @NotNull List<BeanKey> parameters,
                              boolean singleton,
                              boolean lazy) {
        this.factoryKey = factoryKey;
        this.method = method;
        this.returnType = returnType;
        this.parameters = parameters;
        this.singleton = singleton;
        this.lazy = lazy;
    }
    
    @Override
    public Object create(@NotNull Object factoryInstance, @NotNull Object... parameters) {
        return ReflectUtil.invokeMethod(method, factoryInstance, parameters);
    }
    
    @Override
    public List<BeanKey> getParameters() {
        return Collections.unmodifiableList(parameters);
    }
    
}
