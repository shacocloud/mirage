package cc.shacocloud.mirage.bean;

import cc.shacocloud.mirage.bean.meta.BeanDescription;
import cc.shacocloud.mirage.bean.meta.BeanKey;
import cc.shacocloud.mirage.bean.meta.FactoryPoint;
import org.jetbrains.annotations.NotNull;

/**
 * 添加到工厂的 Bean 的一个前置条件，如果满足该条件那么则允许添加到工厂中
 * <p>
 * {@link ConditionHandler}是一个拓展接口，在模块 {@code mirage-context} 中有基于应用上下文的拓展
 *
 * @author 思追(shaco)
 * @see BeanDescription
 * @see FactoryPoint
 */
public interface ConditionHandler {
    
    /**
     * 判断该 {@link BeanDescription} 是否允许被添加到 {@code beanFactory} 中
     *
     * @param beanFactory 被添加的对象工厂
     * @param beanKey     对象唯一键
     * @return 如果返回 true 则添加，反之不添加
     */
    boolean matches(@NotNull ConfigurableBeanFactory beanFactory, @NotNull BeanKey beanKey);
    
    /**
     * 判断该 {@link FactoryPoint} 是否允许被添加到 {@code beanFactory} 中
     *
     * @param beanFactory  被添加的对象工厂
     * @param factoryPoint 添加工厂方法点
     * @return 如果返回 true 则添加，反之不添加
     */
    boolean matches(@NotNull ConfigurableBeanFactory beanFactory, @NotNull FactoryPoint factoryPoint);
    
}
