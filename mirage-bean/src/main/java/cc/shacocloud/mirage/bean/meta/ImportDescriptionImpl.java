package cc.shacocloud.mirage.bean.meta;

import cc.shacocloud.mirage.bean.ImportBeanDescriptionRegistrar;
import cc.shacocloud.mirage.utils.annotation.AnnotatedElementMetadata;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;
import java.util.Map;

/**
 * {@link ImportDescription} 的默认实现
 *
 * @author 思追(shaco)
 */
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class ImportDescriptionImpl implements ImportDescription {
    
    private List<Class<?>> componentClass;
    
    private Map<ImportBeanDescriptionRegistrar, AnnotatedElementMetadata> importBeanDescriptionRegistrar;
    
}
