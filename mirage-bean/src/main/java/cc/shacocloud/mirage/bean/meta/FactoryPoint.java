package cc.shacocloud.mirage.bean.meta;

import cc.shacocloud.mirage.bean.bind.Bean;
import cc.shacocloud.mirage.bean.bind.Configuration;
import org.jetbrains.annotations.NotNull;

import java.lang.reflect.Method;
import java.util.List;

/**
 * 当前接口表示为工厂类的中定义的方法注入点
 *
 * @see Bean
 * @see Configuration
 */
public interface FactoryPoint {
    
    /**
     * 描述包含此工厂成员的实际 Bean 的键，即 {@link Configuration} 标识的对象键
     */
    BeanKey getFactoryKey();
    
    /**
     * 返回此工厂点的方法
     */
    Method getMethod();
    
    /**
     * 创建此工厂点的返回类型的新实例，并注入参数
     */
    Object create(@NotNull Object factoryInstance, @NotNull Object... parameters);
    
    /**
     * 获取执行此工厂点所需的参数的键
     */
    List<BeanKey> getParameters();
    
    /**
     * 此工厂方法的返回值对象的键
     */
    BeanKey getReturnType();
    
    /**
     * 此工厂方法创建的对象是否是单例对象
     */
    boolean isSingleton();
    
    /**
     * 此工厂方法创建的对象是否是懒加载对象
     */
    boolean isLazy();
    
}
