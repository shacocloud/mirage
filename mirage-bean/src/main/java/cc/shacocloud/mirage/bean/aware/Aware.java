package cc.shacocloud.mirage.bean.aware;

import cc.shacocloud.mirage.bean.BeanFactory;

/**
 * 一个感知标识的超类，通常用于对象创建成功后注入工厂相关的容器实例。
 * <p>
 * 例如：在对象初始化成功后注入 {@link BeanFactory} 就使用 {@link BeanFactoryAware}
 *
 * @author 思追(shaco)
 * @see BeanFactoryAware
 */
public interface Aware {
}
