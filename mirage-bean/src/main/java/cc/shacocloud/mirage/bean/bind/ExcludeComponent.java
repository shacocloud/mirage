package cc.shacocloud.mirage.bean.bind;

/**
 * 排除指定组件，用于排除对象工厂中的指定类型的对象或者的指定名称的对象，如果该对象已经初始化那么将无法排除，反之可以。
 * 需要定义在 {@link Configuration} 的类上才能生效
 *
 * @author 思追(shaco)
 */
public @interface ExcludeComponent {
    
    /**
     * 排除的类
     */
    Class<?>[] excludeClasses() default {};
    
    /**
     * 排除的类名称，即 对象工厂中的对象名称
     */
    String[] excludeBeanName() default {};
    
}
