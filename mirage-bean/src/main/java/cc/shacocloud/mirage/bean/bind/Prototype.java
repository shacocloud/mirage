package cc.shacocloud.mirage.bean.bind;

import jakarta.inject.Scope;
import jakarta.inject.Singleton;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;

import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * 与 {@link Singleton} 恰恰相反，每次获取对象都会创建一个新的
 *
 * @author 思追(shaco)
 * @see Singleton
 */
@Scope
@Documented
@Retention(RUNTIME)
public @interface Prototype {
}
