package cc.shacocloud.mirage.bean;

import cc.shacocloud.mirage.bean.exception.BeanCircularDependencyException;
import cc.shacocloud.mirage.bean.meta.BeanKey;
import org.jetbrains.annotations.NotNull;

import java.util.Collection;
import java.util.LinkedHashSet;

/**
 * 用于记录创建对象的过程，以尽可能的解决循环依赖问题
 *
 * @author 思追(shaco)
 */
public class BeanCreating {
    
    private final Collection<BeanKey> beans = new LinkedHashSet<>();
    
    /**
     * 将一个新 bean添加到创建列表
     *
     * @throws BeanCircularDependencyException 如果出现循环依赖则抛出
     */
    public synchronized void add(@NotNull BeanKey bean) throws BeanCircularDependencyException {
        
        if (contains(bean)) {
            throw new BeanCircularDependencyException(beans);
        }
        
        beans.add(bean);
    }
    
    /**
     * 从创建列表中的bean中移除bean
     */
    public synchronized void remove(@NotNull BeanKey bean) {
        beans.remove(bean);
    }
    
    /**
     * 检查当前正在创建的 bean 是否在创建列表中
     */
    public synchronized boolean contains(@NotNull BeanKey bean) {
        return beans.contains(bean);
    }
    
    /**
     * 返回当前正在创建的 bean 的数量
     */
    public synchronized int size() {
        return beans.size();
    }
    
    /**
     * 清理创建列表
     */
    public synchronized void clear() {
        beans.clear();
    }
}
