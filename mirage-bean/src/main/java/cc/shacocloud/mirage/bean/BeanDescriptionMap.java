package cc.shacocloud.mirage.bean;

import cc.shacocloud.mirage.bean.meta.BeanDescription;
import cc.shacocloud.mirage.bean.meta.BeanKey;

/**
 * 用于存储 {@link BeanKey} 和 {@link BeanDescription} 映射关系的 Map
 *
 * @author 思追(shaco)
 */
public interface BeanDescriptionMap extends BeanKeyMap<BeanDescription> {


}
