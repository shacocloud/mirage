package cc.shacocloud.mirage.bean;

import cc.shacocloud.mirage.bean.aware.Aware;
import cc.shacocloud.mirage.bean.aware.BeanFactoryAware;
import cc.shacocloud.mirage.bean.aware.BeanNameAware;
import cc.shacocloud.mirage.bean.bind.Configuration;
import cc.shacocloud.mirage.bean.bind.ExcludeComponent;
import cc.shacocloud.mirage.bean.exception.*;
import cc.shacocloud.mirage.bean.impl.*;
import cc.shacocloud.mirage.bean.meta.*;
import cc.shacocloud.mirage.bean.model.CreatedBean;
import cc.shacocloud.mirage.utils.ClassUtil;
import cc.shacocloud.mirage.utils.Utils;
import cc.shacocloud.mirage.utils.annotation.AnnotatedElementMetadata;
import cc.shacocloud.mirage.utils.collection.CollUtil;
import cc.shacocloud.mirage.utils.collection.SelfSortList;
import cc.shacocloud.mirage.utils.comparator.AnnotationOrderComparator;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.lang.annotation.Annotation;
import java.lang.reflect.AnnotatedElement;
import java.util.*;

/**
 * 抽象的对象工厂实现
 * <p>
 * 从 {@link MirageFactoriesLoader#FACTORIES_RESOURCE_LOCATION} 文件中加载元对象配置信息
 *
 * @author 思追(shaco)
 * @see #init()
 */
@Slf4j
public abstract class AbstractBeanFactory implements ConfigurableBeanFactory, BeanDescriptionRegistry {
    
    // 用于检测循环依赖
    protected final BeanCreating beanCreating = new BeanCreating();
    // BeanKey 的加载器
    @NotNull
    protected final BeanKeyLoader beanKeyLoader;
    // 对象描述处理器，用于加载对象描述信息
    @NotNull
    protected final BeanDescriptionHandler beanDescriptionHandler;
    // 工厂注入点处理器，加载一个对象中的注入点方法
    @NotNull
    protected final FactoryPointHandler factoryPointHandler;
    // 对象条件处理器
    @NotNull
    protected final ConditionHandler conditionHandler;
    @NotNull
    protected final ImportSelector importSelector;
    // 用于记录被排除的对象信息
    @NotNull
    protected final ExcludeBeanKeyInfo excludeBeanKeyInfo = new DefaultExcludeBeanKeyInfo();
    // 对象创建成功后的后置处理器
    protected final List<BeanPostProcessor> beanPostProcessors = new SelfSortList<>(AnnotationOrderComparator.INSTANCE::getOrder);
    protected final List<Class<?>> unscannedBaseClasses = new ArrayList<>();
    // 用于记录对象描述信息
    @Setter
    @NotNull
    protected BeanDescriptionMap beanDescriptionMap = new DefaultBeanDescriptionMap();
    // 用于记录工厂注入点
    @Setter
    @NotNull
    protected FactoryPointMap factoryPointMap = new DefaultFactoryPointMap();
    // 用于记录单例对象
    @Setter
    @NotNull
    protected SingletonBeanInstanceMap singletonBeanInstanceMap = new DefaultSingletonBeanInstanceMap();
    // 导入对象描述注册器
    @NotNull
    protected Map<ImportBeanDescriptionRegistrar, AnnotatedElementMetadata> importBeanDescriptionRegistrarMap = new HashMap<>();
    
    public AbstractBeanFactory(@NotNull BeanKeyLoader beanKeyLoader,
                               @NotNull BeanDescriptionHandler beanDescriptionHandler,
                               @NotNull FactoryPointHandler factoryPointHandler,
                               @NotNull ConditionHandler conditionHandler) {
        this(beanKeyLoader, beanDescriptionHandler, factoryPointHandler, conditionHandler, new DefaultImportSelector());
    }
    
    public AbstractBeanFactory(@NotNull BeanKeyLoader beanKeyLoader,
                               @NotNull BeanDescriptionHandler beanDescriptionHandler,
                               @NotNull FactoryPointHandler factoryPointHandler,
                               @NotNull ConditionHandler conditionHandler,
                               @NotNull ImportSelector importSelector) {
        this.beanKeyLoader = beanKeyLoader;
        this.beanDescriptionHandler = beanDescriptionHandler;
        this.factoryPointHandler = factoryPointHandler;
        this.conditionHandler = conditionHandler;
        this.importSelector = importSelector;
    }
    
    @Override
    public void init() {
        ClassLoader classLoader = getClassLoader();
        
        if (log.isInfoEnabled()) {
            log.info("对象工厂 {}S 初始化开始，工厂类加载器：{}", getClass(), classLoader);
        }
        
        // 排除的组件
        Set<Class<?>> excludeComponentClasses = MirageFactoriesLoader.loadFactoriesExcludeComponent(classLoader);
        excludeBean(excludeComponentClasses.toArray(Class[]::new));
        
        // 加载注册的基础扫描类
        loadBean(unscannedBaseClasses.toArray(Class[]::new));
        unscannedBaseClasses.clear();
        
        // 自动装配的组件
        Set<Class<?>> configurationClasses = MirageFactoriesLoader.loadFactoriesConfiguration(classLoader);
        loadBean(configurationClasses.toArray(Class[]::new));
        
        // 初始化 Bean 的后置处理器
        Map<String, BeanPostProcessor> beanPostProcessorMaps = getBeanByType(BeanPostProcessor.class);
        addBeanPostProcessors(beanPostProcessorMaps.values());
        
        // 初始化导入 BeanDescription 注册表
        for (Map.Entry<ImportBeanDescriptionRegistrar, AnnotatedElementMetadata> entry : importBeanDescriptionRegistrarMap.entrySet()) {
            ImportBeanDescriptionRegistrar importBeanDescriptionRegistrar = entry.getKey();
            invokeAwareMethods(null, importBeanDescriptionRegistrar);
            importBeanDescriptionRegistrar.registerBeanDescriptions(entry.getValue(), this, beanKeyLoader);
        }
        
        // 初始化非懒加载对象
        initNonLazyBean();
    }
    
    /**
     * 加载包路径下的所有Bean信息
     */
    public synchronized void loadBean(@NotNull String... basePackages) {
        final long start = log.isDebugEnabled() ? System.currentTimeMillis() : 0;
        
        Set<BeanKey> beanKeys = beanKeyLoader.findBeanKeys(basePackages);
        
        loadBeanKey(beanKeys);
        
        final long stop = log.isDebugEnabled() ? System.currentTimeMillis() : 0;
        
        if (log.isDebugEnabled()) {
            log.debug(String.format("加载基础包 %s ，耗时 %s/ms", Utils.nullSafeToString(basePackages), stop - start));
        }
    }
    
    /**
     * 加载指定的基础Bean
     */
    public synchronized void loadBean(@NotNull Class<?>... baseClasses) {
        final long start = log.isDebugEnabled() ? System.currentTimeMillis() : 0;
        
        Set<BeanKey> beanKeys = beanKeyLoader.findBeanKeys(baseClasses);
        
        loadBeanKey(beanKeys);
        
        final long stop = log.isDebugEnabled() ? System.currentTimeMillis() : 0;
        
        if (log.isDebugEnabled()) {
            log.debug(String.format("加载基础类 %s 完成，耗时 %s/ms", Utils.nullSafeToString(baseClasses), stop - start));
        }
    }
    
    @Override
    public void registerScanClass(@NotNull Class<?>... baseClasses) {
        this.unscannedBaseClasses.addAll(Arrays.asList(baseClasses));
    }
    
    /**
     * 加载指定的 {@link BeanKey}
     */
    protected synchronized void loadBeanKey(@NotNull Set<BeanKey> beanKeys) {
        long start = log.isDebugEnabled() ? System.currentTimeMillis() : 0;
        
        Set<String> excludeBeanName = new HashSet<>();
        Set<Class<?>> excludeClasses = new HashSet<>();
        
        for (BeanKey beanKey : beanKeys) {
            if (!ClassUtil.isNormalClass(beanKey.getBeanClass()) || excludeBeanKeyInfo.contains(beanKey)) {
                continue;
            }
            
            // 对象条件判断
            boolean matches = conditionHandler.matches(this, beanKey);
            if (!matches) continue;
            
            BeanDescription beanDescription = beanDescriptionHandler.getBeanDescription(beanKey);
            beanDescriptionMap.add(beanKey, beanDescription);
            
            // 如果使用了 @Configuration
            if (beanKey.hasAnnotation(Configuration.class)) {
                // 如果定义了排除注解
                if (beanKey.hasAnnotation(ExcludeComponent.class)) {
                    ExcludeComponent annotation = Objects.requireNonNull(beanKey.getAnnotation(ExcludeComponent.class));
                    excludeBeanName.addAll(Arrays.asList(annotation.excludeBeanName()));
                    excludeClasses.addAll(Arrays.asList(annotation.excludeClasses()));
                }
                
                // 加载工厂注入点
                List<FactoryPoint> factoryPoints = factoryPointHandler.getFactoryPoints(beanKey);
                for (FactoryPoint factoryPoint : factoryPoints) {
                    
                    BeanKey foctoryBeanKey = factoryPoint.getReturnType();
                    if (excludeBeanKeyInfo.contains(foctoryBeanKey)) {
                        continue;
                    }
                    
                    // 工厂方法点条件判断
                    matches = conditionHandler.matches(this, factoryPoint);
                    if (!matches) continue;
                    
                    factoryPointMap.add(foctoryBeanKey, factoryPoint);
                }
                
                // 加载 @Import 导入的对象
                ImportDescription importDescription = importSelector.findImports(beanKey.getBeanClass());
                if (Objects.nonNull(importDescription)) {
                    importBeanDescriptionRegistrarMap.putAll(importDescription.getImportBeanDescriptionRegistrar());
                    
                    // 加载指定组件类型
                    List<Class<?>> componentClass = importDescription.getComponentClass();
                    if (CollUtil.isNotEmpty(componentClass)) {
                        loadBean(componentClass.toArray(new Class[0]));
                    }
                }
            }
        }
        
        if (CollUtil.isNotEmpty(excludeClasses)) {
            excludeBean(excludeClasses.toArray(Class[]::new));
        }
        
        if (CollUtil.isNotEmpty(excludeBeanName)) {
            excludeBean(excludeBeanName.toArray(String[]::new));
        }
        
        if (log.isDebugEnabled()) {
            long stop = System.currentTimeMillis();
            log.debug(String.format("加载 %s 个组件类共花费 %s ms", beanKeys.size(), stop - start));
        }
    }
    
    @Override
    public synchronized void excludeBean(@NotNull Class<?> @NotNull ... classes) throws BeanExcludeException {
        for (Class<?> clazz : classes) {
            BeanKey beanKey = new BeanKey(clazz, BeanKey.ANY_QUALIFIER);
            excludeBeanKey(beanKey);
        }
    }
    
    @Override
    public synchronized void excludeBean(@NotNull String @NotNull ... beanNames) throws BeanExcludeException {
        for (String beanName : beanNames) {
            BeanKey beanKey = new BeanKey(BeanKey.ANY_TYPE, beanName);
            excludeBeanKey(beanKey);
        }
    }
    
    /**
     * 排除指定的 {@link BeanKey}
     */
    protected synchronized void excludeBeanKey(@NotNull BeanKey beanKey) {
        if (excludeBeanKeyInfo.containsKey(beanKey)) return;
        excludeBeanKeyInfo.add(beanKey);
        
        if (singletonBeanInstanceMap.contains(beanKey)) {
            throw new BeanExcludeException(String.format("将排除的 %s 在对象工厂中已经存在初始化的对象无法排除！", beanKey), beanKey);
        }
        
        beanDescriptionMap.remove(beanKey);
        factoryPointMap.remove(beanKey);
    }
    
    /**
     * 根据 {@link BeanKey} 获取对应的对象实例，如果 {@link BeanKey#isRequired()} 为 false 则可能返回 null
     */
    @Nullable
    @SuppressWarnings("unchecked")
    @Override
    public synchronized <T> T getBean(@NotNull BeanKey beanKey) throws BeanException {
        // 如果存在于单例对象中则直接获取
        if (singletonBeanInstanceMap.contains(beanKey)) {
            return (T) singletonBeanInstanceMap.find(beanKey);
        }
        
        // 获取唯一的可用于创建的 BeanKey
        BeanKey useCreateBeanKey = getBeanKey(beanKey);
        if (Objects.isNull(useCreateBeanKey)) return null;
        
        // 创建一个新的对象
        return (T) createBean(useCreateBeanKey);
    }
    
    /**
     * 判断对象 {@link BeanKey} 是否存在
     */
    @Override
    public synchronized boolean containsBean(@NotNull BeanKey beanKey) {
        return singletonBeanInstanceMap.contains(beanKey) || containsBeanKey(beanKey);
    }
    
    /**
     * 判断对象 {@link BeanKey} 是否存在，不考虑已经初始化好的对象
     *
     * @see #containsBean(BeanKey)
     */
    protected synchronized boolean containsBeanKey(@NotNull BeanKey beanKey) {
        return beanDescriptionMap.contains(beanKey) || factoryPointMap.contains(beanKey);
    }
    
    /**
     * 根据 {@link BeanKey} 匹配的多个实例
     */
    @NotNull
    @Override
    public synchronized Map<BeanKey, Object> getBeans(@NotNull BeanKey beanKey) {
        List<BeanKey> beanKeys = getBeanKeys(beanKey);
        
        Map<BeanKey, Object> result = new HashMap<>(beanKeys.size(), 1);
        for (BeanKey bean : beanKeys) {
            Object instance = getBean(bean);
            result.put(bean, instance);
        }
        
        return result;
    }
    
    /**
     * 根据 {@link BeanKey} 匹配实际的可以用来创建对象的 {@link BeanKey}
     */
    @NotNull
    @Override
    public synchronized List<BeanKey> getBeanKeys(@NotNull BeanKey beanKey) {
        Collection<BeanKey> beanKeys = beanDescriptionMap.findBeanKeys(beanKey);
        Collection<BeanKey> factoryBeanKeys = factoryPointMap.findBeanKeys(beanKey);
        
        List<BeanKey> match = new ArrayList<>(beanKeys.size() + factoryBeanKeys.size());
        match.addAll(beanKeys);
        match.addAll(factoryBeanKeys);
        return match;
    }
    
    /**
     * 根据 {@link Annotation} 匹配实际的可以用来创建对象的 {@link BeanKey}
     */
    @NotNull
    @Override
    public synchronized List<BeanKey> getBeanKeysByAnnotation(@NotNull Class<? extends Annotation> annotation) {
        Collection<BeanKey> beanKeys = beanDescriptionMap.findBeanKeysByAnnotation(annotation);
        Collection<BeanKey> factoryBeanKeys = factoryPointMap.findBeanKeysByAnnotation(annotation);
        
        List<BeanKey> match = new ArrayList<>(beanKeys.size() + factoryBeanKeys.size());
        match.addAll(beanKeys);
        match.addAll(factoryBeanKeys);
        return match;
    }
    
    
    /**
     * 根据 {@link BeanKey} 匹配实际的可以用来创建对象的 {@link BeanKey}
     * <p>
     * 如果匹配多个或者未匹配到则抛出例外
     *
     * @return 返回匹配的唯一 {@link BeanKey} 如果参数 {@code beanKey} 允许为空则可能返回 null
     */
    @Override
    @Nullable
    public synchronized BeanKey getBeanKey(@NotNull BeanKey beanKey) throws BeanException {
        List<BeanKey> beanKeys = getBeanKeys(beanKey);
        
        // 存在多个
        if (CollUtil.isNotEmpty(beanKeys) && beanKeys.size() > 1) {
            throw new BeanException("找到多个满足 " + beanKey + " 的Bean：" + beanKeys);
        }
        // 不存在
        else if (CollUtil.isEmpty(beanKeys)) {
            if (!beanKey.isRequired()) {
                return null;
            }
            throw new NoSuchBeanException("没有这样的Bean：" + beanKey);
        }
        
        return beanKeys.get(0);
    }
    
    /**
     * 创建一个新的对象
     * <p>
     * 使用 {@link BeanCreating} 检查循环依赖
     */
    @NotNull
    @Override
    public synchronized Object createBean(@NotNull BeanKey beanKey) throws BeanException {
        try {
            beanCreating.add(beanKey);
            
            // 创建对象并且完成基本的依赖注入
            CreatedBean createdBean = doCreateBean(beanKey);
            
            BeanKey currentBeanKey = createdBean.getBeanKey();
            String beanName = currentBeanKey.getQualifier();
            Object beanInstance = createdBean.getInstance();
            
            // 对象感知处理
            if (beanInstance instanceof Aware) {
                invokeAwareMethods(beanName, beanInstance);
            }
            
            // 初始化前应用
            beanInstance = applyBeanPostProcessorsBeforeInitialization(beanName, beanInstance);
            
            // 初始化方法
            invokeInitMethods(beanName, beanInstance);
            
            // 初始化后应用
            beanInstance = applyBeanPostProcessorsAfterInitialization(beanName, beanInstance);
            
            // 如果是单例则添加到单例映射中
            if (createdBean.isSingleton()) {
                singletonBeanInstanceMap.add(currentBeanKey, beanInstance);
            }
            
            beanCreating.remove(beanKey);
            
            return beanInstance;
        } catch (BeanCircularDependencyException e) {
            beanCreating.clear();
            
            e.setBeanKey(beanKey);
            throw e;
        } catch (Exception e) {
            beanCreating.clear();
            throw new BeanException(String.format("创建对象 %s 时发生例外！", beanKey), e);
        }
    }
    
    @Override
    public @Nullable AnnotatedElement getAnnotatedElementByBeanKey(@NotNull BeanKey beanKey) throws BeanException {
        if (factoryPointMap.containsKey(beanKey)) {
            return Objects.requireNonNull(factoryPointMap.get(beanKey)).getMethod();
        } else if (beanDescriptionMap.containsKey(beanKey)) {
            return beanKey.getBeanClass();
        }
        return null;
    }
    
    @Override
    public void addBeanPostProcessor(@NotNull BeanPostProcessor beanPostProcessor) {
        // 先删除旧的，在添加新的，以避免重复添加
        beanPostProcessors.remove(beanPostProcessor);
        beanPostProcessors.add(beanPostProcessor);
    }
    
    @Override
    public void addBeanPostProcessors(@NotNull Collection<BeanPostProcessor> beanPostProcessors) {
        this.beanPostProcessors.removeAll(beanPostProcessors);
        this.beanPostProcessors.addAll(beanPostProcessors);
    }
    
    @Override
    public synchronized void registerBean(@NotNull String name, @NotNull Object bean) {
        BeanKey beanKey = new BeanKey(bean.getClass(), name);
        BeanDescription description = beanDescriptionHandler.getBeanDescription(beanKey);
        
        // 注入依赖
        autowiredBean(bean, description);
        
        singletonBeanInstanceMap.add(beanKey, bean);
        beanDescriptionMap.add(beanKey, description);
    }
    
    @Override
    public void registerBeanDescription(@NotNull BeanDescription description) {
        BeanKey beanKey = description.getBeanKey();
        if (singletonBeanInstanceMap.containsKey(beanKey)) {
            throw new BeanCannotBeOverriddenException("对象工厂中已经存在初始化的对象无法注册该对象的 BeanDescription！", beanKey);
        }
        
        beanDescriptionMap.add(beanKey, description);
    }
    
    @Override
    public @NotNull BeanDescription getBeanDescription(@NotNull BeanKey beanKey) throws NoSuchBeanException {
        if (beanDescriptionMap.containsKey(beanKey)) {
            return Objects.requireNonNull(beanDescriptionMap.get(beanKey));
        }
        throw new NoSuchBeanException(String.format("没有这样的 %s BeanDescription 定义", beanKey));
    }
    
    /**
     * 创建对象的内部方法，内部区分选择不同的方式进行创建
     */
    protected CreatedBean doCreateBean(@NotNull BeanKey beanKey) {
        if (factoryPointMap.containsKey(beanKey)) {
            return useFactoryPointCreateBean(beanKey);
        }
        
        return useInjectorCreateBean(beanKey);
    }
    
    /**
     * 使用注入器创建
     *
     * @see BeanDescription
     */
    protected CreatedBean useInjectorCreateBean(final BeanKey beanKey) {
        BeanDescription beanDescription = beanDescriptionMap.find(beanKey);
        
        // 基于构造函数创建对象
        BeanConstructor beanConstructor = beanDescription.getBeanConstructor();
        Object[] dependencies = getDependenciesBeans(beanConstructor.getDependencies());
        Object instance = beanConstructor.createInstance(dependencies);
        
        // 注入对象依赖
        autowiredBean(instance, beanDescription);
        
        return new CreatedBean(instance, beanDescription.isSingleton(), beanDescription.getBeanKey());
    }
    
    /**
     * 自动注入对象依赖信息
     *
     * @param instance        被注入的实例
     * @param beanDescription 被注入的实例对象描述
     */
    protected void autowiredBean(@NotNull Object instance,
                                 @NotNull BeanDescription beanDescription) {
        List<BeanInjectionPoint> injectionPoints = beanDescription.getInjectionPoints();
        if (CollUtil.isEmpty(injectionPoints)) return;
        
        for (BeanInjectionPoint injectionPoint : injectionPoints) {
            List<BeanKey> injectionDependencies = injectionPoint.getDependencies();
            Object[] dependenciesBeans = getDependenciesBeans(injectionDependencies);
            injectionPoint.inject(instance, dependenciesBeans);
        }
    }
    
    /**
     * 使用工厂类创建对象
     *
     * @see FactoryPoint
     */
    @Contract("_ -> new")
    protected @NotNull CreatedBean useFactoryPointCreateBean(@NotNull BeanKey beanKey) {
        FactoryPoint factoryPoint = factoryPointMap.find(beanKey);
        BeanKey returnType = factoryPoint.getReturnType();
        
        Object factoryInstance = getBean(factoryPoint.getFactoryKey());
        assert factoryInstance != null;
        
        // 装载参数
        List<BeanKey> parameters = factoryPoint.getParameters();
        Object[] beansForFactoryPoint = getDependenciesBeans(parameters);
        
        // 创建对象
        Object beanInstance = factoryPoint.create(factoryInstance, beansForFactoryPoint);
        
        // 工厂方法的返回值又可能是实际类型的父类或者接口，这边进行一个修正
        Class<?> instanceClass = beanInstance.getClass();
        BeanKey instanceBeanKey = returnType.getBeanClass().equals(instanceClass) ? returnType : new BeanKey(instanceClass, returnType.getQualifier());
        return new CreatedBean(beanInstance, factoryPoint.isSingleton(), instanceBeanKey);
    }
    
    /**
     * 执行 {@link BeanPostProcessor#postProcessBeforeInitialization(String, Object)} 钩子回调
     */
    protected Object applyBeanPostProcessorsBeforeInitialization(String beanName, Object bean) throws BeanException {
        Object result = bean;
        for (BeanPostProcessor processor : beanPostProcessors) {
            result = processor.postProcessBeforeInitialization(beanName, result);
        }
        return result;
    }
    
    /**
     * 执行 {@link BeanPostProcessor#postProcessAfterInitialization(String, Object)} 钩子回调
     */
    protected Object applyBeanPostProcessorsAfterInitialization(String beanName, Object bean) throws BeanException {
        Object result = bean;
        for (BeanPostProcessor processor : beanPostProcessors) {
            result = processor.postProcessAfterInitialization(beanName, result);
        }
        return result;
    }
    
    /**
     * 执行对象的初始化方法
     *
     * @param beanName 对象名称
     * @param bean     对象实例
     * @see InitializingBean
     */
    protected void invokeInitMethods(String beanName, Object bean) throws Exception {
        if (bean instanceof InitializingBean) {
            ((InitializingBean) bean).init();
        }
    }
    
    /**
     * 如果指定对象是 {@link Aware} 的子类则执行感知方法
     *
     * @param beanName 对象名称
     * @param bean     对象实例
     */
    protected void invokeAwareMethods(@Nullable String beanName, @NotNull Object bean) {
        if (bean instanceof BeanFactoryAware) {
            ((BeanFactoryAware) bean).setBeanFactory(this);
        }
        
        if (Objects.nonNull(beanName) && bean instanceof BeanNameAware) {
            ((BeanNameAware) bean).setBeanName(beanName);
        }
    }
    
    /**
     * 初始化非懒加载对象
     */
    protected synchronized void initNonLazyBean() {
        Collection<BeanKey> beanDescriptionBeanKeys = beanDescriptionMap.findBeanKeyBy((beanKey, beanDescription) -> !beanDescription.isLazy());
        for (BeanKey beanKey : beanDescriptionBeanKeys) {
            getBean(beanKey);
        }
        
        Collection<BeanKey> factoryPointBeanKeys = factoryPointMap.findBeanKeyBy(((beanKey, factoryPoint) -> !factoryPoint.isLazy()));
        for (BeanKey beanKey : factoryPointBeanKeys) {
            getBean(beanKey);
        }
    }
    
    /**
     * 将 {@link BeanKey} 转为对应依赖对象
     */
    private Object @NotNull [] getDependenciesBeans(@NotNull List<BeanKey> dependencies) {
        if (CollUtil.isEmpty(dependencies)) return new Object[0];
        
        Object[] dependenciesArray = new Object[dependencies.size()];
        for (int i = 0; i < dependencies.size(); i++) {
            BeanKey parameter = dependencies.get(i);
            Object value = parameter.isProvider() ? new DefaultObjectProvider<>(this, parameter) : getBean(parameter);
            dependenciesArray[i] = value;
        }
        return dependenciesArray;
    }
    
}
