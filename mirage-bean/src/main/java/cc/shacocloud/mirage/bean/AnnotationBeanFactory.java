package cc.shacocloud.mirage.bean;

import cc.shacocloud.mirage.bean.impl.*;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.jetbrains.annotations.NotNull;

/**
 * 基于注解实现的对象工厂
 *
 * @author 思追(shaco)
 */
@Slf4j
public class AnnotationBeanFactory extends AbstractBeanFactory {
    
    @Getter
    private final ClassScanner classScanner;
    
    @Getter
    private final QualifierHandler qualifierHandler;
    
    /**
     * 创建一个通用的对象工厂
     *
     * @param classScanner 类扫描器
     */
    public AnnotationBeanFactory(@NotNull ClassScanner classScanner) {
        this(classScanner, new DefaultQualifierHandler(), new DefaultScopeHandler(), new DefaultConditionHandler());
    }
    
    /**
     * 创建一个通用的对象工厂
     *
     * @param classScanner 类扫描器
     */
    public AnnotationBeanFactory(@NotNull ClassScanner classScanner,
                                 @NotNull ConditionHandler conditionHandler) {
        this(classScanner, new DefaultQualifierHandler(), new DefaultScopeHandler(), conditionHandler);
    }
    
    /**
     * 创建一个通用的对象工厂
     *
     * @param classScanner     类扫描器
     * @param qualifierHandler Bean 名称处理器
     * @param scopeHandler     Bean 范围处理器
     */
    public AnnotationBeanFactory(@NotNull ClassScanner classScanner,
                                 @NotNull QualifierHandler qualifierHandler,
                                 @NotNull ScopeHandler scopeHandler,
                                 @NotNull ConditionHandler conditionHandler) {
        super(new AnnotationBeanKeyLoader(classScanner, qualifierHandler),
                new AnnotationBeanDescriptionHandler(qualifierHandler, scopeHandler),
                new AnnotationFactoryPointHandler(qualifierHandler, scopeHandler), conditionHandler);
        
        this.classScanner = classScanner;
        this.qualifierHandler = qualifierHandler;
    }
    
    @Override
    public ClassLoader getClassLoader() {
        return getClassScanner().getClassLoader();
    }
    
}
