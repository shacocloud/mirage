package cc.shacocloud.mirage.utils.annotation;

import org.junit.jupiter.api.Test;

import java.lang.annotation.*;

/**
 * @author 思追(shaco)
 */
class AnnotatedElementUtilsTest {
    
    @Test
    void toCombination() {
        AnnotatedElementMetadata annotatedElementMetadata = AnnotatedElementUtils.getAnnotatedMetadata(A.class);
        
        A1 a1 = annotatedElementMetadata.getAnnotation(A1.class);
        System.out.println("a1 path：" + a1.path());
        System.out.println("a1 value：" + a1.value());
        System.out.println();
        
        A2 a2 = annotatedElementMetadata.getAnnotation(A2.class);
        System.out.println("a2 method：" + a2.method());
        System.out.println("a2 path：" + a2.path());
        System.out.println("a2 value：" + a2.value());
    }
    
    @Retention(RetentionPolicy.RUNTIME)
    @Target(ElementType.TYPE)
    @Documented
    @A2(method = "method")
    public @interface A1 {
        
        @AliasFor(annotation = A2.class)
        String value() default "";
        
        @AliasFor(annotation = A2.class)
        String path() default "";
        
        
    }
    
    @Retention(RetentionPolicy.RUNTIME)
    @Target(ElementType.TYPE)
    @Documented
    public @interface A2 {
        
        @AliasFor("path")
        String value() default "";
        
        @AliasFor("value")
        String path() default "";
        
        String method() default "";
        
    }
    
    @A1("test")
    public static class A {
    
    }
}