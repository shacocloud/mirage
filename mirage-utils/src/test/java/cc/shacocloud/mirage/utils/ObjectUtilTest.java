package cc.shacocloud.mirage.utils;

import cc.shacocloud.mirage.utils.converter.TypeConverter;
import cc.shacocloud.mirage.utils.converter.TypeConverterSupportImpl;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author 思追(shaco)
 */
class ObjectUtilTest {
    
    @Test
    void getNestedPathValue() {
        Map<String, Object> d = new HashMap<>();
        d.put("d", "111");
        
        List<Object> cc = new ArrayList<>();
        cc.add(d);
        
        List<Object> c = new ArrayList<>();
        c.add(cc);
        
        Map<String, Object> b = new HashMap<>();
        b.put("c", c);
        
        TestA a = new TestA();
        a.setB(b);
        
        Map<String, Object> map = new HashMap<>();
        map.put("a", a);
        
        Object value = ObjectUtil.getNestedPathValue(map, "a.b.c[0][0].d");
        Assertions.assertEquals(value, "111");
    }
    
    @Test
    void setNestedPathValue() {
        TestB d = new TestB();
        
        List<Object> cc = new ArrayList<>();
        cc.add(d);
        
        List<Object> c = new ArrayList<>();
        c.add(cc);
        
        Map<String, Object> b = new HashMap<>();
        b.put("c", c);
        
        TestA a = new TestA();
        a.setB(b);
        
        Map<String, Object> map = new HashMap<>();
        map.put("a", a);
        
        String value = "1";
        
        TypeConverter typeConverter = new TypeConverterSupportImpl();
        ObjectUtil.setNestedPathValue(map, "a.b.c[0][0].d", value, typeConverter);
        
        Assertions.assertEquals(Integer.parseInt(value), d.getD());
    }
    
    
    @Setter
    @Getter
    @NoArgsConstructor
    private static class TestA {
        private Map<String, Object> b;
    }
    
    @Setter
    @Getter
    @NoArgsConstructor
    private static class TestB {
        private Integer d;
    }
}