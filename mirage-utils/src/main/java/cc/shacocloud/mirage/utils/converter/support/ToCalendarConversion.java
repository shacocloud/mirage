package cc.shacocloud.mirage.utils.converter.support;

import cc.shacocloud.mirage.utils.converter.ConversionSupport;
import cc.shacocloud.mirage.utils.converter.TypeDescriptor;
import cc.shacocloud.mirage.utils.date.DateUtil;
import org.jetbrains.annotations.NotNull;

import java.util.Calendar;
import java.util.Date;

/**
 * 转换为 {@link Calendar} 类型的转换器
 *
 * @author 思追(shaco)
 */
public class ToCalendarConversion extends AbstractConversion implements ConversionSupport {
    
    @Override
    public boolean support(@NotNull TypeDescriptor sourceType, @NotNull TypeDescriptor targetType) {
        Class<?> objectType = targetType.getObjectType();
        return Calendar.class.equals(objectType);
    }
    
    @Override
    public Object convert(@NotNull Object source, @NotNull TypeDescriptor sourceType, @NotNull TypeDescriptor targetType) {
        if (source instanceof Calendar) {
            return source;
        } else if (source instanceof Date) {
            return DateUtil.calendar((Date) source);
        } else if (source instanceof Long) {
            return DateUtil.calendar((Long) source);
        } else {
            String valueStr = convertToStr(source);
            return DateUtil.parse(valueStr).toCalendar();
        }
    }
    
    
}
