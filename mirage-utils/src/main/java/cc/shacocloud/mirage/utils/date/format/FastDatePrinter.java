package cc.shacocloud.mirage.utils.date.format;

import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.text.DateFormatSymbols;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

/**
 * {@link java.text.SimpleDateFormat} 的线程安全版本，用于将 {@link Date} 格式化输出
 *
 * @author 思追(shaco)
 * @see FastDateParser
 */
public class FastDatePrinter extends AbstractDateBasic implements DatePrinter {
    
    private static final int MAX_DIGITS = 10; // log10(Integer.MAX_VALUE) ~= 9.3
    private static final ConcurrentMap<TimeZoneDisplayKey, String> C_TIME_ZONE_DISPLAY_CACHE = new ConcurrentHashMap<>(7);
    /**
     * 规则列表.
     */
    private transient Rule[] rules;
    /**
     * 估算最大长度.
     */
    private transient int mMaxLengthEstimate;
    
    /**
     * 构造，内部使用<br>
     *
     * @param pattern  使用{@link java.text.SimpleDateFormat} 相同的日期格式
     * @param timeZone 非空时区{@link TimeZone}
     * @param locale   非空{@link Locale} 日期地理位置
     */
    public FastDatePrinter(final String pattern, final TimeZone timeZone, final Locale locale) {
        super(pattern, timeZone, locale);
        init();
    }
    
    /**
     * 将两位数字追加到给定的缓冲区。
     */
    private static void appendDigits(final @NotNull Appendable buffer, final int value) throws IOException {
        buffer.append((char) (value / 10 + '0'));
        buffer.append((char) (value % 10 + '0'));
    }
    
    /**
     * 将所有数字追加到给定缓冲区
     */
    private static void appendFullDigits(final Appendable buffer, int value, int minFieldWidth) throws IOException {
        //1到4位的专用路径->避免临时工作数组的内存分配
        // see LANG-1248
        if (value < 10000) {
            // 较少的内存分配路径适用于四位数或更少
            
            int nDigits = 4;
            if (value < 1000) {
                --nDigits;
                if (value < 100) {
                    --nDigits;
                    if (value < 10) {
                        --nDigits;
                    }
                }
            }
            for (int i = minFieldWidth - nDigits; i > 0; --i) {
                buffer.append('0');
            }
            
            switch (nDigits) {
                case 4:
                    buffer.append((char) (value / 1000 + '0'));
                    value %= 1000;
                case 3:
                    if (value >= 100) {
                        buffer.append((char) (value / 100 + '0'));
                        value %= 100;
                    } else {
                        buffer.append('0');
                    }
                case 2:
                    if (value >= 10) {
                        buffer.append((char) (value / 10 + '0'));
                        value %= 10;
                    } else {
                        buffer.append('0');
                    }
                case 1:
                    buffer.append((char) (value + '0'));
            }
        } else {
            // 更多内存分配路径适用于任何位数
            // 建立反转的十进制表示法
            final char[] work = new char[MAX_DIGITS];
            int digit = 0;
            while (value != 0) {
                work[digit++] = (char) (value % 10 + '0');
                value = value / 10;
            }
            
            while (digit < minFieldWidth) {
                buffer.append('0');
                --minFieldWidth;
            }
            
            // 反转
            while (--digit >= 0) {
                buffer.append(work[digit]);
            }
        }
    }
    
    /**
     * 使用缓存获取时区显示名称以提高性能。
     *
     * @param tz       要查询的区域
     * @param daylight 如果夏令时，则为真
     * @param style    使用 {@code TimeZone.LONG} 或 {@code TimeZone.SHORT} 的样式
     * @param locale   要使用的区域设置
     * @return 时区的文本名称
     */
    static String getTimeZoneDisplay(final TimeZone tz, final boolean daylight, final int style, final Locale locale) {
        final TimeZoneDisplayKey key = new TimeZoneDisplayKey(tz, daylight, style, locale);
        String value = C_TIME_ZONE_DISPLAY_CACHE.get(key);
        if (value == null) {
            // 这是一个非常慢的调用，所以缓存结果
            value = tz.getDisplayName(daylight, style, locale);
            final String prior = C_TIME_ZONE_DISPLAY_CACHE.putIfAbsent(key, value);
            if (prior != null) {
                value = prior;
            }
        }
        return value;
    }
    
    /**
     * 初始化
     */
    private void init() {
        final List<Rule> rulesList = parsePattern();
        rules = rulesList.toArray(new Rule[0]);
        
        int len = 0;
        for (int i = rules.length; --i >= 0; ) {
            len += rules[i].estimateLength();
        }
        
        mMaxLengthEstimate = len;
    }
    
    /**
     * 返回给定模式的规则列表
     *
     * @throws IllegalArgumentException 如果模式无效
     */
    protected List<Rule> parsePattern() {
        final DateFormatSymbols symbols = new DateFormatSymbols(locale);
        final List<Rule> rules = new ArrayList<>();
        
        final String[] ERAs = symbols.getEras();
        final String[] months = symbols.getMonths();
        final String[] shortMonths = symbols.getShortMonths();
        final String[] weekdays = symbols.getWeekdays();
        final String[] shortWeekdays = symbols.getShortWeekdays();
        final String[] AmPmStrings = symbols.getAmPmStrings();
        
        final int length = pattern.length();
        final int[] indexRef = new int[1];
        
        for (int i = 0; i < length; i++) {
            indexRef[0] = i;
            final String token = parseToken(pattern, indexRef);
            i = indexRef[0];
            
            final int tokenLen = token.length();
            if (tokenLen == 0) {
                break;
            }
            
            Rule rule;
            final char c = token.charAt(0);
            
            switch (c) {
                case 'G': // 时代指示符（文本）
                    rule = new TextField(Calendar.ERA, ERAs);
                    break;
                case 'y': // 年份（数字）
                case 'Y': // 周年
                    if (tokenLen == 2) {
                        rule = TwoDigitYearField.INSTANCE;
                    } else {
                        rule = selectNumberRule(Calendar.YEAR, Math.max(tokenLen, 4));
                    }
                    if (c == 'Y') {
                        rule = new WeekYear((NumberRule) rule);
                    }
                    break;
                case 'M': // 年份中的月份（文本和数字）
                    if (tokenLen >= 4) {
                        rule = new TextField(Calendar.MONTH, months);
                    } else if (tokenLen == 3) {
                        rule = new TextField(Calendar.MONTH, shortMonths);
                    } else if (tokenLen == 2) {
                        rule = TwoDigitMonthField.INSTANCE;
                    } else {
                        rule = UnpaddedMonthField.INSTANCE;
                    }
                    break;
                case 'd': // 月中的一天（数字）
                    rule = selectNumberRule(Calendar.DAY_OF_MONTH, tokenLen);
                    break;
                case 'h': // 小时（安培）（数字，1..12）
                    rule = new TwelveHourField(selectNumberRule(Calendar.HOUR, tokenLen));
                    break;
                case 'H': // 一天中的小时（数字，0..23）
                    rule = selectNumberRule(Calendar.HOUR_OF_DAY, tokenLen);
                    break;
                case 'm': // m以小时为单位（数字）
                    rule = selectNumberRule(Calendar.MINUTE, tokenLen);
                    break;
                case 's': // 分钟秒数(数字)
                    rule = selectNumberRule(Calendar.SECOND, tokenLen);
                    break;
                case 'S': // 毫秒(数字)
                    rule = selectNumberRule(Calendar.MILLISECOND, tokenLen);
                    break;
                case 'E': // 星期几(文本)
                    rule = new TextField(Calendar.DAY_OF_WEEK, tokenLen < 4 ? shortWeekdays : weekdays);
                    break;
                case 'u': // 一周中的某天(数字)
                    rule = new DayInWeekField(selectNumberRule(Calendar.DAY_OF_WEEK, tokenLen));
                    break;
                case 'D': // 一年中的日期(数字)
                    rule = selectNumberRule(Calendar.DAY_OF_YEAR, tokenLen);
                    break;
                case 'F': // 月份中的星期几(数字)
                    rule = selectNumberRule(Calendar.DAY_OF_WEEK_IN_MONTH, tokenLen);
                    break;
                case 'w': // 一年中的周(数字)
                    rule = selectNumberRule(Calendar.WEEK_OF_YEAR, tokenLen);
                    break;
                case 'W': // 每月周数(数字)
                    rule = selectNumberRule(Calendar.WEEK_OF_MONTH, tokenLen);
                    break;
                case 'a': // AM/PM标记(文本)
                    rule = new TextField(Calendar.AM_PM, AmPmStrings);
                    break;
                case 'k': // 一天中的小时(1..24)
                    rule = new TwentyFourHourField(selectNumberRule(Calendar.HOUR_OF_DAY, tokenLen));
                    break;
                case 'K': // 小时(上午/下午)(0..11)
                    rule = selectNumberRule(Calendar.HOUR, tokenLen);
                    break;
                case 'X': // ISO 8601
                    rule = Iso8601_Rule.getRule(tokenLen);
                    break;
                case 'z': // 时区(文本)
                    if (tokenLen >= 4) {
                        rule = new TimeZoneNameRule(timeZone, locale, TimeZone.LONG);
                    } else {
                        rule = new TimeZoneNameRule(timeZone, locale, TimeZone.SHORT);
                    }
                    break;
                case 'Z': // 时区(值)
                    if (tokenLen == 1) {
                        rule = TimeZoneNumberRule.INSTANCE_NO_COLON;
                    } else if (tokenLen == 2) {
                        rule = Iso8601_Rule.ISO8601_HOURS_COLON_MINUTES;
                    } else {
                        rule = TimeZoneNumberRule.INSTANCE_COLON;
                    }
                    break;
                case '\'': // 原文本
                    final String sub = token.substring(1);
                    if (sub.length() == 1) {
                        rule = new CharacterLiteral(sub.charAt(0));
                    } else {
                        rule = new StringLiteral(sub);
                    }
                    break;
                default:
                    throw new IllegalArgumentException("非法模式组件: " + token);
            }
            
            rules.add(rule);
        }
        
        return rules;
    }
    
    /**
     * 执行令牌解析。
     *
     * @param pattern  模式
     * @param indexRef 索引引用
     */
    protected String parseToken(final @NotNull String pattern, final int @NotNull [] indexRef) {
        final StringBuilder buf = new StringBuilder();
        
        int i = indexRef[0];
        final int length = pattern.length();
        
        char c = pattern.charAt(i);
        if (c >= 'A' && c <= 'Z' || c >= 'a' && c <= 'z') {
            // 扫描相同字符的运行，这表示时间模式。
            buf.append(c);
            
            while (i + 1 < length) {
                final char peek = pattern.charAt(i + 1);
                if (peek == c) {
                    buf.append(c);
                    i++;
                } else {
                    break;
                }
            }
        } else {
            // 这会将令牌标识为文本。
            buf.append('\'');
            
            boolean inLiteral = false;
            
            for (; i < length; i++) {
                c = pattern.charAt(i);
                
                if (c == '\'') {
                    if (i + 1 < length && pattern.charAt(i + 1) == '\'') {
                        i++;
                        buf.append(c);
                    } else {
                        inLiteral = !inLiteral;
                    }
                } else if (!inLiteral && (c >= 'A' && c <= 'Z' || c >= 'a' && c <= 'z')) {
                    i--;
                    break;
                } else {
                    buf.append(c);
                }
            }
        }
        
        indexRef[0] = i;
        return buf.toString();
    }
    
    /**
     * 获取所需填充的适当规则。
     */
    protected NumberRule selectNumberRule(final int field, final int padding) {
        switch (padding) {
            case 1:
                return new UnpaddedNumberField(field);
            case 2:
                return new TwoDigitNumberField(field);
            default:
                return new PaddedNumberField(field, padding);
        }
    }
    
    /**
     * 格式化{@code Date}、{@code Calendar}或{@code Long}(毫秒)对象。
     *
     * @param obj 要格式化的对象
     * @return 要格式化的对象格式化的值。
     */
    String format(final Object obj) {
        if (obj instanceof Date) {
            return format((Date) obj);
        } else if (obj instanceof Calendar) {
            return format((Calendar) obj);
        } else if (obj instanceof Long) {
            return format(((Long) obj).longValue());
        } else {
            throw new IllegalArgumentException("未知类别：" + (obj == null ? "<null>" : obj.getClass().getName()));
        }
    }
    
    @Override
    public String format(final long millis) {
        final Calendar c = Calendar.getInstance(timeZone, locale);
        c.setTimeInMillis(millis);
        return applyRulesToString(c);
    }
    
    @Override
    public String format(final Date date) {
        final Calendar c = Calendar.getInstance(timeZone, locale);
        c.setTime(date);
        return applyRulesToString(c);
    }
    
    @Override
    public String format(final Calendar calendar) {
        return format(calendar, new StringBuilder(mMaxLengthEstimate)).toString();
    }
    
    @Override
    public <B extends Appendable> B format(final long millis, final B buf) {
        final Calendar c = Calendar.getInstance(timeZone, locale);
        c.setTimeInMillis(millis);
        return applyRules(c, buf);
    }
    
    // Serializing
    // -----------------------------------------------------------------------
    
    @Override
    public <B extends Appendable> B format(final Date date, final B buf) {
        final Calendar c = Calendar.getInstance(timeZone, locale);
        c.setTime(date);
        return applyRules(c, buf);
    }
    
    @Override
    public <B extends Appendable> B format(@NotNull Calendar calendar, final B buf) {
        // 不要直接传入日历，这将导致 FastDatePrint 的时区被忽略
        if (!calendar.getTimeZone().equals(timeZone)) {
            calendar = (Calendar) calendar.clone();
            calendar.setTimeZone(timeZone);
        }
        return applyRules(calendar, buf);
    }
    
    /**
     * 通过将此打印机的规则应用于给定日历来创建它的字符串表示形式。
     *
     * @param c 要对其应用规则的日历。
     * @return 给定日历的字符串表示形式。
     */
    private @NotNull String applyRulesToString(final Calendar c) {
        return applyRules(c, new StringBuilder(mMaxLengthEstimate)).toString();
    }
    
    /**
     * 通过将规则应用于指定日历来执行格式设置。
     *
     * @param calendar 要格式化的日历
     * @param buf      要格式化的缓冲区
     * @param <B>      可追加的类类型，通常为StringBuilder或StringBuffer。
     * @return 指定的字符串缓冲区
     */
    private <B extends Appendable> B applyRules(final Calendar calendar, final B buf) {
        try {
            for (final Rule rule : this.rules) {
                rule.appendTo(buf, calendar);
            }
        } catch (final IOException e) {
            throw new IllegalArgumentException(e);
        }
        return buf;
    }
    
    /**
     * 估算生成的日期字符串长度<br>
     * 实际生成的字符串长度小于或等于此值
     *
     * @return 日期字符串长度
     */
    public int getMaxLengthEstimate() {
        return mMaxLengthEstimate;
    }
    
    /**
     * 序列化后创建对象。此实现重新初始化瞬变属性。
     */
    private void readObject(final @NotNull ObjectInputStream in) throws IOException, ClassNotFoundException {
        in.defaultReadObject();
        init();
    }
    
    /**
     * 规则
     */
    private interface Rule {
        
        /**
         * 返回结果的估计长度
         *
         * @return 估计的长度
         */
        int estimateLength();
        
        /**
         * 根据规则实现将指定日历的值追加到输出缓冲区
         *
         * @param buf      输出缓冲区
         * @param calendar 待追加的日历
         * @throws IOException 如果发生I/O错误
         */
        void appendTo(Appendable buf, Calendar calendar) throws IOException;
    }
    
    /**
     * 定义数字规则的内部类。
     */
    private interface NumberRule extends Rule {
        
        /**
         * 根据规则实现将指定值追加到输出缓冲区
         *
         * @param buffer 输出缓冲区
         * @param value  要追加的值
         * @throws IOException 如果发生I/O错误
         */
        void appendTo(Appendable buffer, int value) throws IOException;
    }
    
    /**
     * 内部类输出一个常量单个字符。
     */
    private static class CharacterLiteral implements Rule {
        private final char mValue;
        
        /**
         * 构造{@code CharacterLiteral}的新实例以保存指定值。
         *
         * @param value 字符字面
         */
        CharacterLiteral(final char value) {
            mValue = value;
        }
        
        @Override
        public int estimateLength() {
            return 1;
        }
        
        @Override
        public void appendTo(final @NotNull Appendable buffer, final Calendar calendar) throws IOException {
            buffer.append(mValue);
        }
    }
    
    /**
     * 内部类以输出常量字符串。
     */
    private static class StringLiteral implements Rule {
        private final String mValue;
        
        /**
         * 构造{@code StringLiteral}的新实例以保存指定值。
         *
         * @param value 字符串文字
         */
        StringLiteral(final String value) {
            mValue = value;
        }
        
        /**
         * {@inheritDoc}
         */
        @Override
        public int estimateLength() {
            return mValue.length();
        }
        
        /**
         * {@inheritDoc}
         */
        @Override
        public void appendTo(final @NotNull Appendable buffer, final Calendar calendar) throws IOException {
            buffer.append(mValue);
        }
    }
    
    /**
     * 内部类以输出一组值中的一个
     */
    private static class TextField implements Rule {
        private final int mField;
        private final String[] mValues;
        
        TextField(final int field, final String[] values) {
            mField = field;
            mValues = values;
        }
        
        /**
         * {@inheritDoc}
         */
        @Override
        public int estimateLength() {
            int max = 0;
            for (int i = mValues.length; --i >= 0; ) {
                final int len = mValues[i].length();
                if (len > max) {
                    max = len;
                }
            }
            return max;
        }
        
        /**
         * {@inheritDoc}
         */
        @Override
        public void appendTo(final @NotNull Appendable buffer, final @NotNull Calendar calendar) throws IOException {
            buffer.append(mValues[calendar.get(mField)]);
        }
    }
    
    /**
     * 内部类以输出未填充的数字
     */
    private static class UnpaddedNumberField implements NumberRule {
        private final int mField;
        
        UnpaddedNumberField(final int field) {
            mField = field;
        }
        
        /**
         * {@inheritDoc}
         */
        @Override
        public int estimateLength() {
            return 4;
        }
        
        /**
         * {@inheritDoc}
         */
        @Override
        public void appendTo(final Appendable buffer, final @NotNull Calendar calendar) throws IOException {
            appendTo(buffer, calendar.get(mField));
        }
        
        /**
         * {@inheritDoc}
         */
        @Override
        public final void appendTo(final Appendable buffer, final int value) throws IOException {
            if (value < 10) {
                buffer.append((char) (value + '0'));
            } else if (value < 100) {
                appendDigits(buffer, value);
            } else {
                appendFullDigits(buffer, value, 1);
            }
        }
    }
    
    /**
     * 内部类以输出未填充的月份。
     */
    private static class UnpaddedMonthField implements NumberRule {
        static final UnpaddedMonthField INSTANCE = new UnpaddedMonthField();
        
        UnpaddedMonthField() {
            super();
        }
        
        /**
         * {@inheritDoc}
         */
        @Override
        public int estimateLength() {
            return 2;
        }
        
        /**
         * {@inheritDoc}
         */
        @Override
        public void appendTo(final Appendable buffer, final @NotNull Calendar calendar) throws IOException {
            appendTo(buffer, calendar.get(Calendar.MONTH) + 1);
        }
        
        /**
         * {@inheritDoc}
         */
        @Override
        public final void appendTo(final Appendable buffer, final int value) throws IOException {
            if (value < 10) {
                buffer.append((char) (value + '0'));
            } else {
                appendDigits(buffer, value);
            }
        }
    }
    
    /**
     * 内部类以输出填充的数字
     */
    private static class PaddedNumberField implements NumberRule {
        private final int mField;
        private final int mSize;
        
        PaddedNumberField(final int field, final int size) {
            if (size < 3) {
                // Should use UnpaddedNumberField or TwoDigitNumberField.
                throw new IllegalArgumentException();
            }
            mField = field;
            mSize = size;
        }
        
        /**
         * {@inheritDoc}
         */
        @Override
        public int estimateLength() {
            return mSize;
        }
        
        /**
         * {@inheritDoc}
         */
        @Override
        public void appendTo(final Appendable buffer, final @NotNull Calendar calendar) throws IOException {
            appendTo(buffer, calendar.get(mField));
        }
        
        /**
         * {@inheritDoc}
         */
        @Override
        public final void appendTo(final Appendable buffer, final int value) throws IOException {
            appendFullDigits(buffer, value, mSize);
        }
    }
    
    /**
     * 内部类以输出两位数字
     */
    private static class TwoDigitNumberField implements NumberRule {
        private final int mField;
        
        TwoDigitNumberField(final int field) {
            mField = field;
        }
        
        /**
         * {@inheritDoc}
         */
        @Override
        public int estimateLength() {
            return 2;
        }
        
        /**
         * {@inheritDoc}
         */
        @Override
        public void appendTo(final Appendable buffer, final @NotNull Calendar calendar) throws IOException {
            appendTo(buffer, calendar.get(mField));
        }
        
        /**
         * {@inheritDoc}
         */
        @Override
        public final void appendTo(final Appendable buffer, final int value) throws IOException {
            if (value < 100) {
                appendDigits(buffer, value);
            } else {
                appendFullDigits(buffer, value, 2);
            }
        }
    }
    
    /**
     * 内部类以输出两位数的年份
     */
    private static class TwoDigitYearField implements NumberRule {
        static final TwoDigitYearField INSTANCE = new TwoDigitYearField();
        
        TwoDigitYearField() {
            super();
        }
        
        /**
         * {@inheritDoc}
         */
        @Override
        public int estimateLength() {
            return 2;
        }
        
        /**
         * {@inheritDoc}
         */
        @Override
        public void appendTo(final Appendable buffer, final @NotNull Calendar calendar) throws IOException {
            appendTo(buffer, calendar.get(Calendar.YEAR) % 100);
        }
        
        /**
         * {@inheritDoc}
         */
        @Override
        public final void appendTo(final Appendable buffer, final int value) throws IOException {
            appendDigits(buffer, value);
        }
    }
    
    /**
     * 内部类以输出两位数的月份
     */
    private static class TwoDigitMonthField implements NumberRule {
        static final TwoDigitMonthField INSTANCE = new TwoDigitMonthField();
        
        TwoDigitMonthField() {
            super();
        }
        
        /**
         * {@inheritDoc}
         */
        @Override
        public int estimateLength() {
            return 2;
        }
        
        /**
         * {@inheritDoc}
         */
        @Override
        public void appendTo(final Appendable buffer, final Calendar calendar) throws IOException {
            appendTo(buffer, calendar.get(Calendar.MONTH) + 1);
        }
        
        /**
         * {@inheritDoc}
         */
        @Override
        public final void appendTo(final Appendable buffer, final int value) throws IOException {
            appendDigits(buffer, value);
        }
    }
    
    /**
     * 内部类来输出12小时字段。
     */
    private static class TwelveHourField implements NumberRule {
        private final NumberRule mRule;
        
        TwelveHourField(final NumberRule rule) {
            mRule = rule;
        }
        
        /**
         * {@inheritDoc}
         */
        @Override
        public int estimateLength() {
            return mRule.estimateLength();
        }
        
        /**
         * {@inheritDoc}
         */
        @Override
        public void appendTo(final Appendable buffer, final Calendar calendar) throws IOException {
            int value = calendar.get(Calendar.HOUR);
            if (value == 0) {
                value = calendar.getLeastMaximum(Calendar.HOUR) + 1;
            }
            mRule.appendTo(buffer, value);
        }
        
        /**
         * {@inheritDoc}
         */
        @Override
        public void appendTo(final Appendable buffer, final int value) throws IOException {
            mRule.appendTo(buffer, value);
        }
    }
    
    /**
     * 内部类来输出24小时字段
     */
    private static class TwentyFourHourField implements NumberRule {
        private final NumberRule mRule;
        
        TwentyFourHourField(final NumberRule rule) {
            mRule = rule;
        }
        
        /**
         * {@inheritDoc}
         */
        @Override
        public int estimateLength() {
            return mRule.estimateLength();
        }
        
        /**
         * {@inheritDoc}
         */
        @Override
        public void appendTo(final Appendable buffer, final @NotNull Calendar calendar) throws IOException {
            int value = calendar.get(Calendar.HOUR_OF_DAY);
            if (value == 0) {
                value = calendar.getMaximum(Calendar.HOUR_OF_DAY) + 1;
            }
            mRule.appendTo(buffer, value);
        }
        
        /**
         * {@inheritDoc}
         */
        @Override
        public void appendTo(final Appendable buffer, final int value) throws IOException {
            mRule.appendTo(buffer, value);
        }
    }
    
    // -----------------------------------------------------------------------
    
    /**
     * 内部类以输出星期中的数字日。
     */
    private static class DayInWeekField implements NumberRule {
        private final NumberRule mRule;
        
        DayInWeekField(final NumberRule rule) {
            mRule = rule;
        }
        
        @Override
        public int estimateLength() {
            return mRule.estimateLength();
        }
        
        @Override
        public void appendTo(final Appendable buffer, final @NotNull Calendar calendar) throws IOException {
            final int value = calendar.get(Calendar.DAY_OF_WEEK);
            mRule.appendTo(buffer, value != Calendar.SUNDAY ? value - 1 : 7);
        }
        
        @Override
        public void appendTo(final Appendable buffer, final int value) throws IOException {
            mRule.appendTo(buffer, value);
        }
    }
    
    /**
     * 内部类以输出星期中的数字日
     */
    private static class WeekYear implements NumberRule {
        private final NumberRule mRule;
        
        WeekYear(final NumberRule rule) {
            mRule = rule;
        }
        
        @Override
        public int estimateLength() {
            return mRule.estimateLength();
        }
        
        @Override
        public void appendTo(final Appendable buffer, final @NotNull Calendar calendar) throws IOException {
            mRule.appendTo(buffer, calendar.getWeekYear());
        }
        
        @Override
        public void appendTo(final Appendable buffer, final int value) throws IOException {
            mRule.appendTo(buffer, value);
        }
    }
    
    /**
     * 内部类以输出时区名称。
     */
    private static class TimeZoneNameRule implements Rule {
        private final Locale mLocale;
        private final int mStyle;
        private final String mStandard;
        private final String mDaylight;
        
        TimeZoneNameRule(final TimeZone timeZone, final Locale locale, final int style) {
            mLocale = locale;
            mStyle = style;
            
            mStandard = getTimeZoneDisplay(timeZone, false, style, locale);
            mDaylight = getTimeZoneDisplay(timeZone, true, style, locale);
        }
        
        /**
         * {@inheritDoc}
         */
        @Override
        public int estimateLength() {
            // 我们无法访问将传递给appendTo的Calendar对象，因此基于传递给构造函数的时区进行估计
            return Math.max(mStandard.length(), mDaylight.length());
        }
        
        /**
         * {@inheritDoc}
         */
        @Override
        public void appendTo(final Appendable buffer, final Calendar calendar) throws IOException {
            final TimeZone zone = calendar.getTimeZone();
            if (calendar.get(Calendar.DST_OFFSET) != 0) {
                buffer.append(getTimeZoneDisplay(zone, true, mStyle, mLocale));
            } else {
                buffer.append(getTimeZoneDisplay(zone, false, mStyle, mLocale));
            }
        }
    }
    
    /**
     * 内部类将时区输出为数字 {@code +/-HHMM} or {@code +/-HH:MM}.
     */
    private static class TimeZoneNumberRule implements Rule {
        static final TimeZoneNumberRule INSTANCE_COLON = new TimeZoneNumberRule(true);
        static final TimeZoneNumberRule INSTANCE_NO_COLON = new TimeZoneNumberRule(false);
        
        final boolean mColon;
        
        TimeZoneNumberRule(final boolean colon) {
            mColon = colon;
        }
        
        /**
         * {@inheritDoc}
         */
        @Override
        public int estimateLength() {
            return 5;
        }
        
        /**
         * {@inheritDoc}
         */
        @Override
        public void appendTo(final Appendable buffer, final Calendar calendar) throws IOException {
            
            int offset = calendar.get(Calendar.ZONE_OFFSET) + calendar.get(Calendar.DST_OFFSET);
            
            if (offset < 0) {
                buffer.append('-');
                offset = -offset;
            } else {
                buffer.append('+');
            }
            
            final int hours = offset / (60 * 60 * 1000);
            appendDigits(buffer, hours);
            
            if (mColon) {
                buffer.append(':');
            }
            
            final int minutes = offset / (60 * 1000) - 60 * hours;
            appendDigits(buffer, minutes);
        }
    }
    
    private static class Iso8601_Rule implements Rule {
        
        // Sign TwoDigitHours or Z
        static final Iso8601_Rule ISO8601_HOURS = new Iso8601_Rule(3);
        // Sign TwoDigitHours Minutes or Z
        static final Iso8601_Rule ISO8601_HOURS_MINUTES = new Iso8601_Rule(5);
        // Sign TwoDigitHours : Minutes or Z
        static final Iso8601_Rule ISO8601_HOURS_COLON_MINUTES = new Iso8601_Rule(6);
        final int length;
        
        Iso8601_Rule(final int length) {
            this.length = length;
        }
        
        /**
         * Iso8601_Rules的工厂方法。
         *
         * @param tokenLen 指示要格式化的时区字符串长度的标记。
         * @return 可以格式化长度为{@code tokenLen}的时区字符串的Iso8601_Rule。如果不存在这样的规则，则将引发IllegalArgumentException异常。
         */
        static Iso8601_Rule getRule(final int tokenLen) {
            switch (tokenLen) {
                case 1:
                    return Iso8601_Rule.ISO8601_HOURS;
                case 2:
                    return Iso8601_Rule.ISO8601_HOURS_MINUTES;
                case 3:
                    return Iso8601_Rule.ISO8601_HOURS_COLON_MINUTES;
                default:
                    throw new IllegalArgumentException("invalid number of X");
            }
        }
        
        /**
         * {@inheritDoc}
         */
        @Override
        public int estimateLength() {
            return length;
        }
        
        /**
         * {@inheritDoc}
         */
        @Override
        public void appendTo(final Appendable buffer, final @NotNull Calendar calendar) throws IOException {
            int offset = calendar.get(Calendar.ZONE_OFFSET) + calendar.get(Calendar.DST_OFFSET);
            if (offset == 0) {
                buffer.append("Z");
                return;
            }
            
            if (offset < 0) {
                buffer.append('-');
                offset = -offset;
            } else {
                buffer.append('+');
            }
            
            final int hours = offset / (60 * 60 * 1000);
            appendDigits(buffer, hours);
            
            if (length < 5) {
                return;
            }
            
            if (length == 6) {
                buffer.append(':');
            }
            
            final int minutes = offset / (60 * 1000) - 60 * hours;
            appendDigits(buffer, minutes);
        }
    }
    
    // ----------------------------------------------------------------------
    
    /**
     * 充当时区名称复合键的内部类
     */
    private static class TimeZoneDisplayKey {
        private final TimeZone mTimeZone;
        private final int mStyle;
        private final Locale mLocale;
        
        TimeZoneDisplayKey(final TimeZone timeZone, final boolean daylight, final int style, final Locale locale) {
            mTimeZone = timeZone;
            if (daylight) {
                mStyle = style | 0x80000000;
            } else {
                mStyle = style;
            }
            mLocale = locale;
        }
        
        /**
         * {@inheritDoc}
         */
        @Override
        public int hashCode() {
            return (mStyle * 31 + mLocale.hashCode()) * 31 + mTimeZone.hashCode();
        }
        
        /**
         * {@inheritDoc}
         */
        @Override
        public boolean equals(final Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj instanceof TimeZoneDisplayKey) {
                final TimeZoneDisplayKey other = (TimeZoneDisplayKey) obj;
                return mTimeZone.equals(other.mTimeZone) && mStyle == other.mStyle && mLocale.equals(other.mLocale);
            }
            return false;
        }
    }
}
