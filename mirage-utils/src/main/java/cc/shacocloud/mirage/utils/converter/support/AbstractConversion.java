package cc.shacocloud.mirage.utils.converter.support;

import cc.shacocloud.mirage.utils.Utils;
import cc.shacocloud.mirage.utils.charSequence.CharUtil;
import cc.shacocloud.mirage.utils.collection.ArrayUtil;
import cc.shacocloud.mirage.utils.converter.Conversion;
import org.jetbrains.annotations.NotNull;

/**
 * 抽象的转换器，定义一些公共的处理方法逻辑
 *
 * @author 思追(shaco)
 */
public abstract class AbstractConversion implements Conversion {
    
    /**
     * 值转为String，用于内部转换中需要使用String中转的情况<br>
     * 转换规则为：
     *
     * <pre>
     * 1、字符串类型将被强转
     * 2、数组将被转换为逗号分隔的字符串
     * 3、其它类型将调用默认的toString()方法
     * </pre>
     *
     * @param value 值
     */
    protected String convertToStr(@NotNull Object value) {
        String result;
        if (value instanceof CharSequence) {
            result = value.toString();
        } else if (ArrayUtil.isArray(value)) {
            result = Utils.nullSafeToString(value);
        } else if (CharUtil.isChar(value)) {
            //对于ASCII字符使用缓存加速转换，减少空间创建
            result = CharUtil.toString((char) value);
        } else {
            result = value.toString();
        }
        
        return result;
    }
    
    
}
