package cc.shacocloud.mirage.utils.converter;

/**
 * {@link TypeConverter} 和 {@link ConversionRegistry} 接口的聚合
 *
 * @author 思追(shaco)
 */
public interface TypeConverterSupport extends TypeConverter, ConversionRegistry {

}
