package cc.shacocloud.mirage.utils.map;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * {@link MultiValueMap} 的简单实现，它包装了一个 {@link LinkedHashMap}，在 {@link ArrayList} 中存储多个值
 * <p>
 * 此 Map 实现通常不是线程安全的。它主要用于从请求对象公开的数据结构，仅用于单个线程。
 */
public class LinkedMultiValueMap<K, V> extends MultiValueMapAdapter<K, V>
        implements Serializable, Cloneable {
    
    public LinkedMultiValueMap() {
        super(new LinkedHashMap<>());
    }
    
    public LinkedMultiValueMap(int expectedSize) {
        super(new LinkedHashMap<>(expectedSize));
    }
    
    public LinkedMultiValueMap(Map<K, List<V>> otherMap) {
        super(new LinkedHashMap<>(otherMap));
    }
    
    
    /**
     * 创建此地图的深层副本
     */
    public LinkedMultiValueMap<K, V> deepCopy() {
        LinkedMultiValueMap<K, V> copy = new LinkedMultiValueMap<>(size());
        forEach((key, values) -> copy.put(key, new ArrayList<>(values)));
        return copy;
    }
    
    /**
     * 创建此地图的常规副本
     *
     * @see #put(Object, List)
     * @see #putAll(Map)
     * @see LinkedMultiValueMap#LinkedMultiValueMap(Map)
     * @see #deepCopy()
     */
    @Override
    public LinkedMultiValueMap<K, V> clone() {
        return new LinkedMultiValueMap<>(this);
    }
    
}
