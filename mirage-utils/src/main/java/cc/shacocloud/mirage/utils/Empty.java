package cc.shacocloud.mirage.utils;

/**
 * 用来标识空值，即 null 的非空表示
 *
 * @author 思追(shaco)
 */
public class Empty {
    
    public static final Empty INSTANCE = new Empty();
    
    @Override
    public boolean equals(Object obj) {
        return this == obj;
    }
    
    @Override
    public int hashCode() {
        return Empty.class.hashCode();
    }
}
