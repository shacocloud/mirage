package cc.shacocloud.mirage.utils.reflection;

import org.jetbrains.annotations.Nullable;

import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

/**
 * 优先级参数名称发现器
 */
public class PrioritizedParameterNameDiscoverer implements ParameterNameDiscoverer {
    
    private final List<ParameterNameDiscoverer> parameterNameDiscoverers = new ArrayList<>(2);
    
    /**
     * 添加发现器
     */
    public void addDiscoverer(ParameterNameDiscoverer pnd) {
        this.parameterNameDiscoverers.add(pnd);
    }
    
    @Nullable
    @Override
    public String[] getParameterNames(Method method) {
        for (ParameterNameDiscoverer pnd : this.parameterNameDiscoverers) {
            String[] result = pnd.getParameterNames(method);
            if (result != null) {
                return result;
            }
        }
        return null;
    }
    
    @Nullable
    @Override
    public String[] getParameterNames(Constructor<?> ctor) {
        for (ParameterNameDiscoverer pnd : this.parameterNameDiscoverers) {
            String[] result = pnd.getParameterNames(ctor);
            if (result != null) {
                return result;
            }
        }
        return null;
    }
    
}
