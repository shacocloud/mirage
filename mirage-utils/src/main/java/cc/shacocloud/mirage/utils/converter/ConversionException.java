package cc.shacocloud.mirage.utils.converter;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

/**
 * 转换异常
 *
 * @author 思追(shaco)
 * @see TypeConverterSupport
 */
@Getter
public class ConversionException extends TypeMismatchException {
    
    private final TypeDescriptor sourceType;
    
    private final TypeDescriptor targetType;
    
    public ConversionException(@NotNull TypeDescriptor sourceType,
                               @NotNull TypeDescriptor targetType,
                               @Nullable Object value) {
        super(value, targetType.getType(), null);
        this.sourceType = sourceType;
        this.targetType = targetType;
    }
    
    public ConversionException(@NotNull TypeDescriptor sourceType,
                               @NotNull TypeDescriptor targetType,
                               @Nullable Object value,
                               @NotNull String message) {
        super(value, targetType.getType(), message, null);
        this.sourceType = sourceType;
        this.targetType = targetType;
    }
    
    public ConversionException(@NotNull TypeDescriptor sourceType,
                               @NotNull TypeDescriptor targetType,
                               @Nullable Object value,
                               Throwable cause) {
        super(value, targetType.getType(), cause);
        this.sourceType = sourceType;
        this.targetType = targetType;
    }
    
    public ConversionException(@NotNull TypeDescriptor sourceType,
                               @NotNull TypeDescriptor targetType,
                               @Nullable Object value,
                               @NotNull String message,
                               Throwable cause) {
        super(value, targetType.getType(), message, cause);
        this.sourceType = sourceType;
        this.targetType = targetType;
    }
    
    
}
