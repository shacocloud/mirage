package cc.shacocloud.mirage.utils.map;

import cc.shacocloud.mirage.utils.Utils;
import org.jetbrains.annotations.Nullable;

import java.lang.ref.ReferenceQueue;
import java.lang.ref.SoftReference;
import java.lang.ref.WeakReference;
import java.lang.reflect.Array;
import java.util.*;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.ReentrantLock;

/**
 * 一个(弱/软)引用的并发 HashMap
 * <p>
 * 参考 spring
 */
public class ConcurrentReferenceHashMap<K, V> extends AbstractMap<K, V> implements ConcurrentMap<K, V> {
    
    private static final int DEFAULT_INITIAL_CAPACITY = 16;
    
    private static final float DEFAULT_LOAD_FACTOR = 0.75f;
    
    private static final int DEFAULT_CONCURRENCY_LEVEL = 16;
    
    private static final ReferenceType DEFAULT_REFERENCE_TYPE = ReferenceType.SOFT;
    
    private static final int MAXIMUM_CONCURRENCY_LEVEL = 1 << 16;
    
    private static final int MAXIMUM_SEGMENT_SIZE = 1 << 30;
    
    
    /**
     * 使用哈希中的高阶位编制索引的段数组
     */
    private final Segment[] segments;
    
    /**
     * 当每个表的平均引用数超过此值时，将尝试调整大小
     */
    private final float loadFactor;
    
    /**
     * 参考类型：软或弱
     */
    private final ReferenceType referenceType;
    
    /**
     * 用于计算段数组的大小和哈希索引的移位值
     */
    private final int shift;
    
    /**
     * 后期绑定条目集
     */
    @Nullable
    private volatile Set<Map.Entry<K, V>> entrySet;
    
    /**
     * 创建一个新的 {@code ConcurrentReferenceHashMap} 实例
     */
    public ConcurrentReferenceHashMap() {
        this(DEFAULT_INITIAL_CAPACITY, DEFAULT_LOAD_FACTOR, DEFAULT_CONCURRENCY_LEVEL, DEFAULT_REFERENCE_TYPE);
    }
    
    /**
     * 创建一个新的 {@code ConcurrentReferenceHashMap} 实例
     *
     * @param initialCapacity 地图的初始容量
     */
    public ConcurrentReferenceHashMap(int initialCapacity) {
        this(initialCapacity, DEFAULT_LOAD_FACTOR, DEFAULT_CONCURRENCY_LEVEL, DEFAULT_REFERENCE_TYPE);
    }
    
    /**
     * 创建一个新的 {@code ConcurrentReferenceHashMap} 实例
     *
     * @param initialCapacity 地图的初始容量
     * @param loadFactor      负载系数。当每个表的平均引用数超过此值时，将尝试调整大小
     */
    public ConcurrentReferenceHashMap(int initialCapacity, float loadFactor) {
        this(initialCapacity, loadFactor, DEFAULT_CONCURRENCY_LEVEL, DEFAULT_REFERENCE_TYPE);
    }
    
    /**
     * 创建一个新的 {@code ConcurrentReferenceHashMap} 实例
     *
     * @param initialCapacity  地图的初始容量
     * @param concurrencyLevel 将同时写入映射的预期线程数
     */
    public ConcurrentReferenceHashMap(int initialCapacity, int concurrencyLevel) {
        this(initialCapacity, DEFAULT_LOAD_FACTOR, concurrencyLevel, DEFAULT_REFERENCE_TYPE);
    }
    
    /**
     * 创建一个新的 {@code ConcurrentReferenceHashMap} 实例
     *
     * @param initialCapacity 地图的初始容量
     * @param referenceType   用于条目的引用类型（软或弱）
     */
    public ConcurrentReferenceHashMap(int initialCapacity, ReferenceType referenceType) {
        this(initialCapacity, DEFAULT_LOAD_FACTOR, DEFAULT_CONCURRENCY_LEVEL, referenceType);
    }
    
    /**
     * 创建一个新的 {@code ConcurrentReferenceHashMap} 实例
     *
     * @param initialCapacity  地图的初始容量
     * @param loadFactor       负载系数。当每个表的平均引用数超过此值时，将尝试调整大小
     * @param concurrencyLevel 将同时写入映射的预期线程数
     */
    public ConcurrentReferenceHashMap(int initialCapacity, float loadFactor, int concurrencyLevel) {
        this(initialCapacity, loadFactor, concurrencyLevel, DEFAULT_REFERENCE_TYPE);
    }
    
    /**
     * 创建一个新的 {@code ConcurrentReferenceHashMap} 实例
     *
     * @param initialCapacity  地图的初始容量
     * @param loadFactor       负载系数。当每个表的平均引用数超过此值时，将尝试调整大小
     * @param concurrencyLevel 将同时写入映射的预期线程数
     * @param referenceType    用于条目的引用类型（软或弱）
     */
    @SuppressWarnings("unchecked")
    public ConcurrentReferenceHashMap(
            int initialCapacity, float loadFactor, int concurrencyLevel, ReferenceType referenceType) {
        if (initialCapacity < 0) {
            throw new IllegalArgumentException("初始容量不得为负数");
        }
        if (loadFactor < 0f) {
            throw new IllegalArgumentException("载客率必须为正");
        }
        if (concurrencyLevel < 0) {
            throw new IllegalArgumentException("并发级别必须为正");
        }
        if (Objects.isNull(referenceType)) {
            throw new IllegalArgumentException("引用类型不得为空");
        }
        
        this.loadFactor = loadFactor;
        this.shift = calculateShift(concurrencyLevel, MAXIMUM_CONCURRENCY_LEVEL);
        int size = 1 << this.shift;
        this.referenceType = referenceType;
        int roundedUpSegmentCapacity = (int) ((initialCapacity + size - 1L) / size);
        int initialSize = 1 << calculateShift(roundedUpSegmentCapacity, MAXIMUM_SEGMENT_SIZE);
        Segment[] segments = (Segment[]) Array.newInstance(Segment.class, size);
        int resizeThreshold = (int) (initialSize * getLoadFactor());
        for (int i = 0; i < segments.length; i++) {
            segments[i] = new Segment(initialSize, resizeThreshold);
        }
        this.segments = segments;
    }
    
    /**
     * 计算可用于在指定的最大值和最小值之间创建 2 次幂值的偏移值。
     *
     * @param minimumValue 最小值
     * @param maximumValue 最大值
     * @return 计算的偏移（使用 {@code 1 << 偏移} 获取值）
     */
    protected static int calculateShift(int minimumValue, int maximumValue) {
        int shift = 0;
        int value = 1;
        while (value < minimumValue && value < maximumValue) {
            value <<= 1;
            shift++;
        }
        return shift;
    }
    
    protected final float getLoadFactor() {
        return this.loadFactor;
    }
    
    protected final int getSegmentsSize() {
        return this.segments.length;
    }
    
    protected final Segment getSegment(int index) {
        return this.segments[index];
    }
    
    /**
     * 返回 ConcurrentReferenceHashMap.ReferenceManager 的工厂方法。此方法将为每个ConcurrentReferenceHashMap.Segment调用一次。
     */
    protected ReferenceManager createReferenceManager() {
        return new ReferenceManager();
    }
    
    /**
     * 获取给定对象的哈希值，应用额外的哈希函数以减少冲突。此实现使用与 相同的 ConcurrentHashMapWang/Jenkins 算法。子类可以覆盖以提供替代哈希。
     * 形参:
     *
     * @param o 要哈希的对象（可能为空）
     * @return 生成的哈希代码.
     */
    protected int getHash(@Nullable Object o) {
        int hash = (o != null ? o.hashCode() : 0);
        hash += (hash << 15) ^ 0xffffcd7d;
        hash ^= (hash >>> 10);
        hash += (hash << 3);
        hash ^= (hash >>> 6);
        hash += (hash << 2) + (hash << 14);
        hash ^= (hash >>> 16);
        return hash;
    }
    
    @Override
    @Nullable
    public V get(@Nullable Object key) {
        Reference<K, V> ref = getReference(key, Restructure.WHEN_NECESSARY);
        Entry<K, V> entry = (ref != null ? ref.get() : null);
        return (entry != null ? entry.getValue() : null);
    }
    
    @Override
    @Nullable
    public V getOrDefault(@Nullable Object key, @Nullable V defaultValue) {
        Reference<K, V> ref = getReference(key, Restructure.WHEN_NECESSARY);
        Entry<K, V> entry = (ref != null ? ref.get() : null);
        return (entry != null ? entry.getValue() : defaultValue);
    }
    
    @Override
    public boolean containsKey(@Nullable Object key) {
        Reference<K, V> ref = getReference(key, Restructure.WHEN_NECESSARY);
        Entry<K, V> entry = (ref != null ? ref.get() : null);
        return (entry != null && Utils.nullSafeEquals(entry.getKey(), key));
    }
    
    /**
     * 为指定的 {@code 键} 返回 {@link Reference} 的 {@link Entry}，如果未找到，则返回 {@code null}
     *
     * @param key         键（可以是 {@code 空}）
     * @param restructure 此调用期间允许的重组类型
     * @return 引用，如果未找到，则为 {@code null}
     */
    @Nullable
    protected final Reference<K, V> getReference(@Nullable Object key, Restructure restructure) {
        int hash = getHash(key);
        return getSegmentForHash(hash).getReference(key, hash, restructure);
    }
    
    @Override
    @Nullable
    public V put(@Nullable K key, @Nullable V value) {
        return put(key, value, true);
    }
    
    @Override
    @Nullable
    public V putIfAbsent(@Nullable K key, @Nullable V value) {
        return put(key, value, false);
    }
    
    @Nullable
    private V put(@Nullable final K key, @Nullable final V value, final boolean overwriteExisting) {
        return doTask(key, new Task<V>(TaskOption.RESTRUCTURE_BEFORE, TaskOption.RESIZE) {
            @Override
            @Nullable
            protected V execute(@Nullable Reference<K, V> ref, @Nullable Entry<K, V> entry, @Nullable Entries<V> entries) {
                if (entry != null) {
                    V oldValue = entry.getValue();
                    if (overwriteExisting) {
                        entry.setValue(value);
                    }
                    return oldValue;
                }
                Objects.requireNonNull(entries, "无条目段");
                entries.add(value);
                return null;
            }
        });
    }
    
    @Override
    @Nullable
    public V remove(@Nullable Object key) {
        return doTask(key, new Task<V>(TaskOption.RESTRUCTURE_AFTER, TaskOption.SKIP_IF_EMPTY) {
            @Override
            @Nullable
            protected V execute(@Nullable Reference<K, V> ref, @Nullable Entry<K, V> entry) {
                if (entry != null) {
                    if (ref != null) {
                        ref.release();
                    }
                    return entry.value;
                }
                return null;
            }
        });
    }
    
    @Override
    public boolean remove(@Nullable Object key, final @Nullable Object value) {
        Boolean result = doTask(key, new Task<Boolean>(TaskOption.RESTRUCTURE_AFTER, TaskOption.SKIP_IF_EMPTY) {
            @Override
            protected Boolean execute(@Nullable Reference<K, V> ref, @Nullable Entry<K, V> entry) {
                if (entry != null && Utils.nullSafeEquals(entry.getValue(), value)) {
                    if (ref != null) {
                        ref.release();
                    }
                    return true;
                }
                return false;
            }
        });
        return (Boolean.TRUE.equals(result));
    }
    
    @Override
    public boolean replace(@Nullable K key, final @Nullable V oldValue, final @Nullable V newValue) {
        Boolean result = doTask(key, new Task<Boolean>(TaskOption.RESTRUCTURE_BEFORE, TaskOption.SKIP_IF_EMPTY) {
            @Override
            protected Boolean execute(@Nullable Reference<K, V> ref, @Nullable Entry<K, V> entry) {
                if (entry != null && Utils.nullSafeEquals(entry.getValue(), oldValue)) {
                    entry.setValue(newValue);
                    return true;
                }
                return false;
            }
        });
        return (Boolean.TRUE.equals(result));
    }
    
    @Override
    @Nullable
    public V replace(@Nullable K key, final @Nullable V value) {
        return doTask(key, new Task<V>(TaskOption.RESTRUCTURE_BEFORE, TaskOption.SKIP_IF_EMPTY) {
            @Override
            @Nullable
            protected V execute(@Nullable Reference<K, V> ref, @Nullable Entry<K, V> entry) {
                if (entry != null) {
                    V oldValue = entry.getValue();
                    entry.setValue(value);
                    return oldValue;
                }
                return null;
            }
        });
    }
    
    @Override
    public void clear() {
        for (Segment segment : this.segments) {
            segment.clear();
        }
    }
    
    /**
     * 删除任何已被垃圾回收且不再引用的条目。在正常情况下，垃圾回收条目会在地图中添加或删除项目时自动清除。此方法可用于强制清除，当 Map 频繁读取但更新频率较低时非常有用。
     */
    public void purgeUnreferencedEntries() {
        for (Segment segment : this.segments) {
            segment.restructureIfNecessary(false);
        }
    }
    
    @Override
    public int size() {
        int size = 0;
        for (Segment segment : this.segments) {
            size += segment.getCount();
        }
        return size;
    }
    
    @Override
    public boolean isEmpty() {
        for (Segment segment : this.segments) {
            if (segment.getCount() > 0) {
                return false;
            }
        }
        return true;
    }
    
    @Override
    public Set<Map.Entry<K, V>> entrySet() {
        Set<Map.Entry<K, V>> entrySet = this.entrySet;
        if (entrySet == null) {
            entrySet = new EntrySet();
            this.entrySet = entrySet;
        }
        return entrySet;
    }
    
    @Nullable
    private <T> T doTask(@Nullable Object key, Task<T> task) {
        int hash = getHash(key);
        return getSegmentForHash(hash).doTask(hash, key, task);
    }
    
    private Segment getSegmentForHash(int hash) {
        return this.segments[(hash >>> (32 - this.shift)) & (this.segments.length - 1)];
    }
    
    
    /**
     * 此地图支持的各种参考类型
     */
    public enum ReferenceType {
        
        /**
         * 使用 {@link SoftReference SoftReferences}.
         */
        SOFT,
        
        /**
         * 使用 {@link WeakReference WeakReferences}.
         */
        WEAK
    }
    
    /**
     * 可以针对 {@li* 一个{@code Task}所支持的各种选项。
     */
    private enum TaskOption {
        
        RESTRUCTURE_BEFORE, RESTRUCTURE_AFTER, SKIP_IF_EMPTY, RESIZE
    }
    
    
    /**
     * 的类型。
     */
    protected enum Restructure {
        
        WHEN_NECESSARY, NEVER
    }
    
    
    protected interface Reference<K, V> {
        
        /**
         * 返回引用的条目，如果该条目不再可用，则返回 {@code null}。
         */
        @Nullable
        Entry<K, V> get();
        
        /**
         * 返回引用的哈希。
         */
        int getHash();
        
        /**
         * 返回链中的下一个引用，如果没有，则返回 {@code null}。
         */
        @Nullable
        Reference<K, V> getNext();
        
        /**
         * 释放此条目并确保它将从  {@link ReferenceManager#pollForPurge()} 返回。
         */
        void release();
    }
    
    
    /**
     * 允许一个任务访问{@link ConcurrentReferenceHashMap.Segment}条目。
     */
    private interface Entries<V> {
        
        /**
         * 添加一个具有指定值的新条目。
         *
         * @param value 增加的价值
         */
        void add(@Nullable V value);
    }
    
    /**
     * 单个地图条目
     *
     * @param <K> 密钥类型
     * @param <V> 值类型
     */
    protected static final class Entry<K, V> implements Map.Entry<K, V> {
        
        @Nullable
        private final K key;
        
        @Nullable
        private volatile V value;
        
        public Entry(@Nullable K key, @Nullable V value) {
            this.key = key;
            this.value = value;
        }
        
        @Override
        @Nullable
        public K getKey() {
            return this.key;
        }
        
        @Override
        @Nullable
        public V getValue() {
            return this.value;
        }
        
        @Override
        @Nullable
        public V setValue(@Nullable V value) {
            V previous = this.value;
            this.value = value;
            return previous;
        }
        
        @Override
        public String toString() {
            return (this.key + "=" + this.value);
        }
        
        @Override
        @SuppressWarnings("rawtypes")
        public boolean equals(@Nullable Object other) {
            if (this == other) {
                return true;
            }
            if (!(other instanceof Map.Entry)) {
                return false;
            }
            Map.Entry otherEntry = (Map.Entry) other;
            return (Utils.nullSafeEquals(getKey(), otherEntry.getKey()) &&
                    Utils.nullSafeEquals(getValue(), otherEntry.getValue()));
        }
        
        @Override
        public int hashCode() {
            return (Utils.nullSafeHashCode(this.key) ^ Utils.nullSafeHashCode(this.value));
        }
    }
    
    /**
     * oftReference 软参考}的内部{@link Reference}实现。
     */
    private static final class SoftEntryReference<K, V> extends SoftReference<Entry<K, V>> implements Reference<K, V> {
        
        private final int hash;
        
        @Nullable
        private final Reference<K, V> nextReference;
        
        public SoftEntryReference(Entry<K, V> entry, int hash, @Nullable Reference<K, V> next,
                                  ReferenceQueue<Entry<K, V>> queue) {
            
            super(entry, queue);
            this.hash = hash;
            this.nextReference = next;
        }
        
        @Override
        public int getHash() {
            return this.hash;
        }
        
        @Override
        @Nullable
        public Reference<K, V> getNext() {
            return this.nextReference;
        }
        
        @Override
        public void release() {
            enqueue();
            clear();
        }
    }
    
    /**
     * eakReference WeakReferences}的内部{@link Reference}实现。
     */
    private static final class WeakEntryReference<K, V> extends WeakReference<Entry<K, V>> implements Reference<K, V> {
        
        private final int hash;
        
        @Nullable
        private final Reference<K, V> nextReference;
        
        public WeakEntryReference(Entry<K, V> entry, int hash, @Nullable Reference<K, V> next,
                                  ReferenceQueue<Entry<K, V>> queue) {
            
            super(entry, queue);
            this.hash = hash;
            this.nextReference = next;
        }
        
        @Override
        public int getHash() {
            return this.hash;
        }
        
        @Override
        @Nullable
        public Reference<K, V> getNext() {
            return this.nextReference;
        }
        
        @Override
        public void release() {
            enqueue();
            clear();
        }
    }
    
    /**
     * 内部条目集的实施。* 内部入口迭代器的实
     * /**
     * 用于划分映射以允许更好的并发性能的单个段
     */
    @SuppressWarnings("serial")
    protected final class Segment extends ReentrantLock {
        
        private final ReferenceManager referenceManager;
        
        private final int initialSize;
        /**
         * 此段中包含的引用总数。这包括链接引用和已垃圾回收但未清除的引用
         */
        private final AtomicInteger count = new AtomicInteger();
        /**
         * 使用哈希中的低阶位编制索引的引用数组。此属性只能与 {@code resizeThreshold} 一起设置
         */
        private volatile Reference<K, V>[] references;
        /**
         * 应设置调整引用大小时的阈值。当 {@code 计数} 超过此值时，将调整引用的大小。
         */
        private int resizeThreshold;
        
        public Segment(int initialSize, int resizeThreshold) {
            this.referenceManager = createReferenceManager();
            this.initialSize = initialSize;
            this.references = createReferenceArray(initialSize);
            this.resizeThreshold = resizeThreshold;
        }
        
        @Nullable
        public Reference<K, V> getReference(@Nullable Object key, int hash, Restructure restructure) {
            if (restructure == Restructure.WHEN_NECESSARY) {
                restructureIfNecessary(false);
            }
            if (this.count.get() == 0) {
                return null;
            }
            // Use a local copy to protect against other threads writing
            Reference<K, V>[] references = this.references;
            int index = getIndex(hash, references);
            Reference<K, V> head = references[index];
            return findInChain(head, key, hash);
        }
        
        /**
         * 将更新操作应用于此段。该段将在更新期间被锁定。
         *
         * @param hash 键的哈希值
         * @param key  键
         * @param task 更新操作
         * @return 操作的结果
         */
        @Nullable
        public <T> T doTask(final int hash, @Nullable final Object key, final Task<T> task) {
            boolean resize = task.hasOption(TaskOption.RESIZE);
            if (task.hasOption(TaskOption.RESTRUCTURE_BEFORE)) {
                restructureIfNecessary(resize);
            }
            if (task.hasOption(TaskOption.SKIP_IF_EMPTY) && this.count.get() == 0) {
                return task.execute(null, null, null);
            }
            lock();
            try {
                final int index = getIndex(hash, this.references);
                final Reference<K, V> head = this.references[index];
                Reference<K, V> ref = findInChain(head, key, hash);
                Entry<K, V> entry = (ref != null ? ref.get() : null);
                Entries<V> entries = value -> {
                    @SuppressWarnings("unchecked")
                    Entry<K, V> newEntry = new Entry<>((K) key, value);
                    Reference<K, V> newReference = Segment.this.referenceManager.createReference(newEntry, hash, head);
                    Segment.this.references[index] = newReference;
                    Segment.this.count.incrementAndGet();
                };
                return task.execute(ref, entry, entries);
            } finally {
                unlock();
                if (task.hasOption(TaskOption.RESTRUCTURE_AFTER)) {
                    restructureIfNecessary(resize);
                }
            }
        }
        
        /**
         * 清除此细分中的所有项目
         */
        public void clear() {
            if (this.count.get() == 0) {
                return;
            }
            lock();
            try {
                this.references = createReferenceArray(this.initialSize);
                this.resizeThreshold = (int) (this.references.length * getLoadFactor());
                this.count.set(0);
            } finally {
                unlock();
            }
        }
        
        /**
         * 必要时重构底层数据结构。此方法可以增加引用表的大小，并清除任何已被垃圾回收的引用。
         *
         * @param allowResize 如果允许调整大小
         */
        private void restructureIfNecessary(boolean allowResize) {
            int currCount = this.count.get();
            boolean needsResize = allowResize && (currCount > 0 && currCount >= this.resizeThreshold);
            Reference<K, V> ref = this.referenceManager.pollForPurge();
            if (ref != null || (needsResize)) {
                restructure(allowResize, ref);
            }
        }
        
        private void restructure(boolean allowResize, @Nullable Reference<K, V> ref) {
            boolean needsResize;
            lock();
            try {
                int countAfterRestructure = this.count.get();
                Set<Reference<K, V>> toPurge = Collections.emptySet();
                if (ref != null) {
                    toPurge = new HashSet<>();
                    while (ref != null) {
                        toPurge.add(ref);
                        ref = this.referenceManager.pollForPurge();
                    }
                }
                countAfterRestructure -= toPurge.size();
                
                // 重新计算，同时考虑到锁定内的计数和将被清除的项目
                needsResize = (countAfterRestructure > 0 && countAfterRestructure >= this.resizeThreshold);
                boolean resizing = false;
                int restructureSize = this.references.length;
                if (allowResize && needsResize && restructureSize < MAXIMUM_SEGMENT_SIZE) {
                    restructureSize <<= 1;
                    resizing = true;
                }
                
                // 创建新表或重用现有表
                Reference<K, V>[] restructured =
                        (resizing ? createReferenceArray(restructureSize) : this.references);
                
                // 重组
                for (int i = 0; i < this.references.length; i++) {
                    ref = this.references[i];
                    if (!resizing) {
                        restructured[i] = null;
                    }
                    while (ref != null) {
                        if (!toPurge.contains(ref)) {
                            Entry<K, V> entry = ref.get();
                            if (entry != null) {
                                int index = getIndex(ref.getHash(), restructured);
                                restructured[index] = this.referenceManager.createReference(
                                        entry, ref.getHash(), restructured[index]);
                            }
                        }
                        ref = ref.getNext();
                    }
                }
                
                // 替换易失性成员
                if (resizing) {
                    this.references = restructured;
                    this.resizeThreshold = (int) (this.references.length * getLoadFactor());
                }
                this.count.set(Math.max(countAfterRestructure, 0));
            } finally {
                unlock();
            }
        }
        
        @Nullable
        private Reference<K, V> findInChain(Reference<K, V> ref, @Nullable Object key, int hash) {
            Reference<K, V> currRef = ref;
            while (currRef != null) {
                if (currRef.getHash() == hash) {
                    Entry<K, V> entry = currRef.get();
                    if (entry != null) {
                        K entryKey = entry.getKey();
                        if (Utils.nullSafeEquals(entryKey, key)) {
                            return currRef;
                        }
                    }
                }
                currRef = currRef.getNext();
            }
            return null;
        }
        
        @SuppressWarnings({"unchecked"})
        private Reference<K, V>[] createReferenceArray(int size) {
            return new Reference[size];
        }
        
        private int getIndex(int hash, Reference<K, V>[] references) {
            return (hash & (references.length - 1));
        }
        
        /**
         * 返回当前引用数组的大小。
         */
        public int getSize() {
            return this.references.length;
        }
        
        /**
         * 返回此段中的引用总数。
         */
        public int getCount() {
            return this.count.get();
        }
    }
    
    /**
     * 可以进行的结构调整
     * nk Segment} 运行 {@link  Segment#doTask} 的任务。
     */
    private abstract class Task<T> {
        
        private final EnumSet<TaskOption> options;
        
        public Task(TaskOption... options) {
            this.options = (options.length == 0 ? EnumSet.noneOf(TaskOption.class) : EnumSet.of(options[0], options));
        }
        
        public boolean hasOption(TaskOption option) {
            return this.options.contains(option);
        }
        
        /**
         * 执行任务
         *
         * @param ref     找到的引用（或 {@code null}）
         * @param entry   找到的条目（或 {@code null}）
         * @param entries 访问基础条目
         * @return 任务的结果
         * @see #execute(Reference, Entry)
         */
        @Nullable
        protected T execute(@Nullable Reference<K, V> ref, @Nullable Entry<K, V> entry, @Nullable Entries<V> entries) {
            return execute(ref, entry);
        }
        
        /**
         * 方便的方法，可用于那些不需要访问{@link Entries}的任务。
         *
         * @param ref   找到的引用（或{@code null}）。
         * @param entry 找到的条目（或{@code null}）。
         * @return 任务的结果
         * @see #execute(Reference, Entry, Entries)
         */
        @Nullable
        protected T execute(@Nullable Reference<K, V> ref, @Nullable Entry<K, V> entry) {
            return null;
        }
    }
    
    /**
     * 用于管理{@lin
     */
    private class EntrySet extends AbstractSet<Map.Entry<K, V>> {
        
        @Override
        public Iterator<Map.Entry<K, V>> iterator() {
            return new EntryIterator();
        }
        
        @Override
        public boolean contains(@Nullable Object o) {
            if (o instanceof Map.Entry<?, ?>) {
                Map.Entry<?, ?> entry = (Map.Entry<?, ?>) o;
                Reference<K, V> ref = ConcurrentReferenceHashMap.this.getReference(entry.getKey(), Restructure.NEVER);
                Entry<K, V> otherEntry = (ref != null ? ref.get() : null);
                if (otherEntry != null) {
                    return Utils.nullSafeEquals(entry.getValue(), otherEntry.getValue());
                }
            }
            return false;
        }
        
        @Override
        public boolean remove(Object o) {
            if (o instanceof Map.Entry<?, ?>) {
                Map.Entry<?, ?> entry = (Map.Entry<?, ?>) o;
                return ConcurrentReferenceHashMap.this.remove(entry.getKey(), entry.getValue());
            }
            return false;
        }
        
        @Override
        public int size() {
            return ConcurrentReferenceHashMap.this.size();
        }
        
        @Override
        public void clear() {
            ConcurrentReferenceHashMap.this.clear();
        }
    }
    
    private class EntryIterator implements Iterator<Map.Entry<K, V>> {
        
        private int segmentIndex;
        
        private int referenceIndex;
        
        @Nullable
        private Reference<K, V>[] references;
        
        @Nullable
        private Reference<K, V> reference;
        
        @Nullable
        private Entry<K, V> next;
        
        @Nullable
        private Entry<K, V> last;
        
        public EntryIterator() {
            moveToNextSegment();
        }
        
        @Override
        public boolean hasNext() {
            getNextIfNecessary();
            return (this.next != null);
        }
        
        @Override
        public Entry<K, V> next() {
            getNextIfNecessary();
            if (this.next == null) {
                throw new NoSuchElementException();
            }
            this.last = this.next;
            this.next = null;
            return this.last;
        }
        
        private void getNextIfNecessary() {
            while (this.next == null) {
                moveToNextReference();
                if (this.reference == null) {
                    return;
                }
                this.next = this.reference.get();
            }
        }
        
        private void moveToNextReference() {
            if (this.reference != null) {
                this.reference = this.reference.getNext();
            }
            while (this.reference == null && this.references != null) {
                if (this.referenceIndex >= this.references.length) {
                    moveToNextSegment();
                    this.referenceIndex = 0;
                } else {
                    this.reference = this.references[this.referenceIndex];
                    this.referenceIndex++;
                }
            }
        }
        
        private void moveToNextSegment() {
            this.reference = null;
            this.references = null;
            if (this.segmentIndex < ConcurrentReferenceHashMap.this.segments.length) {
                this.references = ConcurrentReferenceHashMap.this.segments[this.segmentIndex].references;
                this.segmentIndex++;
            }
        }
        
        @Override
        public void remove() {
            Objects.requireNonNull(this.last, "没有要删除的元素");
            ConcurrentReferenceHashMap.this.remove(this.last.getKey());
            this.last = null;
        }
    }
    
    protected class ReferenceManager {
        
        private final ReferenceQueue<Entry<K, V>> queue = new ReferenceQueue<>();
        
        /**
         * 用于创建一个新的{@link Reference}的工厂方法。
         *
         * @param entry 所载的条目
         * @param hash  哈希值
         * @param next  链中的下一个引用，如果没有，则为{@code null}
         * @return a new {@link Reference}
         */
        public Reference<K, V> createReference(Entry<K, V> entry, int hash, @Nullable Reference<K, V> next) {
            if (ConcurrentReferenceHashMap.this.referenceType == ReferenceType.WEAK) {
                return new WeakEntryReference<>(entry, hash, next, this.queue);
            }
            return new SoftEntryReference<>(entry, hash, next, this.queue);
        }
        
        /**
         * 返回任何已经被垃圾收集并且可以从*底层结构中清除的引用，如果没有引用需要清除，则返回{@code null}。
         * 这个方法必须是线程安全的，最好是在返回* {@code null}时不阻塞。引用应该被返回一次，而且只有一次。
         *
         * @return 清除的引用或{@code null}。
         */
        @SuppressWarnings("unchecked")
        @Nullable
        public Reference<K, V> pollForPurge() {
            return (Reference<K, V>) this.queue.poll();
        }
    }
    
}
