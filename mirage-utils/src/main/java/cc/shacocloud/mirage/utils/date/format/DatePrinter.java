package cc.shacocloud.mirage.utils.date.format;

import java.util.Calendar;
import java.util.Date;

/**
 * 日期格式化输出接口
 *
 * @author 思追(shaco)
 */
public interface DatePrinter extends DateBasic {
    
    /**
     * 格式化日期表示的毫秒数
     *
     * @param millis 日期毫秒数
     */
    String format(long millis);
    
    /**
     * 使用 {@code GregorianCalendar} 格式化 {@code Date}
     *
     * @param date 日期 {@link Date}
     * @return 格式化后的字符串
     */
    String format(Date date);
    
    /**
     * <p>
     * 设置 {@code 日历} 对象的格式
     * </p>
     * 格式化 {@link Calendar}
     *
     * @param calendar {@link Calendar}
     * @return 格式化后的字符串
     */
    String format(Calendar calendar);
    
    /**
     * 将毫秒 {@code long} 值格式化为提供的 {@code Appendable}。
     *
     * @param millis 要格式化的毫秒值
     * @param buf    要格式化的缓冲区
     * @param <B>    可追加类类型，通常是 StringBuilder 或 StringBuffer
     * @return 通常是 StringBuilder 或 StringBuffer
     */
    <B extends Appendable> B format(long millis, B buf);
    
    /**
     * 使用 {@code GregorianCalendar} 将 {@code Date} 对象格式化为提供的 {@code Appendable}。
     *
     * @param date 要设置格式的日期
     * @param buf  要格式化的缓冲区
     * @param <B>  可追加类类型，通常是 StringBuilder 或 StringBuffer
     * @return 通常是 StringBuilder 或 StringBuffer
     */
    <B extends Appendable> B format(Date date, B buf);
    
    /**
     * <p>
     * 将 {@code Calendar} 对象的格式设置为提供的 {@code Appendable}。
     * </p>
     * 日历上设置的时区仅用于调整时间偏移量。在构造解析器期间指定的时区将确定格式化字符串中使用的时区。
     *
     * @param calendar 要设置格式的日历
     * @param buf      要格式化的缓冲区
     * @param <B>      可追加类类型，通常是 StringBuilder 或 StringBuffer
     * @return 通常是 StringBuilder 或 StringBuffer
     */
    <B extends Appendable> B format(Calendar calendar, B buf);
}
