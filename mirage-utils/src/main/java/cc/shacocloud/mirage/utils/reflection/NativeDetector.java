package cc.shacocloud.mirage.utils.reflection;

/**
 * 用于检测 GraalVM 本机映像环境的公共委托
 *
 * <p>需要使用 {@code -H：+InlineBeforeAnalysis} 本机映像编译器标志，以便允许在生成时删除代码
 */
public abstract class NativeDetector {
    
    private static final boolean imageCode = (System.getProperty("org.graalvm.nativeimage.imagecode") != null);
    
    /**
     * 如果在映像生成上下文中或在映像运行时调用，则返回 {@code true}，否则 {@code false}
     */
    public static boolean inNativeImage() {
        return imageCode;
    }
}
