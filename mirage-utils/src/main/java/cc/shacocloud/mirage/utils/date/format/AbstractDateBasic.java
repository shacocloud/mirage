package cc.shacocloud.mirage.utils.date.format;

import java.io.Serializable;
import java.util.Locale;
import java.util.TimeZone;

/**
 * 抽象的 {@link DateBasic}
 *
 * @author 思追(shaco)
 */
public abstract class AbstractDateBasic implements DateBasic, Serializable {
    
    protected final String pattern;
    protected final TimeZone timeZone;
    protected final Locale locale;
    
    /**
     * 构造，内部使用
     *
     * @param pattern  使用{@link java.text.SimpleDateFormat} 相同的日期格式
     * @param timeZone 非空时区{@link TimeZone}
     * @param locale   非空{@link Locale} 日期地理位置
     */
    protected AbstractDateBasic(final String pattern, final TimeZone timeZone, final Locale locale) {
        this.pattern = pattern;
        this.timeZone = timeZone;
        this.locale = locale;
    }
    
    @Override
    public String getPattern() {
        return pattern;
    }
    
    @Override
    public TimeZone getTimeZone() {
        return timeZone;
    }
    
    @Override
    public Locale getLocale() {
        return locale;
    }
    
    @Override
    public boolean equals(final Object obj) {
        if (!(obj instanceof FastDatePrinter)) {
            return false;
        }
        final AbstractDateBasic other = (AbstractDateBasic) obj;
        return pattern.equals(other.pattern) && timeZone.equals(other.timeZone) && locale.equals(other.locale);
    }
    
    @Override
    public int hashCode() {
        return pattern.hashCode() + 13 * (timeZone.hashCode() + 13 * locale.hashCode());
    }
}
