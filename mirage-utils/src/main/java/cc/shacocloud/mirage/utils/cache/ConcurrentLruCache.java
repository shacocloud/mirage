package cc.shacocloud.mirage.utils.cache;

import org.jetbrains.annotations.NotNull;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedDeque;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import java.util.function.Function;

/**
 * 简单 LRU（最近最少使用）缓存，受指定的缓存限制限制
 *
 * <p>
 * 此实现由用于存储缓存值的 {@code ConcurrentHashMap} 和用于对键进行排序并在缓存满负荷时选择最近最少使用的键的 {@code ConcurrentLinkedDeque} 提供支持。
 *
 * @param <K> 用于缓存检索的密钥的类型
 * @param <V> 缓存值的类型
 * @see #get
 */
public class ConcurrentLruCache<K, V> {
    
    private final int sizeLimit;
    
    private final Function<K, V> generator;
    
    private final ConcurrentHashMap<K, V> cache = new ConcurrentHashMap<>();
    
    private final ConcurrentLinkedDeque<K> queue = new ConcurrentLinkedDeque<>();
    
    private final ReadWriteLock lock = new ReentrantReadWriteLock();
    
    private volatile int size;
    
    
    /**
     * 创建一个具有给定限制和生成器函数的新缓存实例
     *
     * @param sizeLimit 缓存中的最大条目数（0 表示无缓存，始终生成新值）
     * @param generator 为给定键生成新值的函数
     */
    public ConcurrentLruCache(int sizeLimit, @NotNull Function<K, V> generator) {
        if (sizeLimit < 0) {
            throw new IllegalArgumentException("缓存大小限制不得为负数");
        }
        this.sizeLimit = sizeLimit;
        this.generator = generator;
    }
    
    
    /**
     * 从缓存中检索条目，这可能会触发值的生成
     *
     * @param key 检索条目的键
     * @return 缓存值或新生成的值
     */
    public V get(K key) {
        if (this.sizeLimit == 0) {
            return this.generator.apply(key);
        }
        
        V cached = this.cache.get(key);
        if (cached != null) {
            if (this.size < this.sizeLimit) {
                return cached;
            }
            this.lock.readLock().lock();
            try {
                if (this.queue.removeLastOccurrence(key)) {
                    this.queue.offer(key);
                }
                return cached;
            } finally {
                this.lock.readLock().unlock();
            }
        }
        
        this.lock.writeLock().lock();
        try {
            // 在同一密钥上进行并发读取时重试
            cached = this.cache.get(key);
            if (cached != null) {
                if (this.queue.removeLastOccurrence(key)) {
                    this.queue.offer(key);
                }
                return cached;
            }
            // 先生成值，防止大小不一致
            V value = this.generator.apply(key);
            if (this.size == this.sizeLimit) {
                K leastUsed = this.queue.poll();
                if (leastUsed != null) {
                    this.cache.remove(leastUsed);
                }
            }
            this.queue.offer(key);
            this.cache.put(key, value);
            this.size = this.cache.size();
            return value;
        } finally {
            this.lock.writeLock().unlock();
        }
    }
    
    /**
     * 确定此缓存中是否存在给定的键
     *
     * @param key 要检查的键
     * @return 如果存在键为 {@code true}，如果没有匹配的键为 {@code false}
     */
    public boolean contains(K key) {
        return this.cache.containsKey(key);
    }
    
    /**
     * 立即删除给定的键和任何关联的值
     *
     * @param key 逐出条目的密钥
     * @return 如果密钥之前存在为 {@code true}，如果没有匹配的密钥为 {@code false}
     */
    public boolean remove(K key) {
        this.lock.writeLock().lock();
        try {
            boolean wasPresent = (this.cache.remove(key) != null);
            this.queue.remove(key);
            this.size = this.cache.size();
            return wasPresent;
        } finally {
            this.lock.writeLock().unlock();
        }
    }
    
    /**
     * 立即从此缓存中删除所有条目
     */
    public void clear() {
        this.lock.writeLock().lock();
        try {
            this.cache.clear();
            this.queue.clear();
            this.size = 0;
        } finally {
            this.lock.writeLock().unlock();
        }
    }
    
    /**
     * 返回缓存的当前大小
     *
     * @see #sizeLimit()
     */
    public int size() {
        return this.size;
    }
    
    /**
     * 返回缓存中的最大条目数（0 表示无缓存，始终生成新值）
     *
     * @see #size()
     */
    public int sizeLimit() {
        return this.sizeLimit;
    }
    
}
