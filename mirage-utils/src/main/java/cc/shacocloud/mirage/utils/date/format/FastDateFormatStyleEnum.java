package cc.shacocloud.mirage.utils.date.format;

import lombok.Getter;

import java.text.DateFormat;

/**
 * @author 思追(shaco)
 * @see FastDateFormat
 */
public enum FastDateFormatStyleEnum {
    
    FULL(DateFormat.FULL),
    
    LONG(DateFormat.LONG),
    
    MEDIUM(DateFormat.MEDIUM),
    
    SHORT(DateFormat.SHORT),
    ;
    
    @Getter
    private final int value;
    
    FastDateFormatStyleEnum(int value) {
        this.value = value;
    }
}
