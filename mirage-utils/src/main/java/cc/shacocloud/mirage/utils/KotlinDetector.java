package cc.shacocloud.mirage.utils;


import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;

/**
 * 用于检测 Kotlin 存在和识别 Kotlin 类型的公共委托
 */
public abstract class KotlinDetector {
    
    @Nullable
    private static final Class<? extends Annotation> kotlinMetadata;
    
    private static final boolean kotlinReflectPresent;
    
    static {
        Class<?> metadata;
        try {
            metadata = Class.forName("kotlin.Metadata");
        } catch (ClassNotFoundException ex) {
            // Kotlin API 不可用 - 没有 Kotlin 支持
            metadata = null;
        }
        kotlinMetadata = (Class<? extends Annotation>) metadata;
        
        
        boolean _kotlinReflectPresent;
        try {
            Class.forName("kotlin.reflect.full.KClasses");
            _kotlinReflectPresent = true;
        } catch (Exception e) {
            _kotlinReflectPresent = false;
        }
        kotlinReflectPresent = _kotlinReflectPresent;
    }
    
    
    /**
     * 确定 Kotlin 是否存在
     */
    public static boolean isKotlinPresent() {
        return (kotlinMetadata != null);
    }
    
    /**
     * 确定是否存在 Kotlin 反射
     */
    public static boolean isKotlinReflectPresent() {
        return kotlinReflectPresent;
    }
    
    /**
     * 确定给定的 {@code 类} 是否为 Kotlin 类型（上面存在 Kotlin 元数据）
     */
    public static boolean isKotlinType(Class<?> clazz) {
        return kotlinMetadata != null && clazz.getDeclaredAnnotation(kotlinMetadata) != null;
    }
    
    /**
     * 如果该方法是挂起函数，则返回 {@code true}
     */
    public static boolean isSuspendingFunction(@NotNull Method method) {
        if (KotlinDetector.isKotlinType(method.getDeclaringClass())) {
            Class<?>[] types = method.getParameterTypes();
            return types.length > 0 && "kotlin.coroutines.Continuation".equals(types[types.length - 1].getName());
        }
        return false;
    }
    
}
