package cc.shacocloud.mirage.utils.annotation;

import java.lang.annotation.*;

/**
 * {@link AliasFor}是用于声明批注属性别名的批注。
 * <p>
 * 使用场景<br/>
 * 注释中的显式别名：在单个注释中，{@code @AliasFor} 可以在一对属性上声明，以表明它们是彼此可互换的别名。<br/>
 * 元注释中属性的显式别名：如果 的属性设置为与声明它的属性 {@code @AliasFor} 不同的注释，则 annotation 被attribute解释为元注释中属性的别名（即显式元注释属性覆盖）。
 * 这样可以对在注释层次结构中覆盖的确切属性进行精细控制。事实上，{@code @AliasFor} 甚至可以为元注释的属性声明value别名。<br/>
 * 注释中的隐式别名：如果注释中的一个或多个属性被声明为同一元注释属性的属性覆盖（直接或传递），则这些属性将被视为彼此的一组隐式别名，从而导致类似于注释中显式别名的行为。
 * <p>
 * 使用要求 <br/>
 * 注释中的显式别名：<br/>
 * <ul>
 *     <li>组成别名对的每个属性都应使用 {@code @AliasFor} 注释，并且 {@code value}必须引用另一个属性</li>
 *     <li>别名属性必须声明相同的返回类型</li>
 *     <li>别名属性必须声明默认值</li>
 *     <li>{@link #annotation}不应声明</li>
 * </ul>
 * <p>
 * 示例：
 * <pre class="code">
 * &#064;Target(ElementType.METHOD)
 * &#064;Retention(RetentionPolicy.RUNTIME)
 * &#064;Documented
 * &#064;RequestMapping(method = HttpMethod.GET)
 * public &#064;interface GetMapping {
 *
 *     &#064;AliasFor(annotation = RequestMapping.class)
 *     PathPatterns patterns() default PathPatterns.PATH;
 *
 *     &#064;AliasFor(annotation = RequestMapping.class)
 *     String[] value() default {};
 *
 *     &#064;AliasFor(annotation = RequestMapping.class)
 *     String[] path() default {};
 *
 *     &#064;AliasFor(annotation = RequestMapping.class)
 *     String[] consumes() default {};
 *
 *     &#064;AliasFor(annotation = RequestMapping.class)
 *     String[] produces() default {};
 * }
 * </pre>
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
@Documented
public @interface AliasFor {
    
    /**
     * 映射指定注解的别名，如果为空则为当前名称
     */
    String value() default "";
    
    /**
     * 映射注解
     */
    Class<? extends Annotation> annotation() default Annotation.class;
    
}
