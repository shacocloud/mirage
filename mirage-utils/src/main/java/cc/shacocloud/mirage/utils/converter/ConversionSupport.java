package cc.shacocloud.mirage.utils.converter;

import cc.shacocloud.mirage.utils.comparator.Ordered;
import org.jetbrains.annotations.NotNull;

/**
 * {@link Conversion} 的拓展，在执行前判断是否支持该类型的转换
 * <p>
 * 基于 {@link #getOrder} 进行排序判断
 *
 * @author 思追(shaco)
 */
public interface ConversionSupport extends Conversion, Ordered {
    
    /**
     * 判断是否可以将源对象类型转换为 {@code TypeDescriptor} 描述的目标类型
     *
     * @param sourceType 要从中转换的字段的类型描述符
     * @param targetType 要转换为的字段的类型描述符
     * @return the converted object
     */
    boolean support(@NotNull TypeDescriptor sourceType, @NotNull TypeDescriptor targetType);
    
    @Override
    default int getOrder() {
        return 0;
    }
}
