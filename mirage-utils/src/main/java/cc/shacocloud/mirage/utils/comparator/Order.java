package cc.shacocloud.mirage.utils.comparator;

import java.lang.annotation.*;

/**
 * {@code @Order} 定义带批注组件的排序顺序
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE, ElementType.METHOD, ElementType.FIELD})
@Documented
public @interface Order {
    
    /**
     * 值
     * <p>
     * 默认值为 {@link Ordered#LOWEST_PRECEDENCE}.
     *
     * @see Ordered#getOrder()
     */
    int value() default Ordered.LOWEST_PRECEDENCE;
    
}
