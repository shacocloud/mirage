package cc.shacocloud.mirage.utils.date.format;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.text.DateFormatSymbols;
import java.text.ParseException;
import java.text.ParsePosition;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * {@link java.text.SimpleDateFormat} 的线程安全版本，用于解析日期字符串并转换为 {@link Date} 对象
 *
 * @author 思追(shaco)
 * @see FastDatePrinter
 */
public class FastDateParser extends AbstractDateBasic implements DateParser {
    
    static final Locale JAPANESE_IMPERIAL = new Locale("ja", "JP", "JP");
    
    /**
     * 世纪：2000年前为19， 之后为20
     */
    private final int century;
    private final int startYear;
    
    private transient List<StrategyAndWidth> patterns;
    
    // 用于对正则表达式替代方案进行排序的比较器
    // 按区域设置，所有条目必须为小写
    private static final Comparator<String> LONGER_FIRST_LOWERCASE = Comparator.reverseOrder();
    
    /**
     * 使用 {@link FastDateFormat#getInstance(String, TimeZone, Locale)} 或 {@link FastDateFormat} 工厂方法的其他变体来获取缓存的 FastDateParser 实例。
     */
    public FastDateParser(@NotNull String pattern, @NotNull TimeZone timeZone, @NotNull Locale locale) {
        this(pattern, timeZone, locale, null);
    }
    
    /**
     * @param centuryStart 2位数年份解析的世纪之初
     */
    public FastDateParser(final String pattern, final TimeZone timeZone, final Locale locale, final Date centuryStart) {
        super(pattern, timeZone, locale);
        final Calendar definingCalendar = Calendar.getInstance(timeZone, locale);
        
        int centuryStartYear;
        if (centuryStart != null) {
            definingCalendar.setTime(centuryStart);
            centuryStartYear = definingCalendar.get(Calendar.YEAR);
        } else if (locale.equals(JAPANESE_IMPERIAL)) {
            centuryStartYear = 0;
        } else {
            // 从80年前到20年后
            definingCalendar.setTime(new Date());
            centuryStartYear = definingCalendar.get(Calendar.YEAR) - 80;
        }
        century = centuryStartYear / 100 * 100;
        startYear = centuryStartYear - century;
        
        init(definingCalendar);
    }
    
    /**
     * 通过定义字段初始化派生字段。这是从构造函数和读取对象调用的（反序列化）
     *
     * @param definingCalendar 用于初始化此 FastDateParser 的 {@link java.util.Calendar} 实例
     */
    private void init(final Calendar definingCalendar) {
        patterns = new ArrayList<>();
        
        final StrategyParser fm = new StrategyParser(definingCalendar);
        for (; ; ) {
            final StrategyAndWidth field = fm.getNextStrategy();
            if (field == null) {
                break;
            }
            patterns.add(field);
        }
    }
    
    // 用于分析格式字符串的帮助程序类
    
    /**
     * 保持策略和字段宽度
     */
    private static class StrategyAndWidth {
        final Strategy strategy;
        final int width;
        
        StrategyAndWidth(final Strategy strategy, final int width) {
            this.strategy = strategy;
            this.width = width;
        }
        
        int getMaxWidth(final ListIterator<StrategyAndWidth> lt) {
            if (!strategy.isNumber() || !lt.hasNext()) {
                return 0;
            }
            final Strategy nextStrategy = lt.next().strategy;
            lt.previous();
            return nextStrategy.isNumber() ? width : 0;
        }
    }
    
    /**
     * 将格式解析为策略
     */
    private class StrategyParser {
        final private Calendar definingCalendar;
        private int currentIdx;
        
        StrategyParser(final Calendar definingCalendar) {
            this.definingCalendar = definingCalendar;
        }
        
        @Nullable StrategyAndWidth getNextStrategy() {
            if (currentIdx >= pattern.length()) {
                return null;
            }
            
            final char c = pattern.charAt(currentIdx);
            if (isFormatLetter(c)) {
                return letterPattern(c);
            }
            return literal();
        }
        
        @Contract("_ -> new")
        private @NotNull StrategyAndWidth letterPattern(final char c) {
            final int begin = currentIdx;
            while (++currentIdx < pattern.length()) {
                if (pattern.charAt(currentIdx) != c) {
                    break;
                }
            }
            
            final int width = currentIdx - begin;
            return new StrategyAndWidth(getStrategy(c, width, definingCalendar), width);
        }
        
        @Contract(" -> new")
        private @NotNull StrategyAndWidth literal() {
            boolean activeQuote = false;
            
            final StringBuilder sb = new StringBuilder();
            while (currentIdx < pattern.length()) {
                final char c = pattern.charAt(currentIdx);
                if (!activeQuote && isFormatLetter(c)) {
                    break;
                } else if (c == '\'' && (++currentIdx == pattern.length() || pattern.charAt(currentIdx) != '\'')) {
                    activeQuote = !activeQuote;
                    continue;
                }
                ++currentIdx;
                sb.append(c);
            }
            
            if (activeQuote) {
                throw new IllegalArgumentException("未结束的字符！");
            }
            
            final String formatField = sb.toString();
            return new StrategyAndWidth(new CopyQuotedStrategy(formatField), formatField.length());
        }
    }
    
    private static boolean isFormatLetter(final char c) {
        return c >= 'A' && c <= 'Z' || c >= 'a' && c <= 'z';
    }
    
    /**
     * 序列化后创建对象。此实现重新初始化瞬态属性
     *
     * @param in 从中反序列化对象的对象输入流
     * @throws IOException            如果存在 IO 问题
     * @throws ClassNotFoundException 如果找不到类
     */
    private void readObject(final @NotNull ObjectInputStream in) throws IOException, ClassNotFoundException {
        in.defaultReadObject();
        
        final Calendar definingCalendar = Calendar.getInstance(timeZone, locale);
        init(definingCalendar);
    }
    
    @Override
    public Object parseObject(final String source) throws ParseException {
        return parse(source);
    }
    
    @Override
    public Date parse(final String source) throws ParseException {
        final ParsePosition pp = new ParsePosition(0);
        final Date date = parse(source, pp);
        if (date == null) {
            // 添加有关支持的日期范围的注释
            if (locale.equals(JAPANESE_IMPERIAL)) {
                throw new ParseException(locale + " 区域设置不支持公元 1868 年之前的日期，不可解析的日期： " + source, pp.getErrorIndex());
            }
            throw new ParseException("不可解析的日期：" + source, pp.getErrorIndex());
        }
        return date;
    }
    
    @Override
    public Object parseObject(final String source, final ParsePosition pos) {
        return parse(source, pos);
    }
    
    @Override
    public Date parse(final String source, final ParsePosition pos) {
        final Calendar cal = Calendar.getInstance(timeZone, locale);
        cal.clear();
        
        return parse(source, pos, cal) ? cal.getTime() : null;
    }
    
    @Override
    public boolean parse(final String source, final ParsePosition pos, final Calendar calendar) {
        final ListIterator<StrategyAndWidth> lt = patterns.listIterator();
        while (lt.hasNext()) {
            final StrategyAndWidth strategyAndWidth = lt.next();
            final int maxWidth = strategyAndWidth.getMaxWidth(lt);
            if (!strategyAndWidth.strategy.parse(this, calendar, source, pos, maxWidth)) {
                return false;
            }
        }
        return true;
    }
    
    @Contract("_, _ -> param1")
    private static StringBuilder simpleQuote(final StringBuilder sb, final @NotNull String value) {
        for (int i = 0; i < value.length(); ++i) {
            final char c = value.charAt(i);
            switch (c) {
                case '\\':
                case '^':
                case '$':
                case '.':
                case '|':
                case '?':
                case '*':
                case '+':
                case '(':
                case ')':
                case '[':
                case '{':
                    sb.append('\\');
                default:
                    sb.append(c);
            }
        }
        return sb;
    }
    
    /**
     * 获取为字段显示的短整型和长型值
     *
     * @param cal    用于获取短值和多头值的日历
     * @param locale 显示名称的区域设置
     * @param field  返回显示名称的日历字段
     * @param regex  正则表达式
     * @return The map of string display names to field values
     */
    private static @NotNull Map<String, Integer> appendDisplayNames(final @NotNull Calendar cal, final Locale locale, final int field, final StringBuilder regex) {
        final Map<String, Integer> values = new HashMap<>();
        
        final Map<String, Integer> displayNames = cal.getDisplayNames(field, Calendar.ALL_STYLES, locale);
        final TreeSet<String> sorted = new TreeSet<>(LONGER_FIRST_LOWERCASE);
        for (final Map.Entry<String, Integer> displayName : displayNames.entrySet()) {
            final String key = displayName.getKey().toLowerCase(locale);
            if (sorted.add(key)) {
                values.put(key, displayName.getValue());
            }
        }
        for (final String symbol : sorted) {
            simpleQuote(regex, symbol).append('|');
        }
        return values;
    }
    
    /**
     * 使用当前的世纪调整两位数年份为四位数年份
     *
     * @param twoDigitYear 两位数年份
     * @return 介于 CenturyStart (包括)到 CenturyStart +100 (不包括)之间的值
     */
    private int adjustYear(final int twoDigitYear) {
        final int trial = century + twoDigitYear;
        return twoDigitYear >= startYear ? trial : trial + 100;
    }
    
    /**
     * 单个日期字段的分析策略
     */
    private static abstract class Strategy {
        /**
         * 默认实现返回FALSE
         *
         * @return 如果字段为数字返回 true
         */
        boolean isNumber() {
            return false;
        }
        
        abstract boolean parse(FastDateParser parser, Calendar calendar, String source, ParsePosition pos, int maxWidth);
    }
    
    /**
     * 从解析模式中解析单个字段的策略
     */
    private static abstract class PatternStrategy extends Strategy {
        
        private Pattern pattern;
        
        void createPattern(final @NotNull StringBuilder regex) {
            createPattern(regex.toString());
        }
        
        void createPattern(final String regex) {
            this.pattern = Pattern.compile(regex);
        }
        
        @Override
        boolean parse(final FastDateParser parser, final Calendar calendar, final @NotNull String source, final @NotNull ParsePosition pos, final int maxWidth) {
            final Matcher matcher = pattern.matcher(source.substring(pos.getIndex()));
            if (!matcher.lookingAt()) {
                pos.setErrorIndex(pos.getIndex());
                return false;
            }
            pos.setIndex(pos.getIndex() + matcher.end(1));
            setCalendar(parser, calendar, matcher.group(1));
            return true;
        }
        
        abstract void setCalendar(FastDateParser parser, Calendar cal, String value);
    }
    
    /**
     * 从SimpleDateFormat模式中获取给定字段的策略
     *
     * @param f                格式
     * @param width            长度
     * @param definingCalendar 用于获取短值和长值的日历
     * @return 将处理对字段的分析的策略
     */
    private Strategy getStrategy(final char f, final int width, final Calendar definingCalendar) {
        switch (f) {
            case 'D':
                return DAY_OF_YEAR_STRATEGY;
            case 'E':
                return getLocaleSpecificStrategy(Calendar.DAY_OF_WEEK, definingCalendar);
            case 'F':
                return DAY_OF_WEEK_IN_MONTH_STRATEGY;
            case 'G':
                return getLocaleSpecificStrategy(Calendar.ERA, definingCalendar);
            case 'H': // Hour in day (0-23)
                return HOUR_OF_DAY_STRATEGY;
            case 'K': // Hour in am/pm (0-11)
                return HOUR_STRATEGY;
            case 'M':
                return width >= 3 ? getLocaleSpecificStrategy(Calendar.MONTH, definingCalendar) : NUMBER_MONTH_STRATEGY;
            case 'S':
                return MILLISECOND_STRATEGY;
            case 'W':
                return WEEK_OF_MONTH_STRATEGY;
            case 'a':
                return getLocaleSpecificStrategy(Calendar.AM_PM, definingCalendar);
            case 'd':
                return DAY_OF_MONTH_STRATEGY;
            case 'h': // Hour in am/pm (1-12), i.e. midday/midnight is 12, not 0
                return HOUR12_STRATEGY;
            case 'k': // Hour in day (1-24), i.e. midnight is 24, not 0
                return HOUR24_OF_DAY_STRATEGY;
            case 'm':
                return MINUTE_STRATEGY;
            case 's':
                return SECOND_STRATEGY;
            case 'u':
                return DAY_OF_WEEK_STRATEGY;
            case 'w':
                return WEEK_OF_YEAR_STRATEGY;
            case 'y':
            case 'Y':
                return width > 2 ? LITERAL_YEAR_STRATEGY : ABBREVIATED_YEAR_STRATEGY;
            case 'X':
                return ISO8601TimeZoneStrategy.getStrategy(width);
            case 'Z':
                if (width == 2) {
                    return ISO8601TimeZoneStrategy.ISO_8601_3_STRATEGY;
                }
                //$FALL-THROUGH$
            case 'z':
                return getLocaleSpecificStrategy(Calendar.ZONE_OFFSET, definingCalendar);
            default:
                throw new IllegalArgumentException("不支持格式: " + f);
        }
    }
    
    @SuppressWarnings("unchecked")
    private static final ConcurrentMap<Locale, Strategy>[] CACHES = new ConcurrentMap[Calendar.FIELD_COUNT];
    
    /**
     * 获取特定领域的策略缓存
     *
     * @param field 日历
     * @return 策略的区域设置缓存
     */
    private static ConcurrentMap<Locale, Strategy> getCache(final int field) {
        synchronized (CACHES) {
            if (CACHES[field] == null) {
                CACHES[field] = new ConcurrentHashMap<>(3);
            }
            return CACHES[field];
        }
    }
    
    /**
     * 构建解析文本字段的策略
     *
     * @param field            日历
     * @param definingCalendar 用于获取短值和长值的日历
     * @return 针对现场和区域设置的文本策略
     */
    private @NotNull Strategy getLocaleSpecificStrategy(final int field, final Calendar definingCalendar) {
        final ConcurrentMap<Locale, Strategy> cache = getCache(field);
        Strategy strategy = cache.get(locale);
        if (strategy == null) {
            strategy = field == Calendar.ZONE_OFFSET ? new TimeZoneStrategy(locale) : new CaseInsensitiveTextStrategy(field, definingCalendar, locale);
            final Strategy inCache = cache.putIfAbsent(locale, strategy);
            if (inCache != null) {
                return inCache;
            }
        }
        return strategy;
    }
    
    /**
     * 复制解析模式中的静态或引号字段的策略
     */
    private static class CopyQuotedStrategy extends Strategy {
        
        final private String formatField;
        
        /**
         * 构建确保格式字段具有文字文本的策略
         *
         * @param formatField 要匹配的文字文本
         */
        CopyQuotedStrategy(final String formatField) {
            this.formatField = formatField;
        }
        
        @Override
        boolean parse(final FastDateParser parser, final Calendar calendar, final String source, final ParsePosition pos, final int maxWidth) {
            for (int idx = 0; idx < formatField.length(); ++idx) {
                final int sIdx = idx + pos.getIndex();
                if (sIdx == source.length()) {
                    pos.setErrorIndex(sIdx);
                    return false;
                }
                if (formatField.charAt(idx) != source.charAt(sIdx)) {
                    pos.setErrorIndex(sIdx);
                    return false;
                }
            }
            pos.setIndex(formatField.length() + pos.getIndex());
            return true;
        }
    }
    
    /**
     * 在解析模式中处理文本字段的策略
     */
    private static class CaseInsensitiveTextStrategy extends PatternStrategy {
        private final int field;
        final Locale locale;
        private final Map<String, Integer> lKeyValues;
        
        /**
         * 构建解析文本字段的策略
         */
        CaseInsensitiveTextStrategy(final int field, final Calendar definingCalendar, final Locale locale) {
            this.field = field;
            this.locale = locale;
            
            final StringBuilder regex = new StringBuilder();
            regex.append("((?iu)");
            lKeyValues = appendDisplayNames(definingCalendar, locale, field, regex);
            regex.setLength(regex.length() - 1);
            regex.append(")");
            createPattern(regex);
        }
        
        @Override
        void setCalendar(final FastDateParser parser, final @NotNull Calendar cal, final @NotNull String value) {
            final Integer iVal = lKeyValues.get(value.toLowerCase(locale));
            cal.set(field, iVal);
        }
    }
    
    /**
     * 在解析模式中处理数字字段的策略
     */
    private static class NumberStrategy extends Strategy {
        private final int field;
        
        /**
         * 构建解析数字字段的策略
         */
        NumberStrategy(final int field) {
            this.field = field;
        }
        
        /**
         * {@inheritDoc}
         */
        @Override
        boolean isNumber() {
            return true;
        }
        
        @Override
        boolean parse(final FastDateParser parser, final Calendar calendar, final String source, final ParsePosition pos, final int maxWidth) {
            int idx = pos.getIndex();
            int last = source.length();
            
            if (maxWidth == 0) {
                // 如果没有最大宽度，则去除前导空格
                for (; idx < last; ++idx) {
                    final char c = source.charAt(idx);
                    if (!Character.isWhitespace(c)) {
                        break;
                    }
                }
                pos.setIndex(idx);
            } else {
                final int end = idx + maxWidth;
                if (last > end) {
                    last = end;
                }
            }
            
            for (; idx < last; ++idx) {
                final char c = source.charAt(idx);
                if (!Character.isDigit(c)) {
                    break;
                }
            }
            
            if (pos.getIndex() == idx) {
                pos.setErrorIndex(idx);
                return false;
            }
            
            final int value = Integer.parseInt(source.substring(pos.getIndex(), idx));
            pos.setIndex(idx);
            
            calendar.set(field, modify(parser, value));
            return true;
        }
        
        /**
         * 对解析的整数进行任何修改
         *
         * @param parser 解析器
         * @param iValue 解析的整数
         * @return 修改后的值
         */
        int modify(final FastDateParser parser, final int iValue) {
            return iValue;
        }
        
    }
    
    private static final Strategy ABBREVIATED_YEAR_STRATEGY = new NumberStrategy(Calendar.YEAR) {
        /**
         * {@inheritDoc}
         */
        @Override
        int modify(final FastDateParser parser, final int iValue) {
            return iValue < 100 ? parser.adjustYear(iValue) : iValue;
        }
    };
    
    /**
     * 在解析模式中处理时区字段的策略
     */
    static class TimeZoneStrategy extends PatternStrategy {
        private static final String RFC_822_TIME_ZONE = "[+-]\\d{4}";
        private static final String UTC_TIME_ZONE_WITH_OFFSET = "[+-]\\d{2}:\\d{2}";
        private static final String GMT_OPTION = "GMT[+-]\\d{1,2}:\\d{2}";
        
        private final Locale locale;
        private final Map<String, TzInfo> tzNames = new HashMap<>();
        
        private static class TzInfo {
            TimeZone zone;
            int dstOffset;
            
            TzInfo(final TimeZone tz, final boolean useDst) {
                zone = tz;
                dstOffset = useDst ? tz.getDSTSavings() : 0;
            }
        }
        
        /**
         * 区域 ID 的索引
         */
        private static final int ID = 0;
        
        /**
         * 构建解析时区的策略
         */
        TimeZoneStrategy(final Locale locale) {
            this.locale = locale;
            
            final StringBuilder sb = new StringBuilder();
            sb.append("((?iu)" + RFC_822_TIME_ZONE + "|" + UTC_TIME_ZONE_WITH_OFFSET + "|" + GMT_OPTION);
            
            final Set<String> sorted = new TreeSet<>(LONGER_FIRST_LOWERCASE);
            
            final String[][] zones = DateFormatSymbols.getInstance(locale).getZoneStrings();
            for (final String[] zoneNames : zones) {
                // 偏移量 0 是时区 ID，未本地化
                final String tzId = zoneNames[ID];
                if ("GMT".equalsIgnoreCase(tzId)) {
                    continue;
                }
                final TimeZone tz = TimeZone.getTimeZone(tzId);
                // 偏移量 1 是长标准名称
                // 偏移量 2 是短标准名称
                final TzInfo standard = new TzInfo(tz, false);
                TzInfo tzInfo = standard;
                for (int i = 1; i < zoneNames.length; ++i) {
                    switch (i) {
                        case 3: // 偏移量 3 是长夏令时（或夏令时）名称
                            // 偏移量 4 是简短的夏季名称
                            tzInfo = new TzInfo(tz, true);
                            break;
                        case 5: // 偏移量 5 开始其他名称，可能是标准时间
                            tzInfo = standard;
                            break;
                    }
                    if (zoneNames[i] != null) {
                        final String key = zoneNames[i].toLowerCase(locale);
                        // 忽略与附加名称中提供的重复项关联的数据
                        if (sorted.add(key)) {
                            tzNames.put(key, tzInfo);
                        }
                    }
                }
            }
            // 首先订购具有较长字符串的正则表达式替代方案，贪婪匹配将确保使用最长字符串
            for (final String zoneName : sorted) {
                simpleQuote(sb.append('|'), zoneName);
            }
            sb.append(")");
            createPattern(sb);
        }
        
        /**
         * {@inheritDoc}
         */
        @Override
        void setCalendar(final FastDateParser parser, final Calendar cal, final @NotNull String value) {
            if (value.charAt(0) == '+' || value.charAt(0) == '-') {
                final TimeZone tz = TimeZone.getTimeZone("GMT" + value);
                cal.setTimeZone(tz);
            } else if (value.regionMatches(true, 0, "GMT", 0, 3)) {
                final TimeZone tz = TimeZone.getTimeZone(value.toUpperCase());
                cal.setTimeZone(tz);
            } else {
                final TzInfo tzInfo = tzNames.get(value.toLowerCase(locale));
                cal.set(Calendar.DST_OFFSET, tzInfo.dstOffset);
                cal.set(Calendar.ZONE_OFFSET, parser.getTimeZone().getRawOffset());
            }
        }
    }
    
    private static class ISO8601TimeZoneStrategy extends PatternStrategy {
        
        /**
         * 构建解析时区的策略
         */
        ISO8601TimeZoneStrategy(final String pattern) {
            createPattern(pattern);
        }
        
        /**
         * {@inheritDoc}
         */
        @Override
        void setCalendar(final FastDateParser parser, final Calendar cal, final String value) {
            if (Objects.equals(value, "Z")) {
                cal.setTimeZone(TimeZone.getTimeZone("UTC"));
            } else {
                cal.setTimeZone(TimeZone.getTimeZone("GMT" + value));
            }
        }
        
        private static final Strategy ISO_8601_1_STRATEGY = new ISO8601TimeZoneStrategy("(Z|(?:[+-]\\d{2}))");
        private static final Strategy ISO_8601_2_STRATEGY = new ISO8601TimeZoneStrategy("(Z|(?:[+-]\\d{2}\\d{2}))");
        private static final Strategy ISO_8601_3_STRATEGY = new ISO8601TimeZoneStrategy("(Z|(?:[+-]\\d{2}(?::)\\d{2}))");
        
        /**
         * ISO8601 时区战略的工厂方法。
         *
         * @param tokenLen 指示要格式化的时区字符串长度的标记。
         * @return 一个 ISO8601 时区策略，可以格式化长度为 {@code tokenLen} 的时区字符串。如果不存在这样的策略，则会抛出 IllegalArgumentException。
         */
        static Strategy getStrategy(final int tokenLen) {
            switch (tokenLen) {
                case 1:
                    return ISO_8601_1_STRATEGY;
                case 2:
                    return ISO_8601_2_STRATEGY;
                case 3:
                    return ISO_8601_3_STRATEGY;
                default:
                    throw new IllegalArgumentException("无效的长度标识：" + tokenLen);
            }
        }
    }
    
    private static final Strategy NUMBER_MONTH_STRATEGY = new NumberStrategy(Calendar.MONTH) {
        @Override
        int modify(final FastDateParser parser, final int iValue) {
            return iValue - 1;
        }
    };
    private static final Strategy LITERAL_YEAR_STRATEGY = new NumberStrategy(Calendar.YEAR);
    private static final Strategy WEEK_OF_YEAR_STRATEGY = new NumberStrategy(Calendar.WEEK_OF_YEAR);
    private static final Strategy WEEK_OF_MONTH_STRATEGY = new NumberStrategy(Calendar.WEEK_OF_MONTH);
    private static final Strategy DAY_OF_YEAR_STRATEGY = new NumberStrategy(Calendar.DAY_OF_YEAR);
    private static final Strategy DAY_OF_MONTH_STRATEGY = new NumberStrategy(Calendar.DAY_OF_MONTH);
    private static final Strategy DAY_OF_WEEK_STRATEGY = new NumberStrategy(Calendar.DAY_OF_WEEK) {
        @Override
        int modify(final FastDateParser parser, final int iValue) {
            return iValue != 7 ? iValue + 1 : Calendar.SUNDAY;
        }
    };
    private static final Strategy DAY_OF_WEEK_IN_MONTH_STRATEGY = new NumberStrategy(Calendar.DAY_OF_WEEK_IN_MONTH);
    private static final Strategy HOUR_OF_DAY_STRATEGY = new NumberStrategy(Calendar.HOUR_OF_DAY);
    private static final Strategy HOUR24_OF_DAY_STRATEGY = new NumberStrategy(Calendar.HOUR_OF_DAY) {
        @Override
        int modify(final FastDateParser parser, final int iValue) {
            return iValue == 24 ? 0 : iValue;
        }
    };
    private static final Strategy HOUR12_STRATEGY = new NumberStrategy(Calendar.HOUR) {
        @Override
        int modify(final FastDateParser parser, final int iValue) {
            return iValue == 12 ? 0 : iValue;
        }
    };
    private static final Strategy HOUR_STRATEGY = new NumberStrategy(Calendar.HOUR);
    private static final Strategy MINUTE_STRATEGY = new NumberStrategy(Calendar.MINUTE);
    private static final Strategy SECOND_STRATEGY = new NumberStrategy(Calendar.SECOND);
    private static final Strategy MILLISECOND_STRATEGY = new NumberStrategy(Calendar.MILLISECOND);
}
