package cc.shacocloud.mirage.utils.reflection;

import cc.shacocloud.mirage.utils.KotlinDetector;

/**
 * 默认的参数名称发现器
 */
public class DefaultParameterNameDiscoverer extends PrioritizedParameterNameDiscoverer {
    
    public DefaultParameterNameDiscoverer() {
        if (KotlinDetector.isKotlinReflectPresent() && !NativeDetector.inNativeImage()) {
            addDiscoverer(new KotlinReflectionParameterNameDiscoverer());
        }
        addDiscoverer(new StandardReflectionParameterNameDiscoverer());
    }
    
}
