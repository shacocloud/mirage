package cc.shacocloud.mirage.utils;

import java.util.Comparator;
import java.util.Map;

/**
 * 基于{@code String}的路径匹配策略接口
 *
 * <p>默认的实现是{@link AntPathMatcher}，支持ant风格的模式语法
 *
 * @see AntPathMatcher
 */
public interface PathMatcher {
    
    /**
     * 给定的{@code path}是否表示该接口的实现可以匹配的模式
     * <p>
     * 如果返回值是{@code false}，则不需要使用{@link #match}方法，因为对静态路径字符串进行直接相等比较将导致相同的结果。
     *
     * @param path 要检查的路径
     * @return 如果给定的{@code path}代表一个模式，{@code true}
     */
    boolean isPattern(String path);
    
    /**
     * 根据这个PathMatcher的匹配策略，匹配给定的{@code path}和给定的{@code pattern}。
     *
     * @param pattern 要匹配的模式
     * @param path    测试路径
     * @return {@code true}如果提供的{@code path}匹配，{@code false}如果不匹配
     */
    boolean match(String pattern, String path);
    
    /**
     * 根据这个PathMatcher的匹配策略，将给定的{@code path}与给定的{@code pattern}的对应部分进行匹配。
     * <p>
     * 确定模式是否至少与给定的基本路径匹配，假设完整路径也可以匹配。
     *
     * @param pattern 要匹配的模式
     * @param path    测试路径
     * @return {@code true}如果提供的{@code path}匹配，{@code false}如果不匹配
     */
    boolean matchStart(String pattern, String path);
    
    /**
     * 给定一个模式和一个完整路径，确定模式映射部分
     * <p>
     * 该方法被认为是通过实际模式来找出路径的哪一部分是动态匹配的，也就是说，它从给定的完整路径中剥离静态定义的引导路径，只返回路径的实际模式匹配部分。
     * <p>
     * 例如:对于“myroot/*.html”作为模式，“myroot/myfile.html”作为完整路径，该方法应该返回“myfile.html”。
     * 详细的确定规则指定给这个PathMatcher的匹配策略。
     * 一个简单的实现可以在实际模式的情况下返回给定的完整路径，
     * 而在模式不包含任何动态部分的情况下返回空String(即{@code pattern}参数是一个静态路径，
     * 不符合实际{@link #isPattern})。
     * 复杂的实现将区分给定路径模式的静态部分和动态部分。
     *
     * @param pattern 路径模式
     * @param path    自省的完整路径
     * @return 给定{@code path}的模式映射部分(never {@code null})
     */
    String extractPathWithinPattern(String pattern, String path);
    
    /**
     * 给定模式和完整路径，提取URI模板变量。URI模板变量通过(:xxx)表示。
     *
     * @param pattern 路径模式，可能包含URI模板
     * @param path    要从中提取模板变量的完整路径
     * @return 映射，包含变量名作为键;变量值作为值
     */
    Map<String, String> extractUriTemplateVariables(String pattern, String path);
    
    /**
     * 给定一个完整的路径，返回一个{@link Comparator}，适合按照该路径的显式顺序对模式进行排序
     * <p>
     * 所使用的完整算法取决于底层实现，但通常，返回的{@code Comparator}将
     * {@linkplain java.util.List#sort(java.util.Comparator)}对列表进行排序，以便更具体的模式出现在泛型模式之前。
     *
     * @param path 用于比较的完整路径
     * @return 一种能够按显式顺序对模式进行排序的比较器
     */
    Comparator<String> getPatternComparator(String path);
    
    /**
     * 将两个模式合并为返回的新模式
     * <p>
     * 用于组合这两种模式的完整算法取决于底层实现
     *
     * @param pattern1 第一种模式
     * @param pattern2 第二种模式
     * @return 两种模式的结合
     * @throws IllegalArgumentException 当两种模式不能组合时
     */
    String combine(String pattern1, String pattern2);
    
}
