package cc.shacocloud.mirage.utils.converter;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

/**
 * 定义了类型转换方法的接口
 */
public interface TypeConverter {
    
    /**
     * 将该值转换为所需的类型
     *
     * @param source       要转换的值
     * @param requiredType 必须转换为的类型
     * @return 类型转换的结果
     * @throws TypeMismatchException 如果类型转换失败，则抛出该例外
     */
    default @Nullable <T> T convertIfNecessary(@Nullable Object source,
                                               @NotNull Class<T> requiredType) throws TypeMismatchException {
        return convertIfNecessary(source, requiredType, null);
    }
    
    /**
     * 将该值转换为所需的类型
     *
     * @param source         要转换的值
     * @param typeDescriptor 要使用的类型描述符
     * @return 类型转换的结果
     * @throws TypeMismatchException 如果类型转换失败，则抛出该例外
     */
    default @Nullable <T> T convertIfNecessary(@Nullable Object source,
                                               @NotNull TypeDescriptor typeDescriptor) throws TypeMismatchException {
        return convertIfNecessary(source, null, typeDescriptor);
    }
    
    /**
     * 将该值转换为所需的类型
     * <p>
     * 注意：requiredType 或 typeDescriptor 必须有一个不为空！
     *
     * @param source         要转换的值
     * @param requiredType   必须转换为的类型 （如果不知道，则为{@code null}，例如在集合元素的情况下）
     * @param typeDescriptor 要使用的类型描述符（可能是{@code null}）)
     * @return 类型转换的结果
     * @throws TypeMismatchException 如果类型转换失败，则抛出该例外
     */
    @Nullable <T> T convertIfNecessary(@Nullable Object source,
                                       @Nullable Class<T> requiredType,
                                       @Nullable TypeDescriptor typeDescriptor) throws TypeMismatchException;
    
}
