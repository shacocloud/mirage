package cc.shacocloud.mirage.utils.reflection;

import org.jetbrains.annotations.Nullable;

import java.lang.reflect.Constructor;
import java.lang.reflect.Method;

/**
 * 用于发现方法和构造函数的参数名称的接口
 */
public interface ParameterNameDiscoverer {
    
    /**
     * 返回方法的参数名称，如果无法确定，则返回 {@code null}。
     * <p>
     * 如果参数名称仅适用于给定方法的某些参数，而不适用于其他参数，则数组中的单个条目可能为 {@code null}
     */
    @Nullable
    String[] getParameterNames(Method method);
    
    /**
     * 返回构造函数的参数名称，如果无法确定，则返回 {@code null}
     * <p>
     * 如果参数名称仅适用于给定构造函数的某些参数，而不适用于其他参数，则数组中的单个条目可能为 {@code null}
     */
    @Nullable
    String[] getParameterNames(Constructor<?> ctor);
    
}
