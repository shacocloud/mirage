package cc.shacocloud.mirage.utils.resource;

import java.io.*;
import java.net.URL;
import java.nio.charset.Charset;

/**
 * 从基础资源的实际类型（如文件或类路径资源）抽象的资源描述符的接口
 *
 * @author 思追(shaco)
 */
public interface Resource {
    
    /**
     * 获取资源路径
     */
    String getPath();
    
    /**
     * 确定此资源是否以物理形式实际存在
     */
    boolean exists();
    
    /**
     * 指示是否可以通过 {@link #getStream} 读取此资源的非空内容。
     *
     * @see #getStream()
     * @see #exists()
     */
    default boolean isReadable() {
        return exists();
    }
    
    /**
     * 确定此资源是否表示文件系统中的文件
     *
     * @see #getFile()
     */
    default boolean isFile() {
        return false;
    }
    
    /**
     * 返回此资源的文件句柄
     *
     * @throws java.io.FileNotFoundException 如果资源无法解析为绝对文件路径，即资源在文件系统中不可用
     * @see #getStream()
     */
    File getFile() throws IOException;
    
    /**
     * 获得解析后的{@link URL}
     *
     * @return 解析后的{@link URL}
     * @throws IOException 如果资源无法解析为 URL，即资源不能用作描述符
     */
    URL getURL() throws IOException;
    
    /**
     * 获得 {@link InputStream}
     *
     * @return {@link InputStream}
     */
    InputStream getStream() throws IOException;
    
    /**
     * 确定此资源的内容长度
     */
    long contentLength() throws IOException;
    
    /**
     * 确定此资源的上次修改时间戳
     */
    long lastModified() throws IOException;
    
    /**
     * 获得 Reader
     *
     * @param charset 编码
     * @return {@link BufferedReader}
     */
    default BufferedReader getReader(Charset charset) throws IOException {
        return new BufferedReader(new InputStreamReader(getStream(), charset));
    }
    
    /**
     * 返回此资源的说明，用于在使用该资源时的错误输出
     *
     * @see Object#toString()
     */
    String getDescription();
}
