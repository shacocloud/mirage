package cc.shacocloud.mirage.utils.date.format;

import java.text.ParseException;
import java.text.ParsePosition;
import java.util.Calendar;
import java.util.Date;

/**
 * 日期解析接口，用于解析日期字符串为 {@link Date} 对象
 *
 * @author 思追(shaco)
 */
public interface DateParser extends DateBasic {
    
    /**
     * 将日期字符串解析并转换为  {@link Date} 对象<br>
     * 等价于 {@link java.text.DateFormat#parse(String)}
     *
     * @param source 日期字符串
     * @return {@link Date}
     * @throws ParseException 转换异常，被转换的字符串格式错误。
     */
    Date parse(String source) throws ParseException;
    
    /**
     * 将日期字符串解析并转换为  {@link Date} 对象<br>
     * 等价于 {@link java.text.DateFormat#parse(String, ParsePosition)}
     *
     * @param source 日期字符串
     * @param pos    {@link ParsePosition}
     * @return {@link Date}
     */
    Date parse(String source, ParsePosition pos);
    
    /**
     * 根据给定格式转换日期字符串
     *
     * @param source   被转换的日期字符串
     * @param pos      定义开始转换的位置，转换结束后更新转换到的位置
     * @param calendar 要在其中设置分析字段的日历
     * @return true，如果已被解析（pos parsePosition已更新）;否则为假（并且 pos 错误索引已更新）
     * @throws IllegalArgumentException 当日历设置为不宽松，并且解析的字段超出范围时
     */
    boolean parse(String source, ParsePosition pos, Calendar calendar);
    
    /**
     * 将日期字符串解析并转换为 {@link Date} 对象<br>
     *
     * @param source 被转换的日期字符串
     * @return {@link Date}
     * @throws ParseException 如果无法分析指定字符串的开头
     * @see java.text.DateFormat#parseObject(String)
     */
    Object parseObject(String source) throws ParseException;
    
    /**
     * 根据 {@link ParsePosition} 给定将日期字符串解析并转换为  {@link Date} 对象<br>
     *
     * @param source 被转换的日期字符串
     * @param pos    解析位置
     * @return {@link Date}
     * @see java.text.DateFormat#parseObject(String, ParsePosition)
     */
    Object parseObject(String source, ParsePosition pos);
}
