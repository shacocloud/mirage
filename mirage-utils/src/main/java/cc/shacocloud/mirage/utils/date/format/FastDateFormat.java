package cc.shacocloud.mirage.utils.date.format;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;

import java.text.FieldPosition;
import java.text.Format;
import java.text.ParseException;
import java.text.ParsePosition;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

/**
 * FastDateFormat 是一个线程安全的 {@link java.text.SimpleDateFormat} 实现
 *
 * <p>
 * 通过以下静态方法获得此对象: <br>
 * {@link #getInstance(String, TimeZone, Locale)}<br>
 * {@link #getDateInstance(FastDateFormatStyleEnum, TimeZone, Locale)}<br>
 * {@link #getTimeInstance(FastDateFormatStyleEnum, TimeZone, Locale)}<br>
 * {@link #getDateTimeInstance(FastDateFormatStyleEnum, FastDateFormatStyleEnum, TimeZone, Locale)}
 * </p>
 *
 * @author 思追(shaco)
 */
public class FastDateFormat extends Format implements DateParser, DatePrinter {
    
    private static final FormatCache<FastDateFormat> CACHE = new FormatCache<>() {
        
        @Contract("_, _, _ -> new")
        @Override
        protected @NotNull FastDateFormat createInstance(final String pattern, final TimeZone timeZone, final Locale locale) {
            return new FastDateFormat(pattern, timeZone, locale);
        }
    };
    
    private final FastDatePrinter printer;
    private final FastDateParser parser;
    
    // -----------------------------------------------------------------------
    
    /**
     * 构造
     *
     * @param pattern  使用{@link java.text.SimpleDateFormat} 相同的日期格式
     * @param timeZone 非空时区 {@link TimeZone}
     * @param locale   {@link Locale} 日期地理位置
     * @throws NullPointerException if pattern, timeZone, or locale is null.
     */
    protected FastDateFormat(final String pattern, final TimeZone timeZone, final Locale locale) {
        this(pattern, timeZone, locale, null);
    }
    
    /**
     * 构造
     *
     * @param pattern      使用{@link java.text.SimpleDateFormat} 相同的日期格式
     * @param timeZone     非空时区 {@link TimeZone}
     * @param locale       {@link Locale} 日期地理位置
     * @param centuryStart 100年周期的开始用作 2 位数字年份解析的“默认世纪”。如果 centuryStart 为空，则默认为现在80年前
     * @throws NullPointerException 如果模式、时区或区域设置为空
     */
    protected FastDateFormat(final String pattern, final TimeZone timeZone, final Locale locale, final Date centuryStart) {
        printer = new FastDatePrinter(pattern, timeZone, locale);
        parser = new FastDateParser(pattern, timeZone, locale, centuryStart);
    }
    
    /**
     * 获得 FastDateFormat实例，使用默认格式和地区
     */
    public static FastDateFormat getInstance() {
        return CACHE.getInstance();
    }
    
    /**
     * 获得 FastDateFormat 实例，使用默认地区
     *
     * @param pattern 使用{@link java.text.SimpleDateFormat} 相同的日期格式
     * @return FastDateFormat
     * @throws IllegalArgumentException 日期格式问题
     */
    public static FastDateFormat getInstance(final String pattern) {
        return CACHE.getInstance(pattern, null, null);
    }
    
    /**
     * 获得 FastDateFormat 实例
     *
     * @param pattern  使用{@link java.text.SimpleDateFormat} 相同的日期格式
     * @param timeZone 时区{@link TimeZone}
     * @return FastDateFormat
     * @throws IllegalArgumentException 日期格式问题
     */
    public static FastDateFormat getInstance(final String pattern, final TimeZone timeZone) {
        return CACHE.getInstance(pattern, timeZone, null);
    }
    
    // -----------------------------------------------------------------------
    
    /**
     * 获得 FastDateFormat 实例
     *
     * @param pattern 使用{@link java.text.SimpleDateFormat} 相同的日期格式
     * @param locale  {@link Locale} 日期地理位置
     * @return FastDateFormat
     * @throws IllegalArgumentException 日期格式问题
     */
    public static FastDateFormat getInstance(final String pattern, final Locale locale) {
        return CACHE.getInstance(pattern, null, locale);
    }
    
    /**
     * 获得 FastDateFormat 实例
     *
     * @param pattern  使用{@link java.text.SimpleDateFormat} 相同的日期格式
     * @param timeZone 时区{@link TimeZone}
     * @param locale   {@link Locale} 日期地理位置
     * @return FastDateFormat
     * @throws IllegalArgumentException 日期格式问题
     */
    public static FastDateFormat getInstance(final String pattern, final TimeZone timeZone, final Locale locale) {
        return CACHE.getInstance(pattern, timeZone, locale);
    }
    
    /**
     * 获得 FastDateFormat 实例
     *
     * @param dateStyle {@link FastDateFormatStyleEnum}
     * @return 本地化 FastDateFormat
     */
    public static FastDateFormat getDateInstance(FastDateFormatStyleEnum dateStyle) {
        return CACHE.getDateInstance(dateStyle, null, null);
    }
    
    /**
     * 获得 FastDateFormat 实例<br>
     * 支持缓存
     *
     * @param dateStyle {@link FastDateFormatStyleEnum}
     * @param locale    {@link Locale} 日期地理位置
     * @return 本地化 FastDateFormat
     */
    public static FastDateFormat getDateInstance(@NotNull FastDateFormatStyleEnum dateStyle, final Locale locale) {
        return CACHE.getDateInstance(dateStyle, null, locale);
    }
    
    // -----------------------------------------------------------------------
    
    /**
     * 获得 FastDateFormat 实例<br>
     * 支持缓存
     *
     * @param dateStyle {@link FastDateFormatStyleEnum}
     * @param timeZone  时区{@link TimeZone}
     * @return 本地化 FastDateFormat
     */
    public static FastDateFormat getDateInstance(@NotNull FastDateFormatStyleEnum dateStyle, final TimeZone timeZone) {
        return CACHE.getDateInstance(dateStyle, timeZone, null);
    }
    
    /**
     * 获得 FastDateFormat 实例<br>
     * 支持缓存
     *
     * @param dateStyle {@link FastDateFormatStyleEnum}
     * @param timeZone  时区{@link TimeZone}
     * @param locale    {@link Locale} 日期地理位置
     * @return 本地化 FastDateFormat
     */
    public static FastDateFormat getDateInstance(@NotNull FastDateFormatStyleEnum dateStyle, final TimeZone timeZone, final Locale locale) {
        return CACHE.getDateInstance(dateStyle, timeZone, locale);
    }
    
    /**
     * 获得 FastDateFormat 实例
     *
     * @param dateStyle {@link FastDateFormatStyleEnum}
     * @return 本地化 FastDateFormat
     */
    public static FastDateFormat getTimeInstance(@NotNull FastDateFormatStyleEnum dateStyle) {
        return CACHE.getTimeInstance(dateStyle, null, null);
    }
    
    /**
     * 获得 FastDateFormat 实例
     *
     * @param dateStyle {@link FastDateFormatStyleEnum}
     * @param locale    {@link Locale} 日期地理位置
     * @return 本地化 FastDateFormat
     */
    public static FastDateFormat getTimeInstance(@NotNull FastDateFormatStyleEnum dateStyle, final Locale locale) {
        return CACHE.getTimeInstance(dateStyle, null, locale);
    }
    
    // -----------------------------------------------------------------------
    
    /**
     * 获得 FastDateFormat 实例<br>
     * 支持缓存
     *
     * @param dateStyle {@link FastDateFormatStyleEnum}
     * @param timeZone  可选时区，覆盖格式化时间的时区
     * @return 本地化 FastDateFormat
     */
    public static FastDateFormat getTimeInstance(@NotNull FastDateFormatStyleEnum dateStyle, final TimeZone timeZone) {
        return CACHE.getTimeInstance(dateStyle, timeZone, null);
    }
    
    /**
     * 获得 FastDateFormat 实例<br>
     * 支持缓存
     *
     * @param dateStyle {@link FastDateFormatStyleEnum}
     * @param timeZone  可选时区，覆盖格式化时间的时区
     * @param locale    {@link Locale} 日期地理位置
     * @return 本地化 FastDateFormat
     */
    public static FastDateFormat getTimeInstance(@NotNull FastDateFormatStyleEnum dateStyle, final TimeZone timeZone, final Locale locale) {
        return CACHE.getTimeInstance(dateStyle, timeZone, locale);
    }
    
    /**
     * 获得 FastDateFormat 实例<br>
     * 支持缓存
     *
     * @param dateStyle {@link FastDateFormatStyleEnum}
     * @param timeStyle {@link FastDateFormatStyleEnum}
     * @return 本地化 FastDateFormat
     */
    public static FastDateFormat getDateTimeInstance(@NotNull FastDateFormatStyleEnum dateStyle, @NotNull FastDateFormatStyleEnum timeStyle) {
        return CACHE.getDateTimeInstance(dateStyle, timeStyle, null, null);
    }
    
    /**
     * 获得 FastDateFormat 实例<br>
     * 支持缓存
     *
     * @param dateStyle {@link FastDateFormatStyleEnum}
     * @param timeStyle {@link FastDateFormatStyleEnum}
     * @param locale    {@link Locale} 日期地理位置
     * @return 本地化 FastDateFormat
     */
    public static FastDateFormat getDateTimeInstance(@NotNull FastDateFormatStyleEnum dateStyle, @NotNull FastDateFormatStyleEnum timeStyle, final Locale locale) {
        return CACHE.getDateTimeInstance(dateStyle, timeStyle, null, locale);
    }
    
    // ----------------------------------------------------------------------- 构造函数
    
    /**
     * 获得 FastDateFormat 实例<br>
     * 支持缓存
     *
     * @param dateStyle {@link FastDateFormatStyleEnum}
     * @param timeStyle {@link FastDateFormatStyleEnum}
     * @param timeZone  时区{@link TimeZone}
     * @return 本地化 FastDateFormat
     */
    public static FastDateFormat getDateTimeInstance(@NotNull FastDateFormatStyleEnum dateStyle, @NotNull FastDateFormatStyleEnum timeStyle, final TimeZone timeZone) {
        return getDateTimeInstance(dateStyle, timeStyle, timeZone, null);
    }
    
    /**
     * 获得 FastDateFormat 实例<br>
     * 支持缓存
     *
     * @param dateStyle {@link FastDateFormatStyleEnum}
     * @param timeStyle {@link FastDateFormatStyleEnum}
     * @param timeZone  时区{@link TimeZone}
     * @param locale    {@link Locale} 日期地理位置
     * @return 本地化 FastDateFormat
     */
    public static FastDateFormat getDateTimeInstance(@NotNull FastDateFormatStyleEnum dateStyle, @NotNull FastDateFormatStyleEnum timeStyle, final TimeZone timeZone, final Locale locale) {
        return CACHE.getDateTimeInstance(dateStyle, timeStyle, timeZone, locale);
    }
    
    @Override
    public StringBuffer format(final Object obj, final @NotNull StringBuffer toAppendTo, final FieldPosition pos) {
        return toAppendTo.append(printer.format(obj));
    }
    
    @Override
    public String format(final long millis) {
        return printer.format(millis);
    }
    
    @Override
    public String format(final Date date) {
        return printer.format(date);
    }
    
    @Override
    public String format(final Calendar calendar) {
        return printer.format(calendar);
    }
    
    @Override
    public <B extends Appendable> B format(final long millis, final B buf) {
        return printer.format(millis, buf);
    }
    
    @Override
    public <B extends Appendable> B format(final Date date, final B buf) {
        return printer.format(date, buf);
    }
    
    @Override
    public <B extends Appendable> B format(final Calendar calendar, final B buf) {
        return printer.format(calendar, buf);
    }
    
    @Override
    public Date parse(final String source) throws ParseException {
        return parser.parse(source);
    }
    
    @Override
    public Date parse(final String source, final ParsePosition pos) {
        return parser.parse(source, pos);
    }
    
    @Override
    public boolean parse(final String source, final ParsePosition pos, final Calendar calendar) {
        return parser.parse(source, pos, calendar);
    }
    
    @Override
    public Object parseObject(final String source, final ParsePosition pos) {
        return parser.parseObject(source, pos);
    }
    
    @Override
    public String getPattern() {
        return printer.getPattern();
    }
    
    @Override
    public TimeZone getTimeZone() {
        return printer.getTimeZone();
    }
    
    @Override
    public Locale getLocale() {
        return printer.getLocale();
    }
    
    /**
     * 估算生成的日期字符串长度
     * 实际生成的字符串长度小于或等于此值
     *
     * @return 日期字符串长度
     */
    public int getMaxLengthEstimate() {
        return printer.getMaxLengthEstimate();
    }
    
    // Basics
    // -----------------------------------------------------------------------
    @Override
    public boolean equals(final Object obj) {
        if (!(obj instanceof FastDateFormat)) {
            return false;
        }
        final FastDateFormat other = (FastDateFormat) obj;
        return printer.equals(other.printer);
    }
    
    @Override
    public int hashCode() {
        return printer.hashCode();
    }
    
    @Override
    public String toString() {
        return "FastDateFormat[" + printer.getPattern() + "," + printer.getLocale() + "," + printer.getTimeZone().getID() + "]";
    }
}
