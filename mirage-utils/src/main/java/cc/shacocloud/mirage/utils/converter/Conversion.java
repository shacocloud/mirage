package cc.shacocloud.mirage.utils.converter;

import org.jetbrains.annotations.NotNull;

/**
 * 将给定值转换为特定类型
 *
 * @author 思追(shaco)
 */
@FunctionalInterface
public interface Conversion {
    
    /**
     * 将源对象转换为 {@code TypeDescriptor} 描述的目标类型
     *
     * @param source     要转换的源对象（可以是 {@code null}）
     * @param sourceType 要从中转换的字段的类型描述符
     * @param targetType 要转换为的字段的类型描述符
     * @return the converted object
     */
    Object convert(@NotNull Object source, @NotNull TypeDescriptor sourceType, @NotNull TypeDescriptor targetType) throws ConversionException;
    
}