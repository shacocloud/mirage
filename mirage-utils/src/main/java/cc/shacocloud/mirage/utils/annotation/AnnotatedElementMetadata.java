package cc.shacocloud.mirage.utils.annotation;

import java.lang.annotation.Annotation;
import java.lang.reflect.AnnotatedElement;
import java.util.Optional;

/**
 * 注解元素的元数据
 *
 * @author 思追(shaco)
 */
public interface AnnotatedElementMetadata extends AnnotatedElement {
    
    /**
     * 获取指定注解的所有属性
     *
     * @param annotationType 注解类型
     * @param <A>            注解泛型
     * @return {@link AnnotationAttributes}
     */
    <A extends Annotation> AnnotationAttributes getAnnotationAttributes(Class<A> annotationType);
    
    /**
     * 获取注解的指定属性值
     *
     * @param annotationType 注解类型
     * @param attributeName  注解的属性
     * @param type           值类型
     * @param <T>            值类型泛型
     * @param <A>            注解泛型
     * @return
     */
    <T, A extends Annotation> Optional<T> getValue(Class<A> annotationType, String attributeName, Class<T> type);
    
}
