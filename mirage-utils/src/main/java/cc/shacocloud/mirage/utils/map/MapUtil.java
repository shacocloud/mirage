package cc.shacocloud.mirage.utils.map;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;

import java.util.AbstractMap;
import java.util.Map;

/**
 * Map相关工具类
 *
 * @author 思追(shaco)
 */
public class MapUtil {
    
    /**
     * Map是否不为空
     *
     * @param map 集合
     * @return 是否不为空
     */
    public static boolean isNotEmpty(Map<?, ?> map) {
        return !isEmpty(map);
    }
    
    /**
     * Map是否为空
     *
     * @param map 集合
     * @return 是否为空
     */
    public static boolean isEmpty(Map<?, ?> map) {
        return null == map || map.isEmpty();
    }
    
    /**
     * 将键和值转换为{@link AbstractMap.SimpleImmutableEntry}<br>
     * 返回的Entry不可变
     *
     * @param key   键
     * @param value 值
     * @param <K>   键类型
     * @param <V>   值类型
     * @return {@link AbstractMap.SimpleImmutableEntry}
     */
    @Contract("_, _ -> new")
    public static <K, V> Map.@NotNull Entry<K, V> entry(K key, V value) {
        return entry(key, value, true);
    }
    
    /**
     * 将键和值转换为{@link AbstractMap.SimpleEntry} 或者 {@link AbstractMap.SimpleImmutableEntry}
     *
     * @param key         键
     * @param value       值
     * @param <K>         键类型
     * @param <V>         值类型
     * @param isImmutable 是否不可变Entry
     * @return {@link AbstractMap.SimpleEntry} 或者 {@link AbstractMap.SimpleImmutableEntry}
     */
    @Contract("_, _, _ -> new")
    public static <K, V> Map.@NotNull Entry<K, V> entry(K key, V value, boolean isImmutable) {
        return isImmutable ?
                new AbstractMap.SimpleImmutableEntry<>(key, value) :
                new AbstractMap.SimpleEntry<>(key, value);
    }
    
}
