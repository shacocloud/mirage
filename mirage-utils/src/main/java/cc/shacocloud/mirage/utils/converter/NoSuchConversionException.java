package cc.shacocloud.mirage.utils.converter;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

/**
 * 没有这样的转换器异常
 *
 * @author 思追(shaco)
 */
public class NoSuchConversionException extends ConversionException {
    
    public NoSuchConversionException(@NotNull TypeDescriptor sourceType,
                                     @NotNull TypeDescriptor targetType,
                                     @Nullable Object value) {
        super(sourceType, targetType, value,
                String.format("找不到从类型 %s 到类型 %s 的转换器！", sourceType.getType(), targetType.getType()));
    }
    
}
