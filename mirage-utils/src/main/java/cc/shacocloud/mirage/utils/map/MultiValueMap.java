package cc.shacocloud.mirage.utils.map;

import org.jetbrains.annotations.Nullable;

import java.util.List;
import java.util.Map;

/**
 * 存储多个值的 {@code Map} 接口的扩展。
 */
public interface MultiValueMap<K, V> extends Map<K, List<V>> {
    
    /**
     * 返回给定键的第一个值
     *
     * @param key 键
     * @return 指定键的第一个值，如果没有，则为 {@code null}
     */
    @Nullable
    V getFirst(K key);
    
    /**
     * 将给定的单个值添加到给定键的当前值列表中
     *
     * @param key   键
     * @param value 值
     */
    void add(K key, @Nullable V value);
    
    /**
     * 将给定列表的所有值添加到给定键的当前值列表中
     *
     * @param key    键
     * @param values 值
     */
    void addAll(K key, List<? extends V> values);
    
    /**
     * 将给定 {@code MultiValueMap} 的所有值添加到当前值
     */
    void addAll(MultiValueMap<K, V> values);
    
    /**
     * 仅当映射{@link #containsKey}不存在时 使用 {@link #add}添加 给定值，
     */
    default void addIfAbsent(K key, @Nullable V value) {
        if (!containsKey(key)) {
            add(key, value);
        }
    }
    
    /**
     * 在给定键下设置给定的单个值
     */
    void set(K key, @Nullable V value);
    
    /**
     * 在下面设置给定的值
     */
    void setAll(Map<K, V> values);
    
    /**
     * 返回一个 {@code Map}，其中包含此 {@code MultiValueMap} 中包含的第一个值
     *
     * @return 此地图的单值表示形式
     */
    Map<K, V> toSingleValueMap();
    
}
