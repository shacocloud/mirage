package cc.shacocloud.mirage.utils;

import cc.shacocloud.mirage.utils.charSequence.StrUtil;
import cc.shacocloud.mirage.utils.map.ConcurrentReferenceHashMap;
import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.Serializable;
import java.lang.reflect.*;
import java.util.*;

import static cc.shacocloud.mirage.utils.SerializableTypeWrapper.*;

/**
 * 封装了一个Java {@link java.lang.reflect.Type}
 * <p>
 * 提供对 {@link #getSuperType() supertypes}、{@link #getInterfaces() interfaces}和 {@link #getGeneric(int...) generic parameters}的访问，
 * 以及最终 {@link #resolve() resolve}到一个{@link java.lang.Class}。
 * <p>
 * 参考 spring
 */
public class ResolvableType implements Serializable {
    
    /**
     * {@code ResolvableType}在没有值的时候返回。{@code NONE}优先于{@code null}，这样多个方法的调用就可以安全地被链起来。
     */
    public static final ResolvableType NONE = new ResolvableType(EmptyType.INSTANCE, null, null, 0);
    
    private static final ResolvableType[] EMPTY_TYPES_ARRAY = new ResolvableType[0];
    
    private static final ConcurrentReferenceHashMap<ResolvableType, ResolvableType> cache =
            new ConcurrentReferenceHashMap<>(256);
    
    /**
     * 被管理的底层Java类型。
     */
    private final Type type;
    
    /**
     * 该类型的可选提供者。
     */
    @Nullable
    private final TypeProvider typeProvider;
    
    /**
     * 要使用的 {@code VariableResolver}，如果没有可用的解析器，则为 {@code null}
     */
    @Nullable
    private final VariableResolver variableResolver;
    
    /**
     * 数组的组件类型，如果需要推导类型，则为{@code null}
     */
    @Nullable
    private final ResolvableType componentType;
    
    @Nullable
    private final Integer hash;
    
    @Nullable
    private Class<?> resolved;
    
    @Nullable
    private volatile ResolvableType superType;
    
    @Nullable
    private volatile ResolvableType[] interfaces;
    
    @Nullable
    private volatile ResolvableType[] generics;
    
    
    /**
     * 用于创建一个新的{@link ResolvableType}的私有构造函数，用于缓存密钥的目的，没有前期的解析。
     */
    private ResolvableType(Type type,
                           @Nullable TypeProvider typeProvider,
                           @Nullable VariableResolver variableResolver) {
        
        this.type = type;
        this.typeProvider = typeProvider;
        this.variableResolver = variableResolver;
        this.componentType = null;
        this.hash = calculateHashCode();
        this.resolved = null;
    }
    
    /**
     * 用于创建一个新的{@link ResolvableType}的私有构造函数，用于缓存值的目的，具有预先解决和预先计算的哈希。
     */
    private ResolvableType(Type type,
                           @Nullable TypeProvider typeProvider,
                           @Nullable VariableResolver variableResolver,
                           @Nullable Integer hash) {
        
        this.type = type;
        this.typeProvider = typeProvider;
        this.variableResolver = variableResolver;
        this.componentType = null;
        this.hash = hash;
        this.resolved = resolveClass();
    }
    
    /**
     * 私有构造函数用于创建一个新的{@link ResolvableType}，用于非缓存的目的，具有预先解决但懒得计算的哈希。
     */
    private ResolvableType(Type type,
                           @Nullable TypeProvider typeProvider,
                           @Nullable VariableResolver variableResolver,
                           @Nullable ResolvableType componentType) {
        
        this.type = type;
        this.typeProvider = typeProvider;
        this.variableResolver = variableResolver;
        this.componentType = componentType;
        this.hash = null;
        this.resolved = resolveClass();
    }
    
    /**
     * 私有构造函数，用于在{@link ResolvableType}的基础上创建一个新的{@link Class}。
     * <p>
     * 避免了所有的{@code instanceof}检查，以便创建一个直接的{@link Class}包装器。
     */
    private ResolvableType(@Nullable Class<?> clazz) {
        this.resolved = (clazz != null ? clazz : Object.class);
        this.type = this.resolved;
        this.typeProvider = null;
        this.variableResolver = null;
        this.componentType = null;
        this.hash = null;
    }
    
    /**
     * 为指定的{@link Class}返回一个{@link ResolvableType}，使用完整的通用类型信息进行分配性检查。
     *
     * @return {@link ResolvableType}
     * @see #forClass(Class, Class)
     * @see #forClassWithGenerics(Class, Class...)
     */
    @Contract(value = "_ -> new", pure = true)
    public static @NotNull ResolvableType forClass(@Nullable Class<?> clazz) {
        return new ResolvableType(clazz);
    }
    
    /**
     * 为指定的{@link Class}返回一个{@link ResolvableType}，
     * 只对原始类进行可分配性检查（类似于{@link Class#isAssignableFrom}，它作为一个封装器）
     *
     * @return {@link ResolvableType}
     * @see #forClass(Class)
     * @see #getRawClass()
     */
    @Contract(value = "_ -> new", pure = true)
    public static @NotNull ResolvableType forRawClass(@Nullable Class<?> clazz) {
        return new ResolvableType(clazz) {
            @Override
            public ResolvableType[] getGenerics() {
                return EMPTY_TYPES_ARRAY;
            }
            
            @Override
            public boolean isAssignableFrom(Class<?> other) {
                return (clazz == null || ClassUtil.isAssignable(clazz, other));
            }
            
            @Override
            public boolean isAssignableFrom(ResolvableType other) {
                Class<?> otherClass = other.resolve();
                return (otherClass != null && (clazz == null || ClassUtil.isAssignable(clazz, otherClass)));
            }
        };
    }
    
    /**
     * 为指定的基类型（接口或基类）返回一个{@link ResolvableType}，并给定一个实现类。
     * <p>
     * 示例: {@code ResolvableType.forClass(List.class, MyArrayList.class)}.
     *
     * @param baseType            基本类型（不能是{@code null}）。
     * @param implementationClass 实现类
     * @return {@link ResolvableType}
     * @see #forClass(Class)
     * @see #forClassWithGenerics(Class, Class...)
     */
    public static ResolvableType forClass(@NotNull Class<?> baseType, Class<?> implementationClass) {
        ResolvableType asType = forType(implementationClass).as(baseType);
        return (asType == NONE ? forType(baseType) : asType);
    }
    
    /**
     * 为指定的{@link Class}返回一个{@link ResolvableType}，并预先声明了泛型
     *
     * @param clazz    类或接口
     * @param generics 类的泛型
     * @return {@link ResolvableType}
     * @see #forClassWithGenerics(Class, ResolvableType...)
     */
    public static ResolvableType forClassWithGenerics(@NotNull Class<?> clazz, Class<?> @NotNull ... generics) {
        ResolvableType[] resolvableGenerics = new ResolvableType[generics.length];
        for (int i = 0; i < generics.length; i++) {
            resolvableGenerics[i] = forClass(generics[i]);
        }
        return forClassWithGenerics(clazz, resolvableGenerics);
    }
    
    /**
     * 为指定的{@link Class}返回一个{@link ResolvableType}，并预先声明了泛型。
     *
     * @param clazz    类或接口
     * @param generics 类的泛型
     * @return {@link ResolvableType}
     * @see #forClassWithGenerics(Class, Class...)
     */
    public static ResolvableType forClassWithGenerics(@NotNull Class<?> clazz, ResolvableType @NotNull ... generics) {
        TypeVariable<?>[] variables = clazz.getTypeParameters();
        if (variables.length != generics.length) {
            throw new IllegalArgumentException("指定类的泛型数量不匹配:" + clazz.toGenericString());
        }
        
        Type[] arguments = new Type[generics.length];
        for (int i = 0; i < generics.length; i++) {
            ResolvableType generic = generics[i];
            Type argument = (generic != null ? generic.getType() : null);
            arguments[i] = (argument != null && !(argument instanceof TypeVariable) ? argument : variables[i]);
        }
        
        ParameterizedType syntheticType = new SyntheticParameterizedType(clazz, arguments);
        return forType(syntheticType, new TypeVariablesVariableResolver(variables, generics));
    }
    
    /**
     * 为指定的 {@link Field} 返回一个 {@link ResolvableType}
     *
     * @return {@link ResolvableType}
     * @see #forField(Field, Class)
     */
    public static ResolvableType forField(@NotNull Field field) {
        return forType(null, new FieldTypeProvider(field), null);
    }
    
    /**
     * 为指定的{@link Field}返回一个{@link ResolvableType}，并给出实现。
     * <p>
     * 当声明字段的类包括由实现类满足的通用参数变量时，使用这个变体。
     *
     * @param field               字段
     * @param implementationClass 执行类
     * @return {@link ResolvableType}
     * @see #forField(Field)
     */
    public static ResolvableType forField(@NotNull Field field, Class<?> implementationClass) {
        ResolvableType owner = forType(implementationClass).as(field.getDeclaringClass());
        return forType(null, new FieldTypeProvider(field), owner.asVariableResolver());
    }
    
    /**
     * 为指定的{@link Field}返回一个{@link ResolvableType}，并给出实现。
     * <p>
     * 当声明字段的类包括由实现类满足的通用参数变量时，使用这个变体。
     *
     * @param field              字段
     * @param implementationType 执行类
     * @return {@link ResolvableType}
     * @see #forField(Field)
     */
    public static ResolvableType forField(@NotNull Field field, @Nullable ResolvableType implementationType) {
        ResolvableType owner = (implementationType != null ? implementationType : NONE);
        owner = owner.as(field.getDeclaringClass());
        return forType(null, new FieldTypeProvider(field), owner.asVariableResolver());
    }
    
    /**
     * 为指定的{@link Field}返回一个{@link ResolvableType}，并给出嵌套级别
     *
     * @param field        字段
     * @param nestingLevel 嵌套级别（1代表外层；2代表嵌套的*通用类型；等等）
     * @see #forField(Field)
     */
    public static ResolvableType forField(@NotNull Field field, int nestingLevel) {
        return forType(null, new FieldTypeProvider(field), null).getNested(nestingLevel);
    }
    
    /**
     * 为指定的{@link Field}返回一个{@link ResolvableType}，并给出嵌套级别
     * <p>
     * 当声明字段的类包括由实现类满足的通用参数变量时，使用这个变体
     *
     * @param field               字段
     * @param nestingLevel        嵌套级别（1代表外层；2代表嵌套的*通用类型；等等）
     * @param implementationClass 执行类
     * @see #forField(Field)
     */
    public static ResolvableType forField(@NotNull Field field,
                                          int nestingLevel,
                                          @Nullable Class<?> implementationClass) {
        ResolvableType owner = forType(implementationClass).as(field.getDeclaringClass());
        return forType(null, new FieldTypeProvider(field), owner.asVariableResolver()).getNested(nestingLevel);
    }
    
    /**
     * 为指定的{@link Constructor}参数返回一个{@link ResolvableType}
     *
     * @param constructor    构造函数
     * @param parameterIndex 参数索引
     * @return {@link ResolvableType}
     * @see #forConstructorParameter(Constructor, int, Class)
     */
    public static ResolvableType forConstructorParameter(@NotNull Constructor<?> constructor, int parameterIndex) {
        return forMethodParameter(new MethodParameter(constructor, parameterIndex));
    }
    
    /**
     * 为指定的{@link Constructor}参数返回一个{@link ResolvableType}，并给定一个实现
     * <p>
     * 当声明构造函数的类包括通用参数变量时，请使用这个变体，这些变量由实现类满足。
     *
     * @param constructor         构造函数
     * @param parameterIndex      参数索引
     * @param implementationClass 实现类
     * @return {@link ResolvableType}
     * @see #forConstructorParameter(Constructor, int)
     */
    public static ResolvableType forConstructorParameter(@NotNull Constructor<?> constructor,
                                                         int parameterIndex,
                                                         Class<?> implementationClass) {
        MethodParameter methodParameter = new MethodParameter(constructor, parameterIndex, implementationClass);
        return forMethodParameter(methodParameter);
    }
    
    /**
     * 为指定的{@link Method}返回类型返回一个{@link ResolvableType}。
     *
     * @param method 方法返回类型的来源
     * @return {@link ResolvableType}
     * @see #forMethodReturnType(Method, Class)
     */
    public static ResolvableType forMethodReturnType(@NotNull Method method) {
        return forMethodParameter(new MethodParameter(method, -1));
    }
    
    /**
     * 为指定的{@link Method}返回类型，返回一个{@link ResolvableType}
     *
     * @param method              方法返回类型的来源
     * @param implementationClass 实现类
     * @return {@link ResolvableType}
     * @see #forMethodReturnType(Method)
     */
    public static ResolvableType forMethodReturnType(@NotNull Method method, Class<?> implementationClass) {
        MethodParameter methodParameter = new MethodParameter(method, -1, implementationClass);
        return forMethodParameter(methodParameter);
    }
    
    /**
     * 为指定的{@link Method}参数返回一个{@link ResolvableType}
     *
     * @param method         方法返回类型的来源
     * @param parameterIndex 参数索引
     * @return {@link ResolvableType}
     * @see #forMethodParameter(Method, int, Class)
     * @see #forMethodParameter(MethodParameter)
     */
    public static ResolvableType forMethodParameter(@NotNull Method method, int parameterIndex) {
        return forMethodParameter(new MethodParameter(method, parameterIndex));
    }
    
    /**
     * 为指定的{@link Method}参数返回一个{@link ResolvableType}
     *
     * @param method              方法返回类型的来源
     * @param parameterIndex      参数索引
     * @param implementationClass 实现类
     * @return {@link ResolvableType}
     * @see #forMethodParameter(Method, int, Class)
     * @see #forMethodParameter(MethodParameter)
     */
    public static ResolvableType forMethodParameter(@NotNull Method method, int parameterIndex,
                                                    Class<?> implementationClass) {
        MethodParameter methodParameter = new MethodParameter(method, parameterIndex, implementationClass);
        return forMethodParameter(methodParameter);
    }
    
    /**
     * 为指定的{@link MethodParameter}返回一个{@link ResolvableType}
     *
     * @param methodParameter 源方法参数（不能是{@code null}）
     * @return {@link ResolvableType}
     * @see #forMethodParameter(Method, int)
     */
    public static ResolvableType forMethodParameter(MethodParameter methodParameter) {
        return forMethodParameter(methodParameter, (Type) null);
    }
    
    /**
     * 为指定的{@link MethodParameter}返回一个具有*给定实现类型的{@link ResolvableType}。
     *
     * @param methodParameter    源方法参数（不能是{@code null}）
     * @param implementationType 实现类
     * @return {@link ResolvableType}
     * @see #forMethodParameter(MethodParameter)
     */
    public static ResolvableType forMethodParameter(@NotNull MethodParameter methodParameter,
                                                    @Nullable ResolvableType implementationType) {
        
        implementationType = (implementationType != null ? implementationType :
                forType(methodParameter.getContainingClass()));
        ResolvableType owner = implementationType.as(methodParameter.getDeclaringClass());
        return forType(null, new MethodParameterTypeProvider(methodParameter), owner.asVariableResolver()).
                getNested(methodParameter.getNestingLevel(), methodParameter.typeIndexesPerLevel);
    }
    
    /**
     * 为指定的{@link MethodParameter}返回一个{@link ResolvableType}，重写目标类型以解决特定的给定类型。
     *
     * @param methodParameter 源方法参数（不能是{@code null}）
     * @param targetType      要解决的类型（方法参数类型的一部分）
     * @return {@link ResolvableType}
     * @see #forMethodParameter(Method, int)
     */
    public static ResolvableType forMethodParameter(@NotNull MethodParameter methodParameter,
                                                    @Nullable Type targetType) {
        return forMethodParameter(methodParameter, targetType, methodParameter.getNestingLevel());
    }
    
    /**
     * 为指定的{@link MethodParameter}返回一个{@link ResolvableType}，
     * 在一个特定的嵌套级别，覆盖目标类型，以解决特定的给定类型。
     *
     * @param methodParameter 源方法参数（不能是{@code null}）
     * @param targetType      要解决的类型（方法参数类型的一部分）
     * @param nestingLevel    使用的嵌套级别
     * @return {@link ResolvableType}
     * @see #forMethodParameter(Method, int)
     */
    static ResolvableType forMethodParameter(@NotNull MethodParameter methodParameter,
                                             @Nullable Type targetType,
                                             int nestingLevel) {
        ResolvableType owner = forType(methodParameter.getContainingClass()).as(methodParameter.getDeclaringClass());
        return forType(targetType, new MethodParameterTypeProvider(methodParameter), owner.asVariableResolver())
                .getNested(nestingLevel, methodParameter.typeIndexesPerLevel);
    }
    
    /**
     * 返回一个{@link ResolvableType}作为指定的{@code componentType}的数组
     *
     * @param componentType 组件类型
     * @return {@link ResolvableType}
     */
    public static ResolvableType forArrayComponent(@NotNull ResolvableType componentType) {
        Class<?> arrayClass = Array.newInstance(componentType.resolve(), 0).getClass();
        return new ResolvableType(arrayClass, null, null, componentType);
    }
    
    /**
     * 为指定的{@link Type}返回一个{@link ResolvableType}
     * <p>
     * 注意：产生的{@link ResolvableType}实例可能不是{@link Serializable}
     *
     * @param type 源类型（可能是{@code null}）
     * @return {@link ResolvableType}
     * @see #forType(Type, ResolvableType)
     */
    public static ResolvableType forType(@Nullable Type type) {
        return forType(type, null, null);
    }
    
    /**
     * 为指定的{@link Type}返回一个{@link ResolvableType}
     * <p>
     * 注意：产生的{@link ResolvableType}实例可能不是{@link Serializable}
     *
     * @param type  源类型（可能是{@code null}）
     * @param owner 用来解决变量的所有者类型
     * @return {@link ResolvableType}
     * @see #forType(Type)
     */
    public static ResolvableType forType(@Nullable Type type, @Nullable ResolvableType owner) {
        VariableResolver variableResolver = null;
        if (owner != null) {
            variableResolver = owner.asVariableResolver();
        }
        return forType(type, variableResolver);
    }
    
    /**
     * 为指定的{@link ParameterizedTypeReference}返回一个{@link ResolvableType}
     * <p>
     * 注意：产生的{@link ResolvableType}实例可能不是{@link Serializable}
     *
     * @param typeReference {@link ParameterizedTypeReference}
     * @return {@link ResolvableType}
     * @see #forType(Type)
     */
    public static ResolvableType forType(@NotNull ParameterizedTypeReference<?> typeReference) {
        return forType(typeReference.getType(), null, null);
    }
    
    /**
     * 为指定的{@link Type}返回一个由给定的{@link VariableResolver}支持的{@link ResolvableType}
     *
     * @param type             类型或{@code null}
     * @param variableResolver 变量解析器或{@code null}
     * @return {@link ResolvableType}
     */
    static ResolvableType forType(@Nullable Type type, @Nullable VariableResolver variableResolver) {
        return forType(type, null, variableResolver);
    }
    
    /**
     * 为指定的{@link Type}返回一个由给定的{@link VariableResolver}支持的{@link ResolvableType}
     *
     * @param type             类型或{@code null}
     * @param typeProvider     类型解析器或{@code null}
     * @param variableResolver 变量解析器或{@code null}
     * @return {@link ResolvableType}
     */
    static ResolvableType forType(@Nullable Type type,
                                  @Nullable TypeProvider typeProvider,
                                  @Nullable VariableResolver variableResolver) {
        
        if (type == null && typeProvider != null) {
            type = forTypeProvider(typeProvider);
        }
        
        if (type == null) {
            return NONE;
        }
        
        // 对于简单的类引用，立即构建包装器--
        // 不需要昂贵的解析，所以不值得缓存。
        if (type instanceof Class) {
            return new ResolvableType(type, typeProvider, variableResolver, (ResolvableType) null);
        }
        
        // 在访问时清除空条目，因为我们没有一个清理线程或类似的东西
        cache.purgeUnreferencedEntries();
        
        // 检查缓存-- 我们可能有一个ResolvableType曾经被解决过......。
        ResolvableType resultType = new ResolvableType(type, typeProvider, variableResolver);
        ResolvableType cachedType = cache.get(resultType);
        if (cachedType == null) {
            cachedType = new ResolvableType(type, typeProvider, variableResolver, resultType.hash);
            cache.put(cachedType, cachedType);
        }
        resultType.resolved = cachedType.resolved;
        return resultType;
    }
    
    /**
     * 清除内部{@code ResolvableType}/{@code SerializableTypeWrapper}缓存
     */
    public static void clearCache() {
        cache.clear();
        SerializableTypeWrapper.cache.clear();
    }
    
    /**
     * 返回被管理的底层Java {@link Type}。
     */
    public Type getType() {
        return unwrap(this.type);
    }
    
    /**
     * 如果有的话，返回被管理的底层Java {@link Class}；否则{@code null}。
     */
    @Nullable
    public Class<?> getRawClass() {
        if (this.type == this.resolved) {
            return this.resolved;
        }
        Type rawType = this.type;
        if (rawType instanceof ParameterizedType) {
            rawType = ((ParameterizedType) rawType).getRawType();
        }
        return (rawType instanceof Class ? (Class<?>) rawType : null);
    }
    
    /**
     * 返回可解析类型的基本来源。将返回一个{@link Field}、{@link MethodParameter}或{@link Type}，
     * 这取决于{@link ResolvableType} 是如何构造的。
     * 这个方法主要是为了提供对其他JVM语言可能提供的额外类型信息或元数据的访问。
     */
    public Object getSource() {
        Object source = (this.typeProvider != null ? this.typeProvider.getSource() : null);
        return (source != null ? source : this.type);
    }
    
    /**
     * 将此类型作为一个已解析的{@code Class}返回，如果没有特定的类可以被解析，则返回到 {@link java.lang.Object}。
     *
     * @return 解决了的 {@link Class} 或 {@code Object} 的回退问题
     * @see #getRawClass()
     * @see #resolve(Class)
     */
    public Class<?> toClass() {
        return resolve(Object.class);
    }
    
    /**
     * 确定给定的对象是否是这个 {@code ResolvableType} 的一个实例。
     *
     * @param obj 要检查的对象
     * @see #isAssignableFrom(Class)
     */
    public boolean isInstance(@Nullable Object obj) {
        return (obj != null && isAssignableFrom(obj.getClass()));
    }
    
    /**
     * 确定这个 {@code ResolvableType} 是否可以从指定的其他类型中进行分配。
     *
     * @param other 要检查的类型（作为一个{@code Class}）
     * @see #isAssignableFrom(ResolvableType)
     */
    public boolean isAssignableFrom(Class<?> other) {
        return isAssignableFrom(forClass(other), null);
    }
    
    /**
     * 确定这个{@code ResolvableType} 是否可以从指定的其他类型中进行分配
     * <p>
     * 试图遵循与Java编译器相同的规则，
     * 考虑 {@link #resolve() resolved} {@code Class}是否是 {@link Class#isAssignableFrom(Class) assignable from}
     * 给定类型以及所有{@link #getGenerics() generics}是否是可分配的。
     *
     * @param other 要被检查的类型（作为一个{@code ResolvableType}）
     * @return {@code true} 如果指定的其他类型可以被分配给这个 {@code ResolvableType}; {@code false}否则
     */
    public boolean isAssignableFrom(ResolvableType other) {
        return isAssignableFrom(other, null);
    }
    
    private boolean isAssignableFrom(@NotNull ResolvableType other, @Nullable Map<Type, Type> matchedBefore) {
        if (this == NONE || other == NONE) {
            return false;
        }
        
        // 通过委托给组件类型来处理数组
        if (isArray()) {
            return (other.isArray() && getComponentType().isAssignableFrom(other.getComponentType()));
        }
        
        if (matchedBefore != null && matchedBefore.get(this.type) == other.type) {
            return true;
        }
        
        // 处理通配符的界限
        WildcardBounds ourBounds = WildcardBounds.get(this);
        WildcardBounds typeBounds = WildcardBounds.get(other);
        
        // 在形式上，X可以被分配到 <? extends Number>
        if (typeBounds != null) {
            return (ourBounds != null && ourBounds.isSameKind(typeBounds) &&
                    ourBounds.isAssignableFrom(typeBounds.getBounds()));
        }
        
        // 以<? extends Number> 的形式可分配给X...
        if (ourBounds != null) {
            return ourBounds.isAssignableFrom(other);
        }
        
        // 即将进行的主要可分配性检查
        boolean exactMatch = (matchedBefore != null);  // 我们现在正在检查嵌套的通用变量...
        boolean checkGenerics = true;
        Class<?> ourResolved = null;
        if (this.type instanceof TypeVariable) {
            TypeVariable<?> variable = (TypeVariable<?>) this.type;
            // 尝试默认的可变分辨率
            if (this.variableResolver != null) {
                ResolvableType resolved = this.variableResolver.resolveVariable(variable);
                if (resolved != null) {
                    ourResolved = resolved.resolve();
                }
            }
            if (ourResolved == null) {
                // 尝试针对目标类型的可变分辨率
                if (other.variableResolver != null) {
                    ResolvableType resolved = other.variableResolver.resolveVariable(variable);
                    if (resolved != null) {
                        ourResolved = resolved.resolve();
                        checkGenerics = false;
                    }
                }
            }
            if (ourResolved == null) {
                // 未解决的类型变量，可能是嵌套的---绝不坚持精确匹配
                exactMatch = false;
            }
        }
        if (ourResolved == null) {
            ourResolved = resolve(Object.class);
        }
        Class<?> otherResolved = other.toClass();
        
        // 我们需要对泛型进行精确的类型匹配
        // List<CharSequence> 是不能从 List<String>
        if (exactMatch ? !ourResolved.equals(otherResolved) : !ClassUtil.isAssignable(ourResolved, otherResolved)) {
            return false;
        }
        
        if (checkGenerics) {
            ResolvableType[] ourGenerics = getGenerics();
            ResolvableType[] typeGenerics = other.as(ourResolved).getGenerics();
            if (ourGenerics.length != typeGenerics.length) {
                return false;
            }
            if (matchedBefore == null) {
                matchedBefore = new IdentityHashMap<>(1);
            }
            matchedBefore.put(this.type, other.type);
            for (int i = 0; i < ourGenerics.length; i++) {
                if (!ourGenerics[i].isAssignableFrom(typeGenerics[i], matchedBefore)) {
                    return false;
                }
            }
        }
        
        return true;
    }
    
    /**
     * 如果该类型解析为一个代表数组的Class，则返回{@code true}
     *
     * @see #getComponentType()
     */
    public boolean isArray() {
        if (this == NONE) {
            return false;
        }
        return ((this.type instanceof Class && ((Class<?>) this.type).isArray()) ||
                this.type instanceof GenericArrayType || resolveType().isArray());
    }
    
    /**
     * 返回代表数组组件类型的ResolvableType，或者 {@link #NONE}如果该类型不代表数组。
     *
     * @see #isArray()
     */
    public ResolvableType getComponentType() {
        if (this == NONE) {
            return NONE;
        }
        if (this.componentType != null) {
            return this.componentType;
        }
        if (this.type instanceof Class) {
            Class<?> componentType = ((Class<?>) this.type).getComponentType();
            return forType(componentType, this.variableResolver);
        }
        if (this.type instanceof GenericArrayType) {
            return forType(((GenericArrayType) this.type).getGenericComponentType(), this.variableResolver);
        }
        return resolveType().getComponentType();
    }
    
    /**
     * 方便的方法是将此类型作为一个可解析的 {@link Collection}类型返回。
     * <p>
     * 如果这个类型没有实现或扩展 {@link Collection}，则返回{@link #NONE}。
     *
     * @see #as(Class)
     * @see #asMap()
     */
    public ResolvableType asCollection() {
        return as(Collection.class);
    }
    
    /**
     * 方便的方法是将此类型作为一个可解析的{@link Map}类型返回。
     * <p>
     * 如果这个类型没有实现或扩展 {@link Map}，则返回{@link #NONE}。
     *
     * @see #as(Class)
     * @see #asCollection()
     */
    public ResolvableType asMap() {
        return as(Map.class);
    }
    
    
    // --------------------- 静态方法
    
    /**
     * 返回这个类型作为指定类的{@link ResolvableType}。
     * 搜索 {@link #getSuperType() supertype} 和 {@link #getInterfaces() interface} 层次结构以找到匹配，
     * 如果此类型没有实现或扩展指定的类，则返回{@link #NONE}
     *
     * @param type 所需类型(通常是缩小的)
     * @return a {@link ResolvableType}代表这个对象为指定的*类型，或者{@link #NONE}如果不能被解析为该类型的话。
     * @see #asCollection()
     * @see #asMap()
     * @see #getSuperType()
     * @see #getInterfaces()
     */
    public ResolvableType as(Class<?> type) {
        if (this == NONE) {
            return NONE;
        }
        Class<?> resolved = resolve();
        if (resolved == null || resolved == type) {
            return this;
        }
        for (ResolvableType interfaceType : getInterfaces()) {
            ResolvableType interfaceAsType = interfaceType.as(type);
            if (interfaceAsType != NONE) {
                return interfaceAsType;
            }
        }
        return getSuperType().as(type);
    }
    
    /**
     * 返回一个代表该类型的直接超类型的{@link ResolvableType}
     * <p>
     * 如果没有超类型可用，该方法将返回{@link #NONE}。
     * <p>
     * 注意：产生的{@link ResolvableType}实例可能不是{@link Serializable}。
     *
     * @see #getInterfaces()
     */
    public ResolvableType getSuperType() {
        Class<?> resolved = resolve();
        if (resolved == null) {
            return NONE;
        }
        try {
            Type superclass = resolved.getGenericSuperclass();
            if (superclass == null) {
                return NONE;
            }
            ResolvableType superType = this.superType;
            if (superType == null) {
                superType = forType(superclass, this);
                this.superType = superType;
            }
            return superType;
        } catch (TypeNotPresentException ex) {
            // 忽略通用签名中不存在的类型
            return NONE;
        }
    }
    
    /**
     * 返回一个{@link ResolvableType}数组，代表这个类型所实现的直接接口。
     * 如果这个类型没有实现任何接口，则返回一个空数组。
     * <p>
     * 注意：产生的{@link ResolvableType}实例可能不是{@link Serializable}
     *
     * @see #getSuperType()
     */
    public ResolvableType[] getInterfaces() {
        Class<?> resolved = resolve();
        if (resolved == null) {
            return EMPTY_TYPES_ARRAY;
        }
        ResolvableType[] interfaces = this.interfaces;
        if (interfaces == null) {
            Type[] genericIfcs = resolved.getGenericInterfaces();
            interfaces = new ResolvableType[genericIfcs.length];
            for (int i = 0; i < genericIfcs.length; i++) {
                interfaces[i] = forType(genericIfcs[i], this);
            }
            this.interfaces = interfaces;
        }
        return interfaces;
    }
    
    /**
     * 如果该类型包含通用参数，则返回{@code true}
     *
     * @see #getGeneric(int...)
     * @see #getGenerics()
     */
    public boolean hasGenerics() {
        return (getGenerics().length > 0);
    }
    
    /**
     * 如果这个类型只包含不可解决的泛型，也就是说，没有替代它的任何声明的类型变量，则返回{@code true}
     */
    boolean isEntirelyUnresolvable() {
        if (this == NONE) {
            return false;
        }
        ResolvableType[] generics = getGenerics();
        for (ResolvableType generic : generics) {
            if (!generic.isUnresolvableTypeVariable() && !generic.isWildcardWithoutBounds()) {
                return false;
            }
        }
        return true;
    }
    
    /**
     * 确定底层类型是否有任何无法解决的泛型：通过类型本身的无法解决的类型变量
     * 或者通过以原始方式实现泛型接口，即不替换该接口的类型变量。
     * 只有在这两种情况下，结果才是{@code true}
     */
    public boolean hasUnresolvableGenerics() {
        if (this == NONE) {
            return false;
        }
        ResolvableType[] generics = getGenerics();
        for (ResolvableType generic : generics) {
            if (generic.isUnresolvableTypeVariable() || generic.isWildcardWithoutBounds()) {
                return true;
            }
        }
        Class<?> resolved = resolve();
        if (resolved != null) {
            try {
                for (Type genericInterface : resolved.getGenericInterfaces()) {
                    if (genericInterface instanceof Class) {
                        if (forClass((Class<?>) genericInterface).hasGenerics()) {
                            return true;
                        }
                    }
                }
            } catch (TypeNotPresentException ex) {
                // Ignore non-present types in generic signature
            }
            return getSuperType().hasUnresolvableGenerics();
        }
        return false;
    }
    
    /**
     * 确定底层类型是否是一个类型变量，不能通过相关的变量解析器解析。
     */
    private boolean isUnresolvableTypeVariable() {
        if (this.type instanceof TypeVariable) {
            if (this.variableResolver == null) {
                return true;
            }
            TypeVariable<?> variable = (TypeVariable<?>) this.type;
            ResolvableType resolved = this.variableResolver.resolveVariable(variable);
            return resolved == null || resolved.isUnresolvableTypeVariable();
        }
        return false;
    }
    
    /**
     * 确定底层类型是否代表一个没有特定界限的通配符（即等于{@code ? extends Object}）
     */
    private boolean isWildcardWithoutBounds() {
        if (this.type instanceof WildcardType) {
            WildcardType wt = (WildcardType) this.type;
            if (wt.getLowerBounds().length == 0) {
                Type[] upperBounds = wt.getUpperBounds();
                return upperBounds.length == 0 || (upperBounds.length == 1 && Object.class == upperBounds[0]);
            }
        }
        return false;
    }
    
    /**
     * 返回一个指定嵌套级别的{@link ResolvableType}
     * <p>
     * 详见{@link #getNested(int, Map)}
     *
     * @param nestingLevel 嵌套层
     * @return 的{@link ResolvableType}类型，或{@code #NONE}
     */
    public ResolvableType getNested(int nestingLevel) {
        return getNested(nestingLevel, null);
    }
    
    /**
     * 返回一个指定嵌套级别的{@link ResolvableType}
     * <p>
     * 嵌套级别指的是应该被返回的特定泛型参数。
     * 嵌套级别为
     * 1表示这个类型；
     * 2表示第一个嵌套泛型；
     * 3表示第二个；以此类推。
     * <p>
     * 例如，给定{@code List<Set<Integer>}级别1指的是{@code List}，级别2是{@code Set}，级别3是{@code Integer}
     *
     * @param nestingLevel        所需的嵌套级别，从1开始索引，代表当前的类型，2代表第一个嵌套的类，3代表第二个，以此类推。
     * @param typeIndexesPerLevel 一个包含给定嵌套层的通用索引的地图（可能是{@code null}）
     * @return 嵌套层的{@link ResolvableType}，或者{@link #NONE}
     */
    public ResolvableType getNested(int nestingLevel, @Nullable Map<Integer, Integer> typeIndexesPerLevel) {
        ResolvableType result = this;
        for (int i = 2; i <= nestingLevel; i++) {
            if (result.isArray()) {
                result = result.getComponentType();
            } else {
                // 处理派生类型
                while (result != ResolvableType.NONE && !result.hasGenerics()) {
                    result = result.getSuperType();
                }
                Integer index = (typeIndexesPerLevel != null ? typeIndexesPerLevel.get(i) : null);
                index = (index == null ? result.getGenerics().length - 1 : index);
                result = result.getGeneric(index);
            }
        }
        return result;
    }
    
    /**
     * 返回一个{@link ResolvableType}，代表给定索引的通用参数。
     * 索引是基于零的；例如，给定类型 {@code Map<Integer, List<String>>}，{@code getGeneric(0)}将访问 {@code Integer}。
     * 嵌套的泛型可以通过指定多个索引来访问；
     * <p>
     * 例如，{@code getGeneric(1, 0)}将从嵌套的{@code List}中访问{@code String}。为了方便起见，如果没有指定索引，那么将返回第一个泛型。
     * <p>
     * 如果在指定的索引中没有可用的泛型，则返回{@link #NONE}。
     *
     * @param indexes 指向通用参数的索引（可以省略，以返回第一个通用参数）。
     * @return 一个{@link ResolvableType}用于指定的泛型，或者{@link #NONE}
     * @see #hasGenerics()
     * @see #getGenerics()
     * @see #resolveGeneric(int...)
     * @see #resolveGenerics()
     */
    public ResolvableType getGeneric(int @Nullable ... indexes) {
        ResolvableType[] generics = getGenerics();
        if (indexes == null || indexes.length == 0) {
            return (generics.length == 0 ? NONE : generics[0]);
        }
        ResolvableType generic = this;
        for (int index : indexes) {
            generics = generic.getGenerics();
            if (index < 0 || index >= generics.length) {
                return NONE;
            }
            generic = generics[index];
        }
        return generic;
    }
    
    /**
     * 返回一个{@link ResolvableType ResolvableTypes}的数组，代表这个类型的通用参数。
     * 如果没有可用的泛型，则返回一个空数组。如果你需要访问一个特定的泛型，可以考虑使用{@link #getGeneric(int...)}方法，
     * 因为它允许访问嵌套的泛型并防止 {@code IndexOutOfBoundsExceptions}。
     *
     * @return 一个代表通用参数的 {@link ResolvableType ResolvableTypes}数组 (绝不是{@code null})。
     * @see #hasGenerics()
     * @see #getGeneric(int...)
     * @see #resolveGeneric(int...)
     * @see #resolveGenerics()
     */
    public ResolvableType[] getGenerics() {
        if (this == NONE) {
            return EMPTY_TYPES_ARRAY;
        }
        ResolvableType[] generics = this.generics;
        if (generics == null) {
            if (this.type instanceof Class) {
                Type[] typeParams = ((Class<?>) this.type).getTypeParameters();
                generics = new ResolvableType[typeParams.length];
                for (int i = 0; i < generics.length; i++) {
                    generics[i] = ResolvableType.forType(typeParams[i], this);
                }
            } else if (this.type instanceof ParameterizedType) {
                Type[] actualTypeArguments = ((ParameterizedType) this.type).getActualTypeArguments();
                generics = new ResolvableType[actualTypeArguments.length];
                for (int i = 0; i < actualTypeArguments.length; i++) {
                    generics[i] = forType(actualTypeArguments[i], this.variableResolver);
                }
            } else {
                generics = resolveType().getGenerics();
            }
            this.generics = generics;
        }
        return generics;
    }
    
    /**
     * 方便的方法，将 {@link #getGenerics() get} 和 {@link #resolve() resolve}通用参数。
     *
     * @return 一个已解决的通用参数数组（产生的数组 永远不会是{@code null}，但它可能包含{@code null}元素}。
     * @see #getGenerics()
     * @see #resolve()
     */
    public Class<?>[] resolveGenerics() {
        ResolvableType[] generics = getGenerics();
        Class<?>[] resolvedGenerics = new Class<?>[generics.length];
        for (int i = 0; i < generics.length; i++) {
            resolvedGenerics[i] = generics[i].resolve();
        }
        return resolvedGenerics;
    }
    
    /**
     * 方便的方法，将 {@link #getGenerics() get} 和 {@link #resolve()  resolve} 通用参数，
     * 如果有任何类型不能被解决，则使用指定的{@code fallback}。
     *
     * @param fallback 如果解析失败，将使用的后备类
     * @return 一个已解决的通用参数数组
     * @see #getGenerics()
     * @see #resolve()
     */
    public Class<?>[] resolveGenerics(Class<?> fallback) {
        ResolvableType[] generics = getGenerics();
        Class<?>[] resolvedGenerics = new Class<?>[generics.length];
        for (int i = 0; i < generics.length; i++) {
            resolvedGenerics[i] = generics[i].resolve(fallback);
        }
        return resolvedGenerics;
    }
    
    /**
     * 方便的方法，将{@link #getGeneric(int...) get}和 {@link #resolve() resolve}一个特定的通用参数。
     *
     * @param indexes 指向通用参数的索引（可以省略，以返回第一个通用参数）。
     * @return 一个已解决的 {@link Class} 或 {@code null}
     * @see #getGeneric(int...)
     * @see #resolve()
     */
    @Nullable
    public Class<?> resolveGeneric(int... indexes) {
        return getGeneric(indexes).resolve();
    }
    
    /**
     * 将此类型解析为 {@link java.lang.Class}，如果该类型不能被解析，则返回{@code null}。
     * 如果直接解析失败，该方法将考虑 {@link TypeVariable TypeVariables}和 {@link WildcardType WildcardTypes}的边界；
     * 但是，{@code Object.class}的边界将被忽略。
     * <p>
     * 如果该方法返回一个非空的{@code Class}并且{@link #hasGenerics()} 返回{@code false}，
     * 给定的类型将有效包裹一个普通的{@code Class}，如果需要，允许进行普通的{@code Class}处理。
     *
     * @return 已解决的{@link Class}，如果不能解决，则为{@code null}
     * @see #resolve(Class)
     * @see #resolveGeneric(int...)
     * @see #resolveGenerics()
     */
    @Nullable
    public Class<?> resolve() {
        return this.resolved;
    }
    
    /**
     * 将此类型解析为{@link java.lang.Class}，如果类型不能被解析，则返回指定的 {@code fallback}。
     * 如果直接解析失败，这个方法将考虑 {@link TypeVariable TypeVariables} 和 {@link WildcardType WildcardTypes} 的边界；
     * 但是，{@code Object.class}的边界将被忽略。
     *
     * @param fallback 如果解析失败，将使用的后备类·
     * @return 解决的 {@link Class} 或 {@code fallback}
     * @see #resolve()
     * @see #resolveGeneric(int...)
     * @see #resolveGenerics()
     */
    public Class<?> resolve(Class<?> fallback) {
        return (this.resolved != null ? this.resolved : fallback);
    }
    
    @Nullable
    private Class<?> resolveClass() {
        if (this.type == EmptyType.INSTANCE) {
            return null;
        }
        if (this.type instanceof Class) {
            return (Class<?>) this.type;
        }
        if (this.type instanceof GenericArrayType) {
            Class<?> resolvedComponent = getComponentType().resolve();
            return (resolvedComponent != null ? Array.newInstance(resolvedComponent, 0).getClass() : null);
        }
        return resolveType().resolve();
    }
    
    /**
     * 通过单层解析这个类型，返回解析的值或{@link #NONE}
     * <p>
     * 注意：返回的{@link ResolvableType}只能作为一个中介使用，因为它不能被序列化。
     */
    ResolvableType resolveType() {
        if (this.type instanceof ParameterizedType) {
            return forType(((ParameterizedType) this.type).getRawType(), this.variableResolver);
        }
        if (this.type instanceof WildcardType) {
            Type resolved = resolveBounds(((WildcardType) this.type).getUpperBounds());
            if (resolved == null) {
                resolved = resolveBounds(((WildcardType) this.type).getLowerBounds());
            }
            return forType(resolved, this.variableResolver);
        }
        if (this.type instanceof TypeVariable) {
            TypeVariable<?> variable = (TypeVariable<?>) this.type;
            if (this.variableResolver != null) {
                ResolvableType resolved = this.variableResolver.resolveVariable(variable);
                if (resolved != null) {
                    return resolved;
                }
            }
            return forType(resolveBounds(variable.getBounds()), this.variableResolver);
        }
        return NONE;
    }
    
    @Nullable
    private Type resolveBounds(Type[] bounds) {
        if (bounds.length == 0 || bounds[0] == Object.class) {
            return null;
        }
        return bounds[0];
    }
    
    @Nullable
    private ResolvableType resolveVariable(TypeVariable<?> variable) {
        if (this.type instanceof TypeVariable) {
            return resolveType().resolveVariable(variable);
        }
        if (this.type instanceof ParameterizedType) {
            ParameterizedType parameterizedType = (ParameterizedType) this.type;
            Class<?> resolved = resolve();
            if (resolved == null) {
                return null;
            }
            TypeVariable<?>[] variables = resolved.getTypeParameters();
            for (int i = 0; i < variables.length; i++) {
                if (Utils.nullSafeEquals(variables[i].getName(), variable.getName())) {
                    Type actualType = parameterizedType.getActualTypeArguments()[i];
                    return forType(actualType, this.variableResolver);
                }
            }
            Type ownerType = parameterizedType.getOwnerType();
            if (ownerType != null) {
                return forType(ownerType, this.variableResolver).resolveVariable(variable);
            }
        }
        if (this.type instanceof WildcardType) {
            ResolvableType resolved = resolveType().resolveVariable(variable);
            if (resolved != null) {
                return resolved;
            }
        }
        if (this.variableResolver != null) {
            return this.variableResolver.resolveVariable(variable);
        }
        return null;
    }
    
    @Override
    public boolean equals(@Nullable Object other) {
        if (this == other) {
            return true;
        }
        if (!(other instanceof ResolvableType)) {
            return false;
        }
        
        ResolvableType otherType = (ResolvableType) other;
        if (!Utils.nullSafeEquals(this.type, otherType.type)) {
            return false;
        }
        if (this.typeProvider != otherType.typeProvider &&
                (this.typeProvider == null || otherType.typeProvider == null ||
                        !Utils.nullSafeEquals(this.typeProvider.getType(), otherType.typeProvider.getType()))) {
            return false;
        }
        if (this.variableResolver != otherType.variableResolver &&
                (this.variableResolver == null || otherType.variableResolver == null ||
                        !Utils.nullSafeEquals(this.variableResolver.getSource(), otherType.variableResolver.getSource()))) {
            return false;
        }
        return Utils.nullSafeEquals(this.componentType, otherType.componentType);
    }
    
    @Override
    public int hashCode() {
        return (this.hash != null ? this.hash : calculateHashCode());
    }
    
    private int calculateHashCode() {
        int hashCode = Utils.nullSafeHashCode(this.type);
        if (this.typeProvider != null) {
            hashCode = 31 * hashCode + Utils.nullSafeHashCode(this.typeProvider.getType());
        }
        if (this.variableResolver != null) {
            hashCode = 31 * hashCode + Utils.nullSafeHashCode(this.variableResolver.getSource());
        }
        if (this.componentType != null) {
            hashCode = 31 * hashCode + Utils.nullSafeHashCode(this.componentType);
        }
        return hashCode;
    }
    
    /**
     * 将这个{@link ResolvableType}调整为一个{@link VariableResolver}
     */
    @Nullable
    VariableResolver asVariableResolver() {
        if (this == NONE) {
            return null;
        }
        return new DefaultVariableResolver(this);
    }
    
    /**
     * 对{@link #NONE}的自定义序列化支持
     */
    private Object readResolve() {
        return (this.type == EmptyType.INSTANCE ? NONE : this);
    }
    
    /**
     * 返回该类型的一个字符串表示，以其完全解析的形式（包括任何通用参数）
     */
    @Override
    public String toString() {
        if (isArray()) {
            return getComponentType() + "[]";
        }
        if (this.resolved == null) {
            return "?";
        }
        if (this.type instanceof TypeVariable) {
            TypeVariable<?> variable = (TypeVariable<?>) this.type;
            if (this.variableResolver == null || this.variableResolver.resolveVariable(variable) == null) {
                // 不要为toString()设置变量边界......
                // 在自我引用的情况下会导致无限递归。
                return "?";
            }
        }
        if (hasGenerics()) {
            return this.resolved.getName() + '<' + StrUtil.join(getGenerics(), ", ") + '>';
        }
        return this.resolved.getName();
    }
    
    
    /**
     * 用于解决{@link TypeVariable TypeVariables}的策略接口
     */
    interface VariableResolver extends Serializable {
        
        /**
         * 返回解析器的来源（用于 hashCode 和 equals）
         */
        Object getSource();
        
        /**
         * 解决指定的变量
         *
         * @param variable 要解决的变量
         * @return 解决的变量，如果没有找到，则为{@code null}
         */
        @Nullable
        ResolvableType resolveVariable(TypeVariable<?> variable);
    }
    
    
    private static class DefaultVariableResolver implements VariableResolver {
        
        private final ResolvableType source;
        
        DefaultVariableResolver(ResolvableType resolvableType) {
            this.source = resolvableType;
        }
        
        @Override
        @Nullable
        public ResolvableType resolveVariable(TypeVariable<?> variable) {
            return this.source.resolveVariable(variable);
        }
        
        @Override
        public Object getSource() {
            return this.source;
        }
    }
    
    
    private static class TypeVariablesVariableResolver implements VariableResolver {
        
        private final TypeVariable<?>[] variables;
        
        private final ResolvableType[] generics;
        
        public TypeVariablesVariableResolver(TypeVariable<?>[] variables, ResolvableType[] generics) {
            this.variables = variables;
            this.generics = generics;
        }
        
        @Override
        @Nullable
        public ResolvableType resolveVariable(TypeVariable<?> variable) {
            TypeVariable<?> variableToCompare = unwrap(variable);
            for (int i = 0; i < this.variables.length; i++) {
                TypeVariable<?> resolvedVariable = unwrap(this.variables[i]);
                if (Utils.nullSafeEquals(resolvedVariable, variableToCompare)) {
                    return this.generics[i];
                }
            }
            return null;
        }
        
        @Override
        public Object getSource() {
            return this.generics;
        }
    }
    
    
    private static final class SyntheticParameterizedType implements ParameterizedType, Serializable {
        
        private final Type rawType;
        
        private final Type[] typeArguments;
        
        public SyntheticParameterizedType(Type rawType, Type[] typeArguments) {
            this.rawType = rawType;
            this.typeArguments = typeArguments;
        }
        
        @Override
        public String getTypeName() {
            String typeName = this.rawType.getTypeName();
            if (this.typeArguments.length > 0) {
                StringJoiner stringJoiner = new StringJoiner(", ", "<", ">");
                for (Type argument : this.typeArguments) {
                    stringJoiner.add(argument.getTypeName());
                }
                return typeName + stringJoiner;
            }
            return typeName;
        }
        
        @Override
        @Nullable
        public Type getOwnerType() {
            return null;
        }
        
        @Override
        public Type getRawType() {
            return this.rawType;
        }
        
        @Override
        public Type[] getActualTypeArguments() {
            return this.typeArguments;
        }
        
        @Override
        public boolean equals(@Nullable Object other) {
            if (this == other) {
                return true;
            }
            if (!(other instanceof ParameterizedType)) {
                return false;
            }
            ParameterizedType otherType = (ParameterizedType) other;
            return (otherType.getOwnerType() == null && this.rawType.equals(otherType.getRawType()) &&
                    Arrays.equals(this.typeArguments, otherType.getActualTypeArguments()));
        }
        
        @Override
        public int hashCode() {
            return (this.rawType.hashCode() * 31 + Arrays.hashCode(this.typeArguments));
        }
        
        @Override
        public String toString() {
            return getTypeName();
        }
    }
    
    
    /**
     * 处理来自{@link WildcardType WildcardTypes}的边界的内部帮助器。
     */
    private static class WildcardBounds {
        
        private final Kind kind;
        
        private final ResolvableType[] bounds;
        
        /**
         * 内部构造函数用于创建一个新的{@link WildcardBounds}实例
         *
         * @param kind   种类的界线
         * @param bounds 界线
         * @see #get(ResolvableType)
         */
        public WildcardBounds(Kind kind, ResolvableType[] bounds) {
            this.kind = kind;
            this.bounds = bounds;
        }
        
        /**
         * 获取指定类型的{@link WildcardBounds}实例，如果指定类型不能被解析为{@link WildcardType}，则返回 {@code null}
         *
         * @param type 源类型
         * @return a {@link WildcardBounds} instance or {@code null}
         */
        @Nullable
        public static WildcardBounds get(ResolvableType type) {
            ResolvableType resolveToWildcard = type;
            while (!(resolveToWildcard.getType() instanceof WildcardType)) {
                if (resolveToWildcard == NONE) {
                    return null;
                }
                resolveToWildcard = resolveToWildcard.resolveType();
            }
            WildcardType wildcardType = (WildcardType) resolveToWildcard.type;
            Kind boundsType = (wildcardType.getLowerBounds().length > 0 ? Kind.LOWER : Kind.UPPER);
            Type[] bounds = (boundsType == Kind.UPPER ? wildcardType.getUpperBounds() : wildcardType.getLowerBounds());
            ResolvableType[] resolvableBounds = new ResolvableType[bounds.length];
            for (int i = 0; i < bounds.length; i++) {
                resolvableBounds[i] = ResolvableType.forType(bounds[i], type.variableResolver);
            }
            return new WildcardBounds(boundsType, resolvableBounds);
        }
        
        /**
         * 如果这个bounds与指定的bounds是同一类，则返回{@code true}
         */
        public boolean isSameKind(WildcardBounds bounds) {
            return this.kind == bounds.kind;
        }
        
        /**
         * 如果这个界线可以分配给所有指定的类型，则返回{@code true}
         *
         * @param types 要测试的类型
         * @return {@code true}如果这个界限可以分配给所有的类型
         */
        public boolean isAssignableFrom(ResolvableType... types) {
            for (ResolvableType bound : this.bounds) {
                for (ResolvableType type : types) {
                    if (!isAssignable(bound, type)) {
                        return false;
                    }
                }
            }
            return true;
        }
        
        private boolean isAssignable(ResolvableType source, ResolvableType from) {
            return (this.kind == Kind.UPPER ? source.isAssignableFrom(from) : from.isAssignableFrom(source));
        }
        
        /**
         * 返回底层边界
         */
        public ResolvableType[] getBounds() {
            return this.bounds;
        }
        
        /**
         * 各种各样的界限
         */
        enum Kind {UPPER, LOWER}
    }
    
    
    /**
     * 内部{@link Type}用于表示空值
     */
    static class EmptyType implements Type, Serializable {
        
        static final Type INSTANCE = new EmptyType();
        
        Object readResolve() {
            return INSTANCE;
        }
    }
    
}
