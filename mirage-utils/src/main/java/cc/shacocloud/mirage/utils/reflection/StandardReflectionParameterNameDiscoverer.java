package cc.shacocloud.mirage.utils.reflection;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.lang.reflect.Parameter;

/**
 * {@link ParameterNameDiscoverer} 实现，它使用 JDK 8 的反射工具来内省参数名称（基于 “-parameters” 编译器标志）
 */
public class StandardReflectionParameterNameDiscoverer implements ParameterNameDiscoverer {
    
    @Nullable
    @Override
    public String[] getParameterNames(@NotNull Method method) {
        return getParameterNames(method.getParameters());
    }
    
    @Nullable
    @Override
    public String[] getParameterNames(@NotNull Constructor<?> ctor) {
        return getParameterNames(ctor.getParameters());
    }
    
    @Nullable
    private String[] getParameterNames(Parameter @NotNull [] parameters) {
        String[] parameterNames = new String[parameters.length];
        for (int i = 0; i < parameters.length; i++) {
            Parameter param = parameters[i];
            if (!param.isNamePresent()) {
                return null;
            }
            parameterNames[i] = param.getName();
        }
        return parameterNames;
    }
    
}
