package cc.shacocloud.mirage.utils;

import cc.shacocloud.mirage.utils.map.ConcurrentReferenceHashMap;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.lang.reflect.*;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * 用于针对类型变量解析泛型类型的帮助程序类
 *
 * <p>
 * 主要用于在框架中使用，解析方法参数类型，即使它们是泛型声明的
 */
public final class GenericTypeResolver {
    
    /**
     * 从类缓存到类型变量映射
     */
    @SuppressWarnings("rawtypes")
    private static final Map<Class<?>, Map<TypeVariable, Type>> typeVariableCache = new ConcurrentReferenceHashMap<>();
    
    
    private GenericTypeResolver() {
    }
    
    /**
     * 确定给定方法的泛型返回类型的目标类型，其中形式类型变量在给定类上声明。
     *
     * @param method 方法
     * @param clazz  要解析类型变量的类
     * @return 相应的泛型参数或返回类型
     */
    public static Class<?> resolveReturnType(@NotNull Method method, @NotNull Class<?> clazz) {
        return ResolvableType.forMethodReturnType(method, clazz).resolve(method.getReturnType());
    }
    
    /**
     * 针对给定的目标方法解析给定泛型接口的单一类型参数，该方法假定返回给定接口或其实现。
     *
     * @param method     检查返回类型的目标方法
     * @param genericIfc 用于解析类型参数的泛型接口或超类
     * @return 方法返回类型的解析参数类型，如果不可解析或单个参数的类型为 {@link WildcardType}，则为 {@code null}
     */
    @Nullable
    public static Class<?> resolveReturnTypeArgument(@NotNull Method method, Class<?> genericIfc) {
        ResolvableType resolvableType = ResolvableType.forMethodReturnType(method).as(genericIfc);
        if (!resolvableType.hasGenerics() || resolvableType.getType() instanceof WildcardType) {
            return null;
        }
        return getSingleGeneric(resolvableType);
    }
    
    /**
     * 针对假定实现泛型接口的给定目标类解析给定泛型接口的单一类型参数，并可能为其类型变量声明具体类型。
     *
     * @param clazz      要检查的目标类
     * @param genericIfc 用于解析类型参数的泛型接口或超类
     * @return 参数的解析类型，如果无法解析，则为 {@code null}
     */
    @Nullable
    public static Class<?> resolveTypeArgument(Class<?> clazz, Class<?> genericIfc) {
        ResolvableType resolvableType = ResolvableType.forClass(clazz).as(genericIfc);
        if (!resolvableType.hasGenerics()) {
            return null;
        }
        return getSingleGeneric(resolvableType);
    }
    
    @Nullable
    private static Class<?> getSingleGeneric(@NotNull ResolvableType resolvableType) {
        if (resolvableType.getGenerics().length != 1) {
            throw new IllegalArgumentException("泛型接口上预期的 1 个类型参数 [" + resolvableType +
                    "] 但实际发现数量为 " + resolvableType.getGenerics().length);
        }
        
        return resolvableType.getGeneric().resolve();
    }
    
    
    /**
     * 针对给定的目标类解析给定泛型接口的类型参数，该目标类假定实现泛型接口，并可能为其类型变量声明具体类型。
     *
     * @param clazz      要检查的目标类
     * @param genericIfc 用于解析类型参数的泛型接口或超类
     * @return 每个参数的解析类型，数组大小与实际类型参数的数量匹配，如果不可解析，则为 {@code null}
     */
    @Nullable
    public static Class<?> @Nullable [] resolveTypeArguments(Class<?> clazz, Class<?> genericIfc) {
        ResolvableType type = ResolvableType.forClass(clazz).as(genericIfc);
        if (!type.hasGenerics() || type.isEntirelyUnresolvable()) {
            return null;
        }
        return type.resolveGenerics(Object.class);
    }
    
    /**
     * 针对给定的上下文类解析给定的泛型类型，并尽可能替换类型变量
     *
     * @param genericType  （可能）泛型类型
     * @param contextClass 目标类型的上下文类，例如目标类型出现在方法签名中的类（可以是 {@code null}）
     * @return 解析的类型（可能是给定的泛型类型）
     */
    public static Type resolveType(Type genericType, @Nullable Class<?> contextClass) {
        if (contextClass != null) {
            if (genericType instanceof TypeVariable) {
                ResolvableType resolvedTypeVariable = resolveVariable(
                        (TypeVariable<?>) genericType, ResolvableType.forClass(contextClass));
                if (resolvedTypeVariable != ResolvableType.NONE) {
                    Class<?> resolved = resolvedTypeVariable.resolve();
                    if (resolved != null) {
                        return resolved;
                    }
                }
            } else if (genericType instanceof ParameterizedType) {
                ResolvableType resolvedType = ResolvableType.forType(genericType);
                if (resolvedType.hasUnresolvableGenerics()) {
                    ParameterizedType parameterizedType = (ParameterizedType) genericType;
                    Class<?>[] generics = new Class<?>[parameterizedType.getActualTypeArguments().length];
                    Type[] typeArguments = parameterizedType.getActualTypeArguments();
                    ResolvableType contextType = ResolvableType.forClass(contextClass);
                    for (int i = 0; i < typeArguments.length; i++) {
                        Type typeArgument = typeArguments[i];
                        if (typeArgument instanceof TypeVariable) {
                            ResolvableType resolvedTypeArgument = resolveVariable(
                                    (TypeVariable<?>) typeArgument, contextType);
                            if (resolvedTypeArgument != ResolvableType.NONE) {
                                generics[i] = resolvedTypeArgument.resolve();
                            } else {
                                generics[i] = ResolvableType.forType(typeArgument).resolve();
                            }
                        } else {
                            generics[i] = ResolvableType.forType(typeArgument).resolve();
                        }
                    }
                    Class<?> rawClass = resolvedType.getRawClass();
                    if (rawClass != null) {
                        return ResolvableType.forClassWithGenerics(rawClass, generics).getType();
                    }
                }
            }
        }
        return genericType;
    }
    
    private static ResolvableType resolveVariable(TypeVariable<?> typeVariable, @NotNull ResolvableType contextType) {
        ResolvableType resolvedType;
        if (contextType.hasGenerics()) {
            resolvedType = ResolvableType.forType(typeVariable, contextType);
            if (resolvedType.resolve() != null) {
                return resolvedType;
            }
        }
        
        ResolvableType superType = contextType.getSuperType();
        if (superType != ResolvableType.NONE) {
            resolvedType = resolveVariable(typeVariable, superType);
            if (resolvedType.resolve() != null) {
                return resolvedType;
            }
        }
        for (ResolvableType ifc : contextType.getInterfaces()) {
            resolvedType = resolveVariable(typeVariable, ifc);
            if (resolvedType.resolve() != null) {
                return resolvedType;
            }
        }
        return ResolvableType.NONE;
    }
    
    /**
     * 根据给定的类型变量映射解析指定的泛型类型
     *
     * @param genericType 要解析的泛型类型
     * @param map         要解析的类型变量映射
     * @return 如果解析为类，则为类型，否则为 {@code 对象.class}
     */
    @SuppressWarnings("rawtypes")
    public static Class<?> resolveType(Type genericType, Map<TypeVariable, Type> map) {
        return ResolvableType.forType(genericType, new TypeVariableMapVariableResolver(map)).toClass();
    }
    
    /**
     * 为指定的 {@link Class} 构建 {@link TypeVariable#getName} 到 {@link Class} 的映射。搜索所有超类型，包含类型和接口。
     *
     * @see #resolveType(Type, Map)
     */
    @SuppressWarnings("rawtypes")
    public static @NotNull Map<TypeVariable, Type> getTypeVariableMap(Class<?> clazz) {
        Map<TypeVariable, Type> typeVariableMap = typeVariableCache.get(clazz);
        if (typeVariableMap == null) {
            typeVariableMap = new HashMap<>();
            buildTypeVariableMap(ResolvableType.forClass(clazz), typeVariableMap);
            typeVariableCache.put(clazz, Collections.unmodifiableMap(typeVariableMap));
        }
        return typeVariableMap;
    }
    
    @SuppressWarnings("rawtypes")
    private static void buildTypeVariableMap(ResolvableType type, Map<TypeVariable, Type> typeVariableMap) {
        if (type != ResolvableType.NONE) {
            Class<?> resolved = type.resolve();
            if (resolved != null && type.getType() instanceof ParameterizedType) {
                TypeVariable<?>[] variables = resolved.getTypeParameters();
                for (int i = 0; i < variables.length; i++) {
                    ResolvableType generic = type.getGeneric(i);
                    while (generic.getType() instanceof TypeVariable<?>) {
                        generic = generic.resolveType();
                    }
                    if (generic != ResolvableType.NONE) {
                        typeVariableMap.put(variables[i], generic.getType());
                    }
                }
            }
            buildTypeVariableMap(type.getSuperType(), typeVariableMap);
            for (ResolvableType interfaceType : type.getInterfaces()) {
                buildTypeVariableMap(interfaceType, typeVariableMap);
            }
            if (resolved != null && resolved.isMemberClass()) {
                buildTypeVariableMap(ResolvableType.forClass(resolved.getEnclosingClass()), typeVariableMap);
            }
        }
    }
    
    
    @SuppressWarnings({"rawtypes"})
    private static class TypeVariableMapVariableResolver implements ResolvableType.VariableResolver {
        
        private final Map<TypeVariable, Type> typeVariableMap;
        
        public TypeVariableMapVariableResolver(Map<TypeVariable, Type> typeVariableMap) {
            this.typeVariableMap = typeVariableMap;
        }
        
        @Override
        @Nullable
        public ResolvableType resolveVariable(TypeVariable<?> variable) {
            Type type = this.typeVariableMap.get(variable);
            return (type != null ? ResolvableType.forType(type) : null);
        }
        
        @Override
        public Object getSource() {
            return this.typeVariableMap;
        }
    }
    
}
