package cc.shacocloud.mirage.utils.comparator;

/**
 * {@code Ordered} 是一个接口，用于实现集合对象排序使用
 * <p>
 * 实际的 {@link #getOrder} 可以解释为优先级，第一个对象（具有最低顺序值）具有最高优先级。
 *
 * @see OrderComparator
 */
public interface Ordered {
    
    /**
     * 最高优先级值的有用常量
     *
     * @see java.lang.Integer#MIN_VALUE
     */
    int HIGHEST_PRECEDENCE = Integer.MIN_VALUE;
    
    /**
     * 最低优先级值的有用常量
     *
     * @see java.lang.Integer#MAX_VALUE
     */
    int LOWEST_PRECEDENCE = Integer.MAX_VALUE;
    
    
    /**
     * 获取此对象的顺序值
     *
     * @see #HIGHEST_PRECEDENCE
     * @see #LOWEST_PRECEDENCE
     */
    int getOrder();
    
}
