package cc.shacocloud.mirage.utils.converter.support;

import cc.shacocloud.mirage.utils.converter.ConversionSupport;
import cc.shacocloud.mirage.utils.converter.TypeDescriptor;
import org.jetbrains.annotations.NotNull;

/**
 * 转换为 {@link String} 类型的转换器
 *
 * @author 思追(shaco)
 */
public class ToStringConversion extends AbstractConversion implements ConversionSupport {
    
    @Override
    public boolean support(@NotNull TypeDescriptor sourceType, @NotNull TypeDescriptor targetType) {
        Class<?> objectType = targetType.getObjectType();
        return String.class.equals(objectType);
    }
    
    @Override
    public Object convert(@NotNull Object source, @NotNull TypeDescriptor sourceType, @NotNull TypeDescriptor targetType) {
        if (source instanceof String) {
            return source;
        }
        
        return convertToStr(source);
    }
    
}
