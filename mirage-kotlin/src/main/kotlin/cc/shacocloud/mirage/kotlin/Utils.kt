package cc.shacocloud.mirage.kotlin

import cc.shacocloud.mirage.restful.HandlerMethod
import cc.shacocloud.mirage.utils.MethodParameter
import java.lang.reflect.Method
import kotlin.coroutines.Continuation

/**
 * 判断指定方法是否为 kotlin 的协程方法
 *
 * 判断条件：由 suspend 修饰的函数被编译为 class 后，函数会被增加一个 Continuation (续体)类型的参数
 */
fun isSuspendingFunction(handlerMethod: HandlerMethod): Boolean {
    val methodParameters = handlerMethod.methodParameters

    val size = methodParameters?.size
    if (size == null || size == 0) return false

    // 一般来说 Continuation 是最后一个参数
    val methodParameter = methodParameters[size - 1]
    return isCoroutinesMethodParameter(methodParameter)
}

/**
 * 判断指定方法是否为 kotlin 的协程方法
 */
fun isSuspendingFunction(method: Method): Boolean {
    val parameters = method.parameters
    if (parameters.isEmpty()) return false

    return isCoroutinesParameter(parameters[parameters.size - 1].type)
}

/**
 * 判断参数是否为 Continuation
 */
fun isCoroutinesMethodParameter(methodParameter: MethodParameter): Boolean {
    val parameterType = methodParameter.parameterType
    return isCoroutinesParameter(parameterType)
}


/**
 * 判断参数是否为 Continuation 参数
 */
fun isCoroutinesParameter(parameterType: Class<*>): Boolean {
    return parameterType == Continuation::class.java
}