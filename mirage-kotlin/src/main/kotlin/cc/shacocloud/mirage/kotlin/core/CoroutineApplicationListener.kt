package cc.shacocloud.mirage.kotlin.core

import cc.shacocloud.mirage.core.ApplicationEvent
import cc.shacocloud.mirage.core.ApplicationListener
import cc.shacocloud.mirage.kotlin.mirageLaunch
import io.vertx.core.Future

/**
 * 基于 kotlin 协程的应用监听器
 * @author 思追(shaco)
 *
 */
interface CoroutineApplicationListener<E : ApplicationEvent> : ApplicationListener<E> {

    /**
     * 转为协程处理方法
     * @see #doApplicationEvent
     */
    override fun onApplicationEvent(event: E): Future<Void?> {
        return mirageLaunch {
            doApplicationEvent(event)
            return@mirageLaunch null
        }
    }

    /**
     * @see #onApplicationEvent
     */
    suspend fun doApplicationEvent(event: E)

}