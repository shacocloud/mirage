# 发布的版本号
version=2.0.4-SNAPSHOT

# 修改当前pom中的所有版本号并且构建部署
mvn versions:set -DnewVersion="$version" && mvn clean deploy -DskipTests -P mirage-ossrh-release -e -X

# 本地安装
# mvn versions:set -DnewVersion=2.0.4-SNAPSHOT && mvn clean install -DskipTests



