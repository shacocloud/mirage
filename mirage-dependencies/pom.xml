<?xml version="1.0" encoding="UTF-8"?>
<project xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xmlns="http://maven.apache.org/POM/4.0.0"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <parent>
        <artifactId>mirage</artifactId>
        <groupId>cc.shacocloud</groupId>
        <version>2.0.6</version>
    </parent>
    <modelVersion>4.0.0</modelVersion>

    <artifactId>mirage-dependencies</artifactId>
    <packaging>pom</packaging>

    <properties>
        <vertx.version>4.4.1</vertx.version>
        <commons-logging.version>1.2</commons-logging.version>
        <lombok.version>1.18.24</lombok.version>
        <assertj-core.version>3.16.1</assertj-core.version>
        <jackson-bom.version>2.14.0</jackson-bom.version>
        <logback.version>1.4.5</logback.version>
        <slf4j-api.version>2.0.6</slf4j-api.version>
        <validation-api.version>3.0.2</validation-api.version>
        <inject-api.version>2.0.1</inject-api.version>
        <hibernate-validator.version>8.0.0.Final</hibernate-validator.version>
        <glassfish.expressly.version>5.0.0-M2</glassfish.expressly.version>
        <jetbrains-annotations.version>23.0.0</jetbrains-annotations.version>
        <h2.version>2.1.214</h2.version>
        <junit-jupiter.version>5.8.2</junit-jupiter.version>
        <annotation-api.version>2.1.1</annotation-api.version>
        <kotlin.version>1.7.22</kotlin.version>
        <snakeyaml.version>2.0</snakeyaml.version>
        <micrometer.version>1.9.2</micrometer.version>
    </properties>

    <dependencyManagement>
        <dependencies>

            <dependency>
                <groupId>cc.shacocloud</groupId>
                <artifactId>mirage-utils</artifactId>
                <version>${project.version}</version>
            </dependency>

            <dependency>
                <groupId>cc.shacocloud</groupId>
                <artifactId>mirage-restful</artifactId>
                <version>${project.version}</version>
            </dependency>

            <dependency>
                <groupId>cc.shacocloud</groupId>
                <artifactId>mirage-context</artifactId>
                <version>${project.version}</version>
            </dependency>

            <dependency>
                <groupId>cc.shacocloud</groupId>
                <artifactId>mirage-core</artifactId>
                <version>${project.version}</version>
            </dependency>

            <dependency>
                <groupId>cc.shacocloud</groupId>
                <artifactId>mirage-bean</artifactId>
                <version>${project.version}</version>
            </dependency>

            <dependency>
                <groupId>cc.shacocloud</groupId>
                <artifactId>mirage-env</artifactId>
                <version>${project.version}</version>
            </dependency>

            <dependency>
                <groupId>cc.shacocloud</groupId>
                <artifactId>mirage-kotlin</artifactId>
                <version>${project.version}</version>
            </dependency>

            <dependency>
                <groupId>cc.shacocloud</groupId>
                <artifactId>mirage-starter</artifactId>
                <version>${project.version}</version>
            </dependency>

            <dependency>
                <groupId>cc.shacocloud</groupId>
                <artifactId>mirage-loader</artifactId>
                <version>${project.version}</version>
            </dependency>

            <dependency>
                <groupId>org.jetbrains.kotlin</groupId>
                <artifactId>kotlin-bom</artifactId>
                <version>${kotlin.version}</version>
                <type>pom</type>
                <scope>import</scope>
            </dependency>

            <dependency>
                <groupId>io.vertx</groupId>
                <artifactId>vertx-dependencies</artifactId>
                <version>${vertx.version}</version>
                <type>pom</type>
                <scope>import</scope>
            </dependency>

            <dependency>
                <groupId>com.fasterxml.jackson</groupId>
                <artifactId>jackson-bom</artifactId>
                <version>${jackson-bom.version}</version>
                <type>pom</type>
                <scope>import</scope>
            </dependency>

            <dependency>
                <groupId>org.assertj</groupId>
                <artifactId>assertj-core</artifactId>
                <version>${assertj-core.version}</version>
            </dependency>

            <dependency>
                <groupId>commons-logging</groupId>
                <artifactId>commons-logging</artifactId>
                <version>${commons-logging.version}</version>
            </dependency>

            <dependency>
                <groupId>org.projectlombok</groupId>
                <artifactId>lombok</artifactId>
                <version>${lombok.version}</version>
            </dependency>

            <dependency>
                <groupId>ch.qos.logback</groupId>
                <artifactId>logback-parent</artifactId>
                <version>${logback.version}</version>
                <type>pom</type>
                <scope>import</scope>
            </dependency>

            <dependency>
                <groupId>org.slf4j</groupId>
                <artifactId>slf4j-api</artifactId>
                <version>${slf4j-api.version}</version>
            </dependency>

            <dependency>
                <groupId>jakarta.annotation</groupId>
                <artifactId>jakarta.annotation-api</artifactId>
                <version>${annotation-api.version}</version>
            </dependency>

            <dependency>
                <groupId>jakarta.validation</groupId>
                <artifactId>jakarta.validation-api</artifactId>
                <version>${validation-api.version}</version>
            </dependency>

            <dependency>
                <groupId>jakarta.inject</groupId>
                <artifactId>jakarta.inject-api</artifactId>
                <version>${inject-api.version}</version>
            </dependency>

            <dependency>
                <groupId>org.hibernate</groupId>
                <artifactId>hibernate-validator</artifactId>
                <version>${hibernate-validator.version}</version>
            </dependency>

            <dependency>
                <groupId>org.glassfish.expressly</groupId>
                <artifactId>expressly</artifactId>
                <version>${glassfish.expressly.version}</version>
            </dependency>

            <dependency>
                <groupId>org.jetbrains</groupId>
                <artifactId>annotations</artifactId>
                <version>${jetbrains-annotations.version}</version>
            </dependency>

            <dependency>
                <groupId>com.h2database</groupId>
                <artifactId>h2</artifactId>
                <version>${h2.version}</version>
            </dependency>

            <dependency>
                <groupId>org.junit</groupId>
                <artifactId>junit-bom</artifactId>
                <version>${junit-jupiter.version}</version>
                <type>pom</type>
                <scope>import</scope>
            </dependency>

            <dependency>
                <groupId>org.yaml</groupId>
                <artifactId>snakeyaml</artifactId>
                <version>${snakeyaml.version}</version>
            </dependency>

            <dependency>
                <groupId>io.micrometer</groupId>
                <artifactId>micrometer-registry-prometheus</artifactId>
                <version>${micrometer.version}</version>
            </dependency>

            <dependency>
                <groupId>io.micrometer</groupId>
                <artifactId>micrometer-registry-influx</artifactId>
                <version>${micrometer.version}</version>
            </dependency>

            <dependency>
                <groupId>io.micrometer</groupId>
                <artifactId>micrometer-registry-jmx</artifactId>
                <version>${micrometer.version}</version>
            </dependency>

        </dependencies>

    </dependencyManagement>


    <build>
        <pluginManagement>
            <plugins>
                <plugin>
                    <groupId>org.jetbrains.kotlin</groupId>
                    <artifactId>kotlin-maven-plugin</artifactId>
                    <version>${kotlin.version}</version>
                </plugin>

                <plugin>
                    <groupId>cc.shacocloud</groupId>
                    <artifactId>mirage-maven-plugin</artifactId>
                    <version>${project.version}</version>
                </plugin>
            </plugins>
        </pluginManagement>
    </build>

</project>
