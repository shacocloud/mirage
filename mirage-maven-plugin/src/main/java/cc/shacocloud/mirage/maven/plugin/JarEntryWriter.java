package cc.shacocloud.mirage.maven.plugin;

import java.io.IOException;
import java.io.OutputStream;

/**
 * 用于写入 jar 条目数据的接口
 */
@FunctionalInterface
public interface JarEntryWriter {
    
    /**
     * 将条目数据写入指定的输出流
     */
    void write(OutputStream outputStream) throws IOException;
    
    /**
     * 返回将写入的内容的大小，如果大小未知，则返回 {@code -1}
     */
    default int size() {
        return -1;
    }
    
}
