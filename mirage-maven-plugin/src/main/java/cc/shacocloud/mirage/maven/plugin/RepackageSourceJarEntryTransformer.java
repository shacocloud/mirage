package cc.shacocloud.mirage.maven.plugin;

import org.apache.commons.compress.archivers.jar.JarArchiveEntry;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

/**
 * 重新打包时 源jar的条目的转换器
 *
 * @author 思追(shaco)
 */
public class RepackageSourceJarEntryTransformer extends AbstractJarArchiveEntryTransformer {
    
    @Nullable
    @Override
    public JarArchiveEntry transform(@NotNull JarArchiveEntry jarEntry) {
        if (!isTransformable(jarEntry)) {
            return jarEntry;
        }
        
        String transformedName = RepackageMojo.CLASSES_LOCATION + jarEntry.getName();
        JarArchiveEntry transformedEntry = new JarArchiveEntry(transformedName);
        copy(jarEntry, transformedEntry);
        return transformedEntry;
    }
    
    private boolean isTransformable(@NotNull JarArchiveEntry entry) {
        String name = entry.getName();
        if (name.startsWith("META-INF/")) {
            return name.endsWith(".kotlin_module") || name.startsWith("META-INF/services/");
        }
        return !name.startsWith("BOOT-INF/") && !name.equals("module-info.class");
    }
}
