package cc.shacocloud.mirage.maven.plugin;

import org.apache.commons.compress.archivers.jar.JarArchiveEntry;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

/**
 * 重新打包时 依赖条目的转换器
 *
 * @author 思追(shaco)
 */
public class RepackageArtifactLibEntryTransformer extends AbstractJarArchiveEntryTransformer {
    
    @Nullable
    @Override
    public JarArchiveEntry transform(@NotNull JarArchiveEntry jarEntry) {
        // 排除加载器
        if (jarEntry.getName().equals(RepackageMojo.LOADER_LIB_NAME)) {
            return null;
        }
        
        if (!isTransformable(jarEntry)) {
            return jarEntry;
        }
        
        String transformedName = RepackageMojo.LIB_LOCATION + jarEntry.getName();
        JarArchiveEntry transformedEntry = new JarArchiveEntry(transformedName);
        copy(jarEntry, transformedEntry);
        return transformedEntry;
    }
    
    private boolean isTransformable(@NotNull JarArchiveEntry entry) {
        String name = entry.getName();
        return !name.startsWith("BOOT-INF/");
    }
}
