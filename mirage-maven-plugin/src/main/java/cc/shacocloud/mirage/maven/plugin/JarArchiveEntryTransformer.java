package cc.shacocloud.mirage.maven.plugin;

import org.apache.commons.compress.archivers.jar.JarArchiveEntry;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

/**
 * 根据一定的判断逻辑将 {@link JarArchiveEntry} 转为一个新的 {@link JarArchiveEntry}
 *
 * @author 思追(shaco)
 */
@FunctionalInterface
public interface JarArchiveEntryTransformer {
    
    JarArchiveEntryTransformer NONE = (jarEntry) -> jarEntry;
    
    @Nullable
    JarArchiveEntry transform(@NotNull JarArchiveEntry jarEntry);
    
}