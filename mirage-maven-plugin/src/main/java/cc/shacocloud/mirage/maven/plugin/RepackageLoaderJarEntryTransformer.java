package cc.shacocloud.mirage.maven.plugin;

import org.apache.commons.compress.archivers.jar.JarArchiveEntry;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

/**
 * 重新打包时启动装载器条目的转换器
 *
 * @author 思追(shaco)
 */
public class RepackageLoaderJarEntryTransformer extends AbstractJarArchiveEntryTransformer {
    
    @Override
    public @Nullable JarArchiveEntry transform(@NotNull JarArchiveEntry jarEntry) {
        String name = jarEntry.getName();
        if (name.startsWith("META-INF/")) return null;
        return jarEntry;
    }
}
