package cc.shacocloud.mirage.maven.plugin;

import org.apache.maven.plugin.AbstractMojo;
import org.jetbrains.annotations.NotNull;

import java.util.function.Supplier;

/**
 * 抽象的日志类
 *
 * @author 思追(shaco)
 */
public abstract class AbstractLoggingMojo extends AbstractMojo implements Logging {
    
    /**
     * 打印 debug 日志
     */
    @Override
    public void logDebug(@NotNull Supplier<String> supplier) {
        if (getLog().isDebugEnabled()) {
            getLog().debug(supplier.get());
        }
    }
    
    @Override
    public void logDebug(@NotNull Supplier<String> supplier, Object... args) {
        if (getLog().isDebugEnabled()) {
            logging(supplier.get(), args, new LoggingHandler() {
                @Override
                public void log(String message) {
                    getLog().debug(message);
                }
                
                @Override
                public void log(String message, Throwable cause) {
                    getLog().debug(message, cause);
                }
            });
        }
    }
    
    /**
     * 打印 log 日志
     */
    @Override
    public void logInfo(@NotNull Supplier<String> supplier) {
        if (getLog().isInfoEnabled()) {
            getLog().info(supplier.get());
        }
    }
    
    @Override
    public void logInfo(@NotNull Supplier<String> supplier, Object... args) {
        if (getLog().isInfoEnabled()) {
            logging(supplier.get(), args, new LoggingHandler() {
                @Override
                public void log(String message) {
                    getLog().info(message);
                }
                
                @Override
                public void log(String message, Throwable cause) {
                    getLog().info(message, cause);
                }
            });
        }
    }
    
    /**
     * 打印 warn 日志
     */
    @Override
    public void logWarn(@NotNull Supplier<String> supplier) {
        if (getLog().isWarnEnabled()) {
            getLog().warn(supplier.get());
        }
    }
    
    @Override
    public void logWarn(@NotNull Supplier<String> supplier, Object... args) {
        if (getLog().isWarnEnabled()) {
            logging(supplier.get(), args, new LoggingHandler() {
                @Override
                public void log(String message) {
                    getLog().warn(message);
                }
                
                @Override
                public void log(String message, Throwable cause) {
                    getLog().warn(message, cause);
                }
            });
        }
    }
    
    /**
     * 打印 error 日志
     */
    @Override
    public void logError(@NotNull Supplier<String> supplier) {
        if (getLog().isErrorEnabled()) {
            getLog().error(supplier.get());
        }
    }
    
    @Override
    public void logError(@NotNull Supplier<String> supplier, Object... args) {
        if (getLog().isErrorEnabled()) {
            logging(supplier.get(), args, new LoggingHandler() {
                @Override
                public void log(String message) {
                    getLog().error(message);
                }
                
                @Override
                public void log(String message, Throwable cause) {
                    getLog().error(message, cause);
                }
            });
        }
    }
    
}
