package cc.shacocloud.mirage.maven.plugin;

import org.apache.commons.compress.archivers.jar.JarArchiveEntry;
import org.jetbrains.annotations.NotNull;

/**
 * @author 思追(shaco)
 */
public abstract class AbstractJarArchiveEntryTransformer implements JarArchiveEntryTransformer {
    
    /**
     * 源到目标的复制
     */
    protected void copy(@NotNull JarArchiveEntry source, @NotNull JarArchiveEntry target) {
        target.setTime(source.getTime());
        target.setSize(source.getSize());
        target.setMethod(source.getMethod());
        if (source.getComment() != null) {
            target.setComment(source.getComment());
        }
        target.setCompressedSize(source.getCompressedSize());
        target.setCrc(source.getCrc());
        if (source.getCreationTime() != null) {
            target.setCreationTime(source.getCreationTime());
        }
        if (source.getExtra() != null) {
            target.setExtra(source.getExtra());
        }
        if (source.getLastAccessTime() != null) {
            target.setLastAccessTime(source.getLastAccessTime());
        }
        if (source.getLastModifiedTime() != null) {
            target.setLastModifiedTime(source.getLastModifiedTime());
        }
    }
}
