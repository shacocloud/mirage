package cc.shacocloud.mirage.maven.plugin;

import lombok.Getter;
import org.apache.maven.artifact.Artifact;
import org.apache.maven.artifact.handler.ArtifactHandler;
import org.apache.maven.artifact.repository.ArtifactRepository;
import org.apache.maven.artifact.resolver.filter.ArtifactFilter;
import org.apache.maven.artifact.versioning.ArtifactVersion;
import org.apache.maven.artifact.versioning.OverConstrainedVersionException;
import org.apache.maven.artifact.versioning.VersionRange;
import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.List;

/**
 * {@link Artifact} 依赖对象
 *
 * @author 思追(shaco)
 */
@Getter
public class ArtifactLib {
    
    private final String name;
    
    private final File file;
    
    private final String scope;
    
    private final Artifact artifact;
    
    public ArtifactLib(@NotNull Artifact artifact) {
        this.artifact = artifact;
        this.name = getFileName(artifact);
        this.file = artifact.getFile();
        this.scope = artifact.getScope();
    }
    
    /**
     * 打开输入流
     */
    public InputStream openStream() {
        try {
            return new FileInputStream(getFile());
        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        }
    }
    
    /**
     * 返回最后修改时间的时间戳
     */
    public long getLastModified() {
        return getFile().lastModified();
    }
    
    
    @NotNull
    private String getFileName(@NotNull Artifact artifact) {
        StringBuilder sb = new StringBuilder();
        sb.append(getArtifactId()).append("-").append(getBaseVersion());
        String classifier = getClassifier();
        if (classifier != null) {
            sb.append("-").append(classifier);
        }
        sb.append(".").append(artifact.getArtifactHandler().getExtension());
        return sb.toString();
    }
    
    // 委托 ------------------
    
    
    public String getGroupId() {
        return getArtifact().getGroupId();
    }
    
    public String getArtifactId() {
        return getArtifact().getArtifactId();
    }
    
    public String getVersion() {
        return getArtifact().getVersion();
    }
    
    public String getType() {
        return getArtifact().getType();
    }
    
    public String getClassifier() {
        return getArtifact().getClassifier();
    }
    
    public boolean hasClassifier() {
        return getArtifact().hasClassifier();
    }
    
    public String getBaseVersion() {
        return getArtifact().getBaseVersion();
    }
    
    public String getId() {
        return getArtifact().getId();
    }
    
    public String getDependencyConflictId() {
        return getArtifact().getDependencyConflictId();
    }
    
    public ArtifactRepository getRepository() {
        return getArtifact().getRepository();
    }
    
    public String getDownloadUrl() {
        return getArtifact().getDownloadUrl();
    }
    
    public void setDownloadUrl(String downloadUrl) {
        getArtifact().setDownloadUrl(downloadUrl);
    }
    
    public ArtifactFilter getDependencyFilter() {
        return getArtifact().getDependencyFilter();
    }
    
    public void setDependencyFilter(ArtifactFilter artifactFilter) {
        getArtifact().setDependencyFilter(artifactFilter);
    }
    
    public ArtifactHandler getArtifactHandler() {
        return getArtifact().getArtifactHandler();
    }
    
    public List<String> getDependencyTrail() {
        return getArtifact().getDependencyTrail();
    }
    
    public VersionRange getVersionRange() {
        return getArtifact().getVersionRange();
    }
    
    public boolean isSnapshot() {
        return getArtifact().isSnapshot();
    }
    
    public boolean isRelease() {
        return getArtifact().isRelease();
    }
    
    public List<ArtifactVersion> getAvailableVersions() {
        return getArtifact().getAvailableVersions();
    }
    
    public boolean isOptional() {
        return getArtifact().isOptional();
    }
    
    public ArtifactVersion getSelectedVersion() throws OverConstrainedVersionException {
        return getArtifact().getSelectedVersion();
    }
    
    public boolean isSelectedVersionKnown() throws OverConstrainedVersionException {
        return getArtifact().isSelectedVersionKnown();
    }
    
}
