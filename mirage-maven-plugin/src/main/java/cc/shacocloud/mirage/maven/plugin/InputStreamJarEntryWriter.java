package cc.shacocloud.mirage.maven.plugin;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * @author 思追(shaco)
 */
public class InputStreamJarEntryWriter implements JarEntryWriter {
    
    private static final int BUFFER_SIZE = 32 * 1024;
    
    private final InputStream inputStream;
    
    public InputStreamJarEntryWriter(InputStream inputStream) {
        this.inputStream = inputStream;
    }
    
    @Override
    public void write(OutputStream outputStream) throws IOException {
        byte[] buffer = new byte[BUFFER_SIZE];
        int bytesRead;
        while ((bytesRead = this.inputStream.read(buffer)) != -1) {
            outputStream.write(buffer, 0, bytesRead);
        }
        outputStream.flush();
    }
}
